/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */
#include "DataRate.h"
#include <sysc/kernel/sc_time.h>

[[nodiscard]] sc_core::sc_time DataRate::getTransmissionDuration(uint64_t size_bytes) const {
	return {static_cast<double>(size_bytes) / static_cast<double>((value * static_cast<uint64_t>(unit))),sc_core::SC_SEC};
}

