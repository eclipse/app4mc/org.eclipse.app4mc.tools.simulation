/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#include "WrappedType.h"
#include "ConnectionImpl.h"
#include "ConnectionHandlerImpl.h"
#include "CacheStatisticImpl.h"
#include "ProcessingUnitImpl.h"
#include "MemoryImpl.h"

/*
 template specialization in the implemenation file 
 ensures that implementation classes are known, 
 otherwise attempted acceses to static functions of
 incomplete classes might occur
*/

template<> void WrappedType<ConnectionImpl>::sc_trace_internal(
	sc_core::sc_trace_file *tf, 
	const std::shared_ptr<WrappedType<ConnectionImpl>>& v,
	const std::string & SigName)
{
		ConnectionImpl::systemc_trace(tf, &*(v->internalInst()), SigName);
}

template<> void WrappedType<ConnectionHandlerImpl>::sc_trace_internal(
	sc_core::sc_trace_file *tf, 
	const std::shared_ptr<WrappedType<ConnectionHandlerImpl>>& v,
	const std::string & SigName)
{
		ConnectionHandlerImpl::systemc_trace(tf, &*(v->internalInst()), SigName);
}

template<> void WrappedType<CacheStatisticImpl>::sc_trace_internal(
	sc_core::sc_trace_file *tf, 
	const std::shared_ptr<WrappedType<CacheStatisticImpl>>& v,
	const std::string & SigName)
{
		CacheStatisticImpl::systemc_trace(tf, &*(v->internalInst()), SigName);
}

template<> void WrappedType<MemoryImpl>::sc_trace_internal(
	sc_core::sc_trace_file *tf, 
	const std::shared_ptr<WrappedType<MemoryImpl>>& v,
	const std::string & SigName)
{
		MemoryImpl::systemc_trace(tf, &*(v->internalInst()), SigName);
}

template<> void WrappedType<ProcessingUnitImpl>::sc_trace_internal(
	sc_core::sc_trace_file *tf, 
	const std::shared_ptr<WrappedType<ProcessingUnitImpl>>& v,
	const std::string & SigName)
{
		ProcessingUnitImpl::systemc_trace(tf, &*(v->internalInst()), SigName);
}