// /**
//  ********************************************************************************
//  * Copyright (c) 2022 Robert Bosch GmbH
//  * 
//  * This program and the accompanying materials are made
//  * available under the terms of the Eclipse Public License 2.0
//  * which is available at https://www.eclipse.org/legal/epl-2.0/
//  * 
//  * SPDX-License-Identifier: EPL-2.0
// //  * 
// //  * Contributors:
// //  * - Robert Bosch GmbH - initial contribution
// //  *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
// //  ********************************************************************************
// //  */
#include "Stimulus.h"
#include "StimulusImpl.h"
#include "Task.h"

void Stimulus::addTask(const std::shared_ptr<Task>& task){
    getWrappedInst()->addTask(task);
    task->addStimulus(getSharedPtrThis());
}
