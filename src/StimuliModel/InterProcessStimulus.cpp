// /**
//  ********************************************************************************
//  * Copyright (c) 2022 Robert Bosch GmbH
//  * 
//  * This program and the accompanying materials are made
//  * available under the terms of the Eclipse Public License 2.0
//  * which is available at https://www.eclipse.org/legal/epl-2.0/
//  * 
//  * SPDX-License-Identifier: EPL-2.0
//  * 
//  * Contributors:
//  * - Robert Bosch GmbH - initial contribution
//  *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
//  ********************************************************************************
//  */
#include "InterProcessStimulus.h"
#include <memory>
#include "InterProcessStimulusImpl.h"
#include "StimulusImpl.h"
#include "Scheduler.h"
#include "Task.h"

InterProcessStimulus::InterProcessStimulus(const std::string &name):
     WrappedType<InterProcessStimulusImpl>(name){}

InterProcessStimulus::InterProcessStimulus(const std::shared_ptr<InterProcessStimulusImpl>& _inst):
    WrappedType<InterProcessStimulusImpl>(_inst){}

std::shared_ptr<Stimulus> InterProcessStimulus::getSharedPtrThis() /*override */{ return shared_from_this(); }
std::shared_ptr<StimulusImpl> InterProcessStimulus::getWrappedInst() /*override*/ { return internalInst(); }

HasCondition& InterProcessStimulus::getConditionObj() {
    return *internalInst();
}