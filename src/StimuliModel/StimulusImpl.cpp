/**
 ********************************************************************************
 * Copyright (c) 2021 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */
#include "StimulusImpl.h"
#include "TracerApi.h"
#include "Task.h"
#include "TraceManager.h"

void StimulusImpl::stimulate() {
    if (checkCondition()){
	  TraceManager::Inst().stimulus(*this);
	  for (auto task : tasks) {
	    task->stimulateTask(*this);
        }
        instanceCounter++;
    } else {
	  TraceManager::Inst().comment(HasCondition::conditionNotMetMsg(this->getName(), "-"));
    }
}   
