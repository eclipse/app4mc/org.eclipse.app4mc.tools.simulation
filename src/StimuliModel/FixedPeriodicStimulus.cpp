// /**
//  ********************************************************************************
//  * Copyright (c) 2022 Robert Bosch GmbH
//  * 
//  * This program and the accompanying materials are made
//  * available under the terms of the Eclipse Public License 2.0
//  * which is available at https://www.eclipse.org/legal/epl-2.0/
//  * 
//  * SPDX-License-Identifier: EPL-2.0
//  * 
//  * Contributors:
//  * - Robert Bosch GmbH - initial contribution
//  *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
//  ********************************************************************************
//  */
#include "FixedPeriodicStimulus.h"
#include <memory>
#include "FixedPeriodicStimulusImpl.h"
#include "StimulusImpl.h"
#include "Scheduler.h"
#include "Task.h"

FixedPeriodicStimulus::FixedPeriodicStimulus(const std::string &name, const Time &recurrence, const Time &offset):
    WrappedType<FixedPeriodicStimulusImpl>(name, recurrence, offset){}

FixedPeriodicStimulus::FixedPeriodicStimulus(const std::shared_ptr<FixedPeriodicStimulusImpl>& _inst):
    WrappedType<FixedPeriodicStimulusImpl>(_inst){}

std::shared_ptr<Stimulus> FixedPeriodicStimulus::getSharedPtrThis() /*override */{ return shared_from_this(); }
std::shared_ptr<StimulusImpl> FixedPeriodicStimulus::getWrappedInst() /*override*/ { return internalInst(); }

HasCondition& FixedPeriodicStimulus::getConditionObj() {
    return *internalInst();
}
