// /**
//  ********************************************************************************
//  * Copyright (c) 2022 Robert Bosch GmbH
//  * 
//  * This program and the accompanying materials are made
//  * available under the terms of the Eclipse Public License 2.0
//  * which is available at https://www.eclipse.org/legal/epl-2.0/
//  * 
//  * SPDX-License-Identifier: EPL-2.0
//  * 
//  * Contributors:
//  * - Robert Bosch GmbH - initial contribution
//  *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
//  ********************************************************************************
//  */
#include "RelativePeriodicStimulus.h"
#include <memory>
#include "RelativePeriodicStimulusImpl.h"
#include "StimulusImpl.h"
#include "Scheduler.h"
#include "Task.h"

template<typename T>  
RelativePeriodicStimulus::RelativePeriodicStimulus(
       const std::string &name,
        const T &nextOccurrence, 
        const Time &offset):
    WrappedType<RelativePeriodicStimulusImpl>(name, nextOccurrence, offset){}

RelativePeriodicStimulus::RelativePeriodicStimulus(const std::shared_ptr<RelativePeriodicStimulusImpl>& _inst):
    WrappedType<RelativePeriodicStimulusImpl>(_inst){}

std::shared_ptr<Stimulus> RelativePeriodicStimulus::getSharedPtrThis() /*override */{ return shared_from_this(); }
std::shared_ptr<StimulusImpl> RelativePeriodicStimulus::getWrappedInst() /*override*/ { return internalInst(); }

HasCondition& RelativePeriodicStimulus::getConditionObj() {
    return *internalInst();
}

template RelativePeriodicStimulus::RelativePeriodicStimulus(const std::string&, const TimeConstant&, const Time&);
template RelativePeriodicStimulus::RelativePeriodicStimulus(const std::string&, const TimeGaussDistribution&, const Time&);
template RelativePeriodicStimulus::RelativePeriodicStimulus(const std::string&, const TimeWeibullEstimatorsDistribution&, const Time&);
template RelativePeriodicStimulus::RelativePeriodicStimulus(const std::string&, const TimeUniformDistribution&, const Time&);
template RelativePeriodicStimulus::RelativePeriodicStimulus(const std::string&, const TimeBetaDistribution&, const Time&);
template RelativePeriodicStimulus::RelativePeriodicStimulus(const std::string&, const TimeBoundaries&, const Time&);

