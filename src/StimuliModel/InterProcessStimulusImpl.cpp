/**
 ********************************************************************************
 * Copyright (c) 2021 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 * - Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#include "InterProcessStimulusImpl.h"

void InterProcessStimulusImpl::setCounter(uint64_t counter){
    counterInit = counter;
    counterState = counterInit;
}

sc_core::sc_event &InterProcessStimulusImpl::getStimulusEvent(){
    return stimulusEvent;
}

void InterProcessStimulusImpl::stimulate() {
    while(true){            
        sc_core::wait(stimulusEvent);
        if(counterState <= 1){
            StimulusImpl::stimulate();
            counterState = counterInit;
            }
        else{
            --counterState;
        }   
    }
}

std::string InterProcessStimulusImpl::getDescription() const {
    return  "InterProcessStimulus with Counter=" + std::to_string(counterInit);
}
