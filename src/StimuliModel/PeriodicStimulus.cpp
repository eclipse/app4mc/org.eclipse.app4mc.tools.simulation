// /**
//  ********************************************************************************
//  * Copyright (c) 2022 Robert Bosch GmbH
//  * 
//  * This program and the accompanying materials are made
//  * available under the terms of the Eclipse Public License 2.0
//  * which is available at https://www.eclipse.org/legal/epl-2.0/
//  * 
//  * SPDX-License-Identifier: EPL-2.0
//  * 
//  * Contributors:
//  * - Robert Bosch GmbH - initial contribution
//  *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
//  ********************************************************************************
//  */
#include "PeriodicStimulus.h"
#include <memory>
#include "PeriodicStimulusImpl.h"
#include "StimulusImpl.h"
#include "Scheduler.h"
#include "Task.h"

template<typename T>  
PeriodicStimulus::PeriodicStimulus(
        const std::string &name,
        const Time &recurrence, 
        const Time &offset,
        const T &jitter,
        const Time &minDistance
    ): WrappedType<PeriodicStimulusImpl>(name, recurrence, offset, jitter, minDistance){}

PeriodicStimulus::PeriodicStimulus(const std::shared_ptr<PeriodicStimulusImpl>& _inst):
    WrappedType<PeriodicStimulusImpl>(_inst){}

std::shared_ptr<Stimulus> PeriodicStimulus::getSharedPtrThis() /*override */{ return shared_from_this(); }
std::shared_ptr<StimulusImpl> PeriodicStimulus::getWrappedInst() /*override*/ { return internalInst(); }

HasCondition& PeriodicStimulus::getConditionObj() {
    return *internalInst();
}


template PeriodicStimulus::PeriodicStimulus(const std::string&, const Time&, const Time &, const TimeConstant&, const Time&);
template PeriodicStimulus::PeriodicStimulus(const std::string&, const Time&, const Time &, const TimeGaussDistribution&, const Time&);
template PeriodicStimulus::PeriodicStimulus(const std::string&, const Time&, const Time &, const TimeWeibullEstimatorsDistribution&, const Time&);
template PeriodicStimulus::PeriodicStimulus(const std::string&, const Time&, const Time &, const TimeUniformDistribution&, const Time&);
template PeriodicStimulus::PeriodicStimulus(const std::string&, const Time&, const Time &, const TimeBetaDistribution&, const Time&);
template PeriodicStimulus::PeriodicStimulus(const std::string&, const Time&, const Time &, const TimeBoundaries&, const Time&);

