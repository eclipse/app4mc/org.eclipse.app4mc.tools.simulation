// /**
//  ********************************************************************************
//  * Copyright (c) 2022 Robert Bosch GmbH
//  * 
//  * This program and the accompanying materials are made
//  * available under the terms of the Eclipse Public License 2.0
//  * which is available at https://www.eclipse.org/legal/epl-2.0/
//  * 
//  * SPDX-License-Identifier: EPL-2.0
//  * 
//  * Contributors:
//  * - Robert Bosch GmbH - initial contribution
//  *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
//  ********************************************************************************
//  */
#include "SingleStimulus.h"
#include <memory>
#include "SingleStimulusImpl.h"
#include "StimulusImpl.h"
#include "Scheduler.h"
#include "Task.h"

SingleStimulus::SingleStimulus(const std::string &name, const Time &occurrence):
     WrappedType<SingleStimulusImpl>(name, occurrence){}

SingleStimulus::SingleStimulus(const std::shared_ptr<SingleStimulusImpl>& _inst):
    WrappedType<SingleStimulusImpl>(_inst){}

std::shared_ptr<Stimulus> SingleStimulus::getSharedPtrThis() /*override */{ 
    return std::static_pointer_cast<Stimulus>(shared_from_this()); 
    }

std::shared_ptr<StimulusImpl> SingleStimulus::getWrappedInst() /*override*/ { return internalInst(); }

HasCondition& SingleStimulus::getConditionObj() {
    return *internalInst();
}
