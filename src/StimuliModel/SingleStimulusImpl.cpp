/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#include "SingleStimulusImpl.h"
#include "TimeUtilsSystemC.h"
#include "EasyloggingMacros.h"

void SingleStimulusImpl::stimulate() {
    sc_core::wait(convertTime(occurrence));
    VLOG(2) << "SingleStimulus " << this->getDescription();
    StimulusImpl::stimulate();
}

std::string SingleStimulusImpl::getDescription() const {
    std::stringstream tmp;
    tmp << "SingleStimulus @" << this << ", occurrence = " << occurrence.count() << "ps";
    return tmp.str();
}

uint64_t SingleStimulusImpl::instanceCount() const {
    return 1;
}
