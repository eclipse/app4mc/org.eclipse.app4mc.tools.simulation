// /**
//  ********************************************************************************
//  * Copyright (c) 2022 Robert Bosch GmbH
//  * 
//  * This program and the accompanying materials are made
//  * available under the terms of the Eclipse Public License 2.0
//  * which is available at https://www.eclipse.org/legal/epl-2.0/
//  * 
//  * SPDX-License-Identifier: EPL-2.0
//  * 
//  * Contributors:
//  * - Robert Bosch GmbH - initial contribution
//  *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
//  ********************************************************************************
//  */
#include "EventStimulus.h"
#include <memory>
#include "EventStimulusImpl.h"
#include "StimulusImpl.h"
#include "Scheduler.h"
#include "Task.h"

EventStimulus::EventStimulus(const std::string &name, std::shared_ptr<Event> _event):
     WrappedType<EventStimulusImpl>(name, _event){}

EventStimulus::EventStimulus(const std::shared_ptr<EventStimulusImpl>& _inst):
    WrappedType<EventStimulusImpl>(_inst){}

std::shared_ptr<Stimulus> EventStimulus::getSharedPtrThis() /*override */{ return shared_from_this(); }
std::shared_ptr<StimulusImpl> EventStimulus::getWrappedInst() /*override*/ { return internalInst(); }

HasCondition& EventStimulus::getConditionObj() {
    return *internalInst();
}

const std::shared_ptr<Event>& EventStimulus::getEvent(){
    return internalInst()->getEvent();
}
