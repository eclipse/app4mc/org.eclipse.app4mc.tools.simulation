/**
 ********************************************************************************
 * Copyright (c) 2021 University of Rostock and others
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */
#include "TraceManager.h"
#include "TracerApi.h"
#include "DispatchTracer.h"
#include <memory>
#include <vector>
#include "Common.h"
#include "TimeUtils.h"
#include "EasyloggingMacros.h"


const std::shared_ptr<DispatchTracer>& TraceManager::internalInst() {
    static std::shared_ptr<DispatchTracer> instance = std::make_shared<DispatchTracer>();
    return instance;
}

uint32_t TraceManager::registerTracer(std::string tracerName, std::string cliHelpText, std::function<std::shared_ptr<TracerApi>()> creator, const std::type_info& type) {
    static uint32_t idCounter = 0;
    auto& registry = TraceManager::registry();
    if (std::any_of(registry.begin(), registry.end(), [&](const TracerEntry& entry) { return (entry.name == tracerName) || (entry.type == type); })) {
        std::string msg = "tracer with name \"" + tracerName + "\" or Type \"" + type.name() + "\" already registerd";
        throw std::runtime_error(msg);
    }
    registry.emplace_back(TracerEntry{ std::move(tracerName), std::move(cliHelpText), idCounter, std::move(creator), type });
    return idCounter++;
}

std::filesystem::path& TraceManager::pathRef() {
    static auto path = std::filesystem::path("trace");
    return path;
}

bool& TraceManager::useTimePrefix() {
    static bool useTimePrefix = false;
    return useTimePrefix;
}

TracerApi& TraceManager::Inst() {
    return *TraceManager::internalInst();
}

void TraceManager::addTracer(const std::shared_ptr<TracerApi>& newTracer) {
    TraceManager::internalInst()->addTracer(newTracer);
}

void TraceManager::removeAll() {
    TraceManager::internalInst()->removeAll();
}

void TraceManager::createTracerFromList(const std::vector<std::string>& list) {
    auto registry = TraceManager::registry();
    for (const auto& tracer : list) {
        auto regEntry = std::find_if(registry.begin(), registry.end(),
            [&](const TracerEntry& entry) { return entry.name == tracer; });
        if (regEntry != registry.end()) {
            TraceManager::addTracer(regEntry->creator());
        }
    }
}

std::string TraceManager::getCliHelpText() {
    auto registry = TraceManager::registry();

    std::string helpText = "The following Tracer implementations are available. If none is selected, (default) is implicitly used.\n";

    for (const auto& tracer : registry) {
        helpText.append(tracer.name + ":: " + tracer.cliHelpText + "\n");
    }
    return helpText;
}

void TraceManager::setTraceDirectory(const std::string& path) {
    if (TraceManager::internalInst()->isEmpty()) {
        if (!path.empty()) {
            std::filesystem::create_directories(path);
        }
        TraceManager::pathRef() = std::filesystem::path(path);
        return;
    } else {
        VLOG(0) << "Other tracers have already been registered, trace directory remains at " << TraceManager::pathRef();
        return;
    }
}

std::filesystem::path TraceManager::createTraceFilePath(const std::string& fileName) {
    std::filesystem::path path = pathRef();
    if (useTimePrefix()) {
        auto now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        // init with an example value
        std::string prefix{"YEAR-MO-DA-HO-MI-SE"};
        std::strftime(prefix.data(), prefix.size(), "%Y-%m-%d-%H-%M-%S", localtime_safe(&now).get());
        path /= prefix + "_" + fileName;
    } else {
        path /= fileName;
    }
    return path;
}

void TraceManager::enableTimePrefix() {
    useTimePrefix() = true;
}
