/**
 ********************************************************************************
 * Copyright (c) 2020 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */

#include "TracerApi.h"
#include "TraceManager.h"
#include "BTFTracer.h"
#include <ctime>
#include <systemc>
#include <iomanip>

static std::string getResolutionString()
{
    std::string scale;
    auto resolution  = sc_core::sc_get_time_resolution();
        //BTF only allows resolution of [ps,ns,us,ms,s], but systemC allows numbers which are a power of ten.
        if(resolution == sc_core::sc_time(1,sc_core::SC_NS)){
            scale = "ns";
        } else 
        if (resolution == sc_core::sc_time(1,sc_core::SC_PS)){
            scale = "ps";
        }else 
        if (resolution == sc_core::sc_time(1,sc_core::SC_US)){
            scale = "us";
        }else 
        if (resolution == sc_core::sc_time(1,sc_core::SC_MS)){
            scale = "ms";
        }else 
        if (resolution == sc_core::sc_time(1,sc_core::SC_SEC)){
            scale = "s";
        }else 
            throw std::runtime_error("SystemC Time resolution is not compatible with BTF Timeresolution");

        return scale;
}

BtfTracer::BtfTracer(const std::string &traceFile, BtfVersion version, const std::string &description) : btfFile(TraceManager::createTraceFilePath(traceFile),std::ios_base::out), version(version) {
    //write Header
    btfFile << "#version ";
    if (version == BtfVersion::v2_1_5) {
        btfFile << "2.1.5\n";
    } else if (version == BtfVersion::v2_2_0) {
        btfFile << "2.2.0\n";
    }
    btfFile << "#creator app4mc.sim\n";
    btfFile << "# " << description << "\n";
    btfFile << "#simulation seed: " << app4mcsim_seed::getSeedString() << "\n";
    auto now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());

    btfFile << "#creationDate " <<  std::put_time(gmtime_safe(&now).get(),"%Y-%m-%dT%H:%M:%SZ") << "\n";
    btfFile << "#timescale "<< getResolutionString() << "\n";
}