/**
 ********************************************************************************
 * Copyright (c) 2020 University of Rostock and others
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Sebastian Reiser <sebastian.reiser@de.bosch.com> - initial contribution
 ********************************************************************************
 */

#include <memory>
#include "TracerFactory.h"
#include "TracerApi.h"
#include "TraceManager.h"
#include "BTFTracer.h"
#include <filesystem>

std::shared_ptr<TracerApi> TracerFactory::createTracer(
	const int select, 
	const std::string& _path,
	[[maybe_unused]] const std::string& _filename) 	
{
	switch (select) {
	case BTF:
		TraceManager::createTracerFromList({std::string(BtfTracer::tracerName())});
		break;
	case BTF_AGI:
		TraceManager::createTracerFromList({std::string(BtfTracerAgi::tracerName())});
		break;
	case BTF_AGI_BRANCH:
		TraceManager::createTracerFromList({std::string(BtfTracerAgiBranch::tracerName())});
		break;
	case BTF_SIGNAL:
		TraceManager::createTracerFromList({std::string(BtfTracerSignal::tracerName())});
		break;
	default:
		break;
	}
	TraceManager::setTraceDirectory(_path);
	return nullptr;
}	

const std::vector<std::string>& TracerFactory::getTracerNames() {
	static const std::vector<std::string> tracerNames{{std::string(BtfTracer::tracerName())},
	                                     {std::string(BtfTracerAgi::tracerName())},
	                                     {std::string(BtfTracerAgiBranch::tracerName())},
	                                     {std::string(BtfTracerSignal::tracerName())}};
	return tracerNames;
}


std::string_view TracerFactory::getTracerName(int _id){
	switch (_id) {
	case BTF:
		return BtfTracer::tracerName();
	case BTF_AGI:
		return BtfTracerAgi::tracerName();
	case BTF_AGI_BRANCH:
		return BtfTracerAgiBranch::tracerName();
	case BTF_SIGNAL:
		return BtfTracerSignal::tracerName();
	default:
		// do nothing, the string is ignored in TraceManager
		return std::string_view("");
	}
}
