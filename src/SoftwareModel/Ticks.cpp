/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#include "Ticks.h"

Ticks::Ticks() : 
    debugString("TicksItem with 0 Ticks") {}

ExecutionItem Ticks::getNextExecItem(ExecStack &stack, const ProcessingUnitDefinition &puDef) /*override*/ { 
    ELong ticks;
    if(extendedTicks.count(puDef.getName()) > 0){
            ticks = extendedTicks[puDef.getName()]->getSample();
    }
    else{
        if(!defaultTicks)
            throw std::runtime_error("ActivityGraphItem Ticks was executed on PU without proper extended Ticks entry and without default Ticks entry");
        ticks = defaultTicks->getSample();
    }

    if(ticks < 0){
        throw std::runtime_error("ActivityGraphItem Ticks used IDiscreteValueDeviation with a negative result");
    }

    ExecutionItem ret{stack,*this};

    // use existing debug messages, if not changed
    ret.executingCycles = ticks;
    if(ticks != lastTicks){
        description = "Ticks with ";
        description +=std::to_string(ticks);
        description +=" Ticks";
        lastTicks = ticks;
        debugString.set(description);
    }
    return ret;
}

traceID Ticks::getTraceName() const /*override*/ {
    return debugString.getID();
}

std::string Ticks::getDescription() const /*override*/ {
    return description;
}

std::string_view Ticks::getType() const /*override*/ {
    return "Ticks";
}
std::string_view Ticks::getTraceString() const /*override*/{
    return description;
}