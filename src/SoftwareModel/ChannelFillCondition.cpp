/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#include "ChannelFillCondition.h"
#include <memory>
#include "Condition.h"
#include "Channel.h"
#include "RelationalOperator.h"

bool ChannelFillCondition::checkCond(){
	switch (op){
		case RelationalOperator::EQUAL : 		return channel->getFillLevel() == fillLevel;
		case RelationalOperator::GREATER_THAN: 	return channel->getFillLevel() > fillLevel;
		case RelationalOperator::LESS_THAN: 	return channel->getFillLevel() < fillLevel;
		case RelationalOperator::NOT_EQUAL: 	return channel->getFillLevel() != fillLevel;
		default: return false;
	}
}





