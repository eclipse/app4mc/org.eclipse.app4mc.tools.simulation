/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */
#include "TracerApi.h"
#include "TraceManager.h"
#include "Channel.h"
#include "traceString.h"
#include "ChannelAccess.h"
#include <tlm>
#include <utility>
#include "MemoryAccessUtils.h"
#include "ExecutionItem.h"
#include "ExecStack.h"
#include "MappingModel.h"
#include "labelTLMext.h"

ChannelAccess::ChannelAccess(std::shared_ptr<Channel> _queue, const uint64_t _labelAccessStatistic, const MemoryAccessType _command) :
	debugString(traceString("")),
	channel(std::move(_queue)),
	labelAccessStatistic(_labelAccessStatistic),
	command(_command)
{		}

traceID ChannelAccess::getTraceName() const {
	return debugString.getID();
}

ExecutionItem ChannelAccess::getNextExecItem(ExecStack &stack, const ProcessingUnitDefinition &/*puDef*/) {
	ExecutionItem returnValue{stack,*this};
	/* first execution of this ActivityGraphItem -> create fixed payload and debug string*/
	auto [payload, isInitialized] = PayloadCache::Inst().getOrInit(this);
	if (!isInitialized){
		payload->set_address(MappingModel::getMemoryAddress(channel));
		payload->set_command(getTlmCommand(command));
		if (channel->getSizeBytes() > std::numeric_limits<unsigned int>::max())
			throw std::overflow_error("Queue size does not fit into tlm_generic_payload");
		payload->set_data_length(static_cast<unsigned int>(channel->getSizeBytes()));
		payload->set_byte_enable_ptr(0); // Byte enables unused
		payload->set_streaming_width(static_cast<unsigned int>(channel->getSizeBytes())); // Streaming unused -> if size is to big, this would also fail but is catched earlier
		payload->set_dmi_allowed(false); // Clear the DMI hint
	
		auto *labelTLM = new labelTLMext();
		labelTLM->label = (channel);
		//set state and trace, when transaction is completed at memory
		labelTLM->writeCallback = [&](){
			if (command == MemoryAccessType::WRITE) {
				channel->incrementFillLevel();
				TraceManager::Inst().signalWrite(*stack.getParentTask(), *channel);
			} else {
				channel->decrementFillLevel();
				TraceManager::Inst().signalRead(*stack.getParentTask(), *channel);
			}
			
		};
		payload->set_extension(labelTLM);
	}
	payload->set_response_status(tlm::TLM_INCOMPLETE_RESPONSE);

	returnValue.executingCycles = 0;
	returnValue.memoryTransaction = payload;
	if (command == MemoryAccessType::READ)// && std::is_base_of<Channel, T>::value )
		returnValue.memoryTransactionStatistic = std::min(channel->getFillLevel(), labelAccessStatistic);
	else 
		returnValue.memoryTransactionStatistic = labelAccessStatistic;
	returnValue.events = channel->getEvents();
	return returnValue;

}

