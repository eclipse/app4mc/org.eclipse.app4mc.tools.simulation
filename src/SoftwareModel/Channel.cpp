/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#include "Channel.h"
#include "TracerApi.h"
#include "TraceManager.h"


Channel::Channel(const std::string &_name, size_t _sizeBytes,uint64_t _defaultElements, uint64_t _maxElements) :
	AbstractMemoryElement(_name, _sizeBytes)
{
	fillLevel = _defaultElements;
	maxElements = _maxElements;
}

uint64_t Channel::getFillLevel() const {
	return fillLevel;
}
	
uint64_t Channel::incrementFillLevel(uint64_t _incr) {
	fillLevel = fillLevel + _incr;
	if (fillLevel > maxElements && maxElements >0){
		TraceManager::Inst().comment(getDescription() + "exceeded max. capacity " + std::to_string(maxElements));
	}
	return fillLevel;
}

uint64_t Channel::decrementFillLevel(uint64_t _decr) {
	if (fillLevel < _decr ){
		TraceManager::Inst().comment(getDescription() + ": elements to read (" + std::to_string(_decr) + ") exceeds fill level");
	}
	fillLevel = (fillLevel >= _decr) ? fillLevel - _decr : 0;
	return fillLevel;
}

uint64_t Channel::incrementFillLevel() {
	return incrementFillLevel(1);
}

uint64_t Channel::decrementFillLevel() {
	return decrementFillLevel(1);
}

std::string Channel::getDescription() const  {
	return getName() + " (fill level=" + std::to_string(this->getFillLevel()) + ")";
}

traceString Channel::getTraceString() const {
	return traceString(getName());
}





