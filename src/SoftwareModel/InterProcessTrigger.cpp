/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#include "InterProcessTrigger.h"
#include "InterProcessStimulusImpl.h"

InterProcessTrigger::InterProcessTrigger(const std::shared_ptr<InterProcessStimulus> stimulus): stimulus(stimulus), debugString("InterProcessTrigger"){}

std::string InterProcessTrigger::getDescription() const {
    return "InterProcessTrigger";
}

std::string_view InterProcessTrigger::getType() const {
    return "InterProcessTrigger";
}

traceID InterProcessTrigger::getTraceName() const {
    return debugString.getID();
}

ExecutionItem InterProcessTrigger::getNextExecItem(ExecStack &stack, const ProcessingUnitDefinition &/*puDef*/) { 
    ExecutionItem ret{stack,*this};
    ret.finishedItemCallback = [this](){
        this->stimulus->internalInst()->getStimulusEvent().notify(sc_core::SC_ZERO_TIME);
    };
    return ret;
}

std::string_view InterProcessTrigger::getTraceString() const {
    return "";
}
