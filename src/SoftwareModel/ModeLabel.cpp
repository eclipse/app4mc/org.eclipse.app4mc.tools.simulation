/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */


#include "ModeLabel.h"
#include "easylogging++.h"
#include "EasyloggingMacros.h"


ModeLabel::ModeLabel(const std::string &_name, std::shared_ptr<Mode> _mode, const int& _state , size_t _sizeBytes) :
	AbstractMemoryElement(_name, _sizeBytes), 
	mode(_mode), 
	state(_state)
{}

std::shared_ptr<ModeLabel> ModeLabel::Inst(const std::string &_name, std::shared_ptr<Mode> _mode, const int& _state, size_t _sizeBytes) {
	return std::shared_ptr<ModeLabel>(new ModeLabel(_name, _mode, _state, _sizeBytes));
}

std::shared_ptr<ModeLabel> ModeLabel::Inst(const std::string &_name, const int& _state, size_t _sizeBytes) {
	return std::shared_ptr<ModeLabel>(new ModeLabel(_name, Mode::Inst(), _state, _sizeBytes));
}

const int& ModeLabel::getState(){
	return state;
}

void ModeLabel::setState(const int& _state, const ModeAccessType _access){	
	switch (_access) {
	case ModeAccessType::SET:
		VLOG(2)<<"ModeLabel::SET\n\tcurrent state: " << mode->str(state);
		state=_state;
		break;
	case ModeAccessType::INCREMENT:
		VLOG(2)<<"ModeLabel::INCREMENT\n\tcurrent state: " << mode->str(state);
			state += _state;
		break;
	case ModeAccessType::DECREMENT:
		VLOG(2)<<"ModeLabel::DECREMENT\n\tcurrent state: " << mode->str(state);
		state -= _state;
		break;
	default: //ModeAccessType::READ
		VLOG(2)<<"ModeLabel::READ\n\tcurrent state: " << mode->str(state);
		break; //do nothing, as handing a value to a "read" does not make too much sense
	}
	VLOG(2)<< "\t new state: " << mode->str(state);
}

std::string ModeLabel::str() const {
	return mode->str(state);
}



