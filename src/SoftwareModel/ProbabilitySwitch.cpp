/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#include "ProbabilitySwitch.h"
#include "effolkronium/random.hpp"
#include <pcg_random.hpp>
#include "Deviation.h"
#include "TracerApi.h"
#include "TraceManager.h"
#include "ProcessingUnitDefinition.h"
#include "ExecStack.h"
#include "ExecutionItem.h"
#include "traceString.h"

traceID ProbabilitySwitch::getTraceName() const  {
	static traceString trace("ProbabilitySwitch");
	return trace.getID();
}

void ProbabilitySwitch::calculateDist(){
	std::vector<double> weights;
	weights.reserve(entries.size());
	for (auto &entry : entries) {
		weights.emplace_back(entry.probWeight);
	}

	dist = std::discrete_distribution<>(weights.begin(), weights.end());

}

void ProbabilitySwitch::addEntry(const ProbabilitySwitchEntry &entry)
{
	entries.emplace_back(entry);
	calculateDist();
}


using app4mcsim_rand = effolkronium::basic_random_static<pcg32, app4mcsim_seed>;

/* getNextItem return the next Exec Item and set the stack to the following execItem*/
ExecutionItem ProbabilitySwitch::getNextExecItem(ExecStack &execStack, const ProcessingUnitDefinition &puDef) {
	// get one entry according to the weights
	int index = app4mcsim_rand::get(dist);

	if (!entries[index].activityGraphItems.empty()) {
	TraceManager::Inst().activityGraphItemBranch(execStack.getExecutionContext(), *this);
	// need to store the old container in a copyable struct, since context will be changed before the callback is called
	std::reference_wrapper oldContainer = execStack.getExecutionContext();
	auto callback                       = [oldContainer, &execStack, this]() {
		execStack.setActivityItemContainer(oldContainer);
		TraceManager::Inst().activityGraphItemBranchFinish(execStack.getExecutionContext(), *this);
	};
	execStack.setActivityItemContainer(entries[index]);
	execStack.pushExecutionLevel(entries[index].activityGraphItems, callback);

	return (entries[index].activityGraphItems[0])->getNextExecItem(execStack, puDef);
	}
	return ExecutionItem::getEmptyExecutionItem();
}

std::string ProbabilitySwitch::getDescription() const {
	return "ProbabilitySwitch";
}

std::string_view ProbabilitySwitch::getType() const {
	return "ProbabilitySwitch";
}

std::string_view ProbabilitySwitch::getTraceString() const {
	if (lastIndexTaken != -1) {
	debugString = getDescription() + ": case " + entries[lastIndexTaken].getName();
	} else {
	debugString = getDescription() + ": case (null)";
	}
	return debugString;
}
