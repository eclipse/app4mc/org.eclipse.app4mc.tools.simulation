/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#include "RunnableCall.h"
#include "ExecStack.h"
#include "ProcessingUnitDefinition.h"
#include "TracerApi.h"
#include "TraceManager.h"
#include "Task.h"
#include <sysc/tracing/sc_trace.h>


ExecutionItem RunnableCall::getNextExecItem(ExecStack &stack, const ProcessingUnitDefinition &puDef) {
	auto conditionResult = runnable->checkCondition();
	auto containsItems = !runnable->activityGraphItems.empty();

	if ( containsItems && conditionResult) {

		//need to store the old context in a copyable struct, since context will be changed before the callback is called
		std::reference_wrapper oldContext = stack.getExecutionContext();
		//capture currentRunnable by value (to save old state in lambda)
		auto callBack = [oldContext,&stack,this](){
			stack.getParentTask()->runnableCompletedCallback(*runnable);
			stack.setExecutionContext(oldContext);
		};
		stack.setExecutionContext(*runnable);
		stack.pushExecutionLevel(runnable->activityGraphItems,callBack);
		stack.getParentTask()->runnableStartedCallback(*runnable);
		ExecutionItem returnValue = runnable->activityGraphItems[0]->getNextExecItem(stack,puDef);
		return returnValue;
	} else {
	  if (!conditionResult) {
	    TraceManager::Inst().comment(
	        HasCondition::conditionNotMetMsg(runnable->getName(), stack.getActivityItemContainer().getName()));
	  } else if (!containsItems) {
	    TraceManager::Inst().comment(ContainsActivityGraphItems::getEmptyActivityGraphMsg(
	        runnable->getName(), stack.getActivityItemContainer().getName()));
	  }
    }
	return ExecutionItem::getEmptyExecutionItem();
}


void RunnableCall::systemc_trace(sc_core::sc_trace_file *tf, const RunnableCall *v,
	const std::string & signame) {
	sc_trace(tf, v->nameTrace, signame);
}

