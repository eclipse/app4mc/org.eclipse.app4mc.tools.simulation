/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */
#include "ExecutionItem.h"
#include "Ticks.h"
#include "ExecStack.h"
#include "ActivityGraphItem.h"
#include "Task.h"
#include "Runnable.h"

 const ExecutionItem& ExecutionItem::getEmptyExecutionItem(){
        
        static Runnable emptyRunnable("emptyRunnable");
		static auto emptyTask = Task::createTask("emptyTask", 1);
		static Ticks emptyTicks;

        //init global empty item only once
		static ExecutionItem emptyItem = [&](){
			ExecutionItem emptyItemInit{emptyTask->execStack,emptyTicks};
            emptyItemInit.emptyExecItem = true;
			return emptyItemInit;
		}();
        return emptyItem;
 }

 ExecutionItem::ExecutionItem(const ExecStack &stack,const ActivityGraphItem &item) : 
 	emptyExecItem(false),
	currentExecutionContext(stack.getExecutionContext()),
	currentTask(*stack.getParentTask()),currentActivityGraphItem(item)
	{}