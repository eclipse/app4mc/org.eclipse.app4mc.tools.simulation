/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#include "AbstractMemoryElement.h"
#include "ChannelEvent.h"

std::shared_ptr<ChannelEvent> AbstractMemoryElement::addEvent(const std::string& _name, ChannelEventType _type, ChannelEventStateType _state) {
	auto ev = std::make_shared<ChannelEvent>(_name, _type, _state);
	events.emplace_back(ev);
	return ev;
} 
