/**
 ********************************************************************************
 * Copyright (c) 2020 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */

#include "Mode.h"
#include <memory>
#include <stdexcept>
#include "ModeLiteral.h"

std::string Mode::str(const int& _val){
	std::stringstream buf;
	buf<<this->getName()<<"::"<<_val;
	return std::string(buf.str());
}

int EnumMode::addLiteral(const std::string&& literal) {
	auto lit = std::make_shared<ModeLiteral>(literal, *this);
	literalsViaID[lit->literalId]=lit;
	return lit->literalId;
}

std::string EnumMode::str(const int& _val){
	std::stringstream buf;
	buf<<getName()<<"::"<<literalsViaID[_val]->literalString;
	return buf.str();
}
