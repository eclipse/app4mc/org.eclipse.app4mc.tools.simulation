/**
 ********************************************************************************
 * Copyright (c) 2021 University of Rostock and others
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */

#include "WhileLoop.h"
#include "ExecStack.h"
#include "ExecutionItem.h"
#include "ProcessingUnitDefinition.h"
#include "TracerApi.h"
#include "TraceManager.h"
#include "traceString.h"

WhileLoop::WhileLoop() : ContainsActivityGraphItems("WhileLoop"){}

traceID WhileLoop::getTraceName() const /*override*/ {
	static traceString trace("WhileLoop");
	return trace.getID();
}

/* getNextItem return the next Exec Item and set the stack to the following execItem*/
ExecutionItem WhileLoop::getNextExecItem(ExecStack &execStack, const ProcessingUnitDefinition &puDef) {
  if (checkCondition()) {
	if (!activityGraphItems.empty()) {
	  TraceManager::Inst().activityGraphItemBranch(execStack.getExecutionContext(), *this);

	  // need to store the old container in a copyable struct, since context will be changed before the callback is called
	  std::reference_wrapper oldContainer = execStack.getExecutionContext();

	  // oldContainer is captured by value, otherwise the lifetime of the local variable would break things
	  callback = [oldContainer, &execStack, this]() {
		TraceManager::Inst().activityGraphItemBranchFinish(execStack.getExecutionContext(), *this);
		if (checkCondition()) {
		  ++currentIteration;
		  TraceManager::Inst().activityGraphItemBranch(execStack.getExecutionContext(), *this);
		  execStack.addLoopIteration(activityGraphItems);
		} else {
		  currentIteration = 0;
		  execStack.setActivityItemContainer(oldContainer);
		}
	  };

	  execStack.setActivityItemContainer(*this);
	  execStack.pushExecutionLevel(activityGraphItems, callback);
	  return (activityGraphItems[0])->getNextExecItem(execStack, puDef);
	}
  } else {
	TraceManager::Inst().comment(HasCondition::conditionNotMetMsg(getName(), execStack.getActivityItemContainer().getName()));
  }
  return ExecutionItem::getEmptyExecutionItem();
}

std::string WhileLoop::getDescription() const /*override*/ {
	return "WhileLoop";
}

std::string_view WhileLoop::getType() const /*override*/ {
	return "WhileLoop";
}

std::string_view WhileLoop::getTraceString() const /*override*/ {
	debugString = getDescription() + ": iteration number " + std::to_string(currentIteration);
	return debugString;
}
int64_t WhileLoop::getInstanceCount() const /*override*/ {
	return 0;
}