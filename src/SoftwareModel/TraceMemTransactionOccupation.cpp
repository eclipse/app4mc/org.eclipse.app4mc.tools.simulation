/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#include "TraceMemTransactionOccupation.h"
#include "HwAccessPathTLMext.h"
#include "TLMtraceStringExt.h"
#include "labelTLMext.h"
#include "MappingModel.h"
#include "MemoryImpl.h"
#include <tlm>
#include <systemc>
#include <sstream>

void TraceMemTransactionOccupation::systemc_trace(sc_core::sc_trace_file *tf, TraceMemTransactionOccupation *v,
	const std::string & sigName) {
	v->isTraced = true;
	/* Elaboration may not be completed while this call take place(not all port names are known ect.), therefore defer the call to end_of_elaboration call of inheriting class */
	v->tracings.emplace_back(tf, sigName);
}

void TraceMemTransactionOccupation::addTracingPort(std::string portName) {
	memTransactionOccStrings.emplace(portName, "idle");
}

void TraceMemTransactionOccupation::createTracings() {
	for (auto tracingFile : tracings) {
		for (auto &tracingPort : memTransactionOccStrings) {
			std::string finalSigname = tracingFile.second + "." + tracingPort.first;
			sc_trace(tracingFile.first, &tracingPort.second, finalSigname);
		}
	}
}


/* the TraceString of a Port is only updated, if its really traced, and we cache the generated trace string id within the generic_payload */
void TraceMemTransactionOccupation::updateTraceString(const std::string &PortName, std::optional<std::reference_wrapper<tlm::tlm_generic_payload>> payloadHandle) {
	if (isTraced) {
		if (!payloadHandle) {
			memTransactionOccStrings.at(PortName).setID(traceString::getIdleStringID());
		}
		// read possibly cached stringID or create the string from scratch
		else {
			auto &payload = payloadHandle->get();

			TLMtraceStringExt *cache;
			payload.get_extension(cache);


			HwAccessPathTLMext *accPath;
			payload.get_extension(accPath);

			//check whether the cached source is the same (otherwise the Task was moved to another core)
			if (cache && accPath && accPath->elem->getSource() == cache->source) {
				memTransactionOccStrings.at(PortName).setID(cache->traceStringID);
			}
			else {
				std::stringstream newTraceString;
				if (payload.is_read())
					newTraceString << "Read ";
				if (payload.is_write())
					newTraceString << "Write ";
				labelTLMext *labelTLM;
				payload.get_extension(labelTLM);
				if (labelTLM) {
					newTraceString << labelTLM->label->getName() << " at 0x" << std::hex << payload.get_address() << std::dec << "@" << MappingModel::getMemoryMapping(labelTLM->label)->name();
				}
				if (accPath) {
					newTraceString << " from " << accPath->elem->getSource()->name();
				}
				memTransactionOccStrings.at(PortName).set(newTraceString.str());
				//delete old cache if existent
				if (cache) {
					payload.release_extension<TLMtraceStringExt>();
				}
				cache = new TLMtraceStringExt;
				cache->traceStringID = memTransactionOccStrings.at(PortName).getID();
				cache->source = accPath->elem->getSource();
				payload.set_extension(cache);
			}
		}
	}
}
