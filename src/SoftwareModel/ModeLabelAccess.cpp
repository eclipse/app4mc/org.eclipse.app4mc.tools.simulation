/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#include "ModeLabelAccess.h"
#include "ExecutionItem.h"
#include <memory>
#include <systemc>
#include "MappingModel.h"
#include "ModeAccessType.h"
#include "MemoryAccessUtils.h"
#include "TracerApi.h"
#include "TraceManager.h"
#include "ExecStack.h"
#include "labelTLMext.h"

ModeLabelAccess::ModeLabelAccess(std::shared_ptr<ModeLabel> _label, int _value, ModeAccessType _access) : 
	label(_label), 
	value(_value),  
	access(_access),
	debugString(createDebugString(_access)),
	description(createDebugString(_access)){}

ModeLabelAccess::ModeLabelAccess(std::shared_ptr<ModeLabel>_label, const ModeAccessType _access) : 
	label(_label), 
	access(ModeAccessType::READ),
	debugString(createDebugString(_access)),
	description(createDebugString(_access))
	{
		if (_access != ModeAccessType::READ)
			throw std::runtime_error("ModeLabelAccess with command write needs Data to write, see constructor overloads");
	}
		

std::string ModeLabelAccess::createDebugString(ModeAccessType _accessType)
{
	std::string debugStringRet = "ModeLabelAccess (Label=" + std::string(label->getName());
	switch (_accessType){
		case ModeAccessType::READ:
			debugStringRet += ",read";
			break;
		case ModeAccessType::SET:
			debugStringRet += ",set to " + label->mode->str(value);
			break;
		case ModeAccessType::INCREMENT:
			debugStringRet += ",increment by " + label->mode->str(value);
			break;
		case ModeAccessType::DECREMENT:
			debugStringRet += ",decrement by " + label->mode->str(value);
			break;
	}
	debugStringRet += ")";
	return debugStringRet;
}


ExecutionItem ModeLabelAccess::getNextExecItem(ExecStack &stack, const ProcessingUnitDefinition &/*puDef*/)  {
	ExecutionItem returnValue{stack,*this};
	if (MappingModel::getMemoryMapping(label)) {
		/* first execution of this ActivityGraphItem -> create fixed payload and debug string*/		
		auto [payload, isInitialized] = PayloadCache::Inst().getOrInit(this);
		if (!isInitialized){
			payload->set_address(MappingModel::getMemoryAddress(label));
			payload->set_command(getTlmCommand(this->access));
			if (label->getSizeBytes() > std::numeric_limits<unsigned int>::max())
				throw std::overflow_error("LabelSize does not fit into tlm_generic_payload");
			payload->set_data_length(static_cast<unsigned int>(label->getSizeBytes()));
			payload->set_byte_enable_ptr(0); // Byte enables unused
			payload->set_streaming_width(static_cast<unsigned int>(label->getSizeBytes())); // Streaming unused -> if size is to big, this would also fail but is caught earlier
			payload->set_dmi_allowed(false); // Clear the DMI hint

			/*include label pointer */
			auto *labelTLM = new labelTLMext();
			labelTLM->label = label;
			//set state and trace, when transaction is completed at memory
			labelTLM->writeCallback = [&](){
				if (this->access == ModeAccessType::SET ||
					this->access == ModeAccessType::INCREMENT ||
					this->access == ModeAccessType::DECREMENT)
				{
					label->setState(value, this->access);
					TraceManager::Inst().signalWrite(*stack.getParentTask(), *label);
				} else {
					TraceManager::Inst().signalRead(*stack.getParentTask(), *label);
				}
				
			};
			payload->set_extension(labelTLM);
		}
		payload->set_response_status(tlm::TLM_INCOMPLETE_RESPONSE);
		returnValue.memoryTransaction = payload;
		returnValue.executingCycles = 0;
		returnValue.memoryTransactionStatistic = 1;

		return returnValue;
	}
	else {
		//instant state change
		if (this->access == ModeAccessType::SET||
			this->access == ModeAccessType::INCREMENT ||
			this->access == ModeAccessType::DECREMENT)
		{
			label->setState(value, access);
			TraceManager::Inst().signalWrite(*stack.getParentTask(), *label);
		} else{
			TraceManager::Inst().signalRead(*stack.getParentTask(), *label);
		}
		//return emptyItem if no mapping was done -> the mode label Access do not consume time
		return returnValue;
	}

}

traceID ModeLabelAccess::getTraceName() const {
	return debugString.getID();
}

std::string ModeLabelAccess::getDescription() const {
	return description;
}

std::string_view ModeLabelAccess::getType() const {
	return "ModeLabelAccess";
}

std::string_view ModeLabelAccess::getTraceString() const {
	currentStateString=description  +  ", mode label value=" + label->str();
	return std::string_view(currentStateString);	
}

