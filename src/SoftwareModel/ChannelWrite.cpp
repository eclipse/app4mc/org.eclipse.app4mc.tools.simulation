/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#include "ChannelWrite.h"

#include <utility>
#include "ChannelAccess.h"
#include "Channel.h"
#include "traceString.h"
#include "ChannelEvent.h"
#include "ExecStack.h"

std::string ChannelWrite::createDebugString(MemoryAccessType /*_command*/) {
	return "ChannelWrite (Statistic=" + std::to_string(labelAccessStatistic) + " ,Channel=" + std::string(channel->getName()) + ")";
}

ChannelWrite::ChannelWrite(std::shared_ptr<Channel> _queue, uint64_t _labelAccessStatistic) :
	ChannelAccess(_queue, _labelAccessStatistic, MemoryAccessType::WRITE)
{
	debugString.set(createDebugString(MemoryAccessType::WRITE));
}

void ChannelWrite::notifyEvents() {
	for (auto it : channel->getEvents()) {
		if (it->getType() == ChannelEventType::ACCESS_EVENT || it->getType() == ChannelEventType::WRITE_EVENT) {
			it->notify(/*sc_core::SC_ZERO_TIME*/);
		}
	}
}

std::string ChannelWrite::getDescription() const  {
	return "ChannelAccess.Write";
}
std::string_view ChannelWrite::getType() const  {
	return "ChannelSend";
}

std::string_view ChannelWrite::getTraceString() const {
	traceString = ChannelAccess::channel->getDescription();
	return traceString;
}

ExecutionItem ChannelWrite::getNextExecItem(ExecStack &stack, const ProcessingUnitDefinition &puDef)  {
	return ChannelAccess::getNextExecItem(stack,puDef);
}
