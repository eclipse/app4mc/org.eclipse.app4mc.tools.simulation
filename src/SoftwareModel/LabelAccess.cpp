/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#include "LabelAccess.h"
#include <tlm_core/tlm_2/tlm_generic_payload/tlm_gp.h>
#include <memory>
#include <tlm>
#include "DataLabel.h"
#include "ExecutionItem.h"
#include "ExecStack.h"
#include "ActivityGraphItem.h"
#include "TracerApi.h"
#include "TraceManager.h"
#include "MemoryAccessType.h"
#include "traceString.h"
#include "MemoryAccessUtils.h"
#include "MappingModel.h"
#include "labelTLMext.h"


LabelAccess::LabelAccess( std::shared_ptr<DataLabel> _label, uint32_t _AccessLabelStatistic, MemoryAccessType _command) : 
	command(_command), 
	label(_label), 
	labelAccessStatistic(_AccessLabelStatistic), 
	description(createDebugString(_command)), 
	debugString(createDebugString(_command)) {}

std::string LabelAccess::createDebugString(MemoryAccessType _command)
{
	std::string returnString = "LabelAccess (Statistic=" + std::to_string(labelAccessStatistic) + ",Label=" + std::string(label->getName());
	if (_command == MemoryAccessType::READ) {
		returnString += ",read";
	}
	else if (_command == MemoryAccessType::WRITE) {
		returnString += ",write";
	}
	returnString += ")";
	return returnString;
}

LabelAccess::LabelAccess(std::shared_ptr<DataLabel> _label, MemoryAccessType _command) : LabelAccess(_label, 1, _command) {}

ExecutionItem LabelAccess::getNextExecItem(ExecStack &stack, const ProcessingUnitDefinition &/*puDef*/) {
	ExecutionItem returnValue{stack,*this};
	/* first execution of this ActivityGraphItem -> create fixed payload and debug string*/
	auto [payload, isInitialized] = PayloadCache::Inst().getOrInit(this);
	if (!isInitialized){
		payload->set_address(MappingModel::getMemoryAddress(label));
		payload->set_command(getTlmCommand(command));
		if (label->getSizeBytes() > std::numeric_limits<unsigned int>::max())
			throw std::overflow_error("LabelSize does not fit into tlm_generic_payload");
		payload->set_data_length(static_cast<unsigned int>(label->getSizeBytes()));
		payload->set_byte_enable_ptr(0); // Byte enables unused
		payload->set_streaming_width(static_cast<unsigned int>(label->getSizeBytes())); // Streaming unused -> if size is to big, this would also fail but is caught earlier
		payload->set_dmi_allowed(false); // Clear the DMI hint

		createDebugString(command);

		auto *labelTLM = new labelTLMext();
		labelTLM->label = label;
		//trace, when transaction is completed at memory
		labelTLM->writeCallback = [&](){
			if (command == MemoryAccessType::WRITE /*MemoryAccessType::WRITE*/) {
				TraceManager::Inst().signalWrite(*stack.getParentTask(), *label);
			} else {
				TraceManager::Inst().signalRead(*stack.getParentTask(), *label);
			}
			
		};
		payload->set_extension(labelTLM);
	} 
	payload->set_response_status(tlm::TLM_INCOMPLETE_RESPONSE);
	returnValue.executingCycles = 0;
	returnValue.memoryTransaction = payload;
	returnValue.memoryTransactionStatistic = labelAccessStatistic;
	return returnValue;

}



traceID LabelAccess::getTraceName() const {
	return debugString.getID();
}

std::string LabelAccess::getDescription() const {
	return description;
}
std::string_view LabelAccess::getType() const {
	return "LabelAccess";
}
std::string_view LabelAccess::getTraceString() const {
	return description;
}
