/**
 ********************************************************************************
 * Copyright (c) 2020 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */

#include "WaitEvent.h"
#include <memory>
#include <systemc>
#include "Common.h"
#include "Task.h"
#include "OsEvent.h"

std::string WaitEvent::generateTraceString(){
    std::string typeStr;
    if(type == WaitEventType::OR)
        typeStr = "OR";
    else
        typeStr = "AND";
    std::string behaviourStr;
    if(waiting == WaitingBehaviour::active)
        behaviourStr = "active";
    else
        behaviourStr = "passive";

    std::string debugString = "Wait event: " + behaviourStr + " waiting on " + typeStr + "{";
    for(auto &event : eventMask){
        debugString += event->getName() + ",";
    }
    //erase last ,
    debugString.erase(debugString.end()-1);
    debugString += "}";

    return debugString;
}

WaitEvent::WaitEvent(WaitEventType type, WaitingBehaviour waiting) 
    : type(type), waiting(waiting), traceStr(generateTraceString()),trace(traceStr) {}

void WaitEvent::addOsEventToMask(const std::shared_ptr<OsEvent> &osEvent) {
    if(osEvent){
        eventMask.emplace_back(osEvent);
        traceStr = generateTraceString();
        trace.set(traceStr);
    }
}

traceID WaitEvent::getTraceName() const {
    return trace.getID();
}

std::string WaitEvent::getDescription() const{
    return traceStr;
}

std::string_view WaitEvent::getType() const {
    return "WaitEvent";
}

std::string_view WaitEvent::getTraceString() const {
    return traceStr;
}
/*TODO implement proper OSapiOverhead mechanism*/
void WaitEvent::setOverhead(cycle_t cycles){
    overhead = cycles;
}

/* getNextItem return the next Exec Item and set the stack to the currentActivityGraphItem if necessary*/
ExecutionItem WaitEvent::getNextExecItem(ExecStack &stack, const ProcessingUnitDefinition &/*puDef*/) /*override*/ {
    if(eventMask.empty())
        return ExecutionItem::getEmptyExecutionItem();
    
    if(waiting == WaitingBehaviour::active){
            ExecutionItem ret{stack,*this};
            ret.executingCycles = overhead;
            /* we need to construct the list of task specific sc_event's */
        /* check whether any event is already set, and return empty item (since they are or'd)*/
        if(type == WaitEventType::OR){  
            auto orList = std::make_shared<sc_core::sc_event_or_list>();
            for(auto &event : eventMask){
                if(stack.getParentTask()->eventUsedInTask(event))
                {
                    if(stack.getParentTask()->eventIsSet(event)){
                        return ret;
                    }
                    else{
                        *orList |= *(stack.getParentTask()->getScEvent(event));
                    }
                }
                else{
                    stack.getParentTask()->addOsEvent(event);
                    *orList |= *(stack.getParentTask()->getScEvent(event));
                }

            }
            if(!orList->empty())
                ret.orWaitList = orList;
        }
        if(type == WaitEventType::AND){  
            auto andList = std::make_shared<sc_core::sc_event_and_list>() ;
            for(auto event : eventMask){
                if(stack.getParentTask()->eventUsedInTask(event))
                {   
                    if(!stack.getParentTask()->eventIsSet(event)){
                            *andList &=*(stack.getParentTask()->getScEvent(event));
                    }
                }
                else{
                    stack.getParentTask()->addOsEvent(event);
                    *andList &= *(stack.getParentTask()->getScEvent(event));
                }

            }
            if(!andList->empty())
                ret.andWaitList = andList;
        }
        return ret;
    }
    else{
        throw std::runtime_error("passive waiting is currently not supported");
    }
}