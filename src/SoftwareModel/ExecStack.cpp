/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#include "ExecStack.h"
#include <memory>
#include <stdexcept>
#include <utility>
#include "ActivityGraphItem.h"
#include "Task.h"


ExecStack::ExecStack(std::shared_ptr<Task> parentTask): 
    parentTask(parentTask)
{} 


void ExecStack::pushExecutionLevel(std::vector<std::shared_ptr<ActivityGraphItem>> &newLevel, std::function<void()> completionCallback) {
  StackEntryType entry = {newLevel.begin(), newLevel, std::move(completionCallback)};
  stack.emplace_back(std::move(entry));
}

void ExecStack::addLoopIteration(std::vector<std::shared_ptr<ActivityGraphItem>> &loopContainer) {
	if(std::addressof(loopContainer) != std::addressof(stack.back().currentExecutionContainer.get())){
		throw std::runtime_error("Try add loop iteration of a unrelated container");
	}
	stack.back().repeat = true;
}

const ContainsActivityGraphItems& ExecStack::getExecutionContext() const {
	if (currentContext)
		return *currentContext;
	else
		return *parentTask;		
}

const ContainsActivityGraphItems& ExecStack::getActivityItemContainer() const {
	if (currentContainer)
		return *currentContainer;
	else 
		return getExecutionContext();
}


ExecutionItem ExecStack::getNextExecutionItem(const ProcessingUnitDefinition &puDef, bool firstAccess) {
  if (stack.empty()) {
	// return empty item if our list is empty
	return ExecutionItem::getEmptyExecutionItem();
  }

  // point to next item
  if (!firstAccess) {
	stack.back().currentExecutionPointer++;
  }

  bool gotEmptyExecItem = true;

  // effectively this loop is exited by return calls or is endless
  while (gotEmptyExecItem) {
	/* are we at the end of "call" (sub-list of ActivityGraphItems)*/
	if (stack.back().currentExecutionPointer == stack.back().currentExecutionContainer.get().end()) {
	  // call completion callback;
	  if (stack.back().endOfExecutionContainerCallback) {
		stack.back().endOfExecutionContainerCallback();
	  }

	  if (stack.back().repeat) {
		stack.back().repeat                  = false;
		stack.back().currentExecutionPointer = stack.back().currentExecutionContainer.get().begin();
		continue;
	  }

	  // if no loop repeat
	  stack.pop_back();

	  // if stack is !empty go to next item in parent and try again, otherwise we return empty execution item
	  if (!stack.empty()) {
		stack.back().currentExecutionPointer++;
		continue;
	  }

	  return ExecutionItem::getEmptyExecutionItem();
	}
	// not at the end of an container

	auto rv = (*(stack.back().currentExecutionPointer))->getNextExecItem(*this, puDef);

	if (!rv.isEmptyExecItem()) {
	  gotEmptyExecItem = false;
	  return rv;
	}
	// got empty item, not at the end of an execution block, go to next item
	// we should not reach this line, but its easy to recover
	(stack.back().currentExecutionPointer)++;
  }
  return ExecutionItem::getEmptyExecutionItem();
}
