/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#include "ChannelRead.h"
#include <sysc/kernel/sc_time.h>

#include <utility>
#include "ChannelAccess.h"
#include "Channel.h"
#include "ChannelEvent.h"
#include "traceString.h"
#include "ExecutionItem.h"
#include "ExecStack.h"


std::string ChannelRead::createDebugString(MemoryAccessType /*_command*/) {
	return "ChannelRead (Statistic=" + std::to_string(labelAccessStatistic) + " ,Channel=" + std::string(channel->getName()) + ")";
}

ChannelRead::ChannelRead(std::shared_ptr<Channel> _queue, uint64_t _labelAccessStatistic) :
	ChannelAccess(std::move(_queue), _labelAccessStatistic, MemoryAccessType::READ)
{
	debugString.set(createDebugString(MemoryAccessType::READ));
}

void ChannelRead::notifyEvents() {
	for (auto it : channel->getEvents()) {
		if (it->getType() == ChannelEventType::ACCESS_EVENT || it->getType() == ChannelEventType::READ_EVENT)
			it->notify(/*sc_core::SC_ZERO_TIME*/);
	}
}

ExecutionItem ChannelRead::getNextExecItem(ExecStack &stack, const ProcessingUnitDefinition &puDef) {
	return ChannelAccess::getNextExecItem(stack,puDef);
}

std::string ChannelRead::getDescription() const {
	return "ChannelAccess.Read";
}

std::string_view ChannelRead::getType() const {
	return "ChannelReceive";
}

std::string_view ChannelRead::getTraceString() const {
	traceString = ChannelAccess::channel->getDescription();
	return traceString;
}

