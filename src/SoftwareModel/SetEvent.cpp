/**
 ********************************************************************************
 * Copyright (c) 2020 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */

#include "SetEvent.h"
#include <systemc>
#include "Common.h"
#include "Task.h"
#include "ActivityGraphItem.h"
#include "OsEvent.h"

std::string SetEvent::generateTraceString(){
    std::string debugString = "Set event: {";
    for(auto &event : eventMask){
        debugString +=event->getName() + ",";
    }
    //erase last ,
    debugString.erase(debugString.end()-1);
    debugString += "}";

    return debugString;
}

SetEvent::SetEvent(const std::shared_ptr<Task> &task): traceStr(generateTraceString()),trace(traceStr), task(task) {}

void SetEvent::addOsEventToMask(const std::shared_ptr<OsEvent> &osEvent) {
    if(osEvent){
        eventMask.emplace_back(osEvent);
        traceStr = generateTraceString();
        trace.set(traceStr);
    }
}

void SetEvent::setTask(const std::shared_ptr<Task> &_task){task = _task;}

std::string SetEvent::getDescription() const /*override*/{
    return traceStr;
}

    traceID SetEvent::getTraceName()  const /*override*/ {
    return trace.getID();
}

std::string_view SetEvent::getType() const /*override*/ {
    return "SetEvent";
}

std::string_view SetEvent::getTraceString() const /*override*/{
    return traceStr;
}

/*TODO implement proper OSapiOverhead mechanism*/
void SetEvent::setOverhead(cycle_t cycles){
    overhead = cycles;
}

ExecutionItem SetEvent::getNextExecItem(ExecStack &stack, const ProcessingUnitDefinition &/*puDef*/) /*override*/ {
    if(eventMask.empty())
        return ExecutionItem::getEmptyExecutionItem();

    ExecutionItem ret{stack,*this};
    ret.finishedItemCallback = [this](){
        for(auto &event : this->eventMask)
            this->task->setOsEvent(event);
    };
    ret.executingCycles = overhead;
    return ret;
    
}

