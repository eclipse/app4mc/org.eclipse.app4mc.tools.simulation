/**
 ********************************************************************************
 * Copyright (c) 2021 University of Rostock and others
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */

#include "SemaphoreAccess.h"
#include "Semaphore.h"
#include "SemaphoreImpl.h"


void SemaphoreAccess::generateTraceString() {
	using namespace std::literals::string_literals;
	if (accessType == SemaphoreAccessType::request) {
	  description = "SemaphoreAccess request Semaphore \"" + semaphore->getName() + "\"";
	}
	if (accessType == SemaphoreAccessType::release) {
	  description = "SemaphoreAccess release Semaphore \"" + semaphore->getName() + "\"";
	}
	debugString.set(description);
}

SemaphoreAccess::SemaphoreAccess(const std::shared_ptr<Semaphore>& semaphore, SemaphoreAccessType accessType, WaitingBehaviour waitingBehavior) :
      accessType(accessType), semaphore(semaphore->internalInst()), waitingBehavior(waitingBehavior), debugString("") {
	generateTraceString();
	if (this->semaphore == nullptr) {
	  throw std::runtime_error("SemaphoreAccess created on empty shared_ptr<Semaphore>");
	}
}

[[nodiscard]] traceID SemaphoreAccess::getTraceName() const {
	return debugString.getID();
}

[[nodiscard]] std::string_view SemaphoreAccess::getTraceString() const {
	return description;
}

[[nodiscard]] std::string_view SemaphoreAccess::getType() const {
	return "SemaphoreAccess";
}

[[nodiscard]] std::string SemaphoreAccess::getDescription() const {
	return description;
}


  /* getNextItem return the next Exec Item and set the stack to the currentActivityGraphItem if necessary*/
ExecutionItem SemaphoreAccess::getNextExecItem(ExecStack &stack, [[maybe_unused]] const ProcessingUnitDefinition &puDef) {
	ExecutionItem ret{stack, *this};
	if (accessType == SemaphoreAccessType::request) {
	  ret.requestedSemaphore = semaphore;
	} else {
	  ret.finishedItemCallback = [this]() { semaphore->release(); };
	}
	return ret;
}