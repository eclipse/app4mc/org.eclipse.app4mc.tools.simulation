/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#include "Task.h"
#include "Scheduler.h"
#include "OsEvent.h"
#include "ProcessingUnitImpl.h"
#include "AbstractMemoryElement.h"
#include "TracerApi.h"
#include "TraceManager.h"
#include "ExecutionItem.h"
#include "ExecStack.h"
#include "StimulusImpl.h"
#include <sysc/tracing/sc_trace.h>


Task::Task(std::string name, uint32_t _activationLimit) :
    ContainsActivityGraphItems(name), //we move name at second usage, at first we actually need to copy
    Schedulable(std::move(name)),
    nameTracer(Schedulable::getName()),
    currentRunnable(traceString::getIdleStringID()),
    activationLimit(_activationLimit)
{}

std::shared_ptr<Task> Task::createTask(const std::string &_name, uint32_t _activationLimit) {
    Task tmp = Task(_name, _activationLimit); // need to construct Task first as std::make_shared cannot access private constructor
    std::shared_ptr<Task> task = std::make_shared<Task>(tmp);
    task->execStack = ExecStack(task);
    return task;
}

void Task::addStimulus( std::shared_ptr<Stimulus> stimulus)
{
    stimuli.emplace_back(stimulus);
}

void Task::stimulateTask(const StimulusImpl &stimulus) {
    if (getState() != SchedulableState::suspended){
        if(queuedStimuli.size() < activationLimit-1){ //e.g. limit = 1 --> do not queue
            queuedStimuli.push_back({sc_core::sc_time_stamp(),stimulus.traceCopy()});
        } else {
          TraceManager::Inst().taskMTALimitExceeded(*this, *scheduler, stimulus);
        }
    } else{
        ++instanceCounter;
        scheduler->taskActivate(shared_from_this(),stimulus);
    }
}

void Task::activate(const StimulusImpl &stimulus) {   
    Schedulable::activate(stimulus);
    //notify process event (if present)
    for (auto const &it : events) {
        if (it->getType() == ProcessEventType::ACTIVATE){
            it->notify();
        }
    }
    TraceManager::Inst().processActivate(*this, *scheduler, stimulus);
}

void Task::start(const std::shared_ptr<ProcessingUnitImpl> &pu){
    Schedulable::start(pu);
    //notify process event (if present)
    for (auto const &it : events) {
        if (it->getType() == ProcessEventType::START){
            it->notify();
        }
    }
    TraceManager::Inst().processStart(*this, *scheduler, *pu);
}

void Task::preempt(const std::shared_ptr<ProcessingUnitImpl> &pu){
    Schedulable::preempt(pu);
    //notify process event (if present)
    for (auto const &it : events) {
        if (it->getType() == ProcessEventType::PREEMPT){
            it->notify();
        }
    }
    TraceManager::Inst().processPreemt(*this, *scheduler, *pu);
}

void Task::resume(const std::shared_ptr<ProcessingUnitImpl> &pu){
    Schedulable::resume(pu);
    //notify process event (if present)
    for (auto const &it : events) {
        if (it->getType() == ProcessEventType::RESUME){
            it->notify();
        }
    }
    TraceManager::Inst().processResume(*this, *scheduler, *pu);
}

void Task::terminate(const std::shared_ptr<ProcessingUnitImpl> &pu)
{
    Schedulable::terminate(pu);
    if (queuedStimuli.size() > 1){
        scheduler->taskActivate(shared_from_this(),std::get<1>(queuedStimuli.front()));
        queuedStimuli.erase(queuedStimuli.begin());
    }
    //notify process event (if present)
    for (auto const &it : events) {
        if (it->getType() == ProcessEventType::TERMINATE){
            it->notify();
        }
    }
    TraceManager::Inst().processTerminate(*this, *scheduler, *pu);
}


void Task::runnableStartedCallback(Runnable &runnable) {
    runnable.incrementInstanceCount();
    scheduler->runnableStarted(shared_from_this(),runnable);
}

void Task::runnableCompletedCallback(const Runnable &runnable) {
    scheduler->runnableCompleted(shared_from_this(),runnable);
}

ExecutionItem Task::getNextExecItem(const std::shared_ptr<ProcessingUnitImpl> &pu) {
    if(activityGraphItems.empty()){
        scheduler->taskTerminate(shared_from_this());
        //terminate(pu);
        return ExecutionItem::getEmptyExecutionItem();
    }
    if(preemptedExecutionItem){
        auto returnValue = preemptedExecutionItem.value();
        preemptedExecutionItem.reset();
        return returnValue;
    }
    bool restart = false;
    //create exec stack
    if (execStack.isEmpty()) {
      auto terminationCallback = [this]() {
        scheduler->taskTerminate(shared_from_this());
      };
      execStack.pushExecutionLevel(activityGraphItems, terminationCallback);
      restart = true;
    }
    ExecutionItem returnValue = execStack.getNextExecutionItem(*pu->getProcessingUnitDefinition(),restart);
    return returnValue;
}

void Task::addOsEvent(const std::shared_ptr<OsEvent> &osEvent){
    osEvents.emplace(osEvent,TaskOsEventEntry{std::make_shared<sc_core::sc_event>(),false});
}

void Task::setOsEvent(const std::shared_ptr<OsEvent> &osEvent){
    if(osEvent){
        if(!eventUsedInTask(osEvent)){
            addOsEvent(osEvent);
        }
        if(!osEvents.at(osEvent).isSet){
                osEvents.at(osEvent).isSet = true;
                osEvents.at(osEvent).event->notify(sc_core::SC_ZERO_TIME);
        }
    }   
}

void Task::clearOsEvent(const std::shared_ptr<OsEvent> &osEvent){
    if(osEvent){
        if(!eventUsedInTask(osEvent)){
            addOsEvent(osEvent);
        }
        if(osEvents.at(osEvent).isSet){
                osEvents.at(osEvent).isSet = false;
        }
    }
    
}

bool Task::eventUsedInTask(const std::shared_ptr<OsEvent> &event){
    return (osEvents.count(event) > 0);
}

bool Task::eventIsSet(const std::shared_ptr<OsEvent> &event){
    return osEvents.at(event).isSet;
}

std::shared_ptr<sc_core::sc_event> & Task::getScEvent(const std::shared_ptr<OsEvent> &event){
    return osEvents.at(event).event;
}

void Task::storePreemptedExecutionItem(ExecutionItem execItem){
    execItem.activityGraphItemWasInterrupted = true;
    preemptedExecutionItem = execItem;
}

std::vector<std::shared_ptr<ProcessEvent>> Task::getEvents() const { 
    return events; 
}

std::shared_ptr<ProcessEvent> Task::addEvent(const std::string& _name, ProcessEventType _type) {
    auto ev = std::make_shared<ProcessEvent>(_name, _type);
    events.emplace_back(ev);
    return ev;
}

void Task::systemc_trace(sc_core::sc_trace_file *tf, const Task *v,
    const std::string & SigName) {
    sc_trace(tf, v->nameTracer, SigName);

}

    uint32_t Task::getActivationLimit() const {
        return activationLimit;
    }

    void Task::setActivationLimit(uint32_t newActivationLimit){
        activationLimit = newActivationLimit;
    }

    uint32_t Task::getActivations() const {
        return queuedStimuli.size();
    }
