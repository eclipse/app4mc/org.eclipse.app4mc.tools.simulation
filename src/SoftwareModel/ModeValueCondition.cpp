/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#include "ModeValueCondition.h"
#include "ModeLiteral.h"
#include "RelationalOperator.h"
#include "ModeLabel.h"
#include <memory>
#include "EasyloggingMacros.h"


ModeValueCondition::ModeValueCondition(const std::shared_ptr<ModeLabel>& _stateProvider, const int& _value, const RelationalOperator& _op) : 
	stateProvider(_stateProvider), 
	value(_value), 
	op(_op) {	
}
ModeValueCondition::ModeValueCondition(const std::shared_ptr<ModeLabel>& _stateProvider, const std::shared_ptr<ModeLiteral>& _value, const RelationalOperator& _op) : 
	stateProvider(_stateProvider), 
	value(_value->getLiteralId()), 
	op(_op) 
{
	//only equal and not equal operatord are supported in literal (enum) conditions
	if (op==RelationalOperator::LESS_THAN) {
		throw std::runtime_error("Operator '<' (less than) not specified for Mode Literals"); 
	}
	if (op==RelationalOperator::GREATER_THAN) {
		throw std::runtime_error("Operator '>' (greater than) not specified for Mode Literals");
	}
}
	
bool ModeValueCondition::checkCond()  {
	VLOG(2)<<"ModeValueCondition: " << stateProvider->str() << ", compare to value " << value;
	const auto& tmp = stateProvider->getState();
	switch (op){
		case RelationalOperator::EQUAL : 		
			return tmp == value;
		case RelationalOperator::GREATER_THAN: 	
			return tmp > value;
		case RelationalOperator::LESS_THAN: 	
			return tmp < value;
		case RelationalOperator::NOT_EQUAL: 	
			return tmp != value;
		default: 
			return false;
	}
}

