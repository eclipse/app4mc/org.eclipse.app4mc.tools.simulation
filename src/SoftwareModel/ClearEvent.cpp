/**
 ********************************************************************************
 * Copyright (c) 2020 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */

#include "ClearEvent.h"
#include "OsEvent.h"
#include <systemc>
#include "Common.h"
#include "Task.h"
#include "ActivityGraphItem.h"

//class ClearEvent : public ActivityGraphItem{

    std::string ClearEvent::generateTraceString(){
        std::string debugString = "Clear event: {";
        for(auto &event : eventMask){
            debugString +=event->getName() + ",";
        }
        //erase last ,
        debugString.erase(debugString.end()-1);
        debugString += "}";

        return debugString;
    }

    ClearEvent::ClearEvent(): traceStr(generateTraceString()),trace(traceStr) {}

     void ClearEvent::addOsEventToMask(const std::shared_ptr<OsEvent> &osEvent) {
        if(osEvent){
            eventMask.emplace_back(osEvent);
            traceStr = generateTraceString();
            trace.set(traceStr);
        }
    }
    
    std::string ClearEvent::getDescription() const /*override*/{
        return traceStr;
    }

     traceID ClearEvent::getTraceName()  const /*override*/ {
        return trace.getID();
    }

    std::string_view ClearEvent::getType() const /*override*/ {
		return "ClearEvent";
	}

     std::string_view ClearEvent::getTraceString() const /*override*/{
		return traceStr;
	}

    /*TODO implement proper OSapiOverhead mechanism*/
    void ClearEvent::setOverhead(cycle_t cycles){
        overhead = cycles;
    }

    ExecutionItem ClearEvent::getNextExecItem(ExecStack &stack, const ProcessingUnitDefinition &/*puDef*/) /*override*/ {
        if(eventMask.empty())
            return ExecutionItem::getEmptyExecutionItem();
        auto parentTask = stack.getParentTask()->shared_from_this();
        ExecutionItem ret{stack,*this};
        ret.finishedItemCallback = [this,parentTask](){
            for(auto &event : this->eventMask)
                parentTask->clearOsEvent(event);
        };
        ret.executingCycles = overhead;
        return ret;
        
    }
