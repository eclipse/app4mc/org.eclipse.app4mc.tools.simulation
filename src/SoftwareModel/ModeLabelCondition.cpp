/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 *           Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#include "ModeLabelCondition.h"
#include "ModeLabel.h"
#include "RelationalOperator.h"

bool ModeLabelCondition::checkCond(){
	const auto& tmp1 = stateProvider1->getState();
	const auto& tmp2 = stateProvider2->getState();
	switch (op){
		case RelationalOperator::EQUAL :
			return tmp1 == tmp2;
		case RelationalOperator::GREATER_THAN:
			return tmp1 >  tmp2;
		case RelationalOperator::LESS_THAN:
			return tmp1 <  tmp2;
		case RelationalOperator::NOT_EQUAL:
			return tmp1 != tmp2;
		default: 
			return false;
	}
}



