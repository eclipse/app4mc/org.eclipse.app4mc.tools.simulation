/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#include "Switch.h"
#include "ExecStack.h"
#include "TraceManager.h"

//**************************************************
//SWITCH ENTRY _____________________________________

SwitchEntry::SwitchEntry(const std::string& name) : 
	ContainsActivityGraphItems(name), debugString("ModeSwitch: \"" + getName() + "\"") 
{}

int64_t SwitchEntry::getInstanceCount() const /*override*/ {
	return -1;
}

//**************************************************
//SWITCH ___________________________________________

traceID Switch::getTraceName() const /*override*/ {
	static traceString trace("Switch");
	return trace.getID();
}

void Switch::addEntry(const SwitchEntry &entry)
{
	entries.emplace_back(entry);	
}

/* the conditions of the set default entry are not checked*/
void Switch::setDefaultEntry(SwitchEntry &entry) {
	defaultEntry = entry;
}
	
	/* getNextItem return the next Exec Item and set the stack to the following execItem*/
ExecutionItem Switch::getNextExecItem(ExecStack &execStack, const ProcessingUnitDefinition &puDef) /*override*/ {
	bool match = false;	
	//check all entries, whether their conditions are met, and return the first.
	for (auto &entry : entries) {
		if (entry.checkCondition()) {
			match = true;
			lastEntryTaken = &entry;
			if (!entry.activityGraphItems.empty()) {
				TraceManager::Inst().activityGraphItemBranch(execStack.getExecutionContext(), *this);
				//need to store the old container in a copyable struct, since context will be changed before the callback is called
				std::reference_wrapper oldContainer = execStack.getExecutionContext();
				auto callback = [oldContainer,&execStack,this](){
					execStack.setActivityItemContainer(oldContainer);
					TraceManager::Inst().activityGraphItemBranchFinish(execStack.getExecutionContext(), *this);
				};
				execStack.setActivityItemContainer(entry);
				execStack.pushExecutionLevel(entry.activityGraphItems, callback);
				return (entry.activityGraphItems[0])->getNextExecItem(execStack,puDef);
			}
		} else {
			TraceManager::Inst().comment(
				HasCondition::conditionNotMetMsg(entry.getName(), execStack.getActivityItemContainer().getName()));
		}
	}
	if (!match && !defaultEntry.activityGraphItems.empty()) {
		lastEntryTaken = &defaultEntry;
		TraceManager::Inst().activityGraphItemBranch(execStack.getExecutionContext(), *this);
		//need to store the old context in a copyable struct, since context will be changed before the callback is called
		std::reference_wrapper oldContainer = execStack.getExecutionContext();
		auto callback = [oldContainer,&execStack,this](){
			execStack.setActivityItemContainer(oldContainer);
			TraceManager::Inst().activityGraphItemBranchFinish(execStack.getExecutionContext(), *this);
		};
		execStack.setActivityItemContainer(defaultEntry);
		execStack.pushExecutionLevel(defaultEntry.activityGraphItems, callback);
		return defaultEntry.activityGraphItems[0]->getNextExecItem(execStack,puDef);
	}
	return ExecutionItem::getEmptyExecutionItem();
}

std::string Switch::getDescription() const /*override*/ {
	return "Switch";
}

std::string_view Switch::getType() const /*override*/ {
	return "Switch";
}

std::string_view Switch::getTraceString() const /*override*/{
	if (lastEntryTaken != nullptr){
		debugString = getDescription()  + ": case " + lastEntryTaken->getName();
	} else {
		debugString = getDescription()  + ": case (null)";
	}
	return debugString;
}
