
/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#ifndef NOMINMAX
#define NOMINMAX
#include <memory>
#define ADDED_NOMINMAX
#endif
#include "easylogging++.h"


#include "EasyloggingConfig.h"

INITIALIZE_EASYLOGGINGPP

void EasyloggingConfig::configure(const std::string fileName, unsigned short verbose, bool toStdOut, bool toFile){
	el::Configurations defaultConf;
	defaultConf.setToDefault();

	// Values are always std::string
	defaultConf.set(el::Level::Info,
			el::ConfigurationType::Format, "%datetime %level %msg");

	defaultConf.setGlobally(el::ConfigurationType::Filename,fileName);

	// To set GLOBAL configurations you may use
	defaultConf.setGlobally(
			el::ConfigurationType::Format, "%datetime %msg");
	defaultConf.setGlobally(
			el::ConfigurationType::ToFile, toFile?"true":"false");
	defaultConf.setGlobally(
			el::ConfigurationType::ToStandardOutput, toStdOut?"true":"false");
	
	defaultConf.setGlobally(
			el::ConfigurationType::Enabled, "true");
	
	el::Loggers::setVerboseLevel(verbose);

	el::Loggers::reconfigureLogger("default", defaultConf);

	// char *argv[]={ NULL };
	// START_EASYLOGGINGPP	(0,argv);
}

void EasyloggingConfig::enable(bool enable){
	el::Loggers::reconfigureLogger("default", el::ConfigurationType::Enabled, enable?"true":"false");
}

void EasyloggingConfig::disable(){
	enable(false);
}

void EasyloggingConfig::msg(const std::string& s, int verbose){
	VLOG(verbose)<<"EasyloggingConfig::msg: "<<s;
}

