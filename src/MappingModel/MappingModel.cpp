/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 * - University of Rostock
 * 	 Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#include "MappingModel.h"
#include "HardwareModel.h"
#include "AbstractMemoryElement.h"
#include <utility>
#include "EasyloggingMacros.h"
#include "MemoryImpl.h"

std::unordered_map<std::shared_ptr<AbstractMemoryElement>, std::pair<std::shared_ptr<MemoryImpl>,uint64_t>> &MappingModel::memoryMappings(){
	static auto map = new std::unordered_map<std::shared_ptr<AbstractMemoryElement>, std::pair<std::shared_ptr<MemoryImpl>,uint64_t>>() ;
	return *map;
}

std::optional<std::pair<std::shared_ptr<MemoryImpl>,uint64_t>> MappingModel::getCompleteMapping(const std::shared_ptr<AbstractMemoryElement> &_element){
	auto it = MappingModel::memoryMappings().find(_element);
	if(it != MappingModel::memoryMappings().end()){
		return std::make_optional(it->second);
	}
	else{
		return std::nullopt;
	}

}

std::shared_ptr<MemoryImpl> MappingModel::getMemoryMapping(const std::shared_ptr<AbstractMemoryElement> &_element)  {
	auto mapping = MappingModel::getCompleteMapping(_element);
	if(mapping){
		return mapping->first;
	}
	VLOG(0) << "Error: missing mapping for memory element " << _element->getName();
	return nullptr;
	
}

void MappingModel::addMemoryMapping(std::shared_ptr<AbstractMemoryElement> _element, std::shared_ptr<MemoryImpl> _memory) {
	auto mapping = MappingModel::getCompleteMapping(_element);
	if(mapping){
		mapping->first = _memory; 
	}
	else{
		MappingModel::memoryMappings().emplace(_element, std::make_pair(_memory,static_cast<uint64_t>(0)));
	}
	
}

uint64_t MappingModel::getMemoryAddress(const std::shared_ptr<AbstractMemoryElement> &_element) {
	auto mapping = MappingModel::getCompleteMapping(_element);
	if(mapping){
		return mapping->second;
	}
	VLOG(0) << "Error: missing address mapping for memory element " << _element->getName();
	return 0;

}

void MappingModel::addMemoryAddress(std::shared_ptr<AbstractMemoryElement> _element, uint64_t _address) {
		auto mapping = MappingModel::getCompleteMapping(_element);
	if(mapping){
		mapping->second = _address; 
	}
	else{
		MappingModel::memoryMappings().emplace(_element, std::make_pair(nullptr,_address));
	}
	
}

std::string MappingModel::toString() {
	std::string buf = "\nMemory mapping:\n";
	for (auto it = (&MappingModel::memoryMappings())->begin(); it != (&MappingModel::memoryMappings())->end(); it++) {
		auto elementName = it->first->getName();
		std::pair<std::shared_ptr<MemoryImpl>, uint64_t >* pair = &(it->second);
		auto memoryName = pair->first->name();
		auto memoryAddress = pair->second;
		buf.append(elementName);
		buf.append("-->");
		buf.append(memoryName);
		buf.append(" @ ");
		buf.append(std::to_string(memoryAddress));
		buf.append("\n");
	}
	return buf;
}