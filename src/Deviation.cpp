/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */
#include "Deviation.h"
#include <string>
#include "effolkronium/random.hpp"
#include <pcg_random.hpp>
#include "EasyloggingMacros.h"
#ifndef NOMINMAX
  #define NOMINMAX
  #define ADDED_NOMINMAX
  #include "gcem.hpp"
#else
  #include "gcem.hpp"
#endif

using app4mcsim_rand = effolkronium::basic_random_static<pcg32, app4mcsim_seed>;

uint64_t app4mcsim_seed::operator()() {
	auto seed = getSeed();
	std::cout << "Simulation Random Seed: "
	          << getSeedString() << std::endl;
	std::cout << "reuse seed with: --seed " << getSeedString() << std::endl;
	return seed;
}

uint64_t& app4mcsim_seed::getSeed() {
	static uint64_t seed = []() {
  //seed 64bit with at least 64bit from the random device
  //although random_device may always return 32bit number, this is not garuanteed by the std, so we calculate the number of random_device calls
  //we shift in all rand_device values for the seed, which might throw away some random bits(but typically it exactly fits)
	  constexpr auto countRandCalls = integerCeilDiv(std::numeric_limits<uint64_t>::digits, std::numeric_limits<std::random_device::result_type>::digits);
	  std::random_device rnd;
	  uint64_t rv = 0;
	  for (int i = 0; i < countRandCalls; i++) {
		rv = rv << static_cast<unsigned int>(std::numeric_limits<std::random_device::result_type>::digits);
		rv |= rnd();
	  }
	  return rv;
	}();
	return seed;
}

void app4mcsim_seed::setSeed(uint64_t seed) {
	getSeed() = seed;
}

std::string app4mcsim_seed::getSeedString() {
	std::stringstream ss;
    ss << "0x" <<std::hex << getSeed();
  	return ss.str();
}

constexpr int numberOfResampleBoundedDeviation = 10;
template<typename T, typename sampleFun> 
T resampleUntilInBounds(sampleFun fun, T lowerBound, T upperBound){
  for (int i = 0; i< numberOfResampleBoundedDeviation; i++){
      auto res = fun();
      if(res >= lowerBound && res <= upperBound)
        return res;
  }
  //all samples laid out of the boundaries, now we clamp for escape
  VLOG(32) << "Sampling of a Deviation failed for the modeled Boundaries, Clamp the next sample";
  return std::clamp<T>(fun(),lowerBound, upperBound);
}

template<typename T> 
T convertDoubleSample(double sample){
    // for floating point we do not need round
    if constexpr (std::is_floating_point<T>::value) {
      return  static_cast<T>(sample);
          
    } 
    else if constexpr (std::is_same<T,Time>::value) {
     return T(static_cast<Time::rep>(std::round(sample)));
    }
    else{
      return static_cast<T>(std::round(sample));
    }
}



/*-------------------------------------------------------------
	Gauss
-------------------------------------------------------------*/
template<typename T>
ValueGaussDistribution<T>::ValueGaussDistribution(
	meanSdType mean, meanSdType sd, T lowerBound, T upperBound): 
	dist{valueOf<double>(mean), 
	valueOf<double>(sd)}, 
	lowerBound(lowerBound), 
	upperBound(upperBound) 
{
    if (valueOf<double>(sd) < 0 || std::isnan(valueOf<double>(sd)))
      throw std::range_error("standard deviation needs to be a positive number");
    if (std::isnan(valueOf<double>(mean)))
      throw std::range_error("mean needs to be a number");
    if (upperBound < lowerBound)
      throw std::range_error("lowerBound needs to be smaller than upperBound");
    
    // we need this ugly construct, otherwise "c4127 conditional expression is constant" is given on msvc
    // checkDoubleCast defined constexpr, but not const in this context
    auto meanCheckRes = (std::is_integral<T>::value && checkDoubleCast(mean));
    if (meanCheckRes)
      VLOG(32) << "Mean(" << valueOf<ELong>(mean) <<") of GaussDistribution is to big to be stored in double with full integer precision";

    auto sdCheckRes = (std::is_integral<T>::value && checkDoubleCast(sd));
    if (sdCheckRes)
      VLOG(32) << "SD(" << valueOf<ELong>(sd) <<") of GaussDistribution is to big to be stored in double with full integer precision";
}

template<typename T>
T ValueGaussDistribution<T>::getSample() {
return convertDoubleSample<T>(resampleUntilInBounds<double>(
      [this](){return app4mcsim_rand::get(this->dist);},
      valueOf<double>(lowerBound),valueOf<double>(upperBound)));
  }

template class ValueGaussDistribution<ELong>;
template class ValueGaussDistribution<double>;
template class ValueGaussDistribution<Time>;

/*-------------------------------------------------------------
	Weibull
-------------------------------------------------------------*/
template<typename T>
/*static*/ constexpr double ValueWeibullDistribution<T>::paramZeroFunction(
	double shape, double upperBound, double p_upperBound, double mean) 
{
    // the static cast here is a quirk for gcem
    return 1 / static_cast<double>(gcem::exp(static_cast<long double>(gcem::pow(
                   (upperBound / mean) * gcem::tgamma(1 + 1 / static_cast<long double>(shape)), shape)))) -
           p_upperBound;
}

template<typename T>
ValueWeibullDistribution<T>::ValueWeibullDistribution(
	double shape, double scale, T lowerBound, T upperBound):
	lowerBound(lowerBound),
	upperBound(upperBound),
	dist{shape, scale} 
{
	if (upperBound < lowerBound)
		throw std::range_error("lowerBound needs to be smaller than upperBound");
}

template<typename T>
ValueWeibullDistribution<T>::ValueWeibullDistribution(weibullParam param, T lowerBound, T upperBound):
	ValueWeibullDistribution(param.shape, param.scale, lowerBound, upperBound) 
{
	if (std::abs(param.achievedPRemain - param.requestedPRemain) > 0.01) {
		VLOG(32) << "Requested probability for Weibull Distribution is "
				<< param.achievedPRemain << " insteat of "
				<< param.requestedPRemain;
}
}

template<typename T>
T ValueWeibullDistribution<T>::getSample()  {
return convertDoubleSample<T>(resampleUntilInBounds<double>(
	[this](){return valueOf<double>(this->lowerBound) +app4mcsim_rand::get(this->dist);},
	valueOf<double>(lowerBound),valueOf<double>(upperBound)));
}

template<typename T>
/*static*/ constexpr typename ValueWeibullDistribution<T>::weibullParam ValueWeibullDistribution<T>::_findParameter(double mean,
                                              double pRemainPromille,
                                              double lowerBound, double upperBound) {
    weibullParam retVal;
    retVal.shape = 1;
    retVal.scale = 1;
    retVal.requestedPRemain = pRemainPromille;

    double shifted_mean = mean - lowerBound;
    double shifted_upper_bound = upperBound -lowerBound ;
    double p_upperBound = pRemainPromille / 1000;

    /**
     * The approximation from the given parameters boil down to the
     * following problem (latex formulas): p_{upper limit} \equiv
     * CDF_{weibull}(upper limit,shape,scale) where p_{upper limit}
     * = 1 - pRemainPromille/1000
     *
     * Using the Weibull CDF
     * (1) CDF_{weibull} =  1 - e^{- (\frac{x}{scale})^{shape}}
     * and the Expectation Value (Mean Value)
     * (2) E_{avg} = scale \cdot \Gamma(1 + \frac{1}{shape})
     * we can derive a formula for the scale dependend on shape and
     * known values
     *
     * from (2)
     * (3) scale = \frac{E_{avg}}{\Gamma(1 + (1 / shape))}
     * and with (1) and (3) follows
     *
     * (4) p_{upper limit} = e^{-(\frac{upper limit}{E_{avg}}\cdot
     * \Gamma(1+\frac{1}{shape}))^{shape}}
     *
     * from this we form a zero finding problem
     *
     * (5) 0 = e^{-(\frac{upper limit}{E_{avg}}\cdot
     * \Gamma(1+\frac{1}{shape}))^{shape}} - p_{upper limit}
     *
     * The work "Robust Scheduling of Real-Time Applications on
     * Efficient Embedded Multicore Systems"
     * https://mediatum.ub.tum.de/download/1063381/1063381.pdf
     * proposes an algorithm from (4) which is basically the
     * bisection method. Moreover the described algorithm is only
     * proposed for shape > 0, which may not always lead to probable
     * parameters.
     *
     * From (4) we know that \frac{upper limit}{E_{avg}} is always
     * >= 1, as the upper limit is always bigger than the average.
     * The result of the Gamma function have 3 important cases: for
     * shape > 1 it is strictly monotonic increasing with values
     * bigger than 1. For shape < 1 there are 2 interesting
     * intervals, from zero to the minimum of \Gamma (which is
     * something around 1.461) and from minimum of \Gamma to 1. In
     * both intervals \Gamma (1+\frac{1}{shape}) > 0 but \Gamma
     * (1+\frac{1}{shape}) < 1.
     *
     * There may exist 2 solutions of the equation (5) but mostly
     * the first solution is a really small shape (smaller than 0.1)
     * which lead to inappropriate CDFs. Therefore we start from the
     * safe point 0.1 which is for normal upperBound/mean ratios
     * always behind the first solution, and start to search for an
     * sign switch from positive to negative.
     *
     * Afterwards a simple bisection method is applied to find the
     * shape which fits best with given precision. In Contrast to
     * https://mediatum.ub.tum.de/download/1063381/1063381.pdf this
     * also allows solutions for shape factors < 1.
     *
     *
     * */

    if ((shifted_upper_bound / shifted_mean) > 10000)
      throw std::invalid_argument(
          "upper bound and mean have no valid value for parameter "
          "finding ");

    if (p_upperBound > 1) {
      throw std::invalid_argument("pRemainPromille needs to be smaller than 1");
    }

    /* find zero interval */
    double right = 0.2;
    double left = 0.1;
    double right_f = paramZeroFunction(right, shifted_upper_bound, p_upperBound,
                                       shifted_mean);
    double left_f = paramZeroFunction(left, shifted_upper_bound, p_upperBound,
                                      shifted_mean);
    while ((right_f > 0 || left_f < 0) && right < 1000 &&
           (p_upperBound + right_f) > 0) {
      left = right;
      right = right + std::max<double>((0.1 * right), 0.1);
      right_f = paramZeroFunction(right, shifted_upper_bound, p_upperBound,
                                  shifted_mean);
      left_f = paramZeroFunction(left, shifted_upper_bound, p_upperBound,
                                 shifted_mean);
    }
    if (right_f > 0 && left_f < 0) {
      throw std::invalid_argument(
          "there is no valid weibull distribution for the used parameter");
    }

    // if we found the exact solution on one of the interval boarder, return
    // them
    if (left_f == 0) {
      right = left;
      right_f = left_f;
    }

    if (right_f == 0) {
      retVal.shape = right;
      retVal.scale = static_cast<double> (shifted_mean / (gcem::tgamma(1 + (1 / static_cast<long double>(retVal.shape)))));
      return retVal;
    }

    while (true) {
      double half = left + (right - left) / 2;
      double half_f = paramZeroFunction(half, shifted_upper_bound, p_upperBound,
                                        shifted_mean);
      // found solution
      if (half_f == 0.0 || left == right || half == right || half == left) {
        retVal.shape = half;
        retVal.scale = static_cast<double> (shifted_mean / (gcem::tgamma(1 + (1 / static_cast<long double>(retVal.shape)))));
        retVal.achievedPRemain = (half_f + p_upperBound) * 1000;
        return retVal;
      } else {
        if (gcem::sgn(1.0) == gcem::sgn(half_f)) {
          left = half;
        } else {
          right = half;
        }
      }
      if (!gcem::internal::is_finite(left) ||
          !gcem::internal::is_finite(right) ||
          !gcem::internal::is_finite(half_f)) {
        throw std::invalid_argument(
            "there is no valid weibull distribution for the used parameter");
      }
    }
}

template<typename T>
/*static*/ constexpr typename ValueWeibullDistribution<T>::weibullParam ValueWeibullDistribution<T>::findParameter(
	meanType mean, double pRemainPromille, T lowerBound, T upperBound) 
{
    if(checkDoubleCast(mean))
       throw std::invalid_argument(
         "Mean of WeibullEstimator parameter find is to big to be stored in double with full integer precision");
	return _findParameter(valueOf<double>(mean), pRemainPromille, valueOf<double>(lowerBound), valueOf<double>(upperBound));
}

template class ValueWeibullDistribution<ELong>;
template class ValueWeibullDistribution<double>;
template class ValueWeibullDistribution<Time>;


/*-------------------------------------------------------------
	Uniform
-------------------------------------------------------------*/
template<typename T>
ValueUniformDistribution<T>::ValueUniformDistribution(T lowerBound, T upperBound): 
    dist{valueOf<paramType>(lowerBound), 
	valueOf<paramType>(upperBound)}, 
	lowerBound(lowerBound), 
	upperBound(upperBound)
{}

template<typename T>
  T ValueUniformDistribution<T>::getSample()  {
      return T(app4mcsim_rand::get(this->dist));
}
template class ValueUniformDistribution<ELong>;
template class ValueUniformDistribution<double>;
template class ValueUniformDistribution<Time>;

/*-------------------------------------------------------------
	Beta
-------------------------------------------------------------*/
template<typename T>
ValueBetaDistribution<T>::ValueBetaDistribution(double alpha, double beta, T lowerBound, T upperBound): 
    dist1{alpha},
	dist2{beta}, 
	lowerBound(lowerBound), 
	upperBound(upperBound), 
	scaling(valueOf<double>(upperBound-lowerBound))
{}

template<typename T>
T ValueBetaDistribution<T>::getSample()  {
	double x = app4mcsim_rand::get(this->dist1);
	double y = app4mcsim_rand::get(this->dist2);
	double normalized = x/(x+y);
	return convertDoubleSample<T>((normalized*scaling)+valueOf<double>(lowerBound));
}
template class ValueBetaDistribution<ELong>;
template class ValueBetaDistribution<double>;
template class ValueBetaDistribution<Time>;

/*-------------------------------------------------------------
	Boundaries
-------------------------------------------------------------*/
template<typename T>
/*static*/ constexpr double ValueBoundaries<T>::alphaFromType(BoundariesSamplingType type){
    switch (type){
       case BoundariesSamplingType::BestCase: return 0.2;
       case BoundariesSamplingType::WorstCase: return 1;
       case BoundariesSamplingType::AverageCase: return 2;
       case BoundariesSamplingType::CornerCase: return 0.2;
       case BoundariesSamplingType::Uniform: return 1;
       default:
        throw std::runtime_error("Samling Type for Boundaries not defined");
    }
  }

template<typename T>
/*static*/ constexpr double ValueBoundaries<T>::betaFromType(BoundariesSamplingType type){
    switch (type){
       case BoundariesSamplingType::BestCase: return 1;
       case BoundariesSamplingType::WorstCase: return 0.2;
       case BoundariesSamplingType::AverageCase: return 2;
       case BoundariesSamplingType::CornerCase: return 0.2;
       case BoundariesSamplingType::Uniform: return 1;
       default:
        throw std::runtime_error("Samling Type for Boundaries not defined");
    }
}

template<typename T>
ValueBoundaries<T>::ValueBoundaries(BoundariesSamplingType type, T lowerBound, T upperBound): 
	dist1{alphaFromType(type)},
	dist2{betaFromType(type)}, 
	lowerBound(lowerBound), 
	upperBound(upperBound), 
	scaling(valueOf<double>(upperBound-lowerBound))
{}

template<typename T>
T ValueBoundaries<T>::getSample()  {
	double x = app4mcsim_rand::get(this->dist1);
	double y = app4mcsim_rand::get(this->dist2);
	double normalized = x/(x+y);
	return convertDoubleSample<T>((normalized*scaling)+valueOf<double>(lowerBound));
}
template class ValueBoundaries<ELong>;
template class ValueBoundaries<double>;
template class ValueBoundaries<Time>;
