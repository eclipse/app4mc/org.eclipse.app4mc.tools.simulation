/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */
#include "SimRunner.h"
#include <easylogging++.h>
#include <sysc/kernel/sc_externs.h>
#include <sysc/kernel/sc_simcontext.h>
#include <algorithm>
#include <cstddef>
#include <cstdint>
#include <functional>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <type_traits>
#include "EasyloggingConfig.h"
#include "EasyloggingMacros.h"
#include "TimeUtilsSystemC.h"
#include <sysc/tracing/sc_trace.h>
#include <sysc/utils/sc_report_handler.h>
#include <sysc/tracing/sc_trace.h>
#include "traceString.h"
#include "TracerApi.h"
#include "TracerFactory.h"
#include "TraceManager.h"
#include "Deviation.h"
#include "TimeUtilsSystemC.h"
#include "HwStructureImpl.h"
#include "Callbacks.h"
#include "StimulusRegistry.h"


int sc_main([[maybe_unused]] int argc,[[maybe_unused]] char *argv[]){
    //dummy, do nothing
    return 0;
}


sc_core::sc_trace_file* SimRunner::setScTraceFile(const std::string& path){
    if (!tf) {
        tf = sc_core::sc_create_vcd_trace_file(path.c_str());	
    }
    return tf;
}

SimRunner::SimRunner(uint64_t randomSeed,  std::string hwRootName , uint64_t resolution)
    :SimRunner(hwRootName, resolution)
{
    app4mcsim_seed::setSeed(randomSeed);
}

SimRunner::SimRunner(std::string hwRootName, uint64_t resolution)
    :rootName(hwRootName), resolutioninNs(resolution)
{}

SimRunner::~SimRunner(){
    finish();
}

std::unique_ptr<HwStructure>& SimRunner::modelRoot(){
    if (root==nullptr){
        root=std::unique_ptr<HwStructure>(new HwStructure(rootName));
        root->setSimCallback(
            sim_callback_type::start_of_simulation, 
            [](){StimulusRegistry::Inst().stimulate();}
        );
    }
    return root;
}

Time SimRunner::getCurrentSimulationTime(){
    auto resolutionInNs = getTimeresolutionAsDuration();
    auto simTicks =  sc_core::sc_time_stamp().value();
    auto t = resolutionInNs * simTicks;
    return t;
}


Time SimRunner::simulate(Time time){
    try{		
        //create simulation model
        if (!setupComplete){
            sc_core::sc_get_curr_simcontext()->reset();
            sc_core::sc_set_stop_mode(sc_core::SC_STOP_FINISH_DELTA);
            //sc_core::sc_set_stop_mode(sc_core::SC_STOP_IMMEDIATE);
            sc_core::sc_set_time_resolution(static_cast<double>(resolutioninNs), sc_core::SC_NS);
            instantiateTracers(); //note: time resolution must be prior to tracer instantiation
            setup();
            setupComplete=true;
        }
        //start simulation
        auto scTime = convertTime(time);
        sc_core::sc_start(scTime);
    }
    catch (sc_core::sc_report &e) {
        const char* file = e.get_file_name();
        const int line = e.get_line_number();
        const char* msg = e.get_msg();
        VLOG(0) << msg << "\nin file: " << file << "\nline: " << line;
        std::string report = "Error in APP4MCsim/SystemC:\n" + sc_core::sc_report_compose_message(e);
        VLOG(0)<< report;
        throw std::runtime_error(report);
    }
    return timestamp_ns{sc_core::sc_time_stamp().value()};
}

void SimRunner::pause(){
    sc_core::sc_pause();
}

void SimRunner::finish(){
    VLOG(0) << "End of simulation";
    sc_core::sc_stop();
    if (tf != nullptr){
        sc_core::sc_close_vcd_trace_file(tf); //Note: DTOR of tf is private, desctruct by closin file
    }
    StimulusRegistry::Inst().clear();
    TraceManager::removeAll(); //need to remove, before resetting
    root.reset();
    setupComplete=false;
}

const std::vector<std::string>& SimRunner::getTracerNames() {
  return TracerFactory::getTracerNames();
}

void SimRunner::enableTracers(const std::vector<std::string>& _tracerNames, const std::string& _tracerPath, bool _tracerTimePrefix){
    tracerNames = _tracerNames;
    tracerPath = _tracerPath;
    tracerTimePrefix =_tracerTimePrefix;
}

void SimRunner::instantiateTracers(){
    if (!tracerNames.empty()) {
        std::stringstream buf;
        buf<<"Trace file location: "<<tracerPath<<std::endl;
        TraceManager::setTraceDirectory(tracerPath);
        if (tracerTimePrefix){
            TraceManager::enableTimePrefix();
        }
        
        //SystemC VCD trace
        auto vcd = std::find(tracerNames.begin(),tracerNames.end(), "VCD" );
        if (vcd != tracerNames.end()){
            buf<<"Use SystemC/VCD trace"<<std::endl;
            setScTraceFile(tracerPath + std::string("/trace"));
            traceString::outputPath(tracerPath);
            tracerNames.erase(vcd);
        }
        //APP4MCsim tracers (e.g. BtfTracer)
        buf<<"Use APP4MCsim tracers:"<<std::endl;
        for (const auto& n : tracerNames){
            buf<<n<<std::endl;
        }
        TraceManager::createTracerFromList(tracerNames);	
        VLOG(0)<<buf.str();
    }
}

const std::string& SimRunner::version(){
    static std::string v = [](){
        return "0.0.1";
    }();
    return v;
}

