/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Behnaz Pourmohseni <behnaz.pourmohseni@de.bosch.com>
 ********************************************************************************
 */
#include "Event.h"
#include "PubSubEventManager.h"
#include "PubSubEventSubscriber.h"
#include <string>

void PubSubEventManager::addEvent(const std::shared_ptr<Event>& event, const std::shared_ptr<PubSubEventSubscriber>& subscriber) {
	if(eventToSubscriberMap.count(event)){
		assertCondition(eventToSubscriberMap[event] == subscriber, std::string("Event ") + event->getName() + " is registered to a different subscriber");
	} else {
		eventToSubscriberMap[event] = subscriber;
	}
}

void PubSubEventManager::removeEvent(const std::shared_ptr<Event>& event, const std::shared_ptr<PubSubEventSubscriber>& subscriber) {
	
	assertCondition(eventToSubscriberMap.count(event) != 0, std::string("Event not found ") + event->getName());
	assertCondition(eventToSubscriberMap[event] == subscriber, std::string("Subscriber does not match the registered subscriber of the event ") + event->getName());
	eventToSubscriberMap.erase(event);
}
void PubSubEventManager::removeSubscriber(const std::shared_ptr<PubSubEventSubscriber>& subscriber) {
	for (auto it = eventToSubscriberMap.begin(); it != eventToSubscriberMap.end();) {
		it->second == subscriber ? eventToSubscriberMap.erase(it++) : (++it);
	}
}

void PubSubEventManager::notify(const std::shared_ptr<Event>& event) {
	if(eventToSubscriberMap.count(event)) {
		eventToSubscriberMap[event]->update(event);
	}
}
