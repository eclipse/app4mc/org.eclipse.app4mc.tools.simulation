/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Behnaz Pourmohseni <behnaz.pourmohseni@de.bosch.com>
 ********************************************************************************
 */

#include <memory>
#include "EventManager.h"
#include "PubSubEventManager.h"
#include "PubSubEventSubscriber.h"


void PubSubEventSubscriber::addPubSubEvent(const std::shared_ptr<Event>& event) {
    EventManager::Inst<PubSubEventManager>()->addEvent(event, this->shared_from_this());
}

void PubSubEventSubscriber::removePubSubEvent(const std::shared_ptr<Event>& event) {
    EventManager::Inst<PubSubEventManager>()->removeEvent(event, this->shared_from_this());
}

void PubSubEventSubscriber::removeSubscriber(){
    EventManager::Inst<PubSubEventManager>()->removeSubscriber(this->shared_from_this());
}