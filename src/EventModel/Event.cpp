/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#include "Event.h"
#include <sysc/kernel/sc_event.h>
#include <sysc/kernel/sc_time.h>
#include <functional>
#include <memory>
#include "TracerApi.h"
#include "TraceManager.h"
#include "EventManager.h"
#define SC_INCLUDE_DYNAMIC_PROCESSES 
#include <systemc>
#include "TimeUtilsSystemC.h"
#include <sstream>


Event::Event(const std::string& _name): 
	HasName(_name), 
	scEvent(std::make_shared<sc_core::sc_event>())
{}

void Event::notify(){
	TraceManager::Inst().comment("Event notification: " + getName());
	scEvent->notify(sc_core::SC_ZERO_TIME);
	EventManager::notifyManagers(this->shared_from_this());
}

void Event::notify(const Time&time){
	std::ostringstream msg;
	sc_core::sc_time scTime = convertTime(time);
	msg<<"Event notification: "<<getName()<<" scheduled in "<<scTime.to_string();
	TraceManager::Inst().comment(msg.str());
	scEvent->notify(scTime);
	sc_core::sc_spawn([=]() {
			sc_core::wait(*scEvent);
			EventManager::notifyManagers(this->shared_from_this());
		}
	);
}

void Event::cancel(){
	scEvent->cancel();
	TraceManager::Inst().comment("Event canceled: " + getName());
}

void Event::wait() {
	sc_core::wait(*scEvent);
}
