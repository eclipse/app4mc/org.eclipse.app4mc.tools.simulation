/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#include "traceString.h"
#include <iostream>
#include <fstream>
#include <mutex>
#include <iomanip>
#include <map>
#include <functional>
#include <memory>
#include "Types.h"
#include "ProcessingUnitImpl.h"

/*static*/ const std::string& traceString::outputPath(const std::string& _outputPath) {
	static std::string outputPath = _outputPath;
	return outputPath;
}

/*static*/ std::mutex &traceString::fileMut() {
	static std::mutex *_fileMut = new std::mutex();
	return *_fileMut;
}
/*static*/ std::ofstream &traceString::translationFile() {
	static std::ofstream *_translationFile = []() {
		auto out = new std::ofstream(traceString::outputPath() + "/GTKWaveStringMapping.txt");
		out->fill('0'); return out; }();
	return *_translationFile;
}

/*static*/ uint32_t &traceString::maxID() { 
	static uint32_t *maxID = new uint32_t(1); return *maxID; 
}

/*static*/ std::map<std::string, uint32_t, std::less<>> &traceString::IDMap() { 
	static  std::map<std::string, uint32_t, std::less<>> *map = new std::map<std::string, uint32_t, std::less<>>(); return *map; 
}


/*static*/ traceID traceString::getIdleStringID() {
	static traceID id = []() {
		traceString idle("idle");
		return idle.getID();
	}();
	return id;
}

/*explicit*/ traceString::traceString(const std::function<std::string()> &stringFunc) : traceString(stringFunc()) {
}

/*explicit*/ traceString::traceString(const std::function<std::string_view()> &stringFunc) : traceString(stringFunc()) {
}

/*explicit*/ traceString::traceString(std::string_view string) {
	set(string);
}

void traceString::set(std::string_view string) {
	if (IDMap().count(string) == 0) {
		// create critical section for /*static*/ variables
		std::lock_guard<std::mutex> lock(fileMut());

		maxID()++;
		stringID = maxID();
		IDMap().emplace(string, stringID);

		translationFile() << std::hex << std::setw(8) << stringID << " ";

		/* add green backgroundcolor for idle string (also gtkwave feature)*/
		if (string == "idle") {
			translationFile() << "?DarkOrange?idle";
		}
		else {
			translationFile() << string;
		}


		// add some char for empty string, otherwise it will be not translated by gtkwave
		if (string.length() == 0)
			translationFile() << "_";

		translationFile() << std::endl;
	}
	else
	{
		stringID = IDMap().find(string)->second;
	}
}

uint32_t traceString::getID() const{
	return stringID;
}


void traceString::setID(uint32_t newID) {
	stringID = newID;
}


#include <sysc/tracing/sc_trace.h>
void traceString::systemc_trace(sc_core::sc_trace_file *tf,const uint32_t& v, const std::string &name){
	sc_core::sc_trace(tf, v, name);
}
