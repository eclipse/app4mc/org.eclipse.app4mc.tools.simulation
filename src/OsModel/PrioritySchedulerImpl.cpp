/**
 ********************************************************************************
 * Copyright (c) 2022 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 * - Robert Bosch GmbH
 ********************************************************************************
 */

#include <memory>
#include <algorithm>
#include "PrioritySchedulerImpl.h"
#include "Schedulable.h"
#include "ProcessingUnitImpl.h"
#include "StimulusImpl.h"
#include "Common.h"
#include "EasyloggingMacros.h"

void PrioritySchedulerImpl::addTaskMapping(std::shared_ptr<Task> task) {
    if(!task->getTaskAllocation().isParameterSet("priority")){
        throw std::runtime_error("Task \""+task->getName()+"\" uses Priority Scheduler but has no Priority set");
    }
    Scheduler::addTaskMapping(task);
}
void PrioritySchedulerImpl::addTaskMapping(std::shared_ptr<Scheduler> scheduler) {
    if(!scheduler->getTaskAllocation().isParameterSet("priority")){
        throw std::runtime_error("Task \""+scheduler->getName()+"\" uses Priority Scheduler but has no Priority set");
    }
    Scheduler::addTaskMapping(scheduler);
}

void PrioritySchedulerImpl::addResponsibleCore(std::shared_ptr<ProcessingUnitImpl> pu) {
    if(!vectorContains(coresResponsible,pu)){
        Scheduler::addResponsibleCore(pu);
        schedule[pu].reset();
    }
}

std::shared_ptr<Schedulable> PrioritySchedulerImpl::chooseTask(const std::shared_ptr<ProcessingUnitImpl> &pu) {
    auto chosenTask = schedule[pu];
    if(runningTasks.count(pu)>0 && runningTasks[pu] == chosenTask){
        printStateUpdateLog(chosenTask, "continue", pu);
    }
    return chosenTask;
}

void PrioritySchedulerImpl::taskActivate(const std::shared_ptr<Schedulable> &task, const StimulusImpl &stimulus) {
    printStateUpdateLog(task, "activated");
    Scheduler::taskActivate(task,stimulus);
    enqueueTask(task);
    interruptPUs();
} 

void PrioritySchedulerImpl::taskStart(const std::shared_ptr<Schedulable> &task, const std::shared_ptr<ProcessingUnitImpl> &pu) {
    printStateUpdateLog(task, "start", pu);
    Scheduler::taskStart(task,pu);
}

void PrioritySchedulerImpl::taskPreemt(const std::shared_ptr<Schedulable> &task, const std::shared_ptr<ProcessingUnitImpl> &pu) {
    printStateUpdateLog(task, "preempt", pu);
    Scheduler::taskPreemt(task,pu);
}

void PrioritySchedulerImpl::taskResume(const std::shared_ptr<Schedulable> &task,  const std::shared_ptr<ProcessingUnitImpl> &pu) {
    printStateUpdateLog(task, "resume", pu);
    Scheduler::taskResume(task,pu);
}

void PrioritySchedulerImpl::taskTerminate(const std::shared_ptr<Schedulable> &task) {
    printStateUpdateLog(task, "terminate");
    Scheduler::taskTerminate(task);
    dequeueTask(task);
}

void PrioritySchedulerImpl::enqueueTask(const std::shared_ptr<Schedulable> &task){
    vectorRemove(readyQueue, task);
    // get an iterator itr to the first location in the readyQueue for which priority(*itr) < priority(task)
    auto itr = std::lower_bound(readyQueue.begin(), readyQueue.end(), task, comparePrio);
    readyQueue.insert(itr, task);
    updateSchedule();
}   

void PrioritySchedulerImpl::dequeueTask(const std::shared_ptr<Schedulable> &task){
    vectorRemove(readyQueue, task);
    updateSchedule();
}

void PrioritySchedulerImpl::updateSchedule(){   
    //log the state of readyQueue
    printReadyQueueLog();

    int schedSize = std::min(coresResponsible.size(), readyQueue.size());
    std::vector<std::shared_ptr<Schedulable>> schedVect(readyQueue.begin(), readyQueue.begin() + schedSize);
    std::vector<std::shared_ptr<ProcessingUnitImpl>> unassignedCores;

    auto schedVectEnd = schedVect.end();
    for(const auto &pu: coresResponsible) {
        if(runningTasks.count(pu) && vectorContains(schedVect, runningTasks[pu])){
            // the task currently running on the core is part of the new schedule too
            
            // Note: explicit schedule assignment is necessary to make sure if multiple scheduleUpdate calls
            // take place at the same sim time, schedule changes in one call which become deprecated in the 
            // next one will be overwritten   
            schedule[pu] = runningTasks[pu];
            schedVectEnd = std::remove(schedVect.begin(), schedVectEnd, runningTasks[pu]);
        } else {
            unassignedCores.push_back(pu);
        }
    }

    // update the schedule of unassigned cores
    auto itr = schedVect.begin();
    for(const auto &pu : unassignedCores){
        if(itr != schedVectEnd){
            schedule[pu] = *itr;
            itr++;
        } else {
            schedule[pu].reset();
        }
    }

    // log the updated schedule
    printScheduleLog();
}

bool PrioritySchedulerImpl::comparePrio(const std::shared_ptr<Schedulable> &a, const std::shared_ptr<Schedulable> &b){
    return a->getTaskAllocation().getSchedulingParameter<ParameterType::Integer, false>(std::string("priority"))
            >= b->getTaskAllocation().getSchedulingParameter<ParameterType::Integer, false>("priority");
}

void PrioritySchedulerImpl::printLog(const std::string& msg) const {
    VLOG_TIME(2) << "[" + this->getName() + "] | " << msg;
}

void PrioritySchedulerImpl::printScheduleLog(){
    std::stringstream buff;
    buff << "schedule updated: ";
    for(const auto &pu : coresResponsible){
        buff << pu->getName() << ":" << ((schedule[pu]) ? (schedule[pu])->getName() : "()") << ", ";
    }
    buff << "\b\b";
    printLog(buff.str());
}

void PrioritySchedulerImpl::printReadyQueueLog() const {
    std::stringstream buff;
    buff << "readyQueue updated: [";
    for(const auto &t: readyQueue){
        buff << t->getName() << ", ";
    } 
    buff << (readyQueue.empty()? " ]" : "\b\b]");
    printLog(buff.str());
}

void PrioritySchedulerImpl::printStateUpdateLog(const std::shared_ptr<Schedulable> &task, const std::string &_state, const std::shared_ptr<ProcessingUnitImpl> &pu) const {
    std::stringstream buff;
    buff << _state << " " << task->getName();
    if(pu != NULL) {
        buff << " on " << pu->getName();
    }
    printLog(buff.str());
}