/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Behnaz Pourmohseni <behnaz.pourmohseni@de.bosch.com>
 ********************************************************************************
 */

#include "PosixRealtimeSchedulerImpl.h"
#include "Schedulable.h"
#include "Common.h"
#include "PrioritySchedulerImpl.h"
#include "PriorityRoundRobinScheduler.h"
#include "ProcessingUnitImpl.h"
#include "SchedulingParameterHelper.h"

using POLICY = PosixRealtimeSchedulerImpl::PosixSchedPolicy;


std::string PosixRealtimeSchedulerImpl::getSchedulingPolicyName(const POLICY &policy) const {
    switch(policy){
        case PosixSchedPolicy::SCHED_POLICY_FIFO:
            return "SCHED_POLICY_FIFO";
        case PosixSchedPolicy::SCHED_POLICY_RR:
            return "SCHED_POLICY_RR";
        default:
            throw std::runtime_error("scheduling policy has no defined name");
    }
}

POLICY PosixRealtimeSchedulerImpl::getSchedulingPolicy(const std::string &policyName) const {
    if(getSchedulingPolicyName(POLICY::SCHED_POLICY_FIFO)== policyName){
        return POLICY::SCHED_POLICY_FIFO;
    }
    if(getSchedulingPolicyName(POLICY::SCHED_POLICY_RR)== policyName){
        return POLICY::SCHED_POLICY_RR;
    }
    throw std::runtime_error("unknown scheduling policy name: " + policyName);
}

POLICY PosixRealtimeSchedulerImpl::getSchedulingPolicy(const std::shared_ptr<Schedulable> &task) const {
    if(task->getTaskAllocation().isParameterSet(SchedulingParameterHelper::PolicyKey())){
        return getSchedulingPolicy(task->getTaskAllocation().getSchedulingParameter<ParameterType::String, false>(SchedulingParameterHelper::PolicyKey()));
    }
    return POLICY::SCHED_POLICY_FIFO;
}

std::shared_ptr<Schedulable> PosixRealtimeSchedulerImpl::chooseTask(const std::shared_ptr<ProcessingUnitImpl> &pu) {
    auto chosenTask = schedule[pu];
    if(chosenTask){
        switch(getSchedulingPolicy(chosenTask)){
        case PosixSchedPolicy::SCHED_POLICY_FIFO:
            return PrioritySchedulerImpl::chooseTask(pu);
        case PosixSchedPolicy::SCHED_POLICY_RR:
            return PriorityRoundRobinSchedulerImpl::chooseTask(pu);
        default:
            throw std::runtime_error("scheduling policy not supported");
        }
    }
    return chosenTask;
}

void PosixRealtimeSchedulerImpl::taskStart(const std::shared_ptr<Schedulable> &task, const std::shared_ptr<ProcessingUnitImpl> &pu) {
    switch(getSchedulingPolicy(task)){
        case PosixSchedPolicy::SCHED_POLICY_FIFO:
            return PrioritySchedulerImpl::taskStart(task,pu);
        case PosixSchedPolicy::SCHED_POLICY_RR:
            return PriorityRoundRobinSchedulerImpl::taskStart(task,pu);
        default:
            throw std::runtime_error("scheduling policy not supported");
    }
}

void PosixRealtimeSchedulerImpl::taskPreemt(const std::shared_ptr<Schedulable> &task, const std::shared_ptr<ProcessingUnitImpl> &pu) {
    switch(getSchedulingPolicy(task)){
        case PosixSchedPolicy::SCHED_POLICY_FIFO:
            return PrioritySchedulerImpl::taskPreemt(task,pu);
        case PosixSchedPolicy::SCHED_POLICY_RR:
            return PriorityRoundRobinSchedulerImpl::taskPreemt(task,pu);
        default:
            throw std::runtime_error("scheduling policy not supported");
    }
}

void PosixRealtimeSchedulerImpl::taskResume(const std::shared_ptr<Schedulable> &task,  const std::shared_ptr<ProcessingUnitImpl> &pu) {
    switch(getSchedulingPolicy(task)){
        case PosixSchedPolicy::SCHED_POLICY_FIFO:
            return PrioritySchedulerImpl::taskResume(task,pu);
        case PosixSchedPolicy::SCHED_POLICY_RR:
            return PriorityRoundRobinSchedulerImpl::taskResume(task,pu);
        default:
            throw std::runtime_error("scheduling policy not supported");
    }
}

void PosixRealtimeSchedulerImpl::taskTerminate(const std::shared_ptr<Schedulable> &task) {
    switch(getSchedulingPolicy(task)){
        case PosixSchedPolicy::SCHED_POLICY_FIFO:
            return PrioritySchedulerImpl::taskTerminate(task);
        case PosixSchedPolicy::SCHED_POLICY_RR: 
            return PriorityRoundRobinSchedulerImpl::taskTerminate(task);
        default:
            throw std::runtime_error("scheduling policy not supported");
    }
}