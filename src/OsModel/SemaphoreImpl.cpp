/**
 ********************************************************************************
 * Copyright (c) 2021 University of Rostock and others
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */

#include "SemaphoreImpl.h"
#include "EasyloggingMacros.h"

void SemaphoreImpl::release() {
  ++currentState;
  if (currentState > maxValue) {
	VLOG(0) << "value of semaphore exeeded maxValue (" + std::to_string(maxValue) + "), current state is hold at maxValue";
	currentState = maxValue;
  }
  if (currentState > 0) {
	wakeup->notify(sc_core::SC_ZERO_TIME);
  }
}

bool SemaphoreImpl::activeRequest(const sc_core::sc_event &interruptEvent) {
  while (currentState <= 0 && !interruptEvent.triggered()) {
	sc_core::wait(*wakeup | interruptEvent);
  }

  // if we got interrupted, we might also got in parallel the semaphore released
  // therfore check here again the state
  if (currentState > 0) {
	--currentState;
	return true;
  }
  return false;
}
