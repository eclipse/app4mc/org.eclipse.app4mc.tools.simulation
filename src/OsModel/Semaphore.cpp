/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */


#include "Semaphore.h"
#include "SemaphoreImpl.h"

Semaphore::Semaphore(const std::string& name, EInt maxValue , EInt initialValue, SemaphoreType type) :
  WrappedType<SemaphoreImpl>(name, maxValue, initialValue, type) {}

Semaphore::Semaphore(const std::shared_ptr<SemaphoreImpl>& _inst):
    WrappedType<SemaphoreImpl>(_inst){}
