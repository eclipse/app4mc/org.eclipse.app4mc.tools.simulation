// /**
//  ********************************************************************************
//  * Copyright (c) 2022 Robert Bosch GmbH
//  * 
//  * This program and the accompanying materials are made
//  * available under the terms of the Eclipse Public License 2.0
//  * which is available at https://www.eclipse.org/legal/epl-2.0/
//  * 
//  * SPDX-License-Identifier: EPL-2.0
//  * 
//  * Contributors:
//  * - Robert Bosch GmbH - initial contribution
//  *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
//  ********************************************************************************
//  */
#include "PriorityRoundRobinScheduler.h"
#include "PriorityRoundRobinSchedulerImpl.h"
#include "Scheduler.h"

PriorityRoundRobinScheduler::PriorityRoundRobinScheduler(std::string name):
     WrappedType<PriorityRoundRobinSchedulerImpl>(name){}

PriorityRoundRobinScheduler::PriorityRoundRobinScheduler(const std::shared_ptr<PriorityRoundRobinSchedulerImpl>& _inst):
    WrappedType<PriorityRoundRobinSchedulerImpl>(_inst){}

std::shared_ptr<Scheduler> PriorityRoundRobinScheduler::getImplementationInstance() const {
    return internalInst();
}