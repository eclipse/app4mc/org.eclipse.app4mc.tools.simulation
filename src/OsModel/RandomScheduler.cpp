// /**
//  ********************************************************************************
//  * Copyright (c) 2022 Robert Bosch GmbH
//  * 
//  * This program and the accompanying materials are made
//  * available under the terms of the Eclipse Public License 2.0
//  * which is available at https://www.eclipse.org/legal/epl-2.0/
//  * 
//  * SPDX-License-Identifier: EPL-2.0
//  * 
//  * Contributors:
//  * - Robert Bosch GmbH - initial contribution
//  *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
//  ********************************************************************************
//  */
#include "RandomScheduler.h"
#include "RandomSchedulerImpl.h"
#include "Schedulable.h"
#include "ProcessingUnit.h"
#include "TaskAllocation.h"
#include <sysc/kernel/sc_time.h>

RandomScheduler::RandomScheduler(std::string name):
     WrappedType<RandomSchedulerImpl>(name){}

RandomScheduler::RandomScheduler(const std::shared_ptr<RandomSchedulerImpl>& _inst):
    WrappedType<RandomSchedulerImpl>(_inst){}
	
std::shared_ptr<Scheduler> RandomScheduler::getImplementationInstance() const {
    return internalInst();
}