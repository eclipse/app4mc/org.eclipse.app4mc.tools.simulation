/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Behnaz Pourmohseni <behnaz.pourmohseni@de.bosch.com>
 ********************************************************************************
 */

#include "PriorityRoundRobinSchedulerImpl.h"
#include <algorithm>
#include "Common.h"
#include "Schedulable.h"
#include "HardwareModel.h"
#include "TimeUtilsSystemC.h"
#include "TracerApi.h"
#include "TraceManager.h"
#include "ProcessingUnitImpl.h"
#include "SchedulingParameterHelper.h"

sc_core::sc_time PriorityRoundRobinSchedulerImpl::getTimeSliceLength(){
    return convertTime(getAlgorithmParameter<ParameterType::Time, false>(SchedulingParameterHelper::RoundRobinTimesliceLengthKey()));
}

void PriorityRoundRobinSchedulerImpl::addResponsibleCore(std::shared_ptr<ProcessingUnitImpl> pu) {
    if(!vectorContains(coresResponsible,pu)){
        PrioritySchedulerImpl::addResponsibleCore(pu);
        // timer event 
        auto evName = getName() + (std::string)"_TimerTimeoutEvent_" + pu->getName();
        auto exprEv = std::make_shared<sc_core::sc_event>(evName.c_str());
        exprEv->cancel();
        timerTimeoutEvents[pu] = exprEv;
        timerExpired[pu] = false;

        // setting up a timer thread
        auto timerName = getName() + "_Timer_" + pu->getName();
        auto timerOpts = std::make_shared<sc_core::sc_spawn_options>();
        timerOpts->set_sensitivity(exprEv.get());
        timerOpts->spawn_method();
        timerOpts->dont_initialize();
        sc_core::sc_spawn( sc_bind(&PriorityRoundRobinSchedulerImpl::PuTimerTimeoutService, this, pu), timerName.c_str(), &*timerOpts);
        timerOptions[pu] = timerOpts;
    }
}

void PriorityRoundRobinSchedulerImpl::PuTimerTimeoutService(const std::shared_ptr<ProcessingUnitImpl> &pu){ 
    printLog("timer expired on " + pu->getName());
    TraceManager::Inst().comment("Timer expired: " + (std::string)timerTimeoutEvents[pu]->name());
    
    if(runningTasks.count(pu)) {
        timerExpired[pu] = true;
        enqueueTask(runningTasks[pu]);
        pu->interrupt();
    } else {
        throw std::runtime_error("no task is running on the core");
    }
}

std::shared_ptr<Schedulable> PriorityRoundRobinSchedulerImpl::chooseTask(const std::shared_ptr<ProcessingUnitImpl> &pu) {
    auto chosenTask = schedule[pu];
    if(runningTasks.count(pu)>0 && runningTasks[pu] == chosenTask){
        if(timerExpired[pu]){
            // execute the same task for the next timeslice
            usedSlicePortion[chosenTask] = sc_core::SC_ZERO_TIME;
            lastPuAssignTime[chosenTask] = sc_core::sc_time_stamp();
            setTimer(pu, getTimeSliceLength());
            timerExpired[pu] =false;
        } 
        printStateUpdateLog(chosenTask, "continue", pu);
    }
    return chosenTask;
}

void PriorityRoundRobinSchedulerImpl::taskStart(const std::shared_ptr<Schedulable> &task, const std::shared_ptr<ProcessingUnitImpl> &pu) {
    PrioritySchedulerImpl::taskStart(task,pu);
    lastPuAssignTime[task] = sc_core::sc_time_stamp();
    usedSlicePortion[task] = sc_core::SC_ZERO_TIME;
    setTimer(pu, getTimeSliceLength());
}

void PriorityRoundRobinSchedulerImpl::taskPreemt(const std::shared_ptr<Schedulable> &task, const std::shared_ptr<ProcessingUnitImpl> &pu) {
    PrioritySchedulerImpl::taskPreemt(task,pu);
    if(timerExpired[pu]){
        usedSlicePortion[task] = sc_core::SC_ZERO_TIME;
        timerExpired[pu] =false;
    } else {
        usedSlicePortion[task] += sc_core::sc_time_stamp() - lastPuAssignTime[task];
    }
    cancelTimer(pu);
}

void PriorityRoundRobinSchedulerImpl::taskResume(const std::shared_ptr<Schedulable> &task,  const std::shared_ptr<ProcessingUnitImpl> &pu) {
    PrioritySchedulerImpl::taskResume(task,pu);
    lastPuAssignTime[task] = sc_core::sc_time_stamp();
    setTimer(pu, getTimeSliceLength() - usedSlicePortion[task]);
}

void PriorityRoundRobinSchedulerImpl::taskTerminate(const std::shared_ptr<Schedulable> &task) {
    auto it = std::find_if(schedule.begin(), schedule.end(), [&task] (const std::pair<std::shared_ptr<ProcessingUnitImpl>,std::shared_ptr<Schedulable>>& entry) {
    return entry.second == task;
    });
    auto pu = it->first;
    PrioritySchedulerImpl::taskTerminate(task);
    cancelTimer(pu);
    usedSlicePortion.erase(task);
    lastPuAssignTime.erase(task);
}

void PriorityRoundRobinSchedulerImpl::updateSchedule(){   
    PrioritySchedulerImpl::updateSchedule();
    for(const auto &pu : coresResponsible){
        if(!schedule[pu]){
            cancelTimer(pu);
        }
    }
}

void PriorityRoundRobinSchedulerImpl::cancelTimer(const std::shared_ptr<ProcessingUnitImpl> &pu){
    timerTimeoutEvents[pu]->cancel();
}

void PriorityRoundRobinSchedulerImpl::setTimer(const std::shared_ptr<ProcessingUnitImpl> &pu, const sc_core::sc_time time){
    timerTimeoutEvents[pu]->notify(time);
}