/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 * - University of Rostock - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */
#include "RandomSchedulerImpl.h"
#include "ProcessingUnitImpl.h"
#include "effolkronium/random.hpp"
#include <pcg_random.hpp>

using app4mcsim_rand = effolkronium::basic_random_static<pcg32, app4mcsim_seed>;

std::shared_ptr<Schedulable> RandomSchedulerImpl::chooseTask(const std::shared_ptr<ProcessingUnitImpl> &requestingPu) {
		
		//we need to choose a task out of 3 different sets. Since a union every time would be expensive, we roll the dice multiple times.

		//get the current running task with uniform prob. (not totally right, but doesn't matter)
		bool useActiveTask = app4mcsim_rand::get(1.0/mappedTasks.size());
		if(useActiveTask && runningTasks.count(requestingPu) > 0){
			return runningTasks[requestingPu];
		}
		bool useReadyTasks = app4mcsim_rand::get(0.5);
		if (useReadyTasks){
			auto newTask = app4mcsim_rand::get(readyTasks.begin(),readyTasks.end());
			if(newTask != readyTasks.end())
				return *newTask;
		}
		
		auto newTask = app4mcsim_rand::get(activeTasks.begin(),activeTasks.end());
		if(newTask != activeTasks.end())
			return *newTask;
		else
			return nullptr;
	}
