// /**
//  ********************************************************************************
//  * Copyright (c) 2022 Robert Bosch GmbH
//  * 
//  * This program and the accompanying materials are made
//  * available under the terms of the Eclipse Public License 2.0
//  * which is available at https://www.eclipse.org/legal/epl-2.0/
//  * 
//  * SPDX-License-Identifier: EPL-2.0
//  * 
//  * Contributors:
//  * - Robert Bosch GmbH - initial contribution
//  *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
//  ********************************************************************************
//  */
#include "PriorityScheduler.h"
#include "PrioritySchedulerImpl.h"
#include "Scheduler.h"

PriorityScheduler::PriorityScheduler(std::string name):
     WrappedType<PrioritySchedulerImpl>(name){}

PriorityScheduler::PriorityScheduler(const std::shared_ptr<PrioritySchedulerImpl>& _inst):
    WrappedType<PrioritySchedulerImpl>(_inst){}

std::shared_ptr<Scheduler> PriorityScheduler::getImplementationInstance() const {
    return internalInst();
}