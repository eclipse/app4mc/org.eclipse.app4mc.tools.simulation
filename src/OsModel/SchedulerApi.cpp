// /**
//  ********************************************************************************
//  * Copyright (c) 2022 Robert Bosch GmbH
//  * 
//  * This program and the accompanying materials are made
//  * available under the terms of the Eclipse Public License 2.0
//  * which is available at https://www.eclipse.org/legal/epl-2.0/
//  * 
//  * SPDX-License-Identifier: EPL-2.0
//  * 
//  * Contributors:
//  * - Robert Bosch GmbH - initial contribution
//  *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
//  ********************************************************************************
//  */
#include "SchedulerApi.h"
#include "Scheduler.h"
#include "Task.h"
#include "Schedulable.h"
#include "ProcessingUnit.h"
#include "TaskAllocation.h"
#include <sysc/kernel/sc_time.h>
#include <memory>


void SchedulerApi::setExecutionCore(std::shared_ptr<ProcessingUnit> pu){
    getImplementationInstance()->setExecutionCore(pu->internalInst());
}

void SchedulerApi::addResponsibleCore(std::shared_ptr<ProcessingUnit> pu){
    getImplementationInstance()->addResponsibleCore(pu->internalInst());
}

void SchedulerApi::addTaskMapping(const std::shared_ptr<Task>& task){
    getImplementationInstance()->addTaskMapping(task);
}
void SchedulerApi::addTaskMapping(const std::shared_ptr<SchedulerApi>& scheduler){
    getImplementationInstance()->addTaskMapping(scheduler->getImplementationInstance());
}

/*************************************************************
 Schedulable properties
**************************************************************/
void SchedulerApi::setTaskAllocation(const TaskAllocation& newAllocation){
    getImplementationInstance()->setTaskAllocation(newAllocation);
}

const TaskAllocation& SchedulerApi::getTaskAllocation() const {
    return getImplementationInstance()->getTaskAllocation();
}

/*************************************************************
  Scheduling parameter interface implementation & specialization
**************************************************************/
void SchedulerApi::setAlgorithmSchedulingParameter(const SchedulingParameter& parameters)
{
    getImplementationInstance()->setAlgorithmSchedulingParameter(parameters);
}

void SchedulerApi::addAlgorithmSchedulingParameter(const SchedulingParameter& parameters)
{
    getImplementationInstance()->addAlgorithmSchedulingParameter(parameters);
}

void SchedulerApi::setAlgorithmSchedulingParameter(const std::string& name, const ParameterValue& value)
{
    getImplementationInstance()->setAlgorithmSchedulingParameter(name, value);
}

template<ParameterType type, bool isManyType>
std::variant_alternative_t<getTypeIndex(type, isManyType), ParameterValue> SchedulerApi::getAlgorithmParameter(const std::string& name)
{
    return getImplementationInstance()->getAlgorithmParameter<type, isManyType>(name);
}

//non many types
template std::variant_alternative_t<getTypeIndex(ParameterType::Integer, false), ParameterValue> SchedulerApi::getAlgorithmParameter<ParameterType::Integer, false>(const std::string& name);
template std::variant_alternative_t<getTypeIndex(ParameterType::Unsigned, false), ParameterValue> SchedulerApi::getAlgorithmParameter<ParameterType::Unsigned, false>(const std::string& name);
template std::variant_alternative_t<getTypeIndex(ParameterType::Float, false), ParameterValue> SchedulerApi::getAlgorithmParameter<ParameterType::Float, false>(const std::string& name);
template std::variant_alternative_t<getTypeIndex(ParameterType::Bool, false), ParameterValue> SchedulerApi::getAlgorithmParameter<ParameterType::Bool, false>(const std::string& name);
template std::variant_alternative_t<getTypeIndex(ParameterType::String, false), ParameterValue> SchedulerApi::getAlgorithmParameter<ParameterType::String, false>(const std::string& name);
template std::variant_alternative_t<getTypeIndex(ParameterType::Time, false), ParameterValue> SchedulerApi::getAlgorithmParameter<ParameterType::Time, false>(const std::string& name);
//many types (-> std::vector)
template std::variant_alternative_t<getTypeIndex(ParameterType::Integer, true), ParameterValue> SchedulerApi::getAlgorithmParameter<ParameterType::Integer, true>(const std::string& name);
template std::variant_alternative_t<getTypeIndex(ParameterType::Unsigned, true), ParameterValue> SchedulerApi::getAlgorithmParameter<ParameterType::Unsigned, true>(const std::string& name);
template std::variant_alternative_t<getTypeIndex(ParameterType::Float, true), ParameterValue> SchedulerApi::getAlgorithmParameter<ParameterType::Float, true>(const std::string& name);
template std::variant_alternative_t<getTypeIndex(ParameterType::Bool, true), ParameterValue> SchedulerApi::getAlgorithmParameter<ParameterType::Bool, true>(const std::string& name);
template std::variant_alternative_t<getTypeIndex(ParameterType::String, true), ParameterValue> SchedulerApi::getAlgorithmParameter<ParameterType::String, true>(const std::string& name);
template std::variant_alternative_t<getTypeIndex(ParameterType::Time, true), ParameterValue> SchedulerApi::getAlgorithmParameter<ParameterType::Time, true>(const std::string& name);
