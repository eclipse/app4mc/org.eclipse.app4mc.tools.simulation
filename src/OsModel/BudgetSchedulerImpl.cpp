/**
 ********************************************************************************
 * Copyright (c) 2020 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */

#include "BudgetSchedulerImpl.h"
#include <sysc/kernel/sc_dynamic_processes.h>

//TODO use SchedulingParameters to express budget, this is old broken code (not working with real AMALTHEA models anyways)
/*the parameter budget is a easy example, which is the percentage of budget per updateCycle*/
BudgetSchedulerImpl::BudgetSchedulerImpl(std::string name,const std::map<std::shared_ptr<Schedulable>,double> &budget, const sc_core::sc_time &updateCycle) : 
    Scheduler(std::move(name)), budgetPercent(budget), budgetUpdateCycle(updateCycle){     
    timerOptions.set_sensitivity(&timerResetEvent);
    timerOptions.spawn_method();
    sc_core::sc_spawn( sc_bind(&BudgetSchedulerImpl::timerFunction, this), "budgetSchedulerTimer", &timerOptions );

    updateOptions.spawn_method();
    sc_core::sc_spawn( sc_bind(&BudgetSchedulerImpl::updateBudget, this), "updateBudget", &updateOptions );
}


void BudgetSchedulerImpl::updateBudget(){
    for(auto& taskBudget : budget){
        auto newBudget = budgetPercent[taskBudget.first]*budgetUpdateCycle;
        if(overBudget[taskBudget.first] > sc_core::SC_ZERO_TIME){
            if(overBudget[taskBudget.first] > newBudget){
                overBudget[taskBudget.first] -= newBudget;
                newBudget = sc_core::SC_ZERO_TIME;
            }else{
                newBudget -=overBudget[taskBudget.first];
                overBudget[taskBudget.first] = sc_core::SC_ZERO_TIME;
            }
            
        }
        taskBudget.second += newBudget;
    }
    timerResetEvent.notify(sc_core::SC_ZERO_TIME);
    next_trigger(budgetUpdateCycle);
}

void BudgetSchedulerImpl::timerFunction(){   
    //this means the timer elapsed or was externally triggered
    interruptPUs();
    wakeupPUs();
    auto now = sc_core::sc_time_stamp();
    //update budgets of running tasks
    for(auto &task : budget){
        for(auto &puTask : runningTasks){
            if(puTask.second == task.first){
            if(lastStart[task.first] > now)
                    throw std::runtime_error("bookkeeping for lastStart wrong");
                auto runtime = now - lastStart[task.first];
                // if runtime is longer than budget (e.g. memory access), we need to cap the budget to 0.
                // (negative sc_time is not possible)
                if(runtime > task.second){
                    overBudget[task.first] = runtime - task.second;
                    runtime = task.second;
                }
                task.second -= runtime;
                lastStart[task.first] = now;
            } 
        }
    }
    nextSchedulerDecision.clear();
    //initialize to next budgetUpdate, if no task is running
    currentTimeout = budgetUpdateCycle;
    // really dirty way of finding the task with biggest budget for all cores
    for([[maybe_unused]] auto& core : coresResponsible){
        std::shared_ptr<Schedulable> nextMax;
        //get new scheduling decision
        for(auto &task : budget){
            if(nextSchedulerDecision.count(task.first) == 0 && (!nextMax || budget[nextMax] < budget[task.first]))
                nextMax = task.first;
        }
        if(nextMax){
            nextSchedulerDecision.insert(nextMax);
            // if the budget is very small, this typically mean it would be zero, 
            // which mean, there is/are only task(s) with zero budget, so let them run
            if(budget[nextMax] > sc_core::sc_time(10,sc_core::SC_NS))
                currentTimeout = budget[nextMax];
        }
        
    }
    next_trigger(currentTimeout,timerResetEvent);
}

std::shared_ptr<Schedulable> BudgetSchedulerImpl::chooseTask(const std::shared_ptr<ProcessingUnitImpl> &requestingPu) /*override*/{
    //chose old task, if it is in current scheduling decision
    if(runningTasks.count(requestingPu) > 0 && (nextSchedulerDecision.count(runningTasks[requestingPu])>0)){
        return runningTasks[requestingPu];
    }
    //find any executable task
    for(auto &task : activeTasks){
        if(nextSchedulerDecision.count(task)){
            return task;
        }
    }
    for(auto &task : readyTasks){
        if(nextSchedulerDecision.count(task)){
            return task;
        }
    }
    //return empty task
    return {};
}

void BudgetSchedulerImpl::taskTerminate(const std::shared_ptr<Schedulable> &task) /*override*/ {
    Scheduler::taskTerminate(task);
    budget.erase(task);
    overBudget.erase(task);
    timerResetEvent.notify(sc_core::SC_ZERO_TIME);

} 

void BudgetSchedulerImpl::taskStart(const std::shared_ptr<Schedulable> &task, const std::shared_ptr<ProcessingUnitImpl> &pu) /*override*/ {
    Scheduler::taskStart(task,pu);
    lastStart[task] = sc_core::sc_time_stamp();

}

void BudgetSchedulerImpl::taskResume(const std::shared_ptr<Schedulable> &task,  const std::shared_ptr<ProcessingUnitImpl> &pu) /*override*/ {
    Scheduler::taskResume(task,pu);
    lastStart[task] = sc_core::sc_time_stamp();
}

void BudgetSchedulerImpl::taskActivate(const std::shared_ptr<Schedulable> &task, const StimulusImpl &stimulus) /*override*/ {
    Scheduler::taskActivate(task,stimulus);
    budget[task] = budgetPercent[task]*budgetUpdateCycle;
    overBudget[task] = sc_core::SC_ZERO_TIME;
    timerResetEvent.notify(sc_core::SC_ZERO_TIME);
    lastStart[task] = sc_core::sc_time_stamp();
}
