/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#include <systemc>
#include <tlm>
#include "Common.h"
#include "Scheduler.h"
#include "Task.h"
#include "ProcessingUnitImpl.h"
#include <limits>
#include <stdexcept>
#include "TracerApi.h"
#include "TraceManager.h"
#include "EasyloggingMacros.h"
#include "StimulusImpl.h"
#include "ExecutionItem.h"

Scheduler::Scheduler(const std::string &name) : Schedulable(name), mapHint(runningTasks.begin()){}

std::optional<const std::shared_ptr<ProcessingUnitImpl>> Scheduler::removeTaskFromRunningTasks(const std::shared_ptr<Schedulable> &task) {
    /*try mapHint*/
    if (mapHint != runningTasks.end() && mapHint->second == task) {
    auto pu = mapHint->first;
    mapHint = runningTasks.erase(mapHint);
    return pu;
    }

    // this map uses pu as key, therefore need linear search
    for (auto &i : runningTasks) {
    auto [pu, putask] = i;
    if (putask == task) {
        runningTasks.erase(pu);
        return pu;
    }
    }
    return std::nullopt;
}

void Scheduler::executingCoreIsSet(){
    if (!executingCore){
        std::ostringstream msg;
        msg<<"Scheduler "<<this->getName()<<" is not allocated to an executing core. Check mapping model."<<std::endl;
        VLOG(0)<<msg.str();
        throw std::runtime_error(msg.str());		
    }
}

void Scheduler::addTaskMapping(std::shared_ptr<Task> task) {
    task->setScheduler(shared_from_this());
    mappedTasks.insert(task);
}

void Scheduler::addTaskMapping(std::shared_ptr<Scheduler> childScheduler) {
   
    if(!childScheduler->parentScheduler){
        childScheduler->parentScheduler = shared_from_this();
    }
    else{
        throw std::runtime_error("Scheduler "+childScheduler->getName()+ " has already a parent scheduler set");
    }
    mappedTasks.insert(childScheduler);
}

void Scheduler::wakeupPUs() {
    for(auto& core : coresResponsible){
        core->wakeUp();
    }
}

void Scheduler::interruptPUs() 
{
    for(auto& core : coresResponsible){
        core->interrupt();
    }
}

void Scheduler::addResponsibleCore(std::shared_ptr<ProcessingUnitImpl> pu) {

    if(!vectorContains(coresResponsible,pu)){
        coresResponsible.emplace_back(pu);
        pu->setScheduler(shared_from_this());
    }
}

void Scheduler::setExecutionCore(std::shared_ptr<ProcessingUnitImpl> pu)
{
    addResponsibleCore(pu);
    executingCore = pu;
}

void Scheduler::runnableStarted(const std::shared_ptr<Task> &task, const Runnable &runnable) 
{
    TraceManager::Inst().runnableStart(runnable, *task, *this);
}

void Scheduler::runnableCompleted(const std::shared_ptr<Task> &task, const Runnable &runnable) 
{
    TraceManager::Inst().runnableTerminate(runnable, *task, *this);
}

void Scheduler::taskActivate(const std::shared_ptr<Schedulable> &task, const StimulusImpl &stimulus) 
{
    activeTasks.emplace(task);
    task->activate(stimulus);
    //if something new is put into ready wake up pu
    wakeupPUs();
    if(parentScheduler){
        parentScheduler->wakeupPUs();
        if (getState() == SchedulableState::suspended) {
          parentScheduler->taskActivate(shared_from_this(), stimulus);
        }
    }
}

void Scheduler::taskStart(const std::shared_ptr<Schedulable> &task, const std::shared_ptr<ProcessingUnitImpl> &pu) 
{
    runningTasks.insert_or_assign(mapHint,pu,task);
    activeTasks.erase(task);
    task->start(pu);
}

void Scheduler::taskPreemt(const std::shared_ptr<Schedulable> &task,  const std::shared_ptr<ProcessingUnitImpl> &pu) 
{
    auto current = runningTasks.find(pu);
    if (current == runningTasks.end()) {
      std::runtime_error("taskPreemt called on non-running Task");
    }
    mapHint = runningTasks.erase(current);
    readyTasks.emplace(task);
    task->preempt(pu);
}

void Scheduler::taskResume(const std::shared_ptr<Schedulable> &task,  const std::shared_ptr<ProcessingUnitImpl> &pu) 
{
    runningTasks.insert_or_assign(mapHint,pu,task);
    readyTasks.erase(task);
    task->resume(pu);
}

void Scheduler::taskTerminate(const std::shared_ptr<Schedulable> &task) 
{
    auto pu = removeTaskFromRunningTasks(task);
    if(!pu.has_value()){
        throw std::runtime_error("taskTerminate called on non-running Task");
    }
    task->terminate(pu.value());
}



ExecutionItem Scheduler::getNextExecItem(const std::shared_ptr<ProcessingUnitImpl> &pu) {
    auto puPtr =pu;
    auto chosenTask = chooseTask(puPtr);
    while(chosenTask){

    auto lastTaskMapping = runningTasks.find(puPtr);
    mapHint = lastTaskMapping;
    /* preemt last running task if needed*/

    bool taskSchedulable = false;
    /* if there was a task running on this core, but not the current chosen one */
    if(lastTaskMapping != runningTasks.end()){
        if(lastTaskMapping->second != chosenTask){
            /*we need to copy the ptr here, since the iterator may get invalidated*/
            auto lastTaskMappingT = lastTaskMapping->second;
            taskPreemt(lastTaskMappingT,puPtr);
        }
        /*else do nothing, simply use running task for next exec item*/
        taskSchedulable = true;
    }	
    /* if task in active-set (but not ready-set), call start*/
    if(activeTasks.count(chosenTask)>0){
        taskStart(chosenTask, puPtr);
        taskSchedulable = true;
    }

    if(readyTasks.count(chosenTask)>0){
        taskResume(chosenTask, puPtr);
        taskSchedulable = true;
    }
    if(!taskSchedulable)
     {
        throw std::runtime_error("Chosen task of scheduler could not be run");
    }
    auto rv = chosenTask->getNextExecItem(puPtr);

    if(!rv.isEmptyExecItem())
        return rv;
    else
        chosenTask = chooseTask(puPtr);
    } /*end of while*/

    if (parentScheduler) {
      parentScheduler->taskTerminate(shared_from_this());
    }
    return ExecutionItem::getEmptyExecutionItem();
}


void Scheduler::storeInterruptedExecutionItem(ExecutionItem execItem) 
{	
    execItem.currentTask.get().storePreemptedExecutionItem(execItem);
}