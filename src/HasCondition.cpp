/**
 ********************************************************************************
 * Copyright (c) 2021 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 * - Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */


#include "HasCondition.h"
#include <string>

//evaluation
bool HasCondition::checkCondition(){	
	if (condition){
		return condition->checkCond();
	}
	//return true if no condition is given
	return true;
}

std::string HasCondition::conditionNotMetMsg(const std::string& name, const std::string& containerName){
	std::ostringstream buf;
	buf<<"Execution condition of "<<name << " triggered within " << containerName << " not met. Skipping!";
	return buf.str();
}

