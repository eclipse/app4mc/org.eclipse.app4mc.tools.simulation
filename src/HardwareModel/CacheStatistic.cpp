// /**
//  ********************************************************************************
//  * Copyright (c) 2022 Robert Bosch GmbH
//  * 
//  * This program and the accompanying materials are made
//  * available under the terms of the Eclipse Public License 2.0
//  * which is available at https://www.eclipse.org/legal/epl-2.0/
//  * 
//  * SPDX-License-Identifier: EPL-2.0
//  * 
//  * Contributors:
//  * - Robert Bosch GmbH - initial contribution
//  *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
//  ********************************************************************************
//  */
#include "CacheStatistic.h"
#include <list>
#include <memory>
#include <utility>
#include "CacheStatisticImpl.h"
#include "Connection.h"
#include "TimeUtilsSystemC.h"

CacheStatistic::CacheStatistic(const std::string& _name, double _hitRate):
     WrappedType<CacheStatisticImpl>(_name, _hitRate){}

CacheStatistic::CacheStatistic(const std::shared_ptr<CacheStatisticImpl>& _inst):
    WrappedType<CacheStatisticImpl>(_inst){}

template<class T>
void CacheStatistic::setAccessLatency( const T &lat){
    internalInst()->setAccessLatency(lat);
}

void CacheStatistic::setDataRate(const DataRate &rate){
    internalInst()->setDataRate(rate);
}

/* rounds up to next power of 2*/
void CacheStatistic::setLineSize(size_t _lineSize){
    internalInst()->setLineSize(_lineSize);
}

/* round up to next multiple of lineSize which is power of 2*/
void CacheStatistic::setSize(size_t _size){
    internalInst()->setSize(_size);
}

void CacheStatistic::setClockPeriod(const Time& _clock_period){
    internalInst()->setClockPeriod(convertTime(_clock_period));
}

void CacheStatistic::addInitPort(const std::string &name){
    internalInst()->addInitPort(name);
}

void CacheStatistic::addTargetPort(const std::string &name){
    internalInst()->addTargetPort(name);
}


void CacheStatistic::bindConnection(const std::shared_ptr<Connection> &connection, const std::string &portName){
    internalInst()->bindConnection(connection->internalInst(), portName);
}

template void CacheStatistic::setAccessLatency(const DiscreteValueConstant& lat);
template void CacheStatistic::setAccessLatency(const DiscreteValueGaussDistribution& lat);
template void CacheStatistic::setAccessLatency(const DiscreteValueWeibullEstimatorsDistribution& lat);
template void CacheStatistic::setAccessLatency(const DiscreteValueUniformDistribution& lat);
template void CacheStatistic::setAccessLatency(const DiscreteValueBetaDistribution& lat);
template void CacheStatistic::setAccessLatency(const DiscreteValueBoundaries& lat);
