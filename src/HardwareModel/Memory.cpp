// /**
//  ********************************************************************************
//  * Copyright (c) 2022 Robert Bosch GmbH
//  * 
//  * This program and the accompanying materials are made
//  * available under the terms of the Eclipse Public License 2.0
//  * which is available at https://www.eclipse.org/legal/epl-2.0/
//  * 
//  * SPDX-License-Identifier: EPL-2.0
//  * 
//  * Contributors:
//  * - Robert Bosch GmbH - initial contribution
//  *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
//  ********************************************************************************
//  */
#include "Memory.h"
#include <cstdint>
#include <list>
#include <utility>
#include "MemoryImpl.h"
#include "Connection.h"
#include "TimeUtilsSystemC.h"


Memory::Memory(const std::string& _name, uint32_t _size): 
    WrappedType<MemoryImpl>(_name, _size)
{}

Memory::Memory(const std::shared_ptr<MemoryImpl>& _inst):
    WrappedType<MemoryImpl>(_inst)
{}

void Memory::setDataRate(const DataRate &rate){
    internalInst()->setDataRate(rate);
}

//memory element details
template<class T>
void Memory::setAccessLatency( const T &lat){
    internalInst()->setAccessLatency(lat);
}

void Memory::setClockPeriod(const Time& _clock_period){
    internalInst()->setClockPeriod(convertTime(_clock_period));
}

void Memory::addInitPort(const std::string &name){
    internalInst()->addInitPort(name);
}

void Memory::addTargetPort(const std::string &name){
    internalInst()->addTargetPort(name);
}


void Memory::bindConnection(const std::shared_ptr<Connection> &connection, const std::string &portName){
    internalInst()->bindConnection(connection->internalInst(), portName);
}


template void Memory::setAccessLatency(const DiscreteValueConstant& lat);
template void Memory::setAccessLatency(const DiscreteValueGaussDistribution& lat);
template void Memory::setAccessLatency(const DiscreteValueWeibullEstimatorsDistribution& lat);
template void Memory::setAccessLatency(const DiscreteValueUniformDistribution& lat);
template void Memory::setAccessLatency(const DiscreteValueBetaDistribution& lat);
template void Memory::setAccessLatency(const DiscreteValueBoundaries& lat);