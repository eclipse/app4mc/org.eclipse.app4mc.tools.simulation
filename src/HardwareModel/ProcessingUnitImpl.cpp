/**
 ********************************************************************************
 * Copyright (c) 2021 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */

#include "ProcessingUnitImpl.h"
#include "HwAccessPathTLMext.h"
#include "HwModuleBase.h"
#include "Runnable.h"
#include "ConnectionImpl.h"
#include "TracerApi.h"
#include "TraceManager.h"
#include "labelTLMext.h"
#include "MappingModel.h"
#include "SemaphoreImpl.h"
#include <sysc/tracing/sc_trace.h>
#include <stdexcept>
#include "Task.h"
#include "MemoryImpl.h"
#include "WaitEvent.h"
#include "EasyloggingMacros.h"



ProcessingUnitImpl::ProcessingUnitImpl(const std::string& name, const std::shared_ptr<ProcessingUnitDefinition>& definition) : 
	HwModule(name), 
	currentRunnableTrace("idle"), 
	currentTaskTrace("idle"), 
	currentActivityGraphItemType("idle"), 
	definition(definition)
{
	sc_core::sc_get_curr_simcontext()->hierarchy_push(this);
	SC_THREAD(executionLoop);
	sc_core::sc_get_curr_simcontext()->hierarchy_pop();
}

void ProcessingUnitImpl::executionLoop() {

	while (true) {
		if (!scheduler) {
			VLOG_SCMODULE(2) << "missing scheduler, processing unit simulation canceled" << std::endl;
			return;
		}
		ExecutionItem execItem = (scheduler->getNextExecItem)(shared_from_this());
		if (!execItem.isEmptyExecItem()) {
			auto runnableStr = execItem.getExecutionContext().getName();
			auto taskStr =  execItem.getTask().getName();
			auto activityGraphItemStr = execItem.getActivityGraphItem().getDescription();
			VLOG_SCMODULE(2) << "execute new Item: task=" << taskStr << ", runnable=" << runnableStr << ", callgraphitemID=" << activityGraphItemStr << std::endl;
		}

		if (execItem.isEmptyExecItem()) {
			currentRunnableTrace.setID(traceString::getIdleStringID());
			currentActivityGraphItemType.setID(traceString::getIdleStringID());
			currentTaskTrace.setID(traceString::getIdleStringID());
			wait(wakeUpEv);
			continue;
		}
		currentRunnableTrace.setID(execItem.getExecutionContext().nameTrace.getID());
		currentActivityGraphItemType.setID(execItem.getActivityGraphItem().getTraceName());
		currentTaskTrace.setID(execItem.getTask().getTraceID());

		if(!execItem.activityGraphItemWasInterrupted){
	      TraceManager::Inst().activityGraphItemStart(execItem.getExecutionContext(), execItem.getActivityGraphItem());
	    }
		auto ts_before = sc_core::sc_time_stamp();
		auto executionTime = static_cast<double>(execItem.executingCycles) * getClockPeriod();
		wait(executionTime, interruptEv);
		/* calculate time left for execution*/
		if(interruptEv.triggered() && executionTime > sc_core::SC_ZERO_TIME){
			auto executedTime = sc_core::sc_time_stamp() - ts_before;
			auto timeToNextCycle = executedTime % this->getClockPeriod();
			if(timeToNextCycle > sc_core::SC_ZERO_TIME){
				wait(timeToNextCycle);
				executedTime =  sc_core::sc_time_stamp() - ts_before;
			}
			if(executedTime < executionTime){
				// since we already waited until the remainder of getClockPeriod(), we can simply cast
				execItem.executingCycles -= static_cast<cycle_t> (executedTime / this->getClockPeriod());
				scheduler->storeInterruptedExecutionItem(execItem);
				continue;
			}
		}
		if (execItem.memoryTransaction) {
			labelTLMext *labelTLM;
			execItem.memoryTransaction->get_extension(labelTLM);
			sc_core::sc_time delay{};

			for (uint32_t statisticRound = 0; statisticRound < execItem.memoryTransactionStatistic; ++statisticRound) {

				notifyEvents(execItem, ChannelEventStateType::START);

				auto hwAccessElement = hwAccessElems[MappingModel::getMemoryMapping(labelTLM->label)];
				if (hwAccessElement->getAccessPath().empty()) {
					/* rate is defined */
					if (hwAccessElement->dataRate.value != 0)
					{
						delay += hwAccessElement->dataRate.getTransmissionDuration(execItem.memoryTransaction->get_data_length());
					}

					/*read write latency*/
					if (execItem.memoryTransaction->is_read() && hwAccessElement->readLatency)
					{
						delay += static_cast<double>(hwAccessElement->readLatency->getSample()) * (this->getClockPeriod());
					}
					else if (execItem.memoryTransaction->is_write() && hwAccessElement->writeLatency) {
						delay+=static_cast<double>(hwAccessElement->writeLatency->getSample())*(this->getClockPeriod());
					}
					else {
						VLOG(1)<<this->name() \
								<< ": PU tries to access memory " \
								<< MappingModel::getMemoryMapping(labelTLM->label)->getName() \
								<< " but no AccessPath/Rate is defined and no latency." \
								<< " Assuming zero delay!"; 
					}

					wait(delay);
				

					labelTLMext* labelTLMext;
					execItem.memoryTransaction->get_extension(labelTLMext);
					if (labelTLMext != nullptr && labelTLMext->writeCallback) {
						labelTLMext->writeCallback();
					}
				}
				else //hw access path exists
				{
					HwAccessPathTLMext *hwAccessPathTLMext;
					execItem.memoryTransaction->get_extension(hwAccessPathTLMext);
					if (hwAccessPathTLMext == nullptr) {
						hwAccessPathTLMext = new HwAccessPathTLMext();
						execItem.memoryTransaction->set_extension(hwAccessPathTLMext);
					}
					hwAccessPathTLMext->elem = hwAccessElement;
					const auto& initPort = outgoingConnectionToPort[hwAccessElement->getAccessPath()[0]];
					if (!initPort){
						throw(std::runtime_error("Processing unit " + this->getName()  + " is not bound to connection " \
							+ hwAccessElement->getAccessPath()[0]->getName() + " in hw access path"));
					}
					(*initPort)->b_transport(*execItem.memoryTransaction, delay);
				}
				notifyEvents(execItem, ChannelEventStateType::DONE);
			}
		}

		/* active waiting for OsEvents */
		if(execItem.andWaitList){
			wait(*(execItem.andWaitList));
		};
		
		if(execItem.orWaitList){
			wait(*(execItem.orWaitList));
		};

		if(execItem.requestedSemaphore){
			auto waiting = execItem.waiting.value_or(WaitingBehaviour::active);

			if(waiting == WaitingBehaviour::active) {
				if( execItem.requestedSemaphore->activeRequest(interruptEv) == false){
					//interrupted, so store Execitem
					scheduler->storeInterruptedExecutionItem(execItem);
				};
				
			} else {
				throw std::runtime_error("passive semaphore wait currently not support");
			}
		}

		/* mechanism to execute callbacks, e.g. used by clear/setEvent*/
		if(execItem.finishedItemCallback)
			execItem.finishedItemCallback();

	    TraceManager::Inst().activityGraphItemFinish(execItem.getExecutionContext(), execItem.getActivityGraphItem());
    }
}

void ProcessingUnitImpl::notifyEvents(ExecutionItem &execItem, ChannelEventStateType state)
{  
	for (auto event : execItem.events) {
		if (event->getState() == state) {
			if (event->getType() == ChannelEventType::ACCESS_EVENT) {
				event->notify();
			}
			else if (event->getType() == ChannelEventType::WRITE_EVENT && execItem.memoryTransaction->is_write()) {
				event->notify();
			}
			else if (event->getType() == ChannelEventType::READ_EVENT && execItem.memoryTransaction->is_read()) {
				event->notify();
			}
		}
	}
}

void ProcessingUnitImpl::startOfRunnableCallback(const Runnable &runnable){
		currentRunnableTrace.setID(runnable.nameTrace.getID());
}

void ProcessingUnitImpl::endOfRunnableCallback(const Runnable &/*runnable*/){
	//TODO insert tracing
	currentRunnableTrace.setID(traceString::getIdleStringID()); 
}

void ProcessingUnitImpl::systemc_trace(sc_core::sc_trace_file *tf, ProcessingUnitImpl *v,
	const std::string & SigName) {
	sc_trace(tf, v->currentRunnableTrace, SigName + ".currentRunnable");
	sc_trace(tf, v->currentActivityGraphItemType, SigName + ".currentActivityGraphItem");
	sc_trace(tf, v->currentTaskTrace, SigName + ".currentTask");
} 
