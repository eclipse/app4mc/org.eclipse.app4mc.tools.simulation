/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */


#include "ConnectionHandlerImpl.h"
#include "HwAccessPathTLMext.h"

ConnectionHandlerImpl::ConnectionHandlerImpl(const std::string& _name, bool _use_global_queue):
	HwModule(_name),
	use_global_queue(_use_global_queue)
{}

void ConnectionHandlerImpl::setDataRate(const DataRate &rate){
	dataRate = rate;
}

void ConnectionHandlerImpl::registerCallbacks() /*override*/{
	int id = 0;
	for (const auto& [_, targetPort] : targetPortMap){
		//target ports
		targetPortIds[id]=targetPort;
		targetPort->register_b_transport(this, &ConnectionHandlerImpl::b_transport, id);
		wakeups.emplace_back(
			std::make_unique<sc_core::sc_event>((std::string(targetPort->basename()) + "_wakeup").c_str())
		);
		id++;
	}
	for (auto [name , initPort]  :  initPortMap) {
		init_fifos[initPort]=std::queue<int>();
	}
}

void ConnectionHandlerImpl::b_transport(int id, tlm::tlm_generic_payload& trans, sc_core::sc_time& delay) {

	/* identify ports */
	const auto& targetPort = targetPortIds[id];
	HwAccessPathTLMext *tlmExt;
	trans.get_extension(tlmExt);
	const auto& accessElement = tlmExt->elem;
	const auto& incomingConnection = incomingPortToConnection[targetPort];
	const auto& outgoingConnection = accessElement->getAccessPathSuccessor(incomingConnection);
	const auto& initPort = outgoingConnectionToPort[outgoingConnection];

	updateTraceString(targetPort->basename(), trans);

	if (use_global_queue) {
		global_fifo.push(id);
		while (global_fifo.front() != id) {
			wait(*(wakeups[id]));
		}
	}
	else {
		init_fifos.at(initPort).push(id);
		while (init_fifos.at(initPort).front() != id) {
			wait(*(wakeups[id]));
		}
	}
	
	if (dataRate.value > 0) {
		wait(delay + dataRate.getTransmissionDuration(trans.get_data_length()));
		delay = sc_core::SC_ZERO_TIME;
	}
	if (trans.is_read() && readLatency) {
		wait(delay + static_cast<double>(readLatency->getSample())*getClockPeriod());
		delay = sc_core::SC_ZERO_TIME;
	}
	if (trans.is_write() && writeLatency) {
		wait(delay +  static_cast<double>(writeLatency->getSample()) *getClockPeriod());
		delay = sc_core::SC_ZERO_TIME;
	}

	updateTraceString(initPort->basename(), trans);
	(*initPort)->b_transport(trans, delay);

	updateTraceString(initPort->basename());
	updateTraceString(targetPort->basename());

	if (use_global_queue) {
		global_fifo.pop();
		if (!global_fifo.empty()) {
			wakeups[global_fifo.front()]->notify(sc_core::SC_ZERO_TIME);
		}
	}
	else {
		init_fifos.at(initPort).pop();
		if (!init_fifos.at(initPort).empty()) {
			wakeups[init_fifos.at(initPort).front()]->notify(sc_core::SC_ZERO_TIME);
		}
	}
}
