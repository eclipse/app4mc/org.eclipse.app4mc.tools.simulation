/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#include "HwStructure.h"
#include <memory>
#include "CacheStatistic.h"
#include "HwStructureImpl.h"
#include "Connection.h"
#include "HwModule.h"
#include "ProcessingUnit.h"
#include "ProcessingUnitDefinition.h"
#include "Memory.h"
#include "ConnectionHandler.h"
#include "ConnectionHandlerImpl.h"


HwStructure::HwStructure(const std::string& name): 
	WrappedType<HwStructureImpl>(name)
{}

HwStructure::HwStructure(const std::shared_ptr<HwStructureImpl>& inst):
	WrappedType<HwStructureImpl>(inst)
{}

std::shared_ptr<HwStructure> HwStructure::createSubStructure(const std::string& name){
	return [&](){
		auto* ptr = new HwStructure(internalInst()->createSubStructure(name));
		return std::shared_ptr<HwStructure>(ptr);
	}();
}

std::shared_ptr<Connection> HwStructure::createConnection(const std::string& name) {
	auto con = internalInst()->createConnection(name);
	auto rawPtr = new Connection(con);
	return std::shared_ptr<Connection>(rawPtr);
}

std::shared_ptr<ProcessingUnit> HwStructure::createProcessingUnit(const std::string &name, const std::shared_ptr<ProcessingUnitDefinition>& definition){
	return internalInst()->createModule<ProcessingUnit>(name, definition);
}

std::shared_ptr<Memory> HwStructure::createMemory(const std::string& name, uint32_t size){
	return internalInst()->createModule<Memory>(name, size);
}

std::shared_ptr<ConnectionHandler> HwStructure::createConnectionHandler(const std::string& name, bool use_global_queue){
	return internalInst()->createModule<ConnectionHandler>(name, use_global_queue);
}

std::shared_ptr<CacheStatistic> HwStructure::createCacheStatistic(const std::string& name, double hitRate){
	return internalInst()->createModule<CacheStatistic>(name, hitRate);
}

void HwStructure::addInitPort(const std::string &name) {
	internalInst()->addInitPort(name);
}

void HwStructure::addTargetPort(const std::string &name) {
	internalInst()->addTargetPort(name);
}

void HwStructure::bindConnection(const std::shared_ptr<Connection> &connection, const std::string &portName) {
	internalInst()->bindConnection(connection->internalInst(), portName);
}

void HwStructure::setSimCallback(const sim_callback_type& type, const std::function<void()>&& f){
	internalInst()->setSimCallback(type, std::move(f));
}
