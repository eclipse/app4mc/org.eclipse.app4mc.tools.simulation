/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#include <CacheStatisticImpl.h>

#include <effolkronium/random.hpp>
#include <pcg_random.hpp>
#include <systemc>
#include <tlm>
#include <tlm_utils/simple_initiator_socket.h>
#include <tlm_utils/simple_target_socket.h>
#include <functional>
#include <memory>
#include <optional>
#include "Common.h"
#include "Deviation.h"
#include "ProcessingUnitDefinition.h"
#include "ChannelEvent.h"
#include "ExecutionItem.h"
#include "Scheduler.h"
#include "hasNamedPorts.h"
#include "polymorphic_value.h"
#include <cstdint>
#include <queue>
#include <sstream>
#include "HwModule.h"


void CacheStatisticImpl::sanityChecks() /*override*/ {
	// ensure cache has exactly one input an one output port
	if (targetPortMap.size()!= 1){
		std::stringstream buf;
		buf<<"Multiple input ports defined for Cache (should be one):"<<getName();
		for (const auto& [p, c] : incomingPortToConnection){
			buf<<" "<<p->name();
		}
		throw std::runtime_error(buf.str());
	}
	if (initPortMap.size() != 1){
		std::stringstream buf;
		buf<<"Multiple output ports defined for Cache (should be one):"<<getName();
		for (const auto& [p, c] : incomingPortToConnection){
			buf<<" "<<p->name();
		}
		throw std::runtime_error(buf.str());
	}
	hasNamedPorts<CacheStatisticImpl>::sanityChecks();
}

void CacheStatisticImpl::registerCallbacks() /*override*/{
	const auto& targetPort = (incomingPortToConnection.begin())->first;
	targetPort->register_b_transport(this, &CacheStatisticImpl::b_transport);
}

CacheStatisticImpl::CacheStatisticImpl(const std::string& name, double _hitRate): HwModule(name), hitRate(_hitRate) {	}

void CacheStatisticImpl::setDataRate(const DataRate &rate){
	dataRate=rate;
}

/* rounds up to next power of 2*/
void CacheStatisticImpl::setLineSize(size_t _lineSize) {
	offsetBits = (size_t)(ceil(log2(_lineSize)));
	lineSize = (size_t)exp2(offsetBits);
	//the first part of this comparison will compile to a const expression (which might eleminate the whole condition)
	if (std::numeric_limits<size_t>::max() > std::numeric_limits<unsigned int>::max() && lineSize > std::numeric_limits<unsigned int>::max()) {
		throw std::overflow_error("Cacheline will not fit into tlm_generic_payload");
	}
}

/* round up to next multiple of lineSize which is power of 2*/
void CacheStatisticImpl::setSize(size_t _size) {
	auto bitsOfIndex = (size_t)(ceil(log2(_size / lineSize)));
	size = (size_t) (lineSize * exp2(bitsOfIndex));

}

	
using app4mcsim_rand = effolkronium::basic_random_static<pcg32, app4mcsim_seed>;
void CacheStatisticImpl::b_transport(tlm::tlm_generic_payload& trans, sc_core::sc_time& delay) {

	/* identify ports */
	//cache permits only one target port and one init port
	const auto& targetPort = (incomingPortToConnection.begin())->first;
	const auto& initPort = outgoingConnectionToPort.begin()->second;
	
	updateTraceString(targetPort->basename(), trans);
	auto begin = trans.get_address();
	auto end = begin + trans.get_data_length();

	auto linesInvolved = (end >> offsetBits) - (begin >> offsetBits);
	wait(delay);
	delay = sc_core::SC_ZERO_TIME;
	wait(static_cast<double>(accessLatency->getSample())*getClockPeriod());
	for (unsigned int i = 0; i < linesInvolved; ++i) {
		if (app4mcsim_rand::get<bool>(hitRate)) {
			/* cache hit */
		}
		else {
			/* cache miss creates transaction for each effected cache line*/
			missTransaction.deep_copy_from(trans);
			missTransaction.set_address((begin + i) << offsetBits);
			missTransaction.set_data_length(static_cast<unsigned int>(lineSize));
			
			updateTraceString(initPort->basename(), trans);
			(*initPort)->b_transport(trans,delay);

			/* wait possible delay*/
			wait(delay);
			delay = sc_core::SC_ZERO_TIME;
		}

		
	}
	/* Transfer data from cache to requester*/
	/* check precision problems here */
	if (dataRate.value > 0) {
		double bits_transmitted = trans.get_data_length() * 8;
		double transmitCycles = ceil(bits_transmitted / (static_cast<double>(dataRate.unit) * dataRate.value * getClockPeriod().to_seconds() ));
		wait(transmitCycles * getClockPeriod());
	}	
	trans.set_response_status(tlm::TLM_OK_RESPONSE);
	
	updateTraceString(initPort->basename());
	updateTraceString(targetPort->basename());
}
