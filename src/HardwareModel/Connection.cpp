// /**
//  ********************************************************************************
//  * Copyright (c) 2022 Robert Bosch GmbH
//  * 
//  * This program and the accompanying materials are made
//  * available under the terms of the Eclipse Public License 2.0
//  * which is available at https://www.eclipse.org/legal/epl-2.0/
//  * 
//  * SPDX-License-Identifier: EPL-2.0
//  * 
//  * Contributors:
//  * - Robert Bosch GmbH - initial contribution
//  *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
//  ********************************************************************************
//  */
#include "Connection.h"
#include <utility>
#include "ConnectionImpl.h"


Connection::Connection(const std::string& _name): 
    WrappedType<ConnectionImpl>(_name)
{}

Connection::Connection(const std::shared_ptr<ConnectionImpl>& _inst):
    WrappedType<ConnectionImpl>(_inst)
{}

template<class T>
void Connection::setReadLatency( const T &lat) {
    internalInst()->setReadLatency(std::forward(lat));
}
		
template<class T>
void Connection::setWriteLatency( const T &lat) {
    internalInst()->setWriteLatency(std::forward(lat));
}

void Connection::setDataRate(const DataRate &rate){
    internalInst()->setDataRate(rate);
}	
