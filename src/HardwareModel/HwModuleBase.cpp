
/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */
#include <memory>
#include <systemc>
#include "HwModuleBase.h"

const sc_core::sc_time& HwModuleBase::getClockPeriod(){
	return *clock_period;
}

void HwModuleBase::setClockPeriod(const sc_core::sc_time _clock_period){
	this->clock_period = std::make_unique<sc_core::sc_time>(_clock_period);
}
	

