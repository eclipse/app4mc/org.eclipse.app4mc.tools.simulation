// /**
//  ********************************************************************************
//  * Copyright (c) 2022 Robert Bosch GmbH
//  * 
//  * This program and the accompanying materials are made
//  * available under the terms of the Eclipse Public License 2.0
//  * which is available at https://www.eclipse.org/legal/epl-2.0/
//  * 
//  * SPDX-License-Identifier: EPL-2.0
//  * 
//  * Contributors:
//  * - Robert Bosch GmbH - initial contribution
//  *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
//  ********************************************************************************
//  */
#include "ConnectionHandler.h"
#include "ConnectionHandlerImpl.h"
#include "Connection.h"
#include "TimeUtilsSystemC.h"


ConnectionHandler::ConnectionHandler(const std::string& _name, bool _use_global_queue):
	WrappedType<ConnectionHandlerImpl>(_name, _use_global_queue){}

ConnectionHandler::ConnectionHandler(const std::shared_ptr<ConnectionHandlerImpl>& _inst):
	WrappedType<ConnectionHandlerImpl>(_inst){}

void ConnectionHandler::setDataRate(const DataRate &rate){
    internalInst()->setDataRate(rate);
}

//memory element details
template<class T>
void ConnectionHandler::setReadLatency( const T &lat){
    internalInst()->setReadLatency(lat);
}

template<class T>
void ConnectionHandler::setWriteLatency( const T &lat){
    internalInst()->setWriteLatency(lat);
}

void ConnectionHandler::setClockPeriod(const Time& _clock_period){
    internalInst()->setClockPeriod(convertTime(_clock_period));
}

void ConnectionHandler::addInitPort(const std::string &name){
    internalInst()->addInitPort(name);
}

void ConnectionHandler::addTargetPort(const std::string &name){
    internalInst()->addTargetPort(name);
}


void ConnectionHandler::bindConnection(const std::shared_ptr<Connection> &connection, const std::string &portName){
    internalInst()->bindConnection(connection->internalInst(), portName);
}


template void ConnectionHandler::setReadLatency(const DiscreteValueConstant& lat);
template void ConnectionHandler::setReadLatency(const DiscreteValueGaussDistribution& lat);
template void ConnectionHandler::setReadLatency(const DiscreteValueWeibullEstimatorsDistribution& lat);
template void ConnectionHandler::setReadLatency(const DiscreteValueUniformDistribution& lat);
template void ConnectionHandler::setReadLatency(const DiscreteValueBetaDistribution& lat);
template void ConnectionHandler::setReadLatency(const DiscreteValueBoundaries& lat);

template void ConnectionHandler::setWriteLatency(const DiscreteValueConstant& lat);
template void ConnectionHandler::setWriteLatency(const DiscreteValueGaussDistribution& lat);
template void ConnectionHandler::setWriteLatency(const DiscreteValueWeibullEstimatorsDistribution& lat);
template void ConnectionHandler::setWriteLatency(const DiscreteValueUniformDistribution& lat);
template void ConnectionHandler::setWriteLatency(const DiscreteValueBetaDistribution& lat);
template void ConnectionHandler::setWriteLatency(const DiscreteValueBoundaries& lat);