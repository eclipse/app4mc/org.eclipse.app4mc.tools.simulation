/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#include "HwAccessElement.h"
#include "HwModuleBase.h"
#include "Memory.h"
#include "ProcessingUnitImpl.h"
#include "Connection.h"
#include "ProcessingUnit.h"
#include "MemoryImpl.h"

std::shared_ptr<ConnectionImpl> HwAccessElement::getAccessPathSuccessor(const std::shared_ptr<ConnectionImpl>& ref){
	for (auto it=accessPath.begin(); it != accessPath.end(); it++){
		if (*it == ref){
			if (it == accessPath.end()){
				//throw std::runtime_error("HwAccessElement from" + source->getName() + " to " + dest->getName() + )
				throw std::runtime_error("HwAccessElement from" + source->getName() + " to "  + dest->getName() + " does not define an access path via a successor of connection " + ref->getName());
			}
			if (++it != accessPath.end()){	
				return (*(it));
			} else {
					break;
			}
		}
	}
	throw std::runtime_error("HwAccessElement from" + source->getName() + " to " + dest->getName() + " define not define an access path via connection " + ref->getName());
}

void HwAccessElement::setDataRate(const DataRate &rate){
	dataRate = rate;
}

void HwAccessElement::setDest(const std::shared_ptr<Memory>& _dest){
	setDest(_dest->internalInst());
}

void HwAccessElement::setDest(const std::shared_ptr<HwModuleBase>& _dest){
	dest = _dest;
}

void HwAccessElement::setSource(const std::shared_ptr<ProcessingUnitImpl>& _source){
	source=_source;
}

const std::shared_ptr<HwModuleBase>& HwAccessElement::getDest() const {
	return dest;
}

const std::shared_ptr<ProcessingUnitImpl>& HwAccessElement::getSource() const {
	return source;
}

void HwAccessElement::setAccessPath(const std::vector<std::shared_ptr<Connection>>& _accessPath){
	if (!accessPath.empty()){
		throw std::runtime_error("Access path to " + dest->getName() +\
		" via " + printAccessPath() + " already populated.");
	}
	std::transform(_accessPath.begin(), _accessPath.end(), std::back_inserter(accessPath), 
		[&](const std::shared_ptr<Connection>& c){
			return c->internalInst();
	});
}

void HwAccessElement::setAccessPath(const std::vector<std::shared_ptr<ConnectionImpl>>& _accessPath){
	if (!accessPath.empty()){
		throw std::runtime_error("Access path to " + dest->getName() +\
		" via " + printAccessPath() + " already populated.");
	}
	accessPath = _accessPath;
}

const std::vector<std::shared_ptr<ConnectionImpl>>& HwAccessElement::getAccessPath(){
	return accessPath;
}


std::string HwAccessElement::printAccessPath(){
	if (accessPath.empty()){
		return "(access path empty)";
	}
	std::string s;
	std::for_each(accessPath.begin(), accessPath.end(), 
		[&](const std::shared_ptr<ConnectionImpl>& c){
			s.append(c->getName());
			if (c != accessPath[accessPath.size()-1]){
				s.append(", ");
			}

	});
	return s;
}




