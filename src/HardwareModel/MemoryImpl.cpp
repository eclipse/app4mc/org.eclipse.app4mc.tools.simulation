/**
 ********************************************************************************
 * Copyright (c) 2021 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */
#include "MemoryImpl.h"
#include "HardwareModel.h"
#include "labelTLMext.h"
#include "EasyloggingMacros.h"

void MemoryImpl::b_transport(tlm::tlm_generic_payload& trans, sc_core::sc_time& delay)
	{
		updateTraceString(targetPortMap.begin()->first, trans); //name
		tlm::tlm_command cmd = trans.get_command();
		sc_dt::uint64  adr = trans.get_address();
		unsigned int   len = trans.get_data_length();
		unsigned char* byt = trans.get_byte_enable_ptr();
		unsigned int   wid = trans.get_streaming_width();
		if (adr + len > sizeBytes) { // Check for storage address overflow
			trans.set_response_status(tlm::TLM_ADDRESS_ERROR_RESPONSE);
			return;
		}
		if (byt) { // Target unable to support byte enable attribute
			trans.set_response_status(tlm::TLM_BYTE_ENABLE_ERROR_RESPONSE);
			return;
		}
		if (wid < len) { // Target unable to support streaming width attribute
			trans.set_response_status(tlm::TLM_BURST_ERROR_RESPONSE);
			return;
		}
		VLOG_SCMODULE(2) << "Memory Access on addr: " << adr << std::endl; 
		if (cmd == tlm::TLM_WRITE_COMMAND) { // Execute command
		VLOG_SCMODULE(2) << " write; addr: 0x" << std::hex << adr << std::dec << " len: " << len << std::endl;
		}
		else if (cmd == tlm::TLM_READ_COMMAND) {
		VLOG_SCMODULE(2) << " read; addr: 0x" << std::hex << adr << std::dec << ", len: " << len << std::endl;
		}
		wait(delay);
		if (accessLatency) {
			wait(static_cast<double>(accessLatency->getSample())* getClockPeriod());
		}
		/* check precision problems here */
		if (dataRate.value > 0) {
			double bits_transmitted = len * 8;
			double transmitCycles = ceil(bits_transmitted / (static_cast<double>(dataRate.unit) * dataRate.value * getClockPeriod().to_seconds() ));
			wait(transmitCycles * getClockPeriod());
		}

		labelTLMext *labelTLM;
		trans.get_extension(labelTLM);
		if (labelTLM && labelTLM->writeCallback) {
			labelTLM->writeCallback();
		}

		trans.set_response_status(tlm::TLM_OK_RESPONSE); // Successful completion
		updateTraceString(targetPortMap.begin()->first);
	}