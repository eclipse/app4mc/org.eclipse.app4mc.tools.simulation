// /**
//  ********************************************************************************
//  * Copyright (c) 2022 Robert Bosch GmbH
//  * 
//  * This program and the accompanying materials are made
//  * available under the terms of the Eclipse Public License 2.0
//  * which is available at https://www.eclipse.org/legal/epl-2.0/
//  * 
//  * SPDX-License-Identifier: EPL-2.0
//  * 
//  * Contributors:
//  * - Robert Bosch GmbH - initial contribution
//  *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
//  ********************************************************************************
//  */
#include "ProcessingUnit.h"
#include <list>
#include <memory>
#include <utility>
#include "ProcessingUnitImpl.h"
#include "HwAccessElement.h"
#include "Scheduler.h"
#include "TimeUtilsSystemC.h"

ProcessingUnit::ProcessingUnit(const std::string &name, const std::shared_ptr<ProcessingUnitDefinition> &definition):
    WrappedType<ProcessingUnitImpl>(name, definition)
{}

ProcessingUnit::ProcessingUnit(const std::shared_ptr<ProcessingUnitImpl>& _inst):
    WrappedType<ProcessingUnitImpl>(_inst)
{}

void ProcessingUnit::setScheduler(std::shared_ptr<Scheduler> sched) {
	this->internalInst()->scheduler = std::move(sched);
}

void ProcessingUnit::addHWAccessElement(HwAccessElement& elem) {
	elem.setSource(internalInst());
	internalInst()->hwAccessElems[elem.getDest()] = std::make_shared<HwAccessElement>(elem);
}

const std::shared_ptr<ProcessingUnitDefinition>& ProcessingUnit::getProcessingUnitDefinition() const {
	return internalInst()->getProcessingUnitDefinition();
}

void ProcessingUnit::setClockPeriod(const Time& _clock_period){
    internalInst()->setClockPeriod(convertTime(_clock_period));
}

void ProcessingUnit::addInitPort(const std::string &name){
    internalInst()->addInitPort(name);
}

void ProcessingUnit::addTargetPort(const std::string &name){
    internalInst()->addTargetPort(name);
}

void ProcessingUnit::bindConnection(const std::shared_ptr<Connection> &connection, const std::string &portName){
    internalInst()->bindConnection(connection->internalInst(), portName);
}
