/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#include "ConnectionImpl.h"

ConnectionImpl::ConnectionImpl(const std::string& _name) : 
	HasName(_name), 
	sc_module(sc_core::sc_module_name(_name.c_str()))
{
	sc_core::sc_get_curr_simcontext()->hierarchy_push(this);
	in = std::make_unique<ConnectionTargetSocket>();
	out = std::make_unique<ConnectionInitiatorSocket>();
	sc_core::sc_get_curr_simcontext()->hierarchy_pop();
	in->register_b_transport(this, &ConnectionImpl::b_transport);
	addTracingPort("out");
	addTracingPort("in");
}

void ConnectionImpl::b_transport(tlm::tlm_generic_payload & trans, sc_core::sc_time & delay) {
	
	updateTraceString("in", trans);
	if (freqDomainPtr && writeLatency && trans.get_command() == tlm::TLM_WRITE_COMMAND) {
		sc_core::wait(static_cast<double>(writeLatency->getSample())* (freqDomainPtr->getClockPeriod()) + delay);
		delay = sc_core::SC_ZERO_TIME;
	}
	if (freqDomainPtr && readLatency && trans.get_command() == tlm::TLM_READ_COMMAND) {
		sc_core::wait(static_cast<double>(readLatency->getSample())* freqDomainPtr->getClockPeriod() + delay);
		delay = sc_core::SC_ZERO_TIME;
	}

	if (dataRate.value > 0) {
		wait(delay + dataRate.getTransmissionDuration(trans.get_data_length()));
		delay = sc_core::SC_ZERO_TIME;
	}

	updateTraceString("out", trans);
	(*out)->b_transport(trans, delay);
	updateTraceString("in");
	updateTraceString("out");
}

void ConnectionImpl::setDataRate(const DataRate &rate){
	dataRate = rate;
}

void ConnectionImpl::setFreqDomain(std::shared_ptr<HwModuleBase> hwModule) {
	freqDomainPtr = hwModule;
}

void ConnectionImpl::end_of_elaboration() /*override*/ {
	createTracings();
}

ConnectionImpl::ConnectionInitiatorSocket& ConnectionImpl::getInitiatorPort() /*override*/ {
	return *out;
} 

ConnectionImpl::ConnectionTargetSocket& ConnectionImpl::getTargetPort() /*override*/{
	return *in;
}

sc_core::sc_object* ConnectionImpl::getHierarchicalParent() /*override*/ {
	return this->get_parent_object();
}

