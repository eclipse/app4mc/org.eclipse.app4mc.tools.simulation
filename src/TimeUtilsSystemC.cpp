/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#include "TimeUtilsSystemC.h"
#include <ratio>

std::chrono::duration<int64_t,std::nano> getTimeresolutionAsDuration(){
	//since the resolution cannot change after the first call, use static init here
	static std::chrono::duration<int64_t,std::nano> resolution = [](){
		auto sc_resolution = sc_core::sc_time(1,sc_core::SC_SEC);
		constexpr auto secInNS = 1'000'000'000ll;
		auto tuPerSecond = secInNS/sc_resolution.value();

		return std::chrono::duration<int64_t,std::nano>{tuPerSecond};
	}();
	return resolution;
}



