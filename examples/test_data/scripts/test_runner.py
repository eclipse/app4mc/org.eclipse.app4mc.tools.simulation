#!/usr/bin/python

import sys, os, sys


def __compareBTF__(s1,s2):
    # #simulation seed: 0x165cd65c65e831d3
    # #creationDate 2022-07-20T13:33:45Z
    m_seed = '#simulation seed:'
    m_creationDate= '#creationDate'
    if s1.startswith(m_seed) and s2.startswith(m_seed):
        return True
    elif s1.startswith(m_creationDate) and s2.startswith(m_creationDate):
        return True
    else:
        return s1 == s2


#VCD EXAMPLE-----------------------------
# $date
#  Aug 25, 2022       06:22:58
# $end
# 
# $version
# SystemC 2.3.3-Accellera --- Jul  9 2022 10:02:24
# $end
# 
# $timescale
#     1 ns
# $end
# 
# $scope module SystemC $end
# $scope module bus $end
# $var wire   32  aaaba  P1init [31:0]  $end
# .....
#end VCD EXAMPLE-------------------------
vcd_withinDeclarationToSkip = 0

def __compareVCD__(s1,s2):
    global vcd_withinDeclarationToSkip
    m_date = '$date'
    m_version = '$version'
    m_end = '$end'
    err_dateVersion = "VCD compare failed: date or version declaration section could not be parsed"
    #print("compare:\n\t" + s1 + "\n\t" + s2) #for debugging
    if s1.startswith(m_date) and s2.startswith(m_date):
        if vcd_withinDeclarationToSkip == 0:
            vcd_withinDeclarationToSkip = 1
            print("Entered date section")
            return True
        else:
            print(err_dateVersion)
            return False
    elif s1.startswith(m_version) and s2.startswith(m_version):
        if vcd_withinDeclarationToSkip == 0:
            vcd_withinDeclarationToSkip = 1
            print("Entered version section")
            return True
        else:
            print(err_dateVersion)
            return False
    elif s1.startswith(m_end) and s2.startswith(m_end):
        if vcd_withinDeclarationToSkip > 0: # found end identifier AND within a declaration section 
            vcd_withinDeclarationToSkip = 0 # -> terminate declaration section evaluation
            print("...end date/version section")
            return True
        else:
            return True #also return true as '$end' could appear in a trace, e.g in a not further specified section
    elif vcd_withinDeclarationToSkip > 0:
        if vcd_withinDeclarationToSkip > 4:
            print(err_dateVersion + ". Declaration section too long")
            return False
        else:
            vcd_withinDeclarationToSkip += 1 #count the skipped line, the date and version sections should be short (3-4 lines)
            return True  #within declaration section, skip this line
    else:
        return s1 == s2

def main(file1, file2):
    print(sys.argv[0])
    print(os.getcwd())
    p1=os.path.join(os.getcwd(), file1)
    p2=os.path.join(os.getcwd(), file2)
    f1 = open(p1, mode='r+')
    if not f1:
        print("cannot open file " + str(p1))
    f2 = open(p2, mode='r+')
    if not f2:
        print("cannot open file " + str(p2))
    if file1.endswith(".btf") and file2.endswith(".btf"):
        __compare__ = __compareBTF__
    elif file1.endswith(".vcd") and file2.endswith(".vcd"):
        __compare__ = __compareVCD__
    else:
        print("Unknown file format:\n\t" + file1 + "\n\t" + file2)

    for l1 in f1.readlines():
        l2 = f2.readline()
        if (__compare__(l1, l2)):
            continue
        else:
            print("-->not equal:lines not matching: \n   " + l1 + "   " + l2)
            sys.exit(-1)
    print("-->files match")
    sys.exit(0)
    

if __name__ == "__main__":
   main(sys.argv[1], sys.argv[2])
