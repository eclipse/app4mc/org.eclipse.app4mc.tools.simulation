
##aliases for debug build
alias ex_conan='conan install . --build=missing -pr=../conan/profiles/Gcc8Debug'
alias ex_conf='cmake --preset debug'
alias ex_build='cmake --build build/Debug --parallel'
alias ex_install='cmake --install build/Debug --prefix install'
alias ex_test='pushd build/Debug &&  make test && popd'

alias ex_all="ex_conan && ex_conf && ex_build && ex_test && ex_install"