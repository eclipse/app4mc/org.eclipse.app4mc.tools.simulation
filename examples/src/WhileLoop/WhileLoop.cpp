/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#include <memory>
#include "APP4MCsim.h"
#include "SimParamParser.h"

class ExampleRunner : public SimRunner{
public:
void setup() override{
  /* Hardware */
  /* 200Mhz*/
  Time ECU1_Freq_Domain_CycleTime = 10_ns;
  auto pudef = std::make_shared<ProcessingUnitDefinition>("ECU-CPU");
  auto ECU1 = modelRoot()->createSubStructure("ECU1");

  auto core1ECU1 = ECU1->createProcessingUnit("Core1ECU1", pudef);
  core1ECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);

  auto mainMemECU1 = ECU1->createMemory("MainMemECU1", 1 * 1024 * 1024 * 1024);
  mainMemECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);
  mainMemECU1->setAccessLatency<DiscreteValueConstant>({100});

  /* HW_access_elems */
  HwAccessElement mainMemC1Access;
  mainMemC1Access.setDest(mainMemECU1);
  mainMemC1Access.setReadLatency<DiscreteValueConstant>({10});
  mainMemC1Access.setWriteLatency<DiscreteValueConstant>({10});
  core1ECU1->addHWAccessElement(mainMemC1Access);

  /* Software */

  auto endVar = ModeLabel::Inst("CounterVar", 10);
  
  WhileLoop loop;
  loop.addCondition<ModeValueCondition>(endVar,0,RelationalOperator::GREATER_THAN);
  loop.addActivityGraphItem<ModeLabelAccess>(endVar,1,ModeAccessType::DECREMENT);
  loop.addActivityGraphItem<Ticks>(DiscreteValueConstant(100));
  
  auto ra = std::make_shared<Runnable>("RunLoop");
  
  ra->addActivityGraphItem(loop);
  ra->addActivityGraphItem<ModeLabelAccess>(endVar,10 ,ModeAccessType::SET);

  // Stimuli
  auto stimuli_5ms = std::make_shared<FixedPeriodicStimulus>("5ms_Stimuli", 5_ms);

  // Software Model  (Tasks)
  TaskAllocation ta;
  ta.setSchedulingParameter("priority", 0);
  auto t_runnable = Task::createTask("T_runnable");
  stimuli_5ms->addTask(t_runnable);
  t_runnable->addActivityGraphItem<RunnableCall>(ra);
  t_runnable->setTaskAllocation(ta);

  // Mapping
  MappingModel::addMemoryMapping(endVar, mainMemECU1);

  // OS
  auto priorityScheduler = std::make_shared<PriorityScheduler>("PrioSched");
  priorityScheduler->setExecutionCore(core1ECU1);
  priorityScheduler->addTaskMapping(t_runnable);

  // Trace
  scTrace(core1ECU1, "Core1ECU1");
  scTrace(mainMemECU1, "MainMemECU1");

}};

int main([[maybe_unused]] int argc,[[maybe_unused]] char *argv[]) {

	SimParam args;
	SimParamParser::parse(argc, argv, args);

	auto runner = ExampleRunner();
	EasyloggingConfig::configure("app4mcsim.log", 1, true, true);
	runner.enableTracers(args.tracerNames,args.traceDirectory, false);
	
	try {
		runner.simulate(TimeParameter<std::milli>(200));
	}
	catch (const std::runtime_error& e) {
		std::cerr << e.what();
		return -1;
	}
	return 0;
}