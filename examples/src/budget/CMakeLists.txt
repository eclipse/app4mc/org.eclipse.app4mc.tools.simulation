project(budget)
add_executable(budget "budget.cpp")
target_link_libraries(budget PRIVATE 
    #easyloggingpp::easyloggingpp #24.10 
    app4mc.sim::app4mc.sim
   #SystemC::systemc #24.10
    SimParamParser)
#target_precompile_headers(budget REUSE_FROM app4mc.sim_lib)

test_run()
test_check_trace(${DEFAULT_VCD_TRACE_NAME})
test_check_trace(${DEFAULT_BTF_TRACE_NAME})


install(
    TARGETS budget 
    #RUNTIME DESTINATION ${CMAKE_INSTALL_PREFIX}/examples/budget
)