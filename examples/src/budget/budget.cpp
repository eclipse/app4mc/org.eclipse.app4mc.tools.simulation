/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */


#include <memory>
#include "APP4MCsim.h"
#include "SimParamParser.h"

class ExampleRunner : public SimRunner{
public:

	ExampleRunner(uint64_t seed):SimRunner(seed){}

	void setup() override{

		/* ECU1 */
		
		/* 200Mhz*/
		Time ECU1_Freq_Domain_CycleTime= 5_ns;
		auto pudef = std::make_shared<ProcessingUnitDefinition>("ECU-CPU");

		auto ECU1 = modelRoot()->createSubStructure("ECU1");
		ECU1->addInitPort("ECU1_BUSPortInit");
		ECU1->addTargetPort("ECU1_BUSPortResp");

		auto core1ECU1 = ECU1->createProcessingUnit("Core1ECU1", pudef);


		core1ECU1->addInitPort("P1");
		core1ECU1->addInitPort("SP_P");
		core1ECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);


		auto core2ECU1 = ECU1->createProcessingUnit("Core2ECU1", pudef);
		core2ECU1->addInitPort("P1");
		core2ECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);

		//TODO fix size argument
		auto mainMemECU1 = ECU1->createMemory("MainMemECU1", 1 * 1024 * 1024* 1024);
		mainMemECU1->addTargetPort("P1");
		mainMemECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);
		mainMemECU1->setAccessLatency<DiscreteValueWeibullEstimatorsDistribution>({DiscreteValueWeibullEstimatorsDistribution::findParameter(8,0.1,4,32),4,32});

		auto interconnectECU1 = ECU1->createConnectionHandler("InterconnectECU1");
		
		interconnectECU1->addTargetPort("P1");
		interconnectECU1->addTargetPort("P2");
		interconnectECU1->addInitPort("P3");
		interconnectECU1->addInitPort("P4init");
		interconnectECU1->addTargetPort("P4resp");
		interconnectECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);
		interconnectECU1->setReadLatency<DiscreteValueConstant>({5});
		interconnectECU1->setWriteLatency<DiscreteValueConstant>({3});

		auto dataCache = ECU1->createCacheStatistic("DataCache", 0.9);
		dataCache->addInitPort("P2");
		dataCache->addTargetPort("P1");
		dataCache->setLineSize(64);
		dataCache->setSize(256 * 1024);
		dataCache->setClockPeriod(ECU1_Freq_Domain_CycleTime);

		auto scratchpad = ECU1->createMemory("Scratchpad", 2 * 1024 * 1024);
		scratchpad->addTargetPort("P1");
		scratchpad->setAccessLatency<DiscreteValueConstant>({1});
		scratchpad->setClockPeriod(ECU1_Freq_Domain_CycleTime);


		/* Connections */
		auto con1 = ECU1->createConnection("con1");
		auto con2 = ECU1->createConnection("con2");
		auto con3 = ECU1->createConnection("con3");
		auto con4init = ECU1->createConnection("con4init");
		auto con4resp = ECU1->createConnection("con4resp");
		auto con11 = ECU1->createConnection("con11");
		auto con12 = ECU1->createConnection("con12");
		
		core1ECU1->bindConnection(con1, "P1");
		interconnectECU1->bindConnection(con1, "P1");

		core2ECU1->bindConnection(con2, "P1");
		dataCache->bindConnection(con2, "P1");

		interconnectECU1->bindConnection(con3, "P3");
		mainMemECU1->bindConnection(con3, "P1");

		interconnectECU1->bindConnection(con4init, "P4init");
		ECU1->bindConnection/*DelegatedPort*/(con4init, "ECU1_BUSPortInit");


		interconnectECU1->bindConnection(con4resp, "P4resp");
		ECU1->bindConnection/*DelegatedPort*/(con4resp, "ECU1_BUSPortResp");

		core1ECU1->bindConnection(con11, "SP_P");
		scratchpad->bindConnection(con11, "P1");

		dataCache->bindConnection(con12, "P2");
		interconnectECU1->bindConnection(con12, "P2");

		/* ECU 2*/
		auto ECU2 = modelRoot()->createSubStructure("ECU2");
		ECU2->addInitPort("ECU2_BUSPortInit");
		ECU2->addTargetPort("ECU2_BUSPortResp");

		/* 200Mhz*/
		Time ECU2_Freq_Domain_CycleTime = 5_ns;

		auto core1ECU2 = ECU2->createProcessingUnit("Core1ECU2", pudef);
		core1ECU2->addInitPort("P1");
		core1ECU2->setClockPeriod(ECU2_Freq_Domain_CycleTime);

		auto core2ECU2 = ECU2->createProcessingUnit("Core2ECU2", pudef);
		core2ECU2->addInitPort("P1");
		core2ECU2->setClockPeriod(ECU2_Freq_Domain_CycleTime);

		auto mainMemECU2 = ECU2->createMemory("MainMemECU2", 1 * 1024 * 1024 * 1024);
		mainMemECU2->addTargetPort("P1");
		mainMemECU2->setClockPeriod(ECU2_Freq_Domain_CycleTime);
		mainMemECU2->setAccessLatency<DiscreteValueWeibullEstimatorsDistribution>({DiscreteValueWeibullEstimatorsDistribution::findParameter(8,0.1,4,32),4,32});

		auto interconnectECU2 = ECU2->createConnectionHandler("InterconnectECU2");

		interconnectECU2->addTargetPort("P1");
		interconnectECU2->addTargetPort("P2");
		interconnectECU2->addInitPort("P3");
		interconnectECU2->addInitPort("P4init");
		interconnectECU2->addTargetPort("P4resp");
		interconnectECU2->setClockPeriod(ECU2_Freq_Domain_CycleTime);


		/* Connections */
		auto con5 = ECU2->createConnection("con5");
		auto con6 = ECU2->createConnection("con6");
		auto con7 = ECU2->createConnection("con7");
		auto con8init = ECU2->createConnection("con8init");
		auto con8resp = ECU2->createConnection("con8resp");

		core1ECU2->bindConnection(con5, "P1");
		interconnectECU2->bindConnection(con5, "P1");

		core2ECU2->bindConnection(con6, "P1");
		interconnectECU2->bindConnection(con6, "P2");

		interconnectECU2->bindConnection(con7, "P3");
		mainMemECU2->bindConnection(con7, "P1");

		interconnectECU2->bindConnection(con8init, "P4init");
		ECU2->bindConnection/*DelegatedPort*/(con8init, "ECU2_BUSPortInit");
		
		interconnectECU2->bindConnection(con8resp, "P4resp");
		ECU2->bindConnection/*DelegatedPort*/(con8resp, "ECU2_BUSPortResp");


		/* BUS */
		// auto bus = std::make_shared<ConnectionHandler>("BUS", true);
		auto bus = modelRoot()->createConnectionHandler("BUS", true);
		bus->addInitPort("P1init");
		bus->addTargetPort("P1resp");
		bus->addInitPort("P2init");
		bus->addTargetPort("P2resp");
		bus->setDataRate({1000,DataRateUnit::MibitPerSecond});

		// auto con9init = std::make_shared<Connection>("con9init");
		// auto con9resp = std::make_shared<Connection>("con9resp");
		// auto con10init = std::make_shared<Connection>("con10init");
		// auto con10resp = std::make_shared<Connection>("con10resp");
		
		auto con9init  = modelRoot()->createConnection("con9init");
		auto con9resp  = modelRoot()->createConnection("con9resp");
		auto con10init = modelRoot()->createConnection("con10init");
		auto con10resp = modelRoot()->createConnection("con10resp");
		
		bus->bindConnection(con9resp, "P1resp");
		ECU1->bindConnection(con9resp, "ECU1_BUSPortInit");

		bus->bindConnection(con9init, "P1init");
		ECU1->bindConnection(con9init, "ECU1_BUSPortResp");


		bus->bindConnection(con10init, "P2init");
		ECU2->bindConnection(con10init, "ECU2_BUSPortResp");

		bus->bindConnection(con10resp, "P2resp");
		ECU2->bindConnection(con10resp, "ECU2_BUSPortInit");


		/* HW_access_elems */

		HwAccessElement mainMemC1Access;
		mainMemC1Access.setDest(mainMemECU1);
		mainMemC1Access.setReadLatency<DiscreteValueConstant>({8});
		mainMemC1Access.setWriteLatency<DiscreteValueConstant>({8});

		HwAccessElement scratchpadAccess;
		scratchpadAccess.setDest(scratchpad);
		scratchpadAccess.setReadLatency<DiscreteValueConstant>({1});
		scratchpadAccess.setWriteLatency<DiscreteValueConstant>({1});

		HwAccessElement externalMemoryAccessECU1core1;
		externalMemoryAccessECU1core1.setDest(mainMemECU2);
		externalMemoryAccessECU1core1.setAccessPath({
			con1,
			//interconnectECU1, 
			con4init, 
			con9resp, 
			//bus, 
			con10init, 
			con8resp, 
			//interconnectECU2, 
			con7
		});

		core1ECU1->addHWAccessElement(mainMemC1Access);
		core1ECU1->addHWAccessElement(scratchpadAccess);
		core1ECU1->addHWAccessElement(externalMemoryAccessECU1core1);

		HwAccessElement externalMemoryAccessECU1core2;
		externalMemoryAccessECU1core2.setDest(mainMemECU2);
		externalMemoryAccessECU1core2.setAccessPath({
			con2, 
			//dataCache,
			con12, 
			//interconnectECU1, 
			con4init, 
			con9resp, 
			//bus, 
			con10init, 
			con8resp, 
			//interconnectECU2, 
			con7
		});
		
		core2ECU1->addHWAccessElement(mainMemC1Access);
		core2ECU1->addHWAccessElement(externalMemoryAccessECU1core2);

		HwAccessElement mainMemC2Access;
		mainMemC2Access.setDest(mainMemECU2);
		mainMemC2Access.setReadLatency<DiscreteValueConstant>({8});
		mainMemC2Access.setWriteLatency<DiscreteValueConstant>({8});
		HwAccessElement externalMemoryAccessECU2core1;
		externalMemoryAccessECU2core1.setDest(mainMemECU1);
		externalMemoryAccessECU2core1.setAccessPath({
			con5, 
			//interconnectECU2, 
			con8init, 
			con10resp , 
			//bus, 
			con9init, 
			con4resp, 
			//interconnectECU1, 
			con3
		});

		core1ECU2->addHWAccessElement(mainMemC2Access);
		core1ECU2->addHWAccessElement(externalMemoryAccessECU2core1);

		HwAccessElement externalMemoryAccessECU2core2;
		externalMemoryAccessECU2core2.setDest(mainMemECU1);
		externalMemoryAccessECU2core2.setAccessPath({
			con6, 
			//interconnectECU2, 
			con8init, 
			con10resp, 
			//bus, 
			con9init, 
			con4resp, 
			//interconnectECU1, 
			con3
		});

		core2ECU2->addHWAccessElement(mainMemC2Access);
		core2ECU2->addHWAccessElement(externalMemoryAccessECU2core2);


		/* Software */

		auto l1_4B = std::make_shared<DataLabel>("L1_4B",4);
		MappingModel::addMemoryMapping(l1_4B, mainMemECU1);
		MappingModel::addMemoryAddress(l1_4B, 0);

		auto l2_1KB = std::make_shared<DataLabel>("L2_1KB", 1024);
		MappingModel::addMemoryMapping(l2_1KB, mainMemECU1);
		MappingModel::addMemoryAddress(l2_1KB, 0);

		auto l3_4B = std::make_shared<DataLabel>("L3_4B", 4);
		MappingModel::addMemoryMapping(l3_4B, mainMemECU2);
		MappingModel::addMemoryAddress(l3_4B, 0);

		auto l4_1KB = std::make_shared<DataLabel>("L4_1KB", 1024);
		MappingModel::addMemoryMapping(l4_1KB, mainMemECU2);
		MappingModel::addMemoryAddress(l4_1KB, 0);

		auto l5_4B = std::make_shared<DataLabel>("L5_1B", 4);
		MappingModel::addMemoryMapping(l5_4B, scratchpad);
		MappingModel::addMemoryAddress(l5_4B, 0);

		auto mode1 = EnumMode::Inst("mode1");
		auto lita = mode1->addLiteral("a");
		auto litb = mode1->addLiteral("b");
		auto litc = mode1->addLiteral("c");
		auto litd = mode1->addLiteral("d");

		auto modelabel = ModeLabel::Inst("modeLabel", mode1, lita);
		auto modelabel2 = ModeLabel::Inst("modeLabel2", mode1, litc);
		auto modelabel3 = ModeLabel::Inst("modeLabel3", mode1, litd);

		MappingModel::addMemoryMapping(modelabel,mainMemECU1);
		MappingModel::addMemoryMapping(modelabel2,mainMemECU1);
		MappingModel::addMemoryMapping(modelabel3,mainMemECU1);

		Switch innerModeSwitch;

		SwitchEntry cORa("case C||A");
		cORa.addCondition<ModeValueCondition>(modelabel2,litc);
		cORa.addCondition<ModeValueCondition>(modelabel2,lita);
		cORa.addActivityGraphItem<ModeLabelAccess>(modelabel2, litd);
		cORa.addActivityGraphItem<Ticks>(DiscreteValueConstant(10));

		innerModeSwitch.addEntry(cORa);

		SwitchEntry entryI2 ("case D");
		entryI2.addCondition<ModeValueCondition>(modelabel2, litd);
		entryI2.addActivityGraphItem<ModeLabelAccess>(modelabel2, litc);
		entryI2.addActivityGraphItem<Ticks>(DiscreteValueConstant(20000000));
		innerModeSwitch.addEntry(entryI2);

		Switch modeSwitch;
		SwitchEntry entry1("case A");
		entry1.addCondition<ModeValueCondition>(modelabel, lita);
		entry1.addActivityGraphItem<LabelAccess>(l4_1KB, MemoryAccessType::READ);
		entry1.addActivityGraphItem<ModeLabelAccess>(modelabel,litb);
		entry1.addActivityGraphItem<Ticks>(DiscreteValueConstant(30000000));
		entry1.addActivityGraphItem(innerModeSwitch);
		modeSwitch.addEntry(entry1);

		SwitchEntry entry2("case B");
		entry2.addCondition<ModeValueCondition>(modelabel, litb);
		entry2.addActivityGraphItem<LabelAccess>(l4_1KB, MemoryAccessType::READ);
		entry2.addActivityGraphItem<ModeLabelAccess>(modelabel, litc);
		entry2.addActivityGraphItem<Ticks>(DiscreteValueConstant(40000000));
		modeSwitch.addEntry(entry2);

		SwitchEntry entry3("case C&D");
		ConditionConjunction conj;
		conj.addCondition<ModeValueCondition>(modelabel, litc);
		conj.addCondition<ModeValueCondition>(modelabel3, litd);
		entry3.addCondition<ConditionConjunction>(conj);
		entry3.addActivityGraphItem<Ticks>(DiscreteValueConstant(50000000));
		entry3.addActivityGraphItem<ModeLabelAccess>(modelabel, lita);

		modeSwitch.addEntry(entry3);

		ProbabilitySwitch pModeSwitch;
		ProbabilitySwitchEntry pEntry1(0.1);
		pEntry1.addActivityGraphItem<Ticks>(DiscreteValueConstant(100));
		pModeSwitch.addEntry(pEntry1);
		ProbabilitySwitchEntry pEntry2(0.9);
		pEntry2.addActivityGraphItem<Ticks>(DiscreteValueConstant(500));
		pEntry2.addActivityGraphItem<Ticks>(DiscreteValueConstant(500));
		pModeSwitch.addEntry(pEntry2);
		ProbabilitySwitchEntry pEntry3(0.4);
		pEntry3.addActivityGraphItem<Ticks>(DiscreteValueConstant(400));
		pModeSwitch.addEntry(pEntry3);

		/*Runnables are Referable, directly create shared_ptrs */
		auto r1 = std::make_shared<Runnable>("R1");
		//r1->addActivityGraphItem<LabelAccess>(l4_1KB, MemoryAccessType::READ);
		r1->addActivityGraphItem<Ticks>(DiscreteValueConstant(500000));
		//r1->addActivityGraphItem<LabelAccess>(l4_1KB, MemoryAccessType::WRITE);
		//r1->addActivityGraphItem<LabelAccess>(l2_1KB, MemoryAccessType::READ);
		//r1->addActivityGraphItem(modeSwitch);

		
		//giving ActivityGraphItems a name for better overview (optional)
		LabelAccess read_L1_4B_100(l1_4B, 100, MemoryAccessType::READ);
		LabelAccess write_L1_4B_100(l1_4B, 100, MemoryAccessType::READ);
		auto r2 = std::make_shared<Runnable>("R2");

		r2->addActivityGraphItem(read_L1_4B_100);
		r2->addActivityGraphItem<Ticks>(DiscreteValueConstant(100000));
		r2->addActivityGraphItem(write_L1_4B_100);


		LabelAccess read_L2_1KB(l2_1KB, MemoryAccessType::READ);
		LabelAccess write_L2_1KB(l2_1KB, MemoryAccessType::WRITE);
		
		auto r3 = std::make_shared<Runnable>("R3");
		r3->addActivityGraphItem(read_L1_4B_100);
		r3->addActivityGraphItem<Ticks>(DiscreteValueConstant(100000));
		r3->addActivityGraphItem(read_L2_1KB);

		
		DiscreteValueWeibullEstimatorsDistribution weibull_R3(DiscreteValueWeibullEstimatorsDistribution::findParameter(20000, 0.1, 10000, 30000), 10000, 30000);
		
		r3->addActivityGraphItem<Ticks>(weibull_R3);
		r3->addActivityGraphItem(write_L2_1KB);

		LabelAccess read_L3_4B_100(l3_4B, 100, MemoryAccessType::READ);
		LabelAccess write_L3_4B_100(l3_4B, 100, MemoryAccessType::WRITE);
		LabelAccess read_L5_4B_50(l5_4B, 50, MemoryAccessType::READ);

		auto r4 = std::make_shared<Runnable>("R4");
		r4->addActivityGraphItem(read_L3_4B_100);
		r4->addActivityGraphItem(read_L5_4B_50);
		r4->addActivityGraphItem(write_L3_4B_100);
		r4->addActivityGraphItem<Ticks>(DiscreteValueConstant(40000));
		r4->addActivityGraphItem<Ticks>(DiscreteValueConstant(60000));
		r4->addActivityGraphItem<Ticks>(DiscreteValueConstant(40000));
		r4->addActivityGraphItem(pModeSwitch);
		r4->addActivityGraphItem(pModeSwitch);
		r4->addActivityGraphItem(pModeSwitch);
		r4->addActivityGraphItem(pModeSwitch);
		r4->addActivityGraphItem(pModeSwitch);
		r4->addActivityGraphItem(pModeSwitch);
		r4->addActivityGraphItem(pModeSwitch);
		r4->addActivityGraphItem(pModeSwitch);
		r4->addActivityGraphItem(pModeSwitch);
		r4->addActivityGraphItem(pModeSwitch);

		auto stimuli_5ms = std::make_shared<FixedPeriodicStimulus>("5ms_Stimuli", 5_ms);
		//auto stimuli_1ms = std::make_shared<FixedPeriodicStimulus>("1ms_Stimuli", 1_ms);

		auto t1 = Task::createTask("T1");
		stimuli_5ms->addTask(t1);
		t1->addActivityGraphItem<RunnableCall>(r1);

		auto t1_2 = Task::createTask("T1_2");
		stimuli_5ms->addTask(t1_2);
		t1_2->addActivityGraphItem<RunnableCall>(r1);

		auto t1_3 = Task::createTask("T1_3");
		stimuli_5ms->addTask(t1_3);
		t1_3->addActivityGraphItem<RunnableCall>(r1);

		std::map<std::shared_ptr<Schedulable>,double> budget;
		budget[t1]= 0.33;
		budget[t1_2]= 0.33;
		budget[t1_3]= 0.4;

		// auto t2 = Task::createTask("T2");
		// stimuli_1ms->addTask(t2);
		// t2->addActivityGraphItem<RunnableCall>(r2);

		// auto t3 = Task::createTask("T3");
		// stimuli_5ms->addTask(t3);
		// t3->addActivityGraphItem<RunnableCall>(r3);

		// auto t4 = Task::createTask("T4");
		// stimuli_1ms->addTask(t4);
		// t4->addActivityGraphItem<RunnableCall>(r4);

		/*  swapped T1 & T4 because of missing path*/
		// auto randomScheduler1 = std::make_shared<RandomScheduler>("RandSched1");
		// randomScheduler1->setExecutionCore(core1ECU1);
		// randomScheduler1->addTaskMapping(t4);

		// auto randomScheduler2 = std::make_shared<RandomScheduler>("RandSched2");
		// randomScheduler2->setExecutionCore(core2ECU1);
		// randomScheduler2->addTaskMapping(t2);

		// auto randomScheduler3 = std::make_shared<RandomScheduler>("RandSched3");
		// randomScheduler3->setExecutionCore(core1ECU2);
		// randomScheduler3->addTaskMapping(t3);

		auto budgetScheduler4 = std::make_shared<BudgetScheduler>("BudgetScheduler4",budget, 1_ms);
		budgetScheduler4->setExecutionCore(core2ECU2);
		budgetScheduler4->addResponsibleCore(core1ECU2);
		budgetScheduler4->addTaskMapping(t1);
		budgetScheduler4->addTaskMapping(t1_3);
		budgetScheduler4->addTaskMapping(t1_2);
		
		scTrace(core1ECU1, "core1ECU1");
		scTrace(core2ECU1, "core2ECU1");
		scTrace(core1ECU2, "core1ECU2");
		scTrace(core2ECU2, "core2ECU2");
		scTrace(con1, "con1");
		scTrace(con2, "con2");
		scTrace(con3, "con3");
		scTrace(con4init, "con4");
		scTrace(con5, "con5");
		scTrace(con6, "con6");
		scTrace(con7, "con7");
		scTrace(con8init, "con8");
		scTrace(con8resp, "con8resp");
		scTrace(con9init, "con9");
		scTrace(con9resp, "con9resp");
		scTrace(con10init, "con10init");
		scTrace(con10resp, "con10resp");
		scTrace(con11, "con11");
		scTrace(con12, "con12");
		scTrace(interconnectECU1, "interconnectECU1");
		scTrace(bus, "bus");
		scTrace(dataCache, "dataCache");
		scTrace(interconnectECU2, "interconnectECU2");
		scTrace(mainMemECU1, "mainMemECU1");
		scTrace(mainMemECU2, "mainMemECU2");

	}
};

int main([[maybe_unused]] int argc,[[maybe_unused]] char *argv[]) {

	SimParam args;
	SimParamParser::parse(argc, argv, args);

	// set seed (from reference trace)
	auto runner = ExampleRunner(0x4ba31fc50647b700);
	EasyloggingConfig::configure("app4mcsim.log", 1, true, true);
	runner.enableTracers(args.tracerNames,args.traceDirectory, false);
	
	try {
		runner.simulate(TimeParameter<std::milli>(200));
	}
	catch (const std::runtime_error& e) {
		std::cerr << e.what();
		return -1;
	}
	return 0;
}