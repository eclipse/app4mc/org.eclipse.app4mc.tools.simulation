/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Behnaz Pourmohseni <behnaz.pourmohseni@de.bosch.com>
 ********************************************************************************
 */
#pragma once
#include "PubSubEventSubscriber.h"
#include <string>
#include "Event.h"

class MockPubSubEventSubscriber : public PubSubEventSubscriber {

public:

	void update(const std::shared_ptr<Event>& event) override {
		std::cout << "received event update: " << event->getName() << " at time " /*<< sc_core::sc_time_stamp()*/ << std::endl;
	}
};