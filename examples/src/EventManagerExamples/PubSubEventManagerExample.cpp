/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Behnaz Pourmohseni <behnaz.pourmohseni@de.bosch.com>
 ********************************************************************************
 */

#include <memory>
#include "APP4MCsim.h"
#include "SimParamParser.h"
#include "MockPubSubEventSubscriber.h"



class PubSubEventManagerExampleRunner : public SimRunner{
public:

	void setup() override{
    /* Hardware */
    Time ECU_Freq_Domain_CycleTime = 10_ns;
    auto pudef = std::make_shared<ProcessingUnitDefinition>("CoreDef");
    auto ECU = modelRoot()->createSubStructure("ECU");

    auto core1 = ECU->createProcessingUnit("Core1", pudef);
    core1->setClockPeriod(ECU_Freq_Domain_CycleTime);

    /* Stimuli */
    auto stimulus1 = std::make_shared<SingleStimulus>("single_stim1", 0_us);
    auto stimulus2 = std::make_shared<SingleStimulus>("single_stim2", 2_us);
    auto stimulus3 = std::make_shared<SingleStimulus>("single_stim3", 1_us);

    /*  Software */
    auto task1 = Task::createTask("Task1");
    stimulus1->addTask(task1);
    stimulus2->addTask(task1);
    task1->addActivityGraphItem<Ticks>(DiscreteValueConstant(150));

    auto task2 = Task::createTask("Task2");
    stimulus3->addTask(task2);
    task2->addActivityGraphItem<Ticks>(DiscreteValueConstant(100));

    //--> process events
    auto event1 = task1->addEvent("t1_activate", ProcessEventType::ACTIVATE);
    auto event2 = task1->addEvent("t1_start", ProcessEventType::START);
    auto event3 = task1->addEvent("t1_terminate", ProcessEventType::TERMINATE);

    auto event4 = task2->addEvent("t2_activate", ProcessEventType::ACTIVATE);
    auto event5 = task2->addEvent("t2_start", ProcessEventType::START);
    auto event6 = task2->addEvent("t2_terminate", ProcessEventType::TERMINATE);

    auto subscriber = std::make_shared<MockPubSubEventSubscriber>();
    EventManager::Inst<PubSubEventManager>()->addEvent(event1, subscriber);
    EventManager::Inst<PubSubEventManager>()->addEvent(event2, subscriber);
    EventManager::Inst<PubSubEventManager>()->addEvent(event3, subscriber);
    EventManager::Inst<PubSubEventManager>()->addEvent(event4, subscriber);
    EventManager::Inst<PubSubEventManager>()->addEvent(event5, subscriber);
    EventManager::Inst<PubSubEventManager>()->addEvent(event6, subscriber);

    /* OS */
    auto scheduler = std::make_shared<PriorityScheduler>("fifoScheduler");
    scheduler->setExecutionCore(core1);
    scheduler->addResponsibleCore(core1);

    /* mapping */
    TaskAllocation ta_task1;
    ta_task1.setSchedulingParameter("priority", 1);
    task1->setTaskAllocation(ta_task1);

    TaskAllocation ta_task2;
    ta_task2.setSchedulingParameter("priority", 0);
    task2->setTaskAllocation(ta_task2);

    scheduler->addTaskMapping(task1);
    scheduler->addTaskMapping(task2);

    
    /* tracing */
      scTrace(core1, "Core1");
  }
};


int main([[maybe_unused]] int argc,[[maybe_unused]] char *argv[]) {

	SimParam args;
	SimParamParser::parse(argc, argv, args);

	auto runner = PubSubEventManagerExampleRunner();
	EasyloggingConfig::configure("app4mcsim.log", 1, true, true);
	runner.enableTracers(args.tracerNames,args.traceDirectory, false);
	
	try {
		runner.simulate(TimeParameter<std::milli>(args.simulationTimeInMs));
	}
	catch (const std::runtime_error& e) {
		std::cerr << e.what();
		return -1;
	}

	return 0;
}
