/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Behnaz Pourmohseni <behnaz.pourmohseni@de.bosch.com>
 ********************************************************************************
 */


#include <memory>
#include "APP4MCsim.h"
#include "SimParamParser.h"

class ExampleRunner : public SimRunner{
public:
void setup() override{

	sc_core::sc_set_time_resolution(1.0, sc_core::SC_NS);
  SimParam args;
  SimParamParser::parse(argc, argv, args);
	START_EASYLOGGINGPP(argc, argv);

	/* Hardware */
	sc_core::sc_time ECU_Freq_Domain_CycleTime(10, sc_core::SC_NS);
	auto pudef = std::make_shared<ProcessingUnitDefinition>("CoreDef");
	HwStructure ECU("ECU");

	auto core1 = ECU->createProcessingUnit("Core1", pudef);
	core1->setClockPeriod(ECU_Freq_Domain_CycleTime);

	/* OS */
	auto scheduler = std::make_shared<PriorityScheduler>("fifoScheduler");
	scheduler->setExecutionCore(core1);
	scheduler->addResponsibleCore(core1);


	/*  Software */
  auto task1 = Task::createTask("Task1");
	task1->addActivityGraphItem<Ticks>(DiscreteValueConstant(150));
  TaskAllocation ta1;
  ta1.setSchedulingParameter("priority", 1);
  task1->setTaskAllocation(ta1);
	scheduler->addTaskMapping(task1);
	auto event1 = task1->addEvent("t1_activate", ProcessEventType::ACTIVATE);
	auto event2 = task1->addEvent("t1_terminate", ProcessEventType::TERMINATE);
  
  //--> externally managed events
  auto externalEvent1 = std::make_shared<Event>("externalEvent1");
  auto externalEvent2 = std::make_shared<Event>("externalEvent2");
  auto externalEvent3 = std::make_shared<Event>("externalEvent3");
  auto externalEvent4 = std::make_shared<Event>("externalEvent4");
  auto externalEvent5 = std::make_shared<Event>("externalEvent5");
 
  /* External event manager */
  EventManager::Inst<ExternalEventManager>()->addEvent(externalEvent1);
  EventManager::Inst<ExternalEventManager>()->addEvent(externalEvent2);
  EventManager::Inst<ExternalEventManager>()->addEvent(externalEvent3);
  EventManager::Inst<ExternalEventManager>()->addEvent(externalEvent4);
  EventManager::Inst<ExternalEventManager>()->addEvent(externalEvent5);

  /* Pub/sub event manager */
  auto subscriber = std::make_shared<MockPubSubEventSubscriber>();
  EventManager::Inst<PubSubEventManager>()->addEvent(event1, subscriber);
  EventManager::Inst<PubSubEventManager>()->addEvent(event2, subscriber);
  EventManager::Inst<PubSubEventManager>()->addEvent(externalEvent1, subscriber);
  EventManager::Inst<PubSubEventManager>()->addEvent(externalEvent2, subscriber);


	/* Stimuli */
  auto stimulus1 = std::make_shared<SingleStimulus>("single_stim1", 0_us);
  stimulus1->addTask(task1);

  auto event_stim1 = std::make_shared<EventStimulus>("eventStimulus1", externalEvent1);
  auto event_stim2 = std::make_shared<EventStimulus>("eventStimulus2", externalEvent2);
  auto event_stim3 = std::make_shared<EventStimulus>("eventStimulus3", externalEvent3);
  auto event_stim4 = std::make_shared<EventStimulus>("eventStimulus4", externalEvent4);
  auto event_stim5 = std::make_shared<EventStimulus>("eventStimulus5", externalEvent5);

  
	VLOG(0) << "simulation model created";

	/* tracing */
std::string vcdOutputFile = args.traceDirectory + std::string("/trace");
	sc_core::sc_trace_file *tf = sc_core::sc_create_vcd_trace_file(vcdOutputFile.c_str());
  	scTrace(core1, "Core1");


  //finalization callbacks
  try{
    RequiresCallbackManager::Inst().applyPreSim();
  } catch (std::exception &e){
    RequiresCallbackManager::Inst().applyPostSim();
    const char *msg  = e.what();
    VLOG(0) << msg ;
    return -1;
  }

	try {
		VLOG(0) << "starting simulation";
		sc_core::sc_start(2000, sc_core::SC_MS);
	} catch (sc_core::sc_report &e) {
    RequiresCallbackManager::Inst().applyPostSim();
    const char *file = e.get_file_name();
    const int line   = e.get_line_number();
    const char *msg  = e.get_msg();
    VLOG(0) << msg << "\nin file: " << file << "\nline: " << line;
    return -1;
  } catch (std::exception &e){
    RequiresCallbackManager::Inst().applyPostSim();
    const char *msg  = e.what();
    VLOG(0) << msg ;
    return -1;
  }

  RequiresCallbackManager::Inst().applyPostSim();
	sc_core::sc_close_vcd_trace_file(tf);

	VLOG(0) << "simulation done";
	return 0;
}
