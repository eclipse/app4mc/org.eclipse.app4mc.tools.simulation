/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#include <string>
#include <iostream>
#include <fstream>
#include <exception>
#include "APP4MCsim.h"
#include <gtest/gtest.h>


void compareEnding(const std::string& a, const std::string& b){

	if (a.size() == b.size()){
		ASSERT_EQ(a,b);
	} else {
		if (a.size() > b.size()){
			const std::string& slong = a;
			const std::string& sshort = b;
			std::string endingLong = slong.substr(slong.size()-sshort.size(), slong.size()-1);
			ASSERT_EQ(endingLong, sshort);
		} else if (a.size() < b.size()){
			const std::string& slong = b;
			const std::string& sshort = a;
			std::string endingLong = slong.substr(slong.size()-sshort.size(), slong.size()-1);
			ASSERT_EQ(endingLong, sshort);
		}

	}
	
}


TEST(EasyloggingConfigTest, WriteLogAndStdOut){	
	const std::string testString = "success";
	const std::string logfile = "log_WriteLogAndStdOut.log";
	if (std::filesystem::exists(logfile)){
		//clean slate
		std::filesystem::remove(logfile.c_str());
	}
	
	//capture stdout
	testing::internal::CaptureStdout();
	
	//log something
	EasyloggingConfig::configure(logfile, 1, true, true);
	EasyloggingConfig::msg(testString, 1);
	
	//eval stdout
	std::string fromStdout = testing::internal::GetCapturedStdout();
	fromStdout.pop_back(); // remove \n
	compareEnding(fromStdout, testString);

	// //read back log file
	std::ifstream fLog(logfile);
	ASSERT_EQ(fLog.is_open(), true);
	std::string fromFile;
	std::getline(fLog, fromFile);
	compareEnding(fromFile, testString);
	fLog.close();
}

TEST(EasyloggingConfigTest, WriteVerbosityTooLow){	
	const std::string testString = "failed";
	const std::string logfile = "log_WriteVerbosityTooLow.log";

	//capture stdout
	testing::internal::CaptureStdout();
	
	//log something
	EasyloggingConfig::configure(logfile, 0, true, true);
	EasyloggingConfig::msg(testString, 1); //filtered by verbosity 
	
	//eval stdout
	std::string fromStdout = testing::internal::GetCapturedStdout(); //expect empty
	ASSERT_EQ(fromStdout, "");

	// //read back log file
	std::ifstream fLog(logfile);
	ASSERT_EQ(fLog.is_open(), true);
	std::string fromFile;
	std::getline(fLog, fromFile);
	ASSERT_EQ(fromFile, "");
	fLog.close();
}

TEST(EasyloggingConfigTest, GloballyDisabled){	
	const std::string testString = "failed";
	const std::string logfile = "log_GloballyDisabled.log";
	if (std::filesystem::exists(logfile)){
		//clean slate
		std::filesystem::remove(logfile.c_str());
	}

	//capture stdout
	testing::internal::CaptureStdout();
	
	//log something
	EasyloggingConfig::configure(logfile, 1, true, true);
	EasyloggingConfig::disable();
	EasyloggingConfig::msg(testString, 0); 
	
	//eval stdout
	std::string fromStdout = testing::internal::GetCapturedStdout(); //expect empty
	ASSERT_EQ(fromStdout, "");

	// //read back log file
	std::ifstream fLog(logfile);
	ASSERT_EQ(fLog.is_open(), true);
	std::string fromFile;
	std::getline(fLog, fromFile);
	ASSERT_EQ(fromFile, "");
	fLog.close();
}

TEST(EasyloggingConfigTest, EnableEitherOr){	

	const std::string testStringStdout = "onlyStdout";
	const std::string testStringFile = "onlyInFile";
	const std::string logfile = "log_EnableEitherOr.log";
	if (std::filesystem::exists(logfile)){
		//clean slate
		std::filesystem::remove(logfile.c_str());
	}

	//Enable only logging to stdout	------------------------------------
	EasyloggingConfig::configure(logfile, 1, true, false);
	//capture stdout
	testing::internal::CaptureStdout();
	//log message
	EasyloggingConfig::msg(testStringStdout, 0);
	
	//eval stdout
	std::string fromStdout1 = testing::internal::GetCapturedStdout();
	fromStdout1.pop_back(); // remove \n
	compareEnding(fromStdout1, testStringStdout);

	//read back log file
	std::ifstream fLog1(logfile);
	ASSERT_EQ(fLog1.is_open(), true);
	std::string fromFile1;
	std::getline(fLog1, fromFile1);
	ASSERT_EQ(fromFile1, "");

	fLog1.close();

	//Enable only file logging -----------------------------------------
	EasyloggingConfig::configure(logfile, 1, false, true);
	//capture stdout
	testing::internal::CaptureStdout();
	// log message
	EasyloggingConfig::msg(testStringFile, 0); 

	//eval stdout
	std::string fromStdout2 = testing::internal::GetCapturedStdout(); //expect empty
	ASSERT_EQ(fromStdout2, "");

	// ead back log file
	std::ifstream fLog2(logfile);
	ASSERT_EQ(fLog2.is_open(), true);
	std::string fromFile2;
	std::getline(fLog2, fromFile2);
	compareEnding(fromFile2, testStringFile);

	fLog2.close();

}



int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);

  return RUN_ALL_TESTS();
}