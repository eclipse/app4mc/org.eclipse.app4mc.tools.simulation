/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */



#include <memory>
#include "APP4MCsim.h"
#include "SimParamParser.h"

class ExampleRunner : public SimRunner{
public:
	
	ExampleRunner(uint64_t seed):SimRunner(seed){}
	
	void setup() override{
		/* ECU1 */
		Time ECU1_Freq_Domain_CycleTime = 1_us;
		auto pudef = std::make_shared<ProcessingUnitDefinition>("ECU-CPU");

		auto ECU1 = modelRoot()->createSubStructure("ECU1");

		auto core1ECU1 = ECU1->createProcessingUnit("Core1ECU1", pudef);


		core1ECU1->addInitPort("P1");
		core1ECU1->addInitPort("SP_P");
		core1ECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);


		auto core2ECU1 = ECU1->createProcessingUnit("Core2ECU1", pudef);
		core2ECU1->addInitPort("P1");
		core2ECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);

		//TODO fix size argument
		auto mainMemECU1 = ECU1->createMemory("MainMemECU1", 1 * 1024 * 1024* 1024);
		mainMemECU1->addTargetPort("P1");
		mainMemECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);
		mainMemECU1->setAccessLatency<DiscreteValueWeibullEstimatorsDistribution>({DiscreteValueWeibullEstimatorsDistribution::findParameter(8,0.1,4,32),4,32});

		auto interconnectECU1 = ECU1->createConnectionHandler("InterconnectECU1");
		
		interconnectECU1->addTargetPort("P1");
		interconnectECU1->addTargetPort("P2");
		interconnectECU1->addInitPort("P3");
		interconnectECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);
		interconnectECU1->setReadLatency<DiscreteValueConstant>({5});
		interconnectECU1->setWriteLatency<DiscreteValueConstant>({3});

		auto dataCache = ECU1->createCacheStatistic("DataCache", 0.9);
		dataCache->addInitPort("P2");
		dataCache->addTargetPort("P1");
		dataCache->setLineSize(64);
		dataCache->setSize(256 * 1024);
		dataCache->setClockPeriod(ECU1_Freq_Domain_CycleTime);

		auto scratchpad = ECU1->createMemory("Scratchpad", 2 * 1024 * 1024);
		scratchpad->addTargetPort("P1");
		scratchpad->setAccessLatency<DiscreteValueConstant>({1});
		scratchpad->setClockPeriod(ECU1_Freq_Domain_CycleTime);


		/* Connections */
		auto con1 = ECU1->createConnection("con1");
		auto con2 = ECU1->createConnection("con2");
		auto con3 = ECU1->createConnection("con3");
		auto con11 = ECU1->createConnection("con11");
		auto con12 = ECU1->createConnection("con12");
		
		core1ECU1->bindConnection(con1, "P1");
		interconnectECU1->bindConnection(con1, "P1");

		core2ECU1->bindConnection(con2, "P1");
		dataCache->bindConnection(con2, "P1");

		interconnectECU1->bindConnection(con3, "P3");
		mainMemECU1->bindConnection(con3, "P1");

		core1ECU1->bindConnection(con11, "SP_P");
		scratchpad->bindConnection(con11, "P1");

		dataCache->bindConnection(con12, "P2");
		interconnectECU1->bindConnection(con12, "P2");

		/* HW_access_elems */

		HwAccessElement mainMemC1Access;
		mainMemC1Access.setDest(mainMemECU1);
		mainMemC1Access.setAccessPath({
			con1, 
			//interconnectECU1, 
			con3
		});

		HwAccessElement scratchpadAccess;
		scratchpadAccess.setDest(scratchpad);

		core1ECU1->addHWAccessElement(mainMemC1Access);
		core1ECU1->addHWAccessElement(scratchpadAccess);

		HwAccessElement mainMemC2Access;
		mainMemC2Access.setAccessPath({
			con2, 
			//interconnectECU1, 
			con3
		});
		core2ECU1->addHWAccessElement(mainMemC2Access);

		/* Software */

		auto osEvent1 = std::make_shared<OsEvent>("OsEvent1");
		auto osEvent2 = std::make_shared<OsEvent>("OsEvent2");

		WaitEvent wait1( WaitEventType::AND);
		wait1.addOsEventToMask(osEvent1);
		wait1.addOsEventToMask(osEvent2);
		
		WaitEvent wait2( WaitEventType::OR);
		wait2.addOsEventToMask(osEvent1);
		wait2.addOsEventToMask(osEvent2);
		

		ClearEvent clear1;
		clear1.addOsEventToMask(osEvent1);
		clear1.addOsEventToMask(osEvent2);
		clear1.setOverhead(10);


		auto stimuli_5ms = std::make_shared<FixedPeriodicStimulus>("5ms_Stimuli", 5_ms);
		auto stimuli_6ms = std::make_shared<FixedPeriodicStimulus>("6ms_Stimuli", 6_ms);

		auto t1 = Task::createTask("T1");
		auto t2 = Task::createTask("T2");

		SetEvent setEvent1(t1);
		setEvent1.addOsEventToMask(osEvent1);
		setEvent1.setOverhead(10);

		SetEvent setEvent2(t1);
		setEvent2.addOsEventToMask(osEvent2);
		setEvent2.setOverhead(10);


		stimuli_5ms->addTask(t1);

		auto mode1 = EnumMode::Inst("mode1");
		auto lita = mode1->addLiteral("a");
		auto litb = mode1->addLiteral("b");
		
		auto modelabel = ModeLabel::Inst("modeLabel", mode1, lita);
		MappingModel::addMemoryMapping(modelabel,mainMemECU1);

		Switch mswitch;

		SwitchEntry mode_a("mode a");
		mode_a.addCondition<ModeValueCondition>(modelabel,lita);
		mode_a.addActivityGraphItem(wait1);
		mode_a.addActivityGraphItem(clear1);
		mode_a.addActivityGraphItem<ModeLabelAccess>(modelabel,litb);

		SwitchEntry mode_b("mode b");
		mode_b.addCondition<ModeValueCondition>(modelabel,litb);
		mode_b.addActivityGraphItem(wait2);
		mode_b.addActivityGraphItem(clear1);
		mode_b.addActivityGraphItem<ModeLabelAccess>(modelabel,lita);

		mswitch.addEntry(mode_a);
		mswitch.addEntry(mode_b);

		t1->addActivityGraphItem<Ticks>(DiscreteValueConstant(300));
		t1->addActivityGraphItem(mswitch);

		stimuli_6ms->addTask(t2);
		t2->addActivityGraphItem<Ticks>(DiscreteValueConstant(300));
		t2->addActivityGraphItem(setEvent1);
		t2->addActivityGraphItem<Ticks>(DiscreteValueConstant(300));
		t2->addActivityGraphItem(setEvent2);
		
		/*  swapped T1 & T4 because of missing path*/
		auto randomScheduler1 = std::make_shared<RandomScheduler>("RandSched1");
		randomScheduler1->setExecutionCore(core1ECU1);
		randomScheduler1->addTaskMapping(t1);

		auto randomScheduler2 = std::make_shared<RandomScheduler>("RandSched2");
		randomScheduler2->setExecutionCore(core2ECU1);
		randomScheduler2->addTaskMapping(t2);

		scTrace(core1ECU1, "core1ECU1");
		scTrace(core2ECU1, "core2ECU1");
		scTrace(con3, "con3");
		scTrace(interconnectECU1, "interconnectECU1");
		scTrace(dataCache, "dataCache");
		scTrace(mainMemECU1, "mainMemECU1");
	}
};

int main([[maybe_unused]] int argc,[[maybe_unused]] char *argv[]) {

	SimParam args;
	SimParamParser::parse(argc, argv, args);

	auto runner = ExampleRunner(0xb236ee1c09b1c533);
	EasyloggingConfig::configure("app4mcsim.log", 1, true, true);
	runner.enableTracers(args.tracerNames,args.traceDirectory, false);
	
	try {
		runner.simulate(TimeParameter<std::milli>(2000));
	}
	catch (const std::runtime_error& e) {
		std::cerr << e.what();
		return -1;
	}
	return 0;
}