/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */


#include <memory>
#include "APP4MCsim.h"
#include "SimParamParser.h"

class ExampleRunner : public SimRunner{
public:
void setup() override{
	/* ECU1 */

	/* 200Mhz*/
	Time ECU1_Freq_Domain_CycleTime = 10_ns;
	auto pudef = std::make_shared<ProcessingUnitDefinition>("ECU-CPU");
	auto ECU1 = modelRoot()->createSubStructure("ECU1");

	auto core1ECU1 = ECU1->createProcessingUnit("Core1ECU1",pudef);
	core1ECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);

	auto mainMemECU1 = ECU1->createMemory("MainMemECU1", 1 * 1024 * 1024 * 1024);
	mainMemECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);
	mainMemECU1->setAccessLatency<DiscreteValueConstant>({100});


	/* HW_access_elems */
	HwAccessElement mainMemC1Access;
	mainMemC1Access.setDest(mainMemECU1);
	mainMemC1Access.setReadLatency<DiscreteValueConstant>({10});
	mainMemC1Access.setWriteLatency<DiscreteValueConstant>({10});
	core1ECU1->addHWAccessElement(mainMemC1Access);

	/* Software */
	auto modelabel1 = ModeLabel::Inst("modeLabel1", 1, 100);
	auto modelabel2 = ModeLabel::Inst("modeLabel2", 4);

	MappingModel::addMemoryMapping(modelabel1,mainMemECU1);
	MappingModel::addMemoryMapping(modelabel2,mainMemECU1);


	auto ra = std::make_shared<Runnable>("R_a");
	ra->addActivityGraphItem<Ticks>(DiscreteValueConstant(1000000));
	auto rb = std::make_shared<Runnable>("R_b");
	rb->addActivityGraphItem<Ticks>(DiscreteValueConstant(1000000));
	auto rc = std::make_shared<Runnable>("R_c");
	rc->addActivityGraphItem<Ticks>(DiscreteValueConstant(1000000));
	auto rDandD = std::make_shared<Runnable>("R_DandD");
	rDandD->addActivityGraphItem<Ticks>(DiscreteValueConstant(1000000));
	auto rDandNotD = std::make_shared<Runnable>("R_DandNotD");
	rDandNotD->addActivityGraphItem<Ticks>(DiscreteValueConstant(1000000));

	auto rError = std::make_shared<Runnable>("R_ERROR");
	rError->addActivityGraphItem<Ticks>(DiscreteValueConstant(100000000));
	
	auto rDone = std::make_shared<Runnable>("R_DONE_MODESWITCHES_NUMERIC_VALUE");
	rDone->addActivityGraphItem<Ticks>(DiscreteValueConstant(100000000));


	Switch modeSwitch;
	SwitchEntry caseError("case error (default)");
	SwitchEntry caseA("case A");
	SwitchEntry caseBorC("case B || C");
	SwitchEntry caseDandD("case D&D");
	SwitchEntry caseDandNotD("case D & !D");
	SwitchEntry caseDone("case done");

//case default --> ERROR
	caseError.addActivityGraphItem<ModeLabelAccess>(modelabel1, 1000);
	caseError.addActivityGraphItem<ModeLabelAccess>(modelabel2, 1000);
	caseError.addActivityGraphItem<RunnableCall>(rError);

//case done
	ConditionConjunction conjunctionDone;
	conjunctionDone.addCondition<ModeValueCondition>(modelabel2, 10);
	conjunctionDone.addCondition<ModeValueCondition>(modelabel2, 10);
	caseDone.addCondition<ConditionConjunction>(conjunctionDone);
	caseDone.addActivityGraphItem<RunnableCall>(rDone);

//case a
	caseA.addCondition<ModeValueCondition>(modelabel1, 1);
	caseA.addActivityGraphItem<ModeLabelAccess>(modelabel1, 2);
	caseA.addActivityGraphItem<RunnableCall>(ra);

//case b || c --> nested case b, nested case c
	caseBorC.addCondition<ModeValueCondition>(modelabel1, 2);
	caseBorC.addCondition<ModeValueCondition>(modelabel1, 3);
	//innermode switch	
	Switch innerModeSwitch;
	SwitchEntry nestedCaseB ("case B (nested B||C)");
	//nestedCaseB.addCondition<ModeNumeric>(modelabel1, litb);
	nestedCaseB.addCondition<ModeValueCondition>(modelabel1, 2);
	nestedCaseB.addActivityGraphItem<RunnableCall>(rb);
	nestedCaseB.addActivityGraphItem<ModeLabelAccess>(modelabel1, 10, ModeAccessType::INCREMENT);  //use increment by 10
	nestedCaseB.addActivityGraphItem<ModeLabelAccess>(modelabel1, 9, ModeAccessType::DECREMENT);  //use decrement by 9 --> label should be +1 effectively
	innerModeSwitch.addEntry(nestedCaseB);
	SwitchEntry nestedCaseC ("case C (nested B||C)");
	nestedCaseC.addCondition<ModeValueCondition>(modelabel1, 3);
	nestedCaseC.addActivityGraphItem<RunnableCall>(rc);
	nestedCaseC.addActivityGraphItem<ModeLabelAccess>(modelabel1, 4); 
	innerModeSwitch.addEntry(nestedCaseC);
	SwitchEntry nestedCaseDefault ("default(nested B||C)");
	nestedCaseDefault.addActivityGraphItem<RunnableCall>(rError);
	innerModeSwitch.setDefaultEntry(nestedCaseDefault);
	//add inner mode switch as activity item
	caseBorC.addActivityGraphItem<Switch>(innerModeSwitch);

//case modelabel1 == D && modelabel2 == D
	ConditionConjunction conj_caseDandD;// = caseDandD.createConjunction();
	conj_caseDandD.addCondition<ModeValueCondition>(modelabel1, 4);
	conj_caseDandD.addCondition<ModeValueCondition>(modelabel2, 4);
	caseDandD.addCondition<ConditionConjunction>(conj_caseDandD);
	caseDandD.addActivityGraphItem<RunnableCall>(rDandD);
	caseDandD.addActivityGraphItem<ModeLabelAccess>(modelabel2,  1);

//case modelabel1 == D && modelabel2 != D
	ConditionConjunction conj_caseCandNotD;// = caseDandNotD.createConjunction();
	conj_caseCandNotD.addCondition<ModeValueCondition>(modelabel1, 4);
	conj_caseCandNotD.addCondition<ModeValueCondition>(modelabel2, 4, RelationalOperator::LESS_THAN );
	caseDandNotD.addCondition<ConditionConjunction>(conj_caseCandNotD);
	caseDandNotD.addActivityGraphItem<RunnableCall>(rDandNotD);
	caseDandNotD.addActivityGraphItem<ModeLabelAccess>(modelabel1,  10); //done
	caseDandNotD.addActivityGraphItem<ModeLabelAccess>(modelabel2,  10); //done

//add cases at inverse flow a->b->c->d
	modeSwitch.setDefaultEntry(caseError);
	modeSwitch.addEntry(caseDone);
	modeSwitch.addEntry(caseDandNotD);
	modeSwitch.addEntry(caseDandNotD);
	modeSwitch.addEntry(caseDandD);
	modeSwitch.addEntry(caseBorC);
	modeSwitch.addEntry(caseA);
	
	/*Runnables are Referable, directly create shared_ptrs */
	auto r_dispatch = std::make_shared<Runnable>("R_dispatch");
	r_dispatch->addActivityGraphItem<Ticks>(DiscreteValueConstant(100000));
	r_dispatch->addActivityGraphItem<Switch>(modeSwitch);


//Stimuli
	auto stimuli_5ms = std::make_shared<FixedPeriodicStimulus>("5ms_Stimuli", 5_ms);

//Software Model  (Tasks)
	auto t1 = Task::createTask("T1");
	stimuli_5ms->addTask(t1);
	t1->addActivityGraphItem<RunnableCall>(r_dispatch);
	TaskAllocation ta;
	ta.setSchedulingParameter("priority", 0);
	t1->setTaskAllocation(ta);

//OS
	auto priorityScheduler = std::make_shared<PriorityScheduler>("PrioSched");
	priorityScheduler->setExecutionCore(core1ECU1);
	priorityScheduler->addTaskMapping(t1);

//Trace
	scTrace(core1ECU1, "Core1ECU1");
	scTrace(mainMemECU1, "MainMemECU1");
}};

int main([[maybe_unused]] int argc,[[maybe_unused]] char *argv[]) {

	SimParam args;
	SimParamParser::parse(argc, argv, args);

	auto runner = ExampleRunner();
	EasyloggingConfig::configure("app4mcsim.log", 1, true, true);
	runner.enableTracers(args.tracerNames,args.traceDirectory, false);
	
	try {
		runner.simulate(TimeParameter<std::milli>(200));
	}
	catch (const std::runtime_error& e) {
		std::cerr << e.what();
		return -1;
	}
	return 0;
}