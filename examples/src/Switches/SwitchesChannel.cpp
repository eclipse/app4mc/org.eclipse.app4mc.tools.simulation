/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#include <memory>
#include "APP4MCsim.h"
#include "SimParamParser.h"

class ExampleRunner : public SimRunner{
public:
void setup() override{
	/* ECU1 */

	/* 200Mhz*/
	Time ECU1_Freq_Domain_CycleTime = 10_ns;
	auto pudef = std::make_shared<ProcessingUnitDefinition>("ECU-CPU");
	auto ECU1 = modelRoot()->createSubStructure("ECU1");

	auto core1ECU1 = ECU1->createProcessingUnit("Core1ECU1",pudef);
	core1ECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);

	auto mainMemECU1 = ECU1->createMemory("MainMemECU1", 1 * 1024 * 1024 * 1024);
	mainMemECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);
	mainMemECU1->setAccessLatency<DiscreteValueConstant>({100});


	/* HW_access_elems */
	HwAccessElement mainMemC1Access;
	mainMemC1Access.setDest(mainMemECU1);
	mainMemC1Access.setReadLatency<DiscreteValueConstant>({10});
	mainMemC1Access.setWriteLatency<DiscreteValueConstant>({10});
	core1ECU1->addHWAccessElement(mainMemC1Access);

	/* Software */

	auto channel1 = std::make_shared<Channel> ("channel1", 64, 0, 0);
	auto channel2 = std::make_shared<Channel> ("channel2", 64, 1, 0);

	MappingModel::addMemoryMapping(channel1,mainMemECU1);
	MappingModel::addMemoryMapping(channel2,mainMemECU1);


	auto ra = std::make_shared<Runnable>("R_a");
	ra->addActivityGraphItem<Ticks>(DiscreteValueConstant(100));
	auto rb = std::make_shared<Runnable>("R_b");
	rb->addActivityGraphItem<Ticks>(DiscreteValueConstant(200));
	auto rc = std::make_shared<Runnable>("R_c");
	rc->addActivityGraphItem<Ticks>(DiscreteValueConstant(400));
	auto rd = std::make_shared<Runnable>("R_d");
	rd->addActivityGraphItem<Ticks>(DiscreteValueConstant(800));
	// auto rDandNotD = std::make_shared<Runnable>("R_DandNotD");
	// rDandNotD->addActivityGraphItem<Ticks>(DiscreteValueConstant(1000000));

	auto rError = std::make_shared<Runnable>("R_ERROR");
	rError->addActivityGraphItem<Ticks>(DiscreteValueConstant(std::numeric_limits<ELong>::max()));
	
	auto rDone = std::make_shared<Runnable>("R_DONE");
	rDone->addActivityGraphItem<Ticks>(DiscreteValueConstant(std::numeric_limits<ELong>::max()));


	Switch modeSwitch;
	SwitchEntry caseError("case error (default)");
	SwitchEntry caseA_eq0_or_eq1("case A: channel1==0 or channel2==1");
	SwitchEntry caseB_gr0_and_ls1("case B: channel1 > 0 and channel2 < 1");
	SwitchEntry caseC_eq1_and_eq1("case C: channel1 ==1 and channel2==1");
	SwitchEntry caseD_ch1eq1_and_ch2notEq1_and_ch2gr1("case D: channel1==1 and channel2 != 1 and channel2>1");
	SwitchEntry caseDone("case done");

//case default --> ERROR
	caseError.addActivityGraphItem<RunnableCall>(rError);

//case a: increment Channel1
	caseA_eq0_or_eq1.addCondition<ChannelFillCondition>(channel1, 0 /*, RelationalOperator::EQUAL*/); //true
	caseA_eq0_or_eq1.addCondition<ChannelFillCondition>(channel2, 2 /*, RelationalOperator::EQUAL*/); //false
	caseA_eq0_or_eq1.addActivityGraphItem<ChannelWrite>(channel1, 1); //new context: channel1=1 , channel2=1
	caseA_eq0_or_eq1.addActivityGraphItem<RunnableCall>(ra); 

//case b: channel1 > 0 
	ConditionConjunction conj_caseB_gr0_and_ls1;
	conj_caseB_gr0_and_ls1.addCondition<ChannelFillCondition>(channel1, 0, RelationalOperator::GREATER_THAN);
	caseB_gr0_and_ls1.addCondition<ConditionConjunction>(conj_caseB_gr0_and_ls1);
	caseB_gr0_and_ls1.addActivityGraphItem<ChannelWrite>(channel1, 9); //new context: channel1=10 
	caseB_gr0_and_ls1.addActivityGraphItem<ChannelWrite>(channel2, 9); //new context: channel2=10
	caseB_gr0_and_ls1.addActivityGraphItem<RunnableCall>(rb); 

//case c: channel1 == 10  && channel2 == 10
	ConditionConjunction conj_caseC_eq1_and_eq1;
	conj_caseC_eq1_and_eq1.addCondition<ChannelFillCondition>(channel1, 10, RelationalOperator::EQUAL);
	conj_caseC_eq1_and_eq1.addCondition<ChannelFillCondition>(channel2, 10, RelationalOperator::EQUAL);
	caseC_eq1_and_eq1.addCondition<ConditionConjunction>(conj_caseC_eq1_and_eq1);
	caseC_eq1_and_eq1.addActivityGraphItem<ChannelWrite>(channel2, 20); //new context: channel1=10 channel2=30
	caseC_eq1_and_eq1.addActivityGraphItem<RunnableCall>(rc); 

//case d: channel1 == 10  && channel2 != 10 && channel2 >29 && channel2 < 31
	ConditionConjunction conj_caseD_ch1eq1_and_ch2notEq1_and_ch2gr1 ;
	conj_caseD_ch1eq1_and_ch2notEq1_and_ch2gr1.addCondition<ChannelFillCondition>(channel1, 1, RelationalOperator::GREATER_THAN);
	conj_caseD_ch1eq1_and_ch2notEq1_and_ch2gr1.addCondition<ChannelFillCondition>(channel2, 10, RelationalOperator::NOT_EQUAL);
	conj_caseD_ch1eq1_and_ch2notEq1_and_ch2gr1.addCondition<ChannelFillCondition>(channel2, 29, RelationalOperator::GREATER_THAN);
	conj_caseD_ch1eq1_and_ch2notEq1_and_ch2gr1.addCondition<ChannelFillCondition>(channel2, 31, RelationalOperator::LESS_THAN);
	caseD_ch1eq1_and_ch2notEq1_and_ch2gr1.addCondition<ConditionConjunction>(conj_caseD_ch1eq1_and_ch2notEq1_and_ch2gr1);
	caseD_ch1eq1_and_ch2notEq1_and_ch2gr1.addActivityGraphItem<ChannelRead>(channel1, 10); //new context: channel1=0
	caseD_ch1eq1_and_ch2notEq1_and_ch2gr1.addActivityGraphItem<ChannelRead>(channel2, 30); //new context: channel1=0
	caseD_ch1eq1_and_ch2notEq1_and_ch2gr1.addActivityGraphItem<RunnableCall>(rd);

//case done: channel1 == 0  && channel2 == 0
	ConditionConjunction conj_caseDone ;
	conj_caseDone.addCondition<ChannelFillCondition>(channel1, 0, RelationalOperator::EQUAL);
	conj_caseDone.addCondition<ChannelFillCondition>(channel2, 0, RelationalOperator::EQUAL);
	caseDone.addCondition<ConditionConjunction>(conj_caseDone);
	caseDone.addActivityGraphItem<RunnableCall>(rDone);

//add cases at inverse flow a->b->c->d
	modeSwitch.setDefaultEntry(caseError);
	modeSwitch.addEntry(caseDone);
	modeSwitch.addEntry(caseD_ch1eq1_and_ch2notEq1_and_ch2gr1);
	modeSwitch.addEntry(caseD_ch1eq1_and_ch2notEq1_and_ch2gr1);
	modeSwitch.addEntry(caseC_eq1_and_eq1);
	modeSwitch.addEntry(caseB_gr0_and_ls1);
	modeSwitch.addEntry(caseA_eq0_or_eq1);
	
	/*Runnables are Referable, directly create shared_ptrs */
	auto r_dispatch = std::make_shared<Runnable>("R_dispatch");
	r_dispatch->addActivityGraphItem<Ticks>(DiscreteValueConstant(10));
	r_dispatch->addActivityGraphItem<Switch>(modeSwitch);


//Stimuli
	auto stimuli_5ms = std::make_shared<FixedPeriodicStimulus>("5ms_Stimuli", 5_ms);

//Software Model  (Tasks)
	auto t1 = Task::createTask("T1");
	stimuli_5ms->addTask(t1);
	t1->addActivityGraphItem<RunnableCall>(r_dispatch);
	TaskAllocation ta;
	ta.setSchedulingParameter("priority", 0);
	t1->setTaskAllocation(ta);

//OS
	auto priorityScheduler = std::make_shared<PriorityScheduler>("PrioSched");
	priorityScheduler->setExecutionCore(core1ECU1);
	priorityScheduler->addTaskMapping(t1);

//Trace
	scTrace(core1ECU1, "Core1ECU1");
	scTrace(mainMemECU1, "MainMemECU1");

}};

int main([[maybe_unused]] int argc,[[maybe_unused]] char *argv[]) {

	SimParam args;
	SimParamParser::parse(argc, argv, args);

	auto runner = ExampleRunner();
	EasyloggingConfig::configure("app4mcsim.log", 1, true, true);
	runner.enableTracers(args.tracerNames,args.traceDirectory, false);
	
	try {
		runner.simulate(TimeParameter<std::milli>(200));
	}
	catch (const std::runtime_error& e) {
		std::cerr << e.what();
		return -1;
	}
	return 0;
}