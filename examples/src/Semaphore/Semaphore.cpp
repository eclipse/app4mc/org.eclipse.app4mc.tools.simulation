/**
 ********************************************************************************
 * Copyright (c) 2021 University of Rostock and others
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */


#include <memory>
#include "APP4MCsim.h"
#include "SimParamParser.h"

class ExampleRunner : public SimRunner{
public:
  void setup() override{
    /* Hardware */
    /* 200Mhz*/
    Time ECU1_Freq_Domain_CycleTime = 10_ns;
    auto pudef = std::make_shared<ProcessingUnitDefinition>("ECU-CPU");
    auto ECU1 = modelRoot()->createSubStructure("ECU1");

    auto core1ECU1          = ECU1->createProcessingUnit("Core1ECU1", pudef);
    core1ECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);
    auto core2ECU1          = ECU1->createProcessingUnit("Core1ECU2", pudef);
    core2ECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);
    auto core3ECU1          = ECU1->createProcessingUnit("Core1ECU3", pudef);
    core3ECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);

    auto mainMemECU1          = ECU1->createMemory("MainMemECU1", 1 * 1024 * 1024 * 1024);
    mainMemECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);
    mainMemECU1->setAccessLatency<DiscreteValueConstant>({100});

    /* HW_access_elems */
    HwAccessElement mainMemC1Access;
    mainMemC1Access.setDest(mainMemECU1);
    mainMemC1Access.setReadLatency<DiscreteValueConstant>({10});
    mainMemC1Access.setWriteLatency<DiscreteValueConstant>({10});
    core1ECU1->addHWAccessElement(mainMemC1Access);
    core2ECU1->addHWAccessElement(mainMemC1Access);
    core3ECU1->addHWAccessElement(mainMemC1Access);

    /* Software */
    auto sem          = Semaphore::createSemaphore("sem", 1, 1, SemaphoreType::CountingSemaphore);

    auto counterLabel = ModeLabel::Inst("CounterVar", 0);
    auto loopCounter1 = ModeLabel::Inst("loop1", 10);
    auto loopCounter2 = ModeLabel::Inst("loop2", 10);


    // Stimuli
    auto stimuli_50ms = std::make_shared<FixedPeriodicStimulus>("50ms_Stimuli", 50_ms);

    // Software Model  (Tasks)
    WhileLoop loop;
    loop.addActivityGraphItem<SemaphoreAccess>(sem, SemaphoreAccessType::request, WaitingBehaviour::active);
    loop.addActivityGraphItem<Ticks>(DiscreteValueConstant{10});
    loop.addActivityGraphItem<ModeLabelAccess>(counterLabel, 1, ModeAccessType::INCREMENT);
    loop.addActivityGraphItem<SemaphoreAccess>(sem, SemaphoreAccessType::release, WaitingBehaviour::active);
    loop.addActivityGraphItem<Ticks>(DiscreteValueConstant(10));

    WhileLoop loop1 = loop;
    WhileLoop loop2 = loop;

    loop1.addActivityGraphItem<ModeLabelAccess>(loopCounter1, 1, ModeAccessType::DECREMENT);
    loop1.addCondition<ModeValueCondition>(loopCounter1, 0, RelationalOperator::GREATER_THAN);
    loop2.addActivityGraphItem<ModeLabelAccess>(loopCounter2, 1, ModeAccessType::DECREMENT);
    loop2.addCondition<ModeValueCondition>(loopCounter2, 0, RelationalOperator::GREATER_THAN);


    TaskAllocation ta;
    ta.setSchedulingParameter("priority", 0);

    auto producer1 = Task::createTask("producer1");
    stimuli_50ms->addTask(producer1);
    producer1->addActivityGraphItem(loop1);
    producer1->addActivityGraphItem<ModeLabelAccess>(loopCounter1, 10, ModeAccessType::SET);
    producer1->setTaskAllocation(ta);

    auto producer2 = Task::createTask("producer2");
    stimuli_50ms->addTask(producer2);
    producer2->addActivityGraphItem(loop2);
    producer2->addActivityGraphItem<ModeLabelAccess>(loopCounter2, 10, ModeAccessType::SET);
    producer2->setTaskAllocation(ta);

    WhileLoop consumeLoop;
    consumeLoop.addCondition<ModeValueCondition>(counterLabel, 0, RelationalOperator::GREATER_THAN);
    consumeLoop.addActivityGraphItem<SemaphoreAccess>(sem, SemaphoreAccessType::request, WaitingBehaviour::active);
    consumeLoop.addActivityGraphItem<Ticks>(DiscreteValueConstant(12));
    consumeLoop.addActivityGraphItem<SemaphoreAccess>(sem, SemaphoreAccessType::release, WaitingBehaviour::active);
    consumeLoop.addActivityGraphItem<ModeLabelAccess>(counterLabel, 1, ModeAccessType::DECREMENT);


    auto consumer = Task::createTask("consumer");
    stimuli_50ms->addTask(consumer);
    consumer->addActivityGraphItem(consumeLoop);
    consumer->setTaskAllocation(ta);

    // Mapping
    MappingModel::addMemoryMapping(counterLabel, mainMemECU1);
    MappingModel::addMemoryMapping(loopCounter1, mainMemECU1);
    MappingModel::addMemoryMapping(loopCounter2, mainMemECU1);

    // OS
    auto priorityScheduler = std::make_shared<PriorityScheduler>("PrioSched");
    priorityScheduler->setExecutionCore(core1ECU1);
    priorityScheduler->addResponsibleCore(core2ECU1);
    priorityScheduler->addResponsibleCore(core3ECU1);
    priorityScheduler->addTaskMapping(producer1);
    priorityScheduler->addTaskMapping(producer2);
    priorityScheduler->addTaskMapping(consumer);


    // Trace
    scTrace(core1ECU1, "Core1ECU1");
    scTrace(core2ECU1, "Core2ECU1");
    scTrace(core3ECU1, "Core3ECU1");
    scTrace(mainMemECU1, "MainMemECU1");

  }
};

int main([[maybe_unused]] int argc,[[maybe_unused]] char *argv[]) {

	SimParam args;
	SimParamParser::parse(argc, argv, args);

	auto runner = ExampleRunner();
	EasyloggingConfig::configure("app4mcsim.log", 1, true, true);
	runner.enableTracers(args.tracerNames,args.traceDirectory, false);
	
	try {
		runner.simulate(TimeParameter<std::milli>(200));
	}
	catch (const std::runtime_error& e) {
		std::cerr << e.what();
		return -1;
	}
	return 0;
}