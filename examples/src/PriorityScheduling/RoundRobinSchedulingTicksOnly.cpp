/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Behnaz Pourmohseni <behnaz.pourmohseni@de.bosch.com>
 ********************************************************************************
 */


#include <memory>
#include "APP4MCsim.h"
#include "SimParamParser.h"
#include "SchedulingParameterHelper.h"

class ExampleRunner : public SimRunner{
public:
  void setup() override{
    /* Hardware */
    Time ECU_Freq_Domain_CycleTime = 10_ns;
    auto pudef = std::make_shared<ProcessingUnitDefinition>("CoreDef");
    auto ECU = modelRoot()->createSubStructure("ECU");

    auto mainMem = ECU->createMemory("Memory", 1 * 1024 * 1024 * 1024);
    mainMem->setClockPeriod(ECU_Freq_Domain_CycleTime);

    auto core1 = ECU->createProcessingUnit("Core1", pudef);
    core1->setClockPeriod(ECU_Freq_Domain_CycleTime);
    
    HwAccessElement mainMemC1Access;
    mainMemC1Access.setDest(mainMem);
    mainMemC1Access.setDataRate({100, DataRateUnit::kbitPerSecond});
    mainMemC1Access.setReadLatency(DiscreteValueConstant(0));
    core1->addHWAccessElement(mainMemC1Access);

    auto core2 = ECU->createProcessingUnit("Core2", pudef);
    core2->setClockPeriod(ECU_Freq_Domain_CycleTime);

    HwAccessElement mainMemC2Access;
    mainMemC2Access.setDest(mainMem);
    mainMemC2Access.setDataRate({100, DataRateUnit::kbitPerSecond});
    mainMemC2Access.setReadLatency(DiscreteValueConstant(0));
    core2->addHWAccessElement(mainMemC2Access);

    /* Stimuli */
    auto stimuli0 = std::make_shared<FixedPeriodicStimulus>("Stim0", 100_ms);
    auto stimuli1 = std::make_shared<FixedPeriodicStimulus>("Stim1", 100_ms, 2_ms);
    auto stimuli2 = std::make_shared<FixedPeriodicStimulus>("Stim2", 100_ms, 5_ms);
    auto stimuli3 = std::make_shared<FixedPeriodicStimulus>("Stim3", 100_ms, 12_ms);


    /* Software */
    auto task1 = Task::createTask("Task1");
    stimuli0->addTask(task1);
    task1->addActivityGraphItem<Ticks>(DiscreteValueConstant(2100000));
    
    auto task2 = Task::createTask("Task2");
    stimuli0->addTask(task2);
    task2->addActivityGraphItem<Ticks>(DiscreteValueConstant(2700000));
    
    auto task3 = Task::createTask("Task3");
    stimuli1->addTask(task3);
    task3->addActivityGraphItem<Ticks>(DiscreteValueConstant(4200000));
    
    auto task4 = Task::createTask("Task4");
    stimuli3->addTask(task4);
    task4->addActivityGraphItem<Ticks>(DiscreteValueConstant(2800000));

    auto task5 = Task::createTask("Task5");
    stimuli2->addTask(task5);
    task5->addActivityGraphItem<Ticks>(DiscreteValueConstant(2700000));

    auto task6 = Task::createTask("Task6");
    stimuli3->addTask(task6);
    task6->addActivityGraphItem<Ticks>(DiscreteValueConstant(2200000));


    TaskAllocation ta_task1;
    ta_task1.setSchedulingParameter("priority", 0);
    task1->setTaskAllocation(ta_task1);

    TaskAllocation ta_task2;
    ta_task2.setSchedulingParameter("priority", 0);
    task2->setTaskAllocation(ta_task2);

    TaskAllocation ta_task3;
    ta_task3.setSchedulingParameter("priority", 1);
    task3->setTaskAllocation(ta_task3);

    TaskAllocation ta_task4;
    ta_task4.setSchedulingParameter("priority", 0);
    task4->setTaskAllocation(ta_task4);

    TaskAllocation ta_task5;
    ta_task5.setSchedulingParameter("priority", 1);
    task5->setTaskAllocation(ta_task5);

    TaskAllocation ta_task6;
    ta_task6.setSchedulingParameter("priority", 2);
    task6->setTaskAllocation(ta_task6);

    /* OS and mapping */
    auto rrScheduler = std::make_shared<PriorityRoundRobinScheduler>("rrSched");
    rrScheduler->setAlgorithmSchedulingParameter(SchedulingParameterHelper::RoundRobinTimesliceLengthKey(), 4_ms);
    rrScheduler->setExecutionCore(core1);
    rrScheduler->addResponsibleCore(core1);
    rrScheduler->addResponsibleCore(core2);
    rrScheduler->addTaskMapping(task1);
    rrScheduler->addTaskMapping(task2);
    rrScheduler->addTaskMapping(task3);
    rrScheduler->addTaskMapping(task4);
    rrScheduler->addTaskMapping(task5);
    rrScheduler->addTaskMapping(task6);

    /* tracing */
    scTrace(core1, "Core1");
    scTrace(core2, "Core2");
    scTrace(mainMem, "Memory");
  }
};

int main([[maybe_unused]] int argc,[[maybe_unused]] char *argv[]) {

	SimParam args;
	SimParamParser::parse(argc, argv, args);

	auto runner = ExampleRunner();
	EasyloggingConfig::configure("app4mcsim.log", 1, true, true);
	runner.enableTracers(args.tracerNames,args.traceDirectory, false);
	
	try {
		runner.simulate(TimeParameter<std::milli>(2000));
	}
	catch (const std::runtime_error& e) {
		std::cerr << e.what();
		return -1;
	}
	return 0;
}