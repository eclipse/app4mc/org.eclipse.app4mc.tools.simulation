project(parameter)
add_executable(parameter "parameter.cpp")
target_link_libraries(parameter PRIVATE 
    #easyloggingpp::easyloggingpp #24.10 
    app4mc.sim::app4mc.sim
   #SystemC::systemc #24.10
    SimParamParser)
#target_precompile_headers(parameter REUSE_FROM app4mc.sim_lib)
add_test(NAME Run_parameter COMMAND parameter ${ADDITIONAL_TEST_ARGS} -o "${CMAKE_SOURCE_DIR}/test_data/traces/${PROJECT_NAME}")


install(
    TARGETS parameter 
    #RUNTIME DESTINATION ${CMAKE_INSTALL_PREFIX}/examples/parameter
)