/**
 ********************************************************************************
 * Copyright (c) 2022 University of Rostock and others
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */

#include <memory>
#include "APP4MCsim.h"
#include "SimParamParser.h"

int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv) {
  using namespace std::literals;
  Parameter stringList({"abc", "cde"});
  stringList.add(std::vector<std::string>{"abc"});

  for (const auto& i : stringList.get<ParameterType::String, true>()) {
	std::cout << i << " ";
  }
  std::cout << "\n";

  Parameter test({1, 2, 3});
  test.add(std::vector<ELong>({4, 5, 6}));
  test.add(1);


  for (const auto& i : test.get<ParameterType::Integer, true>()) {
	std::cout << i << " ";
  }
  std::cout << "\n";


  SchedulingParameter param({{"test", test}, {"test2"s, stringList}, {"test3",Parameter(1)}});
  PriorityScheduler sched{"prio"};
  sched.setAlgorithmSchedulingParameter({{"test",1},{"test2",{"abc","cde"}}});
  
  std::cout << sched.getAlgorithmParameter<ParameterType::Integer, false>("test") << " " <<  sched.getAlgorithmParameter<ParameterType::String, true>("test2")[0] << "\n";
  return 0;
}