/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Sebastian Reiser <sebastian.reiser@de.bosch.com>
 *  Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */
#include "SimParamParser.h"
#include "CLI/CLI.hpp"
#include <string_view>
#include <stdexcept>

int SimParamParser::parse(int argc, char** argv, SimParam& args)

{
  CLI::App app("APP4MCsim: abstract timing simulation");
  app.allow_extras();
  app.add_option("-o,--output", args.traceDirectory, "Trace output directory");
  app.add_option("-t,--time", args.simulationTimeInMs, "simulation time in ms");
  app.add_option("-r,--tracer",args.tracerNames, "select tracer(s)")->delimiter(',');
  app.add_flag("-p, --TracefileTimePrefix", args.useTraceFileTimePrefix, "add \"%Y-%m-%d-%H-%M-%S\" prefix to tracefiles to not override old files");
  auto* seedArg = app.add_option("-s,--seed", args.simulationSeed, "seed for the underlying pcg pseudo random number generator; any non-negative 64bit value is possible");
  seedArg->check(CLI::TypeValidator<uint64_t>());
  app.add_option("-v,--verbosity", args.loggingVerbosity, "logging verbosity [range 0-3]");

  CLI11_PARSE(app, argc, argv);

  for (auto& tracer : args.tracerNames) {
  // remove any leading whitespace
  tracer.erase(0, tracer.find_first_not_of(" \t\n\r\f\v"));
  }

  // rework list of Tracernames, that every Tracer appear only once
  std::sort(args.tracerNames.begin(), args.tracerNames.end());
  auto last = std::unique(args.tracerNames.begin(), args.tracerNames.end());
  args.tracerNames.erase(last, args.tracerNames.end());

  return 0;
}
