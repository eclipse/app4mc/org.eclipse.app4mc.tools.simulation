/**
 ********************************************************************************
 * Copyright (c) 2020 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Sebastian Reiser <sebastian.reiser@de.bosch.com> - initial contribution
 ********************************************************************************
 */
#pragma once
#include <string>
#include <vector>

struct SimParam
{
    /* data */
    std::string traceDirectory = "trace";
    int simulationTimeInMs = 1000;
    std::vector<std::string> tracerNames;
    bool useTraceFileTimePrefix = false;
	uint64_t simulationSeed = 0;
    bool enableTimePrefix = false;
    int loggingVerbosity = 1;
};

class SimParamParser {
private:
    SimParamParser() = default;
public:
    static int parse(int argc, char** argv, SimParam& args);
};