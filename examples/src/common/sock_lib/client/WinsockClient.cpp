/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Sebastian Reiser <sebastian.reiser@de.bosch.com> - initial contribution
 * - derived from Microsoft Winsock2 example
 ********************************************************************************
 */

#include "SockClient.h"
#include <sstream>
#include <iostream>

void SockClient::setupSocket(){
    // Initialize Winsock
    int iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {
        std::ostringstream err;
        err << "SockClient::setupSocket - WSAStartup failed with error" << iResult <<", port="<<port<<std::endl;
        std::cerr<<err.str();
        const char* what = err.str().c_str();
        throw std::exception(what);
    }

    ZeroMemory( &hints, sizeof(hints) );
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    // Resolve the server address and port
    iResult = getaddrinfo("localhost", port, &hints, &result);
    if ( iResult != 0 ) {
        std::ostringstream err;
        err << "SockClient::setupSocket - getaddrinfo failed with error" << iResult <<", port="<<port<<std::endl;
        std::cerr<<err.str();
        //WSACleanup();
        throw std::exception(err.str().c_str());
    }
}

void SockClient::connectSocket(){
    // Attempt to connect to an address until one succeeds
    for(auto ptr=result; ptr != NULL ;ptr=ptr->ai_next) {

        // Create a SOCKET for connecting to server
        connectedSocket = socket(ptr->ai_family, ptr->ai_socktype, 
            ptr->ai_protocol);
        if (connectedSocket == INVALID_SOCKET) {
            std::ostringstream err;
            err<<"SockClient::connectSocket failed with error "<< WSAGetLastError() <<", port="<<port<<std::endl;
            std::cerr<<err.str();
            //WSACleanup();
            throw std::exception(err.str().c_str());
        }

        // Connect to server.
        std::cout<<"SockClient::connectSocket attempting to connect to server"<<std::endl;
        int iResult = connect( connectedSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (iResult == SOCKET_ERROR) {
            closesocket(connectedSocket);
            connectedSocket = INVALID_SOCKET;
            continue;
        } else {
            std::cout<<"SockClient::connectSocket connected to server via port "<<port<<std::endl;
            return;
        }
    }
    std::stringstream buf;
    buf<<"SockClient::connectSocket: unable to connect to server via port "<<port<<std::endl;
    throw std::exception(buf.str().c_str());
}

SockClient::~SockClient(){
    WSACleanup();
    freeaddrinfo(result);
}

