/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Sebastian Reiser <sebastian.reiser@de.bosch.com> - initial contribution
 * - derived from Microsoft Winsock2 example
 ********************************************************************************
 */

#pragma once


#ifdef IS_WIN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iostream>
#include <sstream>

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


class WinsockBase
{
 private:
     char* recvbuf;
     const int recvbuflen;
 protected: 
     WSADATA wsaData;
     SOCKET connectedSocket = INVALID_SOCKET;
     struct addrinfo *result = NULL,
                 hints;
     const char *port;
     WinsockBase(const char* /*_port*/, int /*_recvbuflen*/);
     virtual void setupSocket() = 0;
     virtual void connectSocket() = 0;
 public:

     void initialize(){
         setupSocket();
         connectSocket();
     }

     int socketSend(std::string /*msg*/);
     int socketReceive(std::stringstream& /*buf*/);
     ~WinsockBase();
};

#endif //IS_WIN
