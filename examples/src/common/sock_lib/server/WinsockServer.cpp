/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Sebastian Reiser <sebastian.reiser@de.bosch.com> - initial contribution
 * - derived from Microsoft Winsock2 example
 ********************************************************************************
 */

#include "include/SockServer.h"
#include <sstream>
#include <iostream>


void SockServer::setupSocket(){

    // Initialize Winsock
    int iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {
        std::ostringstream b;
        b<<"SockServer::setupSocket - WSAStartup failed with error " << iResult << ", port="<<port<<std::endl;
        throw std::exception(b.str().c_str());
    }

    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    //hints.ai_socktype = SOCK_DGRAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE;

    // Resolve the server address and port
    iResult = getaddrinfo(NULL, port, &hints, &result);
    if ( iResult != 0 ) {
        std::ostringstream b;
        b << "SockServer::setupSocket - getaddrinfo failed with error " << iResult <<", port="<<port<<std::endl;
        WSACleanup();
        throw std::exception(b.str().c_str());
    }

    // Create a SOCKET for connecting to server
    ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
    if (ListenSocket == INVALID_SOCKET) {
        std::ostringstream err;
        err << "SockServer::setupSocket - socket failed with error " << WSAGetLastError() <<", port="<<port<<std::endl;
        throw std::exception(err.str().c_str());
    }

    // Setup the TCP listening socket
    iResult = bind( ListenSocket, result->ai_addr, (int)result->ai_addrlen);
    if (iResult == SOCKET_ERROR) {
        std::ostringstream err;
        err << "SockServer::setupSocket - bind failed with error" << WSAGetLastError() << ", port="<<port<<std::endl;
        freeaddrinfo(result);
        closesocket(ListenSocket);
        WSACleanup();
        throw std::exception(err.str().c_str());
    }

    freeaddrinfo(result);
}


void  SockServer::connectSocket(){
    int iResult = listen(ListenSocket, SOMAXCONN);
    if (iResult == SOCKET_ERROR) {
        std::ostringstream err;
        err << "SockServer::connectSocket - listen failed with error" << WSAGetLastError() << ", port="<<port<<std::endl;
        closesocket(ListenSocket);
        WSACleanup();
        throw std::exception(err.str().c_str());
    }

    // Accept a client socket
    connectedSocket = accept(ListenSocket, NULL, NULL);
    if (connectedSocket == INVALID_SOCKET) {
        std::ostringstream err;
        err << "WinsockServer::connectSocket - accept failed with error" << WSAGetLastError() << ", port="<<port<<std::endl;
        closesocket(ListenSocket);
        WSACleanup();
        throw std::exception(err.str().c_str());
    }

    // No longer need server socket
    closesocket(ListenSocket);
}


