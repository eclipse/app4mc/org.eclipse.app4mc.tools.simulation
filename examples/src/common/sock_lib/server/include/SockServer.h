/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Sebastian Reiser <sebastian.reiser@de.bosch.com> - initial contribution
 * - derived from Microsoft Winsock2 example
 ********************************************************************************
 */

#pragma once

#define DEFAULT_BUFLEN 512

#ifdef IS_WIN
#include "WinsockBase.h"

class SockServer: public WinsockBase
{
private:
    SOCKET ListenSocket = INVALID_SOCKET;
    void setupSocket();
    void connectSocket();
public:
    SockServer(const char* _port, const int _recvbuflen = DEFAULT_BUFLEN)
    : WinsockBase(_port, _recvbuflen)
        {      };

    // ~SockServer();
};

#else 
#include <sstream>
#include <iostream>
class SockServer
{
private:
    void setupSocket(){};
    void connectSocket(){};
public:
    SockServer(const char* /*_port*/, const int _recvbuflen = DEFAULT_BUFLEN) {};
    void initialize(){};
    int socketSend(std::string /*msg*/){return 0;};
    int socketReceive(std::stringstream& /*buf*/){return 0;};
};

#endif