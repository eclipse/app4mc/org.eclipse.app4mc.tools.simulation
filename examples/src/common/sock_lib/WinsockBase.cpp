/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Sebastian Reiser <sebastian.reiser@de.bosch.com> - initial contribution
 * - derived from Microsoft Winsock2 example
 ********************************************************************************
 */

#include "WinsockBase.h"
#include <sstream>
#include <iostream>

#define EOT (char)0x04

WinsockBase::WinsockBase(const char* _port,const int _recvbuflen): 
    port(_port), 
    recvbuf(new char[_recvbuflen]),
    recvbuflen(_recvbuflen)
{}


int WinsockBase::socketSend(std::string msg){
    msg.push_back(EOT);
    // Send an initial buffer
    int iResult = send(connectedSocket, msg.c_str(), msg.length(), 0 );
    std::cout << "socket sent:\n" << msg << std::endl;
    if (iResult == SOCKET_ERROR) {
        std::ostringstream err;
        err<<"WinsockBase::socketSend failed with error"<< WSAGetLastError();
        closesocket(connectedSocket);
        WSACleanup();
        std::cerr<<err.str()<<std::endl;
        throw new std::exception(err.str().c_str());
        return 1;
    }
    return 0;
}

int WinsockBase::socketReceive(std::stringstream& buf){
    // Receive until the peer shuts down the connection
    int iResult;
    do {
        iResult = recv(connectedSocket, recvbuf, recvbuflen, 0);
        if (iResult > 0) {
            if (recvbuf[iResult-1] == EOT){
                recvbuf[iResult-1] = '\0';
                buf<<recvbuf;
                std::cout << "\nsocket received:\n" << buf.str() << std::endl;
                return 0;
            }  
            buf<<recvbuf;
        } else if (iResult < 0)  {
            std::ostringstream err;
            err<<"WinsockBase::socketReceive failed with error"<< WSAGetLastError();
            closesocket(connectedSocket);
            WSACleanup();
            throw new std::exception(err.str().c_str());
            return 1;
        }

    } while (iResult > 0);
    return 1; //gracefully close
}



WinsockBase::~WinsockBase(){
    delete[] recvbuf;
    closesocket(connectedSocket);
    WSACleanup();
}