
/**
 ********************************************************************************
 * Copyright (c) 2020 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */

#include "APP4MCsim.h"
#pragma once

struct ExampleHardware{
	
    std::shared_ptr<ProcessingUnit> core1ECU1;
    std::shared_ptr<ProcessingUnit> core2ECU1;
    std::shared_ptr<Memory> mainMemECU1;
    std::shared_ptr<ConnectionHandler> interconnectECU1;
    std::shared_ptr<CacheStatistic> dataCache;
    std::shared_ptr<Memory> scratchpad;
    std::shared_ptr<Connection> con1;
    std::shared_ptr<Connection> con2;
    std::shared_ptr<Connection> con3;
    std::shared_ptr<Connection> con11;
    std::shared_ptr<Connection> con12;

    ExampleHardware(const std::unique_ptr<HwStructure>& root ){

		// set seed (get e.g. from simulation output)
		//app4mcsim_seed::setSeed(0x770405dc,0xaca9422b,0xd0f62a89,0x65881478);
		/* ECU1 */
		
		/* 200Mhz*/
		Time ECU1_Freq_Domain_CycleTime = 1_us;
		auto pudef = std::make_shared<ProcessingUnitDefinition>("ECU-CPU");


		auto ECU1 = root->createSubStructure("ECU1");

		core1ECU1 = ECU1->createProcessingUnit("Core1ECU1", pudef);


		core1ECU1->addInitPort("P1");
		core1ECU1->addInitPort("SP_P");
		core1ECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);


		core2ECU1 = ECU1->createProcessingUnit("Core2ECU1", pudef);
		core2ECU1->addInitPort("P1");
		core2ECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);

		//TODO fix size argument
		mainMemECU1 = ECU1->createMemory("MainMemECU1", 1 * 1024 * 1024* 1024);
		mainMemECU1->addTargetPort("P1");
		mainMemECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);
		mainMemECU1->setAccessLatency<DiscreteValueWeibullEstimatorsDistribution>({DiscreteValueWeibullEstimatorsDistribution::findParameter(8,0.1,4,32),4,32});

		interconnectECU1 = ECU1->createConnectionHandler("InterconnectECU1");
		
		interconnectECU1->addTargetPort("P1");
		interconnectECU1->addTargetPort("P2");
		interconnectECU1->addInitPort("P3");
		interconnectECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);
		interconnectECU1->setReadLatency<DiscreteValueConstant>({5});
		interconnectECU1->setWriteLatency<DiscreteValueConstant>({3});

		dataCache = ECU1->createCacheStatistic("DataCache", 0.9);
		dataCache->addInitPort("P2");
		dataCache->addTargetPort("P1");
		dataCache->setLineSize(64);
		dataCache->setSize(256 * 1024);
		dataCache->setClockPeriod(ECU1_Freq_Domain_CycleTime);

		scratchpad = ECU1->createMemory("Scratchpad", 2 * 1024 * 1024);
		scratchpad->addTargetPort("P1");
		scratchpad->setAccessLatency<DiscreteValueConstant>({1});
		scratchpad->setClockPeriod(ECU1_Freq_Domain_CycleTime);


		/* Connections */
		con1 = ECU1->createConnection("con1");
		con2 = ECU1->createConnection("con2");
		con3 = ECU1->createConnection("con3");
		con11 = ECU1->createConnection("con11");
		con12 = ECU1->createConnection("con12");
		
		core1ECU1->bindConnection(con1, "P1");
		interconnectECU1->bindConnection(con1, "P1");

		core2ECU1->bindConnection(con2, "P1");
		dataCache->bindConnection(con2, "P1");

		interconnectECU1->bindConnection(con3, "P3");
		mainMemECU1->bindConnection(con3, "P1");

		core1ECU1->bindConnection(con11, "SP_P");
		scratchpad->bindConnection(con11, "P1");

		dataCache->bindConnection(con12, "P2");
		interconnectECU1->bindConnection(con12, "P2");

		/* HW_access_elems */

		HwAccessElement mainMemC1Access;
		mainMemC1Access.setDest(mainMemECU1);
		mainMemC1Access.setAccessPath({con1, /*interconnectECU1,*/ con3});

		HwAccessElement scratchpadAccess;
		scratchpadAccess.setDest(scratchpad);

		core1ECU1->addHWAccessElement(mainMemC1Access);
		core1ECU1->addHWAccessElement(scratchpadAccess);

		HwAccessElement mainMemC2Access;
		mainMemC2Access.setAccessPath({con2, /*cache,*/ con12, /*interconnectECU1,*/ con3});
		core2ECU1->addHWAccessElement(mainMemC2Access);

    }
};