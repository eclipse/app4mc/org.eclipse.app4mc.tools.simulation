/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */


#include <memory>
#include "APP4MCsim.h"
#include "SimParamParser.h"

class ExampleRunner : public SimRunner{
public:
	void setup() override{
	//Hardware
		/* 100Mhz*/
		Time ECU1_Freq_Domain_CycleTime = 10_ns;
		auto pudef = std::make_shared<ProcessingUnitDefinition>("ECU-CPU");

		/* ECU1 */
		auto ECU1 = modelRoot()->createSubStructure("ECU1");

		auto core1ECU1 = ECU1->createProcessingUnit("Core1ECU1",pudef);
		core1ECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);

		auto mainMemECU1 = ECU1->createMemory("MainMemECU1", 1 * 1024 * 1024 * 1024);
		mainMemECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);
		mainMemECU1->setAccessLatency<DiscreteValueConstant>({100});

		/* HW_access_elems */
		HwAccessElement mainMemC1Access;
		mainMemC1Access.setDest(mainMemECU1);
		mainMemC1Access.setReadLatency<DiscreteValueConstant>({10});
		mainMemC1Access.setWriteLatency<DiscreteValueConstant>({10});
		core1ECU1->addHWAccessElement(mainMemC1Access);

	/* Software */
		auto enumMode = EnumMode::Inst("mode1");
		auto lita = enumMode->addLiteral("a");
		auto litb = enumMode->addLiteral("b");
		auto litSkip = enumMode->addLiteral("skip");
		auto litNotUsed = enumMode->addLiteral("this_literal_may_never_be_used");
		
		// auto modelabel1 = ModeLabel::Inst("modeLabel1", enumMode, lita);
		// auto modelabelb = ModeLabel::Inst("modeLabel1", enumMode, litb);
		
		auto modelabel1 = ModeLabel::Inst("modeLabel1", enumMode, lita);
		auto modelabelb = ModeLabel::Inst ("modeLabel1", enumMode, litb);

	//Stimuli
		auto stimulus_5ms = std::make_shared<FixedPeriodicStimulus>("5ms_Stimulus", 5_ms);
		auto stimulus_ipc = std::make_shared<InterProcessStimulus>("ipc_Stimulus");
		stimulus_ipc->addCondition<ModeValueCondition>(modelabel1, lita);
		auto stimulus_ipc_neverRun = std::make_shared<InterProcessStimulus>("ipc_Stimulus_condition_never_met");
		stimulus_ipc_neverRun->addCondition<ModeValueCondition>(modelabel1, litNotUsed); //literal is never set --> evaluates to false always

	//Software Model  (Runnables)
		auto rAlways = std::make_shared<Runnable>("R_Always");
		rAlways->addActivityGraphItem<Ticks>(DiscreteValueConstant(10000));
		rAlways->addActivityGraphItem<InterProcessTrigger>(stimulus_ipc);

		auto ra = std::make_shared<Runnable>("R_a");
		ra->addCondition<ModeValueCondition>(modelabel1, lita);
		ra->addActivityGraphItem<Ticks>(DiscreteValueConstant(100000));
		ra->addActivityGraphItem<InterProcessTrigger>(stimulus_ipc);
		ra->addActivityGraphItem<InterProcessTrigger>(stimulus_ipc_neverRun);
		ra->addActivityGraphItem<ModeLabelAccess>(modelabel1, litSkip);

		auto rb = std::make_shared<Runnable>("R_b");
		ConditionConjunction conjunction;
		conjunction.addCondition<ModeLabelCondition>(modelabel1, modelabelb); //is skip
		conjunction.addCondition<ModeValueCondition>(modelabel1, litb); //unnecessary check, but for the sake of a conjunction
		rb->addCondition<ConditionConjunction>(conjunction);
		rb->addActivityGraphItem<ModeLabelAccess>(modelabel1, lita);
		rb->addActivityGraphItem<Ticks>(DiscreteValueConstant(100000));

		auto rEnableB = std::make_shared<Runnable>("R_reset");
		rEnableB->addCondition<ModeValueCondition>(modelabel1, litSkip); //unnecessary check, but for the sake of a conjunction
		rEnableB->addActivityGraphItem<ModeLabelAccess>(modelabel1, litb); //start rb in the next cycle
		rEnableB->addActivityGraphItem<Ticks>(DiscreteValueConstant(10000));

	//Software Model  (Tasks)
		TaskAllocation talloc;

		auto t1 = Task::createTask("T1");
		stimulus_5ms->addTask(t1);
		t1->addActivityGraphItem<RunnableCall>(ra);
		t1->addActivityGraphItem<RunnableCall>(rb);
		t1->addActivityGraphItem<RunnableCall>(rEnableB);
		talloc.setSchedulingParameter("priority", 10);
		t1->setTaskAllocation(talloc);

		auto ta = Task::createTask("T_a");
		stimulus_ipc->addTask(ta);
		ta->addActivityGraphItem<Ticks>(DiscreteValueConstant(50000));
		talloc.setSchedulingParameter("priority", 1);
		ta->setTaskAllocation(talloc);

		auto tNeverStart = Task::createTask("T_shallNeverStart");
		stimulus_ipc_neverRun->addTask(tNeverStart);
		tNeverStart->addActivityGraphItem<Ticks>(DiscreteValueConstant(500000000000));
		talloc.setSchedulingParameter("priority", 100);
		tNeverStart->setTaskAllocation(talloc);


	//mapping
		MappingModel::addMemoryMapping(modelabel1,mainMemECU1);
		MappingModel::addMemoryMapping(modelabelb,mainMemECU1);

	//OS
		auto priorityScheduler = std::make_shared<PriorityScheduler>("PrioSched");
		priorityScheduler->setExecutionCore(core1ECU1);
		priorityScheduler->addTaskMapping(t1);
		priorityScheduler->addTaskMapping(ta);
		priorityScheduler->addTaskMapping(tNeverStart);

		scTrace(core1ECU1, "Core1ECU1");
		scTrace(mainMemECU1, "MainMemECU1");
	}
};

int main([[maybe_unused]] int argc,[[maybe_unused]] char *argv[]) {

	SimParam args;
	SimParamParser::parse(argc, argv, args);

	auto runner = ExampleRunner();
	EasyloggingConfig::configure("app4mcsim.log", 1, true, true);
	runner.enableTracers(args.tracerNames,args.traceDirectory, false);
	
	try {
		runner.simulate(TimeParameter<std::milli>(200));
	}
	catch (const std::runtime_error& e) {
		std::cerr << e.what();
		return -1;
	}
	return 0;
}