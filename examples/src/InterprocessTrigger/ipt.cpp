/**
 ********************************************************************************
 * Copyright (c) 2020 University of Rostock
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - University of Rostock - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#include <memory>
#include "APP4MCsim.h"
#include "SimParamParser.h"

#include "../exampleHardware/ExampleHardware.hpp"

class ExampleRunner : public SimRunner{
public:
	void setup() override{
		
		ExampleHardware hw(modelRoot());

		auto stimuli_5ms = std::make_shared<FixedPeriodicStimulus>("5ms_Stimuli", 5_ms);
		auto stimuli_ipc = std::make_shared<InterProcessStimulus>("ipc_Stimuli");

		auto t1 = Task::createTask("T1");
		auto t2 = Task::createTask("T2");

		stimuli_ipc->addTask(t1);
		t1->addActivityGraphItem<Ticks>(DiscreteValueConstant(301));
		

		stimuli_5ms->addTask(t2);
		t2->addActivityGraphItem<Ticks>(DiscreteValueConstant(302));
		t2->addActivityGraphItem<InterProcessTrigger>(stimuli_ipc);
		t2->addActivityGraphItem<Ticks>(DiscreteValueConstant(303));

		/*  swapped T1 & T4 because of missing path*/
		auto randomScheduler1 = std::make_shared<RandomScheduler>("RandSched1");
		randomScheduler1->setExecutionCore(hw.core1ECU1);
		randomScheduler1->addTaskMapping(t1);

		auto randomScheduler2 = std::make_shared<RandomScheduler>("RandSched2");
		randomScheduler2->setExecutionCore(hw.core2ECU1);
		randomScheduler2->addTaskMapping(t2);

		scTrace(hw.core1ECU1, "core1ECU1");
		scTrace(hw.core2ECU1, "core2ECU1");
		scTrace(hw.con3, "con3");
		scTrace(hw.interconnectECU1, "interconnectECU1");
		scTrace(hw.dataCache, "dataCache");
		scTrace(hw.mainMemECU1, "mainMemECU1");
	}
};

int main([[maybe_unused]] int argc,[[maybe_unused]] char *argv[]) {

	SimParam args;
	SimParamParser::parse(argc, argv, args);

	auto runner = ExampleRunner();
	EasyloggingConfig::configure("app4mcsim.log", 1, true, true);
	runner.enableTracers(args.tracerNames,args.traceDirectory, false);
	
	try {
		runner.simulate(TimeParameter<std::milli>(2000));
	}
	catch (const std::runtime_error& e) {
		std::cerr << e.what();
		return -1;
	}
	return 0;
}