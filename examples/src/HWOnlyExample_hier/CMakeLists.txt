
project(hw_example_hier)
add_executable(hw_example_hier "HWOnlyExample_hier.cpp")
target_link_libraries(hw_example_hier PRIVATE 
    #easyloggingpp::easyloggingpp #24.10 
    app4mc.sim::app4mc.sim
   #SystemC::systemc #24.10
    SimParamParser)
#target_precompile_headers(hw_example_hier REUSE_FROM app4mc.sim_lib)

test_run()
test_check_trace(${DEFAULT_VCD_TRACE_NAME})
test_check_trace(${DEFAULT_BTF_TRACE_NAME})

install(
    TARGETS hw_example_hier 
)