/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#include <string>
#include <iostream>
#include <fstream>
#include <exception>
#include "APP4MCsim.h"
#include <gtest/gtest.h>


class DestructionTestRunner : public SimRunner{
public:
	void setup() override{
		return;
	}
};


TEST(DestructionTest, DeconstructionSmokeTest){	
	std::cout<<"FIRST INSTANCE ----------------"<<std::endl;
	auto* runner1 = new DestructionTestRunner();
	std::cout<<"FIRST INSTANCE DTOR-----------"<<std::endl;
	delete runner1;
	std::cout<<"SECOND INSTANCE ----------------"<<std::endl;
	auto* runner2 = new DestructionTestRunner();
	std::cout<<"SECOND INSTANCE DTOR-----------"<<std::endl;
	delete runner2;
}


int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);

  return RUN_ALL_TESTS();
}