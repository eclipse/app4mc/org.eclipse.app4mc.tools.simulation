/**
 ********************************************************************************
 * Copyright (c) 2021 University of Rostock
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - University of Rostock - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#include <memory>
#include "APP4MCsim.h"
#include "SimParamParser.h"

#include "../exampleHardware/ExampleHardware.hpp"

class ExampleRunner : public SimRunner{
public:
	void setup() override{

		// set seed (get e.g. from simulation output)
		//app4mcsim_seed::setSeed(0x770405dc,0xaca9422b,0xd0f62a89,0x65881478);
		ExampleHardware hw(modelRoot());
		
		/* Software */

		
		auto stimuli_5ms = std::make_shared<FixedPeriodicStimulus>("5ms_Stimuli", 5_ms);
		auto stimuli_1ms = std::make_shared<FixedPeriodicStimulus>("1ms_Stimuli", 1_ms);

		TaskAllocation ta;

		ta.setSchedulingParameter("priority",1);

		auto t1 = Task::createTask("T1");
		auto t2 = Task::createTask("T2");
		auto c1 = Task::createTask("Child1");
		auto c2 = Task::createTask("Child2");

		stimuli_1ms->addTask(t1);
		t1->addActivityGraphItem<Ticks>(DiscreteValueConstant(301));
		ta.setSchedulingParameter("priority",1);
		t1->setTaskAllocation(ta);

		stimuli_5ms->addTask(t2);
		t2->addActivityGraphItem<Ticks>(DiscreteValueConstant(302));
		t2->addActivityGraphItem<Ticks>(DiscreteValueConstant(303));
		ta.setSchedulingParameter("priority",2);
		t2->setTaskAllocation(ta);

		stimuli_5ms->addTask(c2);
		c2->addActivityGraphItem<Ticks>(DiscreteValueConstant(302));
		c2->addActivityGraphItem<Ticks>(DiscreteValueConstant(303));
		ta.setSchedulingParameter("priority",3);
		c2->setTaskAllocation(ta);

		stimuli_5ms->addTask(c1);
		c1->addActivityGraphItem<Ticks>(DiscreteValueConstant(302));
		c1->addActivityGraphItem<Ticks>(DiscreteValueConstant(303));
		ta.setSchedulingParameter("priority",4);
		c1->setTaskAllocation(ta);
		
		auto prioSchedParent = std::make_shared<PriorityScheduler>("PrioParent");
		prioSchedParent->setExecutionCore(hw.core1ECU1);
		prioSchedParent->addTaskMapping(t1);
		prioSchedParent->addTaskMapping(t2);

		auto prioSchedchild = std::make_shared<PriorityScheduler>("PrioChild");
		//prioSchedchild->setExecutionCore(hw.core1ECU1);
		prioSchedchild->addTaskMapping(c1);
		prioSchedchild->addTaskMapping(c2);
		ta.setSchedulingParameter("priority",10);
		prioSchedchild->setTaskAllocation(ta);
		
		prioSchedParent->addTaskMapping(prioSchedchild);

		scTrace(hw.core1ECU1, "core1ECU1");
		scTrace(hw.core2ECU1, "core2ECU1");
		scTrace(hw.con3, "con3");
		scTrace(hw.interconnectECU1, "interconnectECU1");
		scTrace(hw.dataCache, "dataCache");
		scTrace(hw.mainMemECU1, "mainMemECU1");
	}
};


int main([[maybe_unused]] int argc,[[maybe_unused]] char *argv[]) {

	SimParam args;
	SimParamParser::parse(argc, argv, args);

	auto runner = ExampleRunner();
	EasyloggingConfig::configure("app4mcsim.log", 1, true, true);
	runner.enableTracers(args.tracerNames,args.traceDirectory, false);
	
	try {
		runner.simulate(TimeParameter<std::milli>(2000));
	}
	catch (const std::runtime_error& e) {
		std::cerr << e.what();
		return -1;
	}
	return 0;
}