/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Sebastian Reiser <sebastian.reiser@de.bosch.com> - initial contribution
 ********************************************************************************
 */

#include "ExternalEventSubscriber.h"
#include "EventAPI.h"
#include "Event.h"
// #include "TracerApi.h"

#define SC_INCLUDE_DYNAMIC_PROCESSES


using json = nlohmann::ordered_json; // allows preserving the insertion order of JSON objects
using EVENT = eventAPI::JSON_TAGS;

json ExternalEventSubscriber::eventToJson(const std::shared_ptr<Event>& event, const sc_core::sc_time_tuple& timeTuple){
    json j_entry;
    if (watchedEvents.count(event) == 0){
        VLOG(0)<<"ExternalEventSubscriber::eventToJson - Event "<<event->getName()<<" is not watched by this manager. Skipping!";
    } else {
        j_entry[EVENT::NAME] = event->getName();
        j_entry[EVENT::TIME_VALUE] = timeTuple.to_double();
        j_entry[EVENT::TIME_UNIT] =  timeTuple.unit_symbol();
    }
    return j_entry;
}


enum class TIMESPEC {INVALID, IMMEDIATE, TIMED};

static TIMESPEC getTimeSpecType(const json& j_ev){
    //check if time spec has value and unit
    if(j_ev.contains(EVENT::TIME_VALUE) && j_ev.contains(EVENT::TIME_UNIT)){
        return TIMESPEC::TIMED;
    }else if(!j_ev.contains(EVENT::TIME_VALUE) && !j_ev.contains(EVENT::TIME_UNIT)){
        return TIMESPEC::IMMEDIATE;
    } else {
        //incomplete either value or unit missing
        return  TIMESPEC::INVALID; 
    }
}


void ExternalEventSubscriber::notifyFromJsonEntry(const json& j_ev){
    if (j_ev.contains(EVENT::NAME)){
        if (eventMap.count(j_ev[EVENT::NAME])){
            auto ev = eventMap[j_ev[EVENT::NAME]];
            if (getTimeSpecType(j_ev) == TIMESPEC::TIMED){
                double t = j_ev[EVENT::TIME_VALUE];
                std::string u = j_ev[EVENT::TIME_UNIT];
                //cancel prior occurence (latest is greatest)
                ev->cancel();
                //notify event is time value is zero or positive, otherwise skip notification. this will simply cancel the prior defined occurene
                if (t >= 0.0) {
                    ev->notify(sc_core::sc_time(t, u.c_str()));
                } else {
                    VLOG(1)<<"INFO: External Event " + ev->getName() + " has been cancelled via neg. time value";
                }
            }else  if (getTimeSpecType(j_ev) == TIMESPEC::IMMEDIATE){
                //notify event according to time spec, cancel any prior occurences (latest is greatest)
                ev->cancel();
                ev->notify(sc_core::SC_ZERO_TIME);
            } else{
                std::ostringstream err;  
                err<<"ERROR: ExternalEventSubscriber::notifyFromJsonEntry - invalid time specification for event "<<j_ev[EVENT::NAME]<<". Abort!";
                VLOG(0)<<err.str();
                throw std::runtime_error(err.str().c_str());
            }
        } else {
            std::ostringstream err;  
            err<<"ERROR: ExternalEventSubscriber::notifyFromJsonEntry - event "<<j_ev[EVENT::NAME]<<" is not registered as external. Abort!";
            VLOG(0)<<err.str();
            throw std::runtime_error(err.str().c_str());
        }
    } else {
        VLOG(0)<<"WARNING: ExternalEventIF::notifyFromJsonEntry - no event name given. Skipping!";
    }
}


/**
 * This synchronous communication informs the external server about the external events in the system and the
 * timebase of the simulation.
 * 
 * tx (info): 
 * {    timeBaseValue: 10,
 *      timeBaseUnit: ns
 *      events:[
 *          {name : event1, currentTimeValue: 0.0, currentTimeUnit: s},
 *          {name : event2, currentTimeValue: 0.0, currentTimeUnit: s},
 *          {name : event3, currentTimeValue: 0.0, currentTimeUnit: s},
 *          {name : event4, currentTimeValue: 0.0, currentTimeUnit: s},
 *          {name : event5, currentTimeValue: 0.0, currentTimeUnit: s},
 * }
 * 
 * rx (response): 
 * {    
 *      events:[
 *          {name : event1, currentTimeValue: 1.0,currentTimeUnit: ms}, #--> update occurence to notify at 1.0 ms
 *          {name : event2,  currentTimeValue: -1.0,currentTimeUnit: ms}, #--> do not notify 
 *          {name : event3}, #--> notify at proposed time 0.0 s
 *         # no entry in response for event --> do not notify
 *          
 *      ]
 * }
**/
void ExternalEventSubscriber::simSetup(){    
//request/info
    json j_tx;
    // top-level information
    sc_core::sc_time res = sc_core::sc_get_time_resolution();
    auto sc_resolution_tuple = sc_core::sc_time_tuple(res);   
    j_tx[EVENT::TIMEBASE_VALUE] = sc_resolution_tuple.value();
    j_tx[EVENT::TIMEBASE_UNIT] =  sc_resolution_tuple.unit_symbol();
    auto j_events_tx = json::array();
    for (auto pair : eventMap){
        auto entry = eventToJson(pair.second, sc_core::sc_time_tuple(sc_core::sc_time(0, sc_core::sc_time_unit::SC_NS)));
        j_events_tx.push_back(entry);
    } 
    j_tx[EVENT::EVENTS] = j_events_tx;
    std::string serializedJson = j_tx.dump();
    client->socketSend(serializedJson);
    VLOG(0)<<"ExternalEventIF::sendServerInfo - Sent info  "<< serializedJson;

//response
    std::stringstream rx;
    client->socketReceive(rx);
    auto j_rx = json::parse(rx);

    if (j_rx.contains(EVENT::EVENTS)){
        for (auto& j_ev :j_rx[EVENT::EVENTS]){ 
            //auto time = getScTimeFromJsonEntry(j_ev);
            if (eventMap.count(j_ev[EVENT::NAME])){              
                if (getTimeSpecType(j_ev) == TIMESPEC::IMMEDIATE){
                    //only spawn threads for initial events at time 0
                    //as we need to ensure event stimuli waiting for 
                    //these events have reached the respective "wait" statement
                    sc_core::sc_spawn([=]() {
                            wait(sc_core::SC_ZERO_TIME); //postpone notification by 1 delta
                            notifyFromJsonEntry(j_ev);
                        }
                    );
                } else {
                    notifyFromJsonEntry(j_ev);
                }
            }
        }
    }
}  


/**
 * This synchronous communication informs the external server about the external events in the system and the
 * timebase of the simulation.
 * 
 * tx (info): 
 *      events:[
 *          {name : event1, currentTimeValue: 23.0, currentTimeUnit: ms} #--> event 1 occured at 23 ms (in timing simulation)
 *      ]
 * }
 * 
 * rx (response): 
 * {    
 *      events:[
 *          {name : event1, currentTimeValue: 42.0,currentTimeUnit: ms}, 
 *              #--> optional: raise event 1 again in 42 ms, if another occurence has been planned, cancel old and apply new specification (latest is greatest)
 *          {name : event2,  currentTimeValue: -1.0 ,currentTimeUnit: ms}, 
 *              #--> optional: do not raise event 2, cancel future occurences
 *          {name : event3}, 
 *              #--> immediate notification
 *         # no entry in response for event --> do not notify
 *          
 *      ]
 * }
**/
void ExternalEventSubscriber::notify(const std::shared_ptr<Event>& event)
{
    if(watchedEvents.count(event) == 0){
        // the event is not watched by this manager
        return;
    }
    
    //REQUEST/Notification
    json j_tx;
    j_tx[EVENT::TIME_VALUE] = sc_core::sc_time_stamp().to_default_time_units();
    auto j_events_tx = json::array();
    j_events_tx.push_back(eventToJson(event, sc_core::sc_time_tuple(sc_core::sc_time_stamp())));
    j_tx[EVENT::EVENTS] = j_events_tx;
    client->socketSend(j_tx.dump());

    //RESPONSE
    std::stringstream rx;
    client->socketReceive(rx);
    json j_rx = json::parse(rx);
    for (auto& j_ev: j_rx[EVENT::EVENTS]){
        notifyFromJsonEntry(j_ev);
    }
}


// void ExternalEventSubscriber::addEvent(const std::shared_ptr<Event> &e){
//     eventMap[e->getName()] = e;
//     watchedEvents.insert(e);
// }


void ExternalEventSubscriber::start_of_simulation(){
    client->initialize();
    simSetup();
}


void ExternalEventSubscriber::end_of_simulation(){
}

