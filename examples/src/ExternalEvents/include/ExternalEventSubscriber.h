/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */
#pragma once
#include <string>
#include <systemc>
#include "PubSubEventSubscriber.h"
#include "Common.h"
#include "SockClient.h"
#include "easylogging++.h"
#include "nlohmann/json.hpp"

class Event;


class ExternalEventSubscriber : public PubSubEventSubscriber,  public sc_core::sc_module {
private:
    std::unique_ptr<SockClient> client = nullptr;
    std::map<std::string, std::shared_ptr<Event>> eventMap;
	std::set<std::shared_ptr<Event>> watchedEvents;
	const char* port;

	void notifyEventSpawn(std::shared_ptr<Event> e, sc_core::sc_time occurrence);
	void notifyFromJsonEntry(const nlohmann::ordered_json& eventEntry);
	nlohmann::ordered_json eventToJson(const std::shared_ptr<Event>& event, const sc_core::sc_time_tuple& timeTuple);

public:
	ExternalEventSubscriber(const char* _port="27017"): PubSubEventSubscriber(),
		client(std::make_unique<SockClient>(_port)),
		port(_port)
	{
		VLOG(0)<<"ExternalEventSubscriber::ExternalEventSubscriber - port="<<_port<<std::endl;
	}

	// //event register and callback methods
	// void addEvent(const std::shared_ptr<Event>& e){
	// 	this->addPubSubEvent(e);
	// };
	
	void notify(const std::shared_ptr<Event>& event);

		
	//socket communication
    void simSetup();
	
	virtual void update(const std::shared_ptr<Event>& event) override{
		notify(event);
	}

	//simulation phase callbacks
	void start_of_simulation() override;
	void end_of_simulation() override;
	
	friend class EventManager;
};
