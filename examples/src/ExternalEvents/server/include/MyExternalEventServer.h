/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Sebastian Reiser <sebastian.reiser@de.bosch.com> - initial contribution
 ********************************************************************************
 */

#pragma once
#include "ExternalEventServer.h"

class MyExternalEventServer: public ExternalEventServer{
private:
    std::list<std::shared_ptr<mock::Event>> editDefaultSetup() override;
    std::list<std::shared_ptr<mock::Event>> processRequest(std::shared_ptr<mock::Event> triggeringEvent) override;
public:
    MyExternalEventServer(const char* _port) : ExternalEventServer(_port){}
};

