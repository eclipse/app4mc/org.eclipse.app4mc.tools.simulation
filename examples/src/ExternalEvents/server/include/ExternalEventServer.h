/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Sebastian Reiser <sebastian.reiser@de.bosch.com> - initial contribution
 ********************************************************************************
 */

#pragma once

#include <iostream>
#include <map>
#include <list>
#include <set>
#include <limits.h>
#include <memory>
#include "easylogging++.h"
#include "MockImplementations.h"
#include "SockServer.h"

class ExternalEventServer{
private: 
    SockServer server;
    const char* port;
    std::string name;
    bool interactiveMode = false;
    void processingLoop();
    void simSetup();
protected:
    std::map<std::string, std::shared_ptr<mock::Event>> eventMap;
    virtual std::list<std::shared_ptr<mock::Event>> editDefaultSetup() = 0;
    virtual std::list<std::shared_ptr<mock::Event>> processRequest(std::shared_ptr<mock::Event>) = 0;
public:  

    ExternalEventServer(const char* _port) : port(_port), server(_port){ 
        VLOG(0)<<"ExternalEventServer::ExternalEventServer - port="<<_port<<std::endl; 
        server.initialize();
    };

    void run(){
        simSetup();
        processingLoop();
    }

    void setInteractiveMode(bool);
};
