/**
 ********************************************************************************
 * Copyright (c) 2020 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Sebastian Reiser <sebastian.reiser@de.bosch.com> - initial contribution
 ***********
 */

#pragma once
#include "CLI/CLI.hpp"

struct AppParams
{
    bool interactiveMode = false;
    std::string port ="";
};

class AppParamParser {
public:
    static int parse(int argc, char** argv, AppParams& args){
        CLI::App app("External Server: abstract timing simulation");
        app.add_option("-p,--port", args.port, "socket port");
        app.add_flag("-i,--interactive", args.interactiveMode, "interactive Mode");
  
        CLI11_PARSE(app, argc, argv);

        VLOG(0) << "server called with" << std::endl 
            << "\t-i " << args.interactiveMode << std::endl 
            << "\t-p " << args.port << std::endl;
        return 0;
    }
};
