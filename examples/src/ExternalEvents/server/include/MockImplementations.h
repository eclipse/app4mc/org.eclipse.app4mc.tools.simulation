#pragma once

#include <iostream>
#include <chrono>
#include <sstream>

namespace mock {

    using Time = typename std::chrono::duration<double, std::milli>;

    template<typename Ratio>
    using TimeParameter = typename std::chrono::duration<double, Ratio>;

    struct TimeUnits{
        inline static std::string FS = "fs";
        inline static std::string PS = "ps";
        inline static std::string NS = "ns";
        inline static std::string US = "us";
        inline static std::string MS = "ms";
        inline static std::string S = "s";
    };

    //this template is used to reduce bloat in deviations for type dependend specializations
    template<class ReturnT,class InputT>
    ReturnT constexpr valueOf(InputT in){
        if constexpr (std::is_same<InputT,Time>::value)
            return static_cast<ReturnT>(in.count());
        else
            return static_cast<ReturnT>(in);
    }

    template<class  U>
    static std::string unitOf(std::chrono::duration<double, U> ){		
        if constexpr (std::is_same<U, std::pico>::value)
            return TimeUnits::PS;
        else if constexpr (std::is_same<U, std::micro>::value)
            return TimeUnits::US;
        else if constexpr (std::is_same<U, std::milli>::value)
            return TimeUnits::MS;
        else if constexpr (std::is_same<U, std::ratio<1,1>>::value)
            return TimeUnits::S;
        else 
            throw std::runtime_error("Unknown time unit specification: legal arguments ps, us, ms, s");
    }
        
    static Time timeInst(double val, const std::string& unit){
        if (unit == TimeUnits::PS)
            return TimeParameter<std::pico>(val);
        else if (unit == TimeUnits::NS)
            return TimeParameter<std::nano>(val);
        else if (unit == TimeUnits::US)
            return TimeParameter<std::micro>(val);
        else if (unit == TimeUnits::MS)
            return TimeParameter<std::milli>(val);
        else if (unit == TimeUnits::S)
            return TimeParameter<std::ratio<1,1>>(val);
        else 
            throw std::runtime_error("Unknown time unit specification: legal arguments ps, us, ms, s");
    }

    class Event {
    private:
        std::string name;
        Time nextOccurence;
        const Time initialOccurence;
    public:
        Event(std::string _name, const Time _nextOccurence): 
            name(_name), nextOccurence(_nextOccurence), initialOccurence(_nextOccurence){ };
        
        std::string getName(){return name;}

        Time getNextOccurence(){return nextOccurence;}
        void setNextOccurence(Time d){nextOccurence =d;}

        std::string str(){
            std::stringstream buf;
            buf<<"Event:\n\tname: "<<name<<"\n\tinitialOccurence: "<<unitOf(nextOccurence);
            return buf.str();
        }
    };
}
