// /**
//  ********************************************************************************
//  * Copyright (c) 2022 Robert Bosch GmbH and others
//  * 
//  * This program and the accompanying materials are made
//  * available under the terms of the Eclipse Public License 2.0
//  * which is available at https://www.eclipse.org/legal/epl-2.0/
//  * 
//  * SPDX-License-Identifier: EPL-2.0
//  * 
//  * Contributors:
//  * - Sebastian Reiser <sebastian.reiser@de.bosch.com> - initial contribution
//  ********************************************************************************
//  */

#include "nlohmann/json.hpp"
#include "EventAPI.h"
#include <sstream>
#ifndef NOMINMAX
#define NOMINMAX
#endif
#include "easylogging++.h"
#include "AppParamParser.h"
#include "ExternalEventServer.h"
#include "MockImplementations.h"

using json = nlohmann::ordered_json; // allows preserving the insertion order of JSON objects
using EVENT_TAGS = eventAPI::JSON_TAGS;

using namespace mock;

//helper functions
static json toJson(std::shared_ptr<Event> ev){
    auto j = json::object();
    j[EVENT_TAGS::NAME] = ev->getName();
    if (ev->getNextOccurence().count()!= 0.0){
        j[EVENT_TAGS::TIME_VALUE] = valueOf<double>(ev->getNextOccurence());
        j[EVENT_TAGS::TIME_UNIT] = unitOf(ev->getNextOccurence());
    }
    return j;
}


void ExternalEventServer::simSetup(){
//REQUEST
    std::stringstream buf;
    server.socketReceive(buf);
    json j_rx = json::parse(buf);
    
    if(!j_rx.contains(EVENT_TAGS::TIMEBASE_VALUE) && !j_rx.contains(EVENT_TAGS::TIMEBASE_UNIT)){
        throw new std::invalid_argument(
            "ExternalEventServer::parseServerInfo - The JSON object does not contain simulation time base information, expected key: " +
            std::string(EVENT_TAGS::TIMEBASE_VALUE) + " and " +  std::string(EVENT_TAGS::TIMEBASE_UNIT));
    }
    
    if (j_rx.contains(EVENT_TAGS::EVENTS)){
        for (auto& entry :j_rx[EVENT_TAGS::EVENTS]){  
            if(!entry.contains(EVENT_TAGS::NAME)){
                throw new std::invalid_argument(
                    "ExternalEventServer::parseServerInfo - The JSON object does not specify an event name, expected key: " + 
                    std::string(EVENT_TAGS::NAME));
            }
            auto eventName = entry[EVENT_TAGS::NAME];           
            auto eventNextOccurenceValue = 0.0;
            std::string eventNextOccurenceUnit = TimeUnits::PS;
            if(!(entry.contains(EVENT_TAGS::TIME_VALUE) &&entry.contains(EVENT_TAGS::TIME_UNIT))){
                VLOG(0)<<"ExternalEventServer::parseServerInfo - The JSON object does not specify an occurence, assuming default value: "
                    << eventNextOccurenceValue<<" "<<eventNextOccurenceUnit;
            } else {
                eventNextOccurenceValue = entry[EVENT_TAGS::TIME_VALUE];
                eventNextOccurenceUnit = entry[EVENT_TAGS::TIME_UNIT];
            }
            auto eventNextOccurence = timeInst(eventNextOccurenceValue, eventNextOccurenceUnit);
            auto ev = std::make_shared<Event>(eventName, eventNextOccurence);
            eventMap[eventName] = ev;
        }
    }

//MODIFY
    //call virtual function to compile client/simulation response
    auto modifiedList = editDefaultSetup();
     
//RESPONSE
    json j_tx;
    auto j_events_tx = json::array();
    
    for(auto& ev : modifiedList){
        j_events_tx.push_back(toJson(ev));
    }
    j_tx[EVENT_TAGS::EVENTS] = j_events_tx;

    server.socketSend(j_tx.dump());
}

void ExternalEventServer::processingLoop(){
    while (true){
        std::stringstream buf;
        if (server.socketReceive(buf) != 0){
            return;
        }
        json j_rx = json::parse(buf);       
        if (j_rx.contains(EVENT_TAGS::EVENTS))
        {
            json j_tx;
            auto j_events_tx = json::array();
            for (auto& j_event : j_rx[EVENT_TAGS::EVENTS]){
                for( auto& ev : processRequest(eventMap[j_event[EVENT_TAGS::NAME]])){
                    j_events_tx.push_back(toJson(ev));
                };
            }
            j_tx[EVENT_TAGS::EVENTS] = j_events_tx;
            server.socketSend(j_tx.dump()); 
        } else {
            throw std::runtime_error(" ExternalEventServer::processingLoop -  received incomplete/empty event notification");
        }
    }
}

void ExternalEventServer::setInteractiveMode(bool _interactiveMode){
    this->interactiveMode = _interactiveMode;
}
