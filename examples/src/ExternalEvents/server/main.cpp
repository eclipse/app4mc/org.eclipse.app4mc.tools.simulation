
#include "nlohmann/json.hpp"
#include <sstream>
#ifndef NOMINMAX
#define NOMINMAX
#endif
#include "easylogging++.h"
#include "AppParamParser.h"
#include "MyExternalEventServer.h"


//INITIALIZE_EASYLOGGINGPP

int main(int argc, char *argv[]) 
{
    //START_EASYLOGGINGPP(argc, argv);
    AppParams args;
    AppParamParser::parse(argc, argv,args);
    if (args.port.empty()){
        args.port="27017";
    }
    MyExternalEventServer server = MyExternalEventServer(args.port.c_str());
    server.run();   
    return 0;
}