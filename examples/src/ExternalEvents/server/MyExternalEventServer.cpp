/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Sebastian Reiser <sebastian.reiser@de.bosch.com> - initial contribution
 ********************************************************************************
 */

#include "MyExternalEventServer.h"

using namespace mock;

std::list<std::shared_ptr<Event>> MyExternalEventServer::editDefaultSetup() {
    int cnt =1;
    auto ret = std::list<std::shared_ptr<Event>>();
    for (auto& pair : eventMap){
        auto ev = pair.second;
        if (ev->getName() == "externalEvent1"){
            //return event with default occurence spec: proposed time 0.0 is applied
            ret.push_back(ev);
        } else if (ev->getName() == "externalEvent2"){
            //shift the notification time by 1 ms
            ev->setNextOccurence(TimeParameter<std::milli>(-1));
            ret.push_back(ev);
        } else if (ev->getName() == "externalEvent3"){
            //disable this event (until raised from within simulation)
            ev->setNextOccurence(TimeParameter<std::milli>(0));
            ret.push_back(ev);
        }  else if (ev->getName() == "externalEvent4"){
            //do nothing case: event will not be raised
            ev->setNextOccurence(TimeParameter<std::milli>(3));
            ret.push_back(ev);
        }  else if (ev->getName() == "externalEvent5"){
            // raise event in 42 ms,this will later be waived by issueing an occurence at -1 ms
            ev->setNextOccurence(TimeParameter<std::milli>(42));
            ret.push_back(ev);
        } else {
            //return event without occurence spec: proposed time 0.0 is applied
            ret.push_back(ev);
        }
        cnt++;
    }
    return ret;
}

/**
 * returns: list of events to raised in simulation after the specified time
 */
std::list<std::shared_ptr<Event>> MyExternalEventServer::processRequest(std::shared_ptr<Event> triggeringEvent)  {
    static double incr = 0.0;
    auto modifiedEvents = std::list<std::shared_ptr<Event>>();
    if (triggeringEvent->getName() == "externalEvent1"){
        //occurence of event 1 
        triggeringEvent->setNextOccurence(TimeParameter<std::milli>(10 + incr));
        modifiedEvents.push_back(triggeringEvent);
        //issue occurence for event 2: simultaneous to event 1
        eventMap["externalEvent2"]->setNextOccurence(TimeParameter<std::milli>(10 + incr));
        modifiedEvents.push_back(eventMap["externalEvent2"]);
        //issue occurence for event 3: this event is actually never triggered in simulation, as it is being reset
        //when event 1 appears. 
        eventMap["externalEvent3"]->setNextOccurence(TimeParameter<std::milli>(11 + incr));
        modifiedEvents.push_back(eventMap["externalEvent3"]);
        //issue occurence for event 4: prior to event 1, this event will be communicated to server (this)
        //but response is always empry
        eventMap["externalEvent4"]->setNextOccurence(TimeParameter<std::milli>(0));
        modifiedEvents.push_back(eventMap["externalEvent4"]);
        //waive occurence of event 5, which was initially scheduled at 42 ms --> this event should never occur
        //Time in systemC is internally an unsigned integer, specifying -1 will be transformed to the 
        //maximum possible simulation time.
        eventMap["externalEvent5"]->setNextOccurence(TimeParameter<std::milli>(-1));
        modifiedEvents.push_back(eventMap["externalEvent5"]);

        //increment the time between events by 1 ms
        incr = incr  + 1.0;
    }
    return modifiedEvents;
}
