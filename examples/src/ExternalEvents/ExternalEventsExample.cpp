/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */


#include <memory>
#include "APP4MCsim.h"
#include "SimParamParser.h"

class ExampleRunner : public SimRunner{
public:
void setup() override{
  //START_EASYLOGGINGPP(argc, argv);
  sc_core::sc_set_time_resolution(1.0, sc_core::SC_NS);
  SimParam args;
  SimParamParser::parse(argc, argv, args);

  /* Hardware */
  /* 100Mhz*/
  Time ECU1_Freq_Domain_CycleTime = 10_ns;
  auto pudef = std::make_shared<ProcessingUnitDefinition>("ECU-CPU");
  auto ECU1 = modelRoot()->createSubStructure("ECU1");

  auto core1ECU1 = ECU1->createProcessingUnit("Core1ECU1", pudef);
  core1ECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);

  auto mainMemECU1 = ECU1->createMemory("MainMemECU1", 1 * 1024 * 1024 * 1024);
  mainMemECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);
  mainMemECU1->setAccessLatency<DiscreteValueConstant>({100});

  /* HW_access_elems */
  HwAccessElement mainMemC1Access;
  mainMemC1Access.setDest(mainMemECU1);
  mainMemC1Access.setReadLatency<DiscreteValueConstant>({10});
  mainMemC1Access.setWriteLatency<DiscreteValueConstant>({10});
  core1ECU1->addHWAccessElement(mainMemC1Access);

  /* Software */
  auto ra = std::make_shared<Runnable>("runnableA");
  ra->addActivityGraphItem<Ticks>(DiscreteValueConstant(100));

  /** Event 
   * for details on the testscases, see MyExternalEventServer.h/.cpp
  */
  auto subscriber = std::make_shared<ExternalEventSubscriber>();
  auto externalEvent1 = std::make_shared<Event>("externalEvent1");
  auto externalEvent2 = std::make_shared<Event>("externalEvent2");
  auto externalEvent3 = std::make_shared<Event>("externalEvent3");
  auto externalEvent4 = std::make_shared<Event>("externalEvent4");
  auto externalEvent5 = std::make_shared<Event>("externalEvent5");
  EventManager::Inst<PubSubEventManager>()->addEvent(externalEvent1, subscriber); //subscribe to event
  EventManager::Inst<PubSubEventManager>()->addEvent(externalEvent2, subscriber);
  EventManager::Inst<PubSubEventManager>()->addEvent(externalEvent3, subscriber);
  EventManager::Inst<PubSubEventManager>()->addEvent(externalEvent4, subscriber);
  EventManager::Inst<PubSubEventManager>()->addEvent(externalEvent5, subscriber);

  // Stimuli
  auto event_stim1 = std::make_shared<EventStimulus>("eventStimulus1", externalEvent1);
  auto event_stim2 = std::make_shared<EventStimulus>("eventStimulus2", externalEvent2);
  auto event_stim3 = std::make_shared<EventStimulus>("eventStimulus3", externalEvent3);
  auto event_stim4 = std::make_shared<EventStimulus>("eventStimulus4", externalEvent4);
  auto event_stim5 = std::make_shared<EventStimulus>("eventStimulus5", externalEvent5);

  // Software Model  (Tasks)
  auto task1 = Task::createTask("task1");
  event_stim1->addTask(task1);
  task1->addActivityGraphItem<RunnableCall>(ra);
  task1->setActivationLimit(1);
  TaskAllocation ta1;
  ta1.setSchedulingParameter("priority", 1);
  task1->setTaskAllocation(ta1);

  auto task2 = Task::createTask("task2");
  event_stim2->addTask(task2);
  task2->addActivityGraphItem<RunnableCall>(ra);
  task2->setActivationLimit(1);
  TaskAllocation ta2;
  ta2.setSchedulingParameter("priority", 1);
  task2->setTaskAllocation(ta2);

  auto task3 = Task::createTask("task3");
  event_stim3->addTask(task3);
  task3->addActivityGraphItem<RunnableCall>(ra);
  task3->setActivationLimit(1);
  TaskAllocation ta3;
  ta3.setSchedulingParameter("priority", 1);
  task3->setTaskAllocation(ta3);

  auto task4 = Task::createTask("task4");
  event_stim4->addTask(task4);
  task4->addActivityGraphItem<RunnableCall>(ra);
  task4->setActivationLimit(1);
  TaskAllocation ta4;
  ta4.setSchedulingParameter("priority", 1);
  task4->setTaskAllocation(ta4);


  auto task5 = Task::createTask("task5");
  event_stim5->addTask(task5);
  task5->addActivityGraphItem<RunnableCall>(ra);
  task5->setActivationLimit(1);
  TaskAllocation ta5;
  ta5.setSchedulingParameter("priority", 1);
  task5->setTaskAllocation(ta5);
  
  // OS
  auto fppScheduler = std::make_shared<PriorityScheduler>("FPP Scheduler");
  fppScheduler->setExecutionCore(core1ECU1);
  fppScheduler->addTaskMapping(task1);
  fppScheduler->addTaskMapping(task2);
  fppScheduler->addTaskMapping(task3);
  fppScheduler->addTaskMapping(task4);
  fppScheduler->addTaskMapping(task5);

  // Trace
  TraceManager::createTracerFromList({"BtfTracerAgiBranch"});

  //finalization callbacks
  try{
    RequiresCallbackManager::Inst().applyPreSim();
  } catch (std::exception &e){
    RequiresCallbackManager::Inst().applyPostSim();
    const char *msg  = e.what();
    VLOG(0) << msg ;
    return -1;
  } 

  std::string vcdOutputFile = args.traceDirectory + "trace";
	sc_core::sc_trace_file *tf = sc_core::sc_create_vcd_trace_file(vcdOutputFile.c_str());
  scTrace(core1ECU1, "Core1ECU1");
  scTrace(mainMemECU1, "MainMemECU1");

  try {
	sc_core::sc_start(200, sc_core::SC_MS);
  } catch (sc_core::sc_report &e) {
    RequiresCallbackManager::Inst().applyPostSim();
    const char *file = e.get_file_name();
    const int line   = e.get_line_number();
    const char *msg  = e.get_msg();
    VLOG(0) << msg << "\nin file: " << file << "\nline: " << line;
    return -1;
  } catch (std::exception &e){
    RequiresCallbackManager::Inst().applyPostSim();
    const char *msg  = e.what();
    VLOG(0) << msg ;
    return -1;
  } 
  
  RequiresCallbackManager::Inst().applyPostSim();
  sc_core::sc_close_vcd_trace_file(tf);

  VLOG(0) << " done ";
  return 0;
}