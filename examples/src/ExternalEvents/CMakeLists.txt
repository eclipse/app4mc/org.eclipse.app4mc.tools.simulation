# leave redundant settings below in place (visual studio seems to default to c++ 11 if statements below are missing)
set (CMAKE_CXX_STANDARD 17 CACHE STRING
     "C++ standard to build all targets.")
set (CMAKE_CXX_STANDARD_REQUIRED ON CACHE BOOL
     "The with CMAKE_CXX_STANDARD selected C++ standard is a requirement.")
mark_as_advanced (CMAKE_CXX_STANDARD_REQUIRED)

add_subdirectory(../libs/sock_lib ${CMAKE_CURRENT_SOURCE_DIR})

project(ExternalEventExample)
add_executable(ExternalEventExample "ExternalEventsExample.cpp" "ExternalEventSubscriber.cpp")
target_link_libraries(ExternalEventExample app4mc.sim sockClient_lib PRIVATE 
    app4mc.sim::app4mc.sim
    SimParamParser)
target_include_directories(ExternalEventExample PRIVATE 
     ${CMAKE_INCLUDE_PATH}     
     ${CMAKE_CURRENT_SOURCE_DIR}/include)
#target_precompile_headers(ExternalEventExample REUSE_FROM app4mc.sim_lib)

#Skip test for now, as only windows implementation is functional
#add_test(NAME Run_ExternalScheduler COMMAND ExternalScheduler ${ADDITIONAL_TEST_ARGS} -o "${CMAKE_SOURCE_DIR}/test_data/traces/${PROJECT_NAME}")

# add_test(
#     NAME "${PROJECT_NAME}_CheckReference"
#     COMMAND ${PYTHON_INTERPRETER} ${PYTHON_TEST_RUNNER} ${REFERENCE_TRACE_LOC}/${PROJECT_NAME}/${TRACE_NAME} ${TRACE_LOC}/${PROJECT_NAME}/${TRACE_NAME}
# )

install(
    TARGETS ExternalEventExample 
    #RUNTIME DESTINATION ${CMAKE_INSTALL_PREFIX}/examples/ExternalEventExample}
)

add_subdirectory(server)