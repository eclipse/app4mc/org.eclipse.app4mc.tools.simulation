/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */


#include <memory>
#include "APP4MCsim.h"
#include "SimParamParser.h"

class ExampleRunner : public SimRunner{
public:
	void setup() override{

		/* ECU1 */

		/* 100Mhz*/
		Time ECU1_Freq_Domain_CycleTime = 10_ns;
		auto pudef = std::make_shared<ProcessingUnitDefinition>("ECU-CPU");
		auto ECU1 = modelRoot()->createSubStructure("ECU1");

		auto core1ECU1 = ECU1->createProcessingUnit("Core1ECU1",pudef);
		core1ECU1->addInitPort("outCore");
		core1ECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);


		//TODO fix size argument
		auto mainMemECU1 = ECU1->createMemory("MainMemECU1", 1 * 1024 * 1024 * 1024);
		mainMemECU1->addTargetPort("inMem");
		mainMemECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);
		mainMemECU1->setAccessLatency<DiscreteValueConstant>({100});

		auto interconnectECU1 = ECU1->createConnectionHandler("InterconnectECU1");
		interconnectECU1->addTargetPort("inInterconnect");
		interconnectECU1->addInitPort("outInterconnect");
		interconnectECU1->setClockPeriod(ECU1_Freq_Domain_CycleTime);
		interconnectECU1->setReadLatency<DiscreteValueConstant>({10});
		interconnectECU1->setWriteLatency<DiscreteValueConstant>({10});



		/* Connections */
		auto core2interconnect = ECU1->createConnection("core2interconnect");
		auto interconnect2mem = ECU1->createConnection("interconnect2mem");

		core1ECU1->bindConnection(core2interconnect, "outCore");
		interconnectECU1->bindConnection(core2interconnect, "inInterconnect");

		interconnectECU1->bindConnection(interconnect2mem, "outInterconnect");
		mainMemECU1->bindConnection(interconnect2mem, "inMem");

		/* HW_access_elems */
		HwAccessElement mainMemC1Access;
		mainMemC1Access.setDest(mainMemECU1);
		mainMemC1Access.setAccessPath({
			core2interconnect,
			//interconnectECU1, 
			interconnect2mem,
		});	
		core1ECU1->addHWAccessElement(mainMemC1Access);

		/* Software */
		auto channel1 = std::make_shared<Channel>("channel1", 64, 20, 0);
		MappingModel::addMemoryMapping(channel1, mainMemECU1);
		MappingModel::addMemoryAddress(channel1, 0);

		auto ra = std::make_shared<Runnable>("R_a");
		ChannelWrite channel1_write_A(channel1, 1);
		ra->addActivityGraphItem(channel1_write_A);
		ra->addActivityGraphItem<Ticks>(DiscreteValueConstant(100));

		auto rb = std::make_shared<Runnable>("R_b");
		rb->addActivityGraphItem<ChannelRead>(channel1, 2);
		rb->addActivityGraphItem<Ticks>(DiscreteValueConstant(1000));

		auto stimuli_5ms = std::make_shared<FixedPeriodicStimulus>("5ms_Stimuli", 5_ms);
		auto queue1WriteEvent = channel1->addEvent("queue1:write", ChannelEventType::WRITE_EVENT, ChannelEventStateType::DONE);
		auto channel1WriteStimulus = std::make_shared<EventStimulus>("queue1WriteStimulus", queue1WriteEvent);


		auto t1 = Task::createTask("T1");
		stimuli_5ms->addTask(t1);
		t1->addActivityGraphItem<RunnableCall>(ra);

		auto t2 = Task::createTask("T2");
		channel1WriteStimulus->addTask(t2);
		t2->addActivityGraphItem<RunnableCall>(rb);

		auto randomScheduler1 = std::make_shared<RandomScheduler>("RandomSched");
		randomScheduler1->setExecutionCore(core1ECU1);
		randomScheduler1->addTaskMapping(t1);
		randomScheduler1->addTaskMapping(t2);

		scTrace(core1ECU1, "Core1ECU1");
		scTrace(interconnectECU1, "InterconnectECU1");
		scTrace(mainMemECU1, "MainMemECU1");
		scTrace(core2interconnect, "core.interconnect");
		scTrace(interconnect2mem, "interconnect.mem");

	}
};

int main([[maybe_unused]] int argc,[[maybe_unused]] char *argv[]) {

	SimParam args;
	SimParamParser::parse(argc, argv, args);

	auto runner = ExampleRunner();
	EasyloggingConfig::configure("app4mcsim.log", 1, true, true);
	runner.enableTracers(args.tracerNames,args.traceDirectory, false);
	
	try {
		runner.simulate(TimeParameter<std::milli>(200));
	}
	catch (const std::runtime_error& e) {
		std::cerr << e.what();
		return -1;
	}
	return 0;
}
