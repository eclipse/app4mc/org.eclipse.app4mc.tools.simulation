/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Behnaz Pourmohseni <behnaz.pourmohseni@de.bosch.com>
 ********************************************************************************
 */


#include <memory>
#include "APP4MCsim.h"
#include "SimParamParser.h"

class ExampleRunner : public SimRunner{
private: 
public:

	void setup() override{
		/* Hardware */
		Time ECU_Freq_Domain_CycleTime = 10_ns;
		auto pudef = std::make_shared<ProcessingUnitDefinition>("CoreDef");
		auto ECU = modelRoot()->createSubStructure("ECU");

		auto core1 = ECU->createProcessingUnit("Core1", pudef);
		core1->setClockPeriod(ECU_Freq_Domain_CycleTime);


		/* Stimuli */
		auto stimulus1 = std::make_shared<SingleStimulus>("single_stim1", 0_us);
		auto stimulus2 = std::make_shared<SingleStimulus>("single_stim2", 2_us);
		auto stimulus3 = std::make_shared<SingleStimulus>("single_stim3", 1_us);


		/*  Software */
		auto task1 = Task::createTask("Task1");
		stimulus1->addTask(task1);
		stimulus2->addTask(task1);
		task1->addActivityGraphItem<Ticks>(DiscreteValueConstant(150));

		auto task2 = Task::createTask("Task2");
		stimulus3->addTask(task2);
		task2->addActivityGraphItem<Ticks>(DiscreteValueConstant(100));


		//--> process events and event stimuli
		auto event1 = task2->addEvent("t2_activate", ProcessEventType::ACTIVATE);
		auto evStimulus1 = std::make_shared<EventStimulus>("t2_activate_stim", event1);

		auto event2 = task2->addEvent("t2_start", ProcessEventType::START);
		auto evStimulus2 = std::make_shared<EventStimulus>("t2_start_stim", event2);

		auto event3 = task2->addEvent("t2_preempt", ProcessEventType::PREEMPT);
		auto evStimulus3 = std::make_shared<EventStimulus>("t2_preempt_stim", event3);

		auto event4 = task2->addEvent("t2_resume", ProcessEventType::RESUME);
		auto evStimulus4 = std::make_shared<EventStimulus>("t2_resume_stim", event4);

		auto event5 = task2->addEvent("t2_terminate", ProcessEventType::TERMINATE);
		auto evStimulus5 = std::make_shared<EventStimulus>("t2_terminate_stim", event5);

		auto task_received = Task::createTask("Task_ReceivedEventStim");
		task_received->addActivityGraphItem<Ticks>(DiscreteValueConstant(1));
		//use  task 2 activate event to trigger another task
		//all other events are going out of context with the setup() method --> call to destructor
		//hence no "stimulate()" thread shall be spawned for them 
		evStimulus1->addTask(task_received);
		
		//if we want to trigger task receiced upon all task2 process event, add the following lines
		// evStimulus2->addTask(task_received);
		// evStimulus3->addTask(task_received);
		// evStimulus4->addTask(task_received);
		// evStimulus5->addTask(task_received);
		// evStimulus6->addTask(task_received);
		//<--


		/* OS */
		auto scheduler = std::make_shared<PriorityScheduler>("fifoScheduler");
		scheduler->setExecutionCore(core1);
		scheduler->addResponsibleCore(core1);


		/* mapping */
		TaskAllocation ta_task1;
		ta_task1.setSchedulingParameter("priority", 10);
		task1->setTaskAllocation(ta_task1);

		TaskAllocation ta_task2;
		ta_task2.setSchedulingParameter("priority", 9);
		task2->setTaskAllocation(ta_task2);
		
		TaskAllocation ta_task_received;
		ta_task_received.setSchedulingParameter("priority", 0);
		task_received->setTaskAllocation(ta_task_received);

		scheduler->addTaskMapping(task1);
		scheduler->addTaskMapping(task2);
		scheduler->addTaskMapping(task_received);


		/* tracing */
		scTrace(core1, "Core1");
}
};

int main([[maybe_unused]] int argc,[[maybe_unused]] char *argv[]) {

	SimParam args;
	SimParamParser::parse(argc, argv, args);

	auto runner = ExampleRunner();
	EasyloggingConfig::configure("app4mcsim.log", 1, true, true);
	runner.enableTracers(args.tracerNames,args.traceDirectory, false);
	
	try {
		runner.simulate(TimeParameter<std::milli>(10));
	}
	catch (const std::runtime_error& e) {
		std::cerr << e.what();
		return -1;
	}
	return 0;
}