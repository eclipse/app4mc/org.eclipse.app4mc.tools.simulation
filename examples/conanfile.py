import os
from conans import ConanFile, tools
from conan.tools.cmake import CMake, CMakeToolchain, CMakeDeps, cmake_layout


class app4mcsim_examples_conan(ConanFile):
    name = "app4mc.sim.examples"
    version = "0.0.1"

    requires = [
        "app4mc.sim/0.0.1",
        "cli11/2.2.0",
        "gtest/cci.20210126"
    ]

    # Optional metadata
    license = "tbd"
    author = "tbd"
    url = "tbd"
    description = "Simulation library for Amalthea models"
    topics = ("simulation", "event simulation", "Amalthea", "APP4MC", "app4mc")

    # Binary configuration
    settings = {
        "os", 
        "build_type", 
        "arch", 
        "compiler"
        }
    options = {
        "shared": [True, False], 
        "fPIC": [True, False],
        "buildExamples" : [True, False], #not used in examples
        "verbose" : [True, False] # not used in examples
        }
    default_options = {
        "shared": True, 
        "fPIC": True,
        "buildExamples" : False,  #not used in examples
        "verbose" : False  #not used in examples
        }
    generators = {"CMakeDeps", "CMakeToolchain"} #{"cmake", "cmake_find_package"} #"cmake_paths"

    # Sources are located in the same place as this recipe, copy them to the recipe
    exports_sources = {"src/*.cpp", "src/*/include/*.h", "src/*/include/*.hpp", "CMakeLists.txt", "src/*CMakeLists.txt"}

    #no_copy_source = True

    #Remove unused options from package id generation
    def build_id(self):
        del self.info_build.options.buildExamples
        del self.info_build.options.verbose
    def package_id(self):
        del self.info.options.buildExamples
        del self.info.options.verbose

    
    def generate(self):
        tc = CMakeToolchain(self)
        if (self.settings.compiler == 'gcc'):
            cc="gcc-" + str(self.settings.compiler.version)
            cxx="g++-" + str(self.settings.compiler.version)
            tc.cache_variables["CMAKE_C_COMPILER"] = cc
            tc.cache_variables["CMAKE_CXX_COMPILER"] = cxx
        elif (self.settings.compiler == 'clang'):
            cc="clang-" + str(self.settings.compiler.version)
            cxx="clang++-" + str(self.settings.compiler.version) 
            tc.cache_variables["CMAKE_C_COMPILER"] = cc
            tc.cache_variables["CMAKE_CXX_COMPILER"] = cxx
        if str(self.settings.build_type).lower() == "debug":
            tc.cache_variables["CMAKE_CXX_FLAGS"] = str(" -g") + str(" -O0")
        tc.generate()
        deps = CMakeDeps(self)
        deps.generate()

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def layout(self):
        cmake_layout(self)
        #self.folders.source ="."
        self.folders.source =  os.path.abspath(".")
        self.folders.build = os.path.join("build", str(self.settings.build_type))
        self.folders.generators = os.path.join(self.folders.build, "conan")
        # this information is relative to the build folder
        self.cpp.build.libdirs = ["."]             # maps to ./build/<build_type>
        self.cpp.build.bindirs = ["bin"]           # maps to ./build/<build_type>/bin

    def build(self):
        cmake = CMake(self)
        #if str(self.settings.build_type).lower() == "debug":
        #    cmake.definitions["CONAN_CXX_FLAGS"] +=(" -g -O0")
        cmake.configure()
        cmake.build()

    def package(self):
        #self.copy("*.h")
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.bindirs = ["bin"]
        self.cpp_info.requires = ["app4mc.sim/", "cli11"]
