import os

from conan.errors import ConanInvalidConfiguration
from conan.tools.cmake import CMake, CMakeDeps, CMakeToolchain, cmake_layout
from conans import ConanFile, tools


class app4mcsim_lib_conan(ConanFile):
    name = "app4mc.sim"
    version = "0.0.1"
    # Optional metadata
    license = "tbd"
    author = "tbd"
    url = "https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.tools.simulation.git"
    branch = "main"
    description = "Simulation library for Amalthea models"
    topics = ("simulation", "event simulation", "Amalthea", "APP4MC", "app4mc")

    # Binary configuration
    settings = {"os", "build_type", "arch", "compiler"}
    options = {
        "shared": [True, False],
        "fPIC": [True, False],
        "buildExamples": [True, False],
        "verbose": [True, False],
    }
    default_options = {
        "shared": True,
        "fPIC": True,
        "buildExamples": False,
        "verbose": False,
    }
    generators = {"CMakeDeps", "CMakeToolchain"}

    def validate(self):
        if int(self.settings.compiler.cppstd.value) < 17:
            raise ConanInvalidConfiguration("app4mc.sim requires at least C++17")

    def export_sources(self):
        localSources = [
            "src/",
            "include/",
            "cmake/",
            "licenses/",
            "CMakeLists.txt",
            "examples/src/",
            "examples/test_data/",
            "examples/CMakeLists.txt",
        ]
        if all(os.path.exists(s) for s in localSources):
            # copy local sources
            self.output.info("using local sources to build")
            [self.copy("{}*".format(s) if s.endswith("/") else s, ".") for s in localSources]
        elif any(os.path.exists(s) for s in localSources):
            # some, but not all, sources are present, issue an error as this is most likely
            # accidental and shall not be mitigated/hidden by cloning from a remote repo
            self.output.error("Local source file set is incomplete")

    def source(self):
        if not os.listdir("../export_source"):  # local sources have not been exported
            self.output.info("Cloning app4mc.sim from {} : {}".format(self.url, self.branch))
            git = tools.Git()
            git.clone(url=self.url, branch=self.branch, shallow = True)

    def requirements(self):
        self.requires("systemc/2.3.3")
        self.requires("easyloggingpp/9.97.0")
        self.requires("polymorphic_value/1.3.0")
        self.requires("effolkronium-random/1.4.0")
        self.requires("gcem/1.14.1")
        self.requires("pcg-cpp/cci.20210406")
        if self.options.buildExamples == "True":
            self.requires("cli11/2.2.0")
            self.requires("gtest/cci.20210126")

    def generate(self):
        tc = CMakeToolchain(self)
        # uncomment to use custom toolchain file name
        if (self.settings.compiler == 'gcc' and self.settings.compiler.version == 8):
             tc.cache_variables["APP4MCSIM_LIBSTDCXX_STATIC_LINK"] = "ON"
        if self.options.buildExamples:
            tc.variables["APP4MCSIM_BUILD_EXAMPLES"] = "ON"
        tc.variables["APP4MCSIM_PRESETFILE_ENABLED"] = "ON"
        tc.generate()
        deps = CMakeDeps(self)
        deps.generate()

        packageIdInfo = open("packageId.info", "w+", encoding="utf-8")
        packageIdInfo.write("app4mc.sim package hash is {}".format(self.info.package_id()))
        packageIdInfo.flush()

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def layout(self):
        # this information is relative to the source folder
        self.cpp.source.includedirs = [
            "include",
            "include/EventModel",
            "include/HardwareModel",
            "include/MappingModel",
            "include/OsModel",
            "include/SoftwareModel",
            "include/StimuliModel",
            "include/Tracer",
        ]  # maps to ../include
        self.folders.build = os.path.join("build", str(self.settings.build_type))
        self.folders.generators = os.path.join(self.folders.build, "conan")
        self.generatorsString = str(self.folders.generators)
        # this information is relative to the build folder
        self.cpp.build.libs = ["libapp4mc.sim.so"]
        self.cpp.build.libdirs = ["."]  # maps to ./build/<build_type>/
        self.cpp.build.bindirs = ["bin"]  # maps to ./build/<build_type>/bin

    def build(self):
        cmake = CMake(self)
        cmake.verbose = self.options.verbose
        cmake.configure()
        cmake.build()

    def package(self):
        cmake = CMake(self)
        self.copy("*", "licences", "licenses")
        cmake.install()

    # Exclude build examples and verbosity option from hash generation,
    # as they do not influence binary compatibility
    def build_id(self):
        del self.info_build.options.buildExamples
        del self.info_build.options.verbose

    def package_id(self):
        del self.info.options.buildExamples
        del self.info.options.verbose

    # using self.cpp.package in layout
    def package_info(self):
        self.cpp_info.includedirs = [
            "include",
            "include/EventModel",
            "include/HardwareModel",
            "include/MappingModel",
            "include/OsModel",
            "include/SoftwareModel",
            "include/StimuliModel",
            "include/Tracer",
        ]
        self.cpp_info.libdirs = ["lib"]
        self.cpp_info.libs = ["libapp4mc.sim.so"]
        self.env_info.app4mcsim_toolchain_loc = self.folders.generators
