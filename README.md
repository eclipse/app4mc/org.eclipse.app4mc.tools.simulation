
# Eclipse APP4MC - SystemC-based timing simulation

This repository contains the User Libraries that are required to run
a generated application in a timing simulation (based on SystemC).

To learn more about this project, read the [wiki](https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.tools.simulation/-/wikis/home). 

## Build

Todo

```
$ <command to buid>
```


## License

[Eclipse Public License (EPL) v2.0][1]

[1]: https://www.eclipse.org/legal/epl-2.0/