#!/usr/bin/python

import sys, getopt, os, sys

def errprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def __errprintUseMsg__():
    errprint(\
    """usage: configure.py \n\
        -h, --help
        -c, --compiler <GCC8(default)|GCC9|Clang|MSVC]>
        -t, --buildtype <Debug|Release>
        -b, --buildFolder <build folder, output folder or conan generated cmake files>
        -i, --installFolder <cmake install folder> 
        -g, --generator <Unix Makefiles|Visual Studio 16 2019|Ninja>
        -x, --conanBuildFlag <missing, buildAll, thisOnly>
        -e, --buildExamples
        """
    )

def main(argv):
    print("test")
    #defaults
    buildFolder = "build"
    installFolder = "install"
    compiler="gcc8"
    buildtype="Debug"
    conanBuildFlag="missing"
    generator = "\"Unix Makefiles\""
    buildExamples="False"

    #get  arguments
    try:
      opts, args = getopt.getopt(argv,"hc:t:b:i:g:x:e",
      ["help", "compiler=","buildtype=","buildFolder=","installFolder=","generator=","conanBuildFlag=", "buildExamples"])
    except getopt.GetoptError as err:
        errprint ("Unknown parameters" +err.msg)
        __errprintUseMsg__()
        sys.exit(2) 


    for opt, arg in opts:
        if opt in ("-h", "--help"):
            __errprintUseMsg__()
            sys.exit()
        elif opt in ("-c", "--compiler"):
            compiler = arg.lower()
        elif opt in ("-t", "--buildtype"):
            buildtype = arg
        elif opt in ("-b", "--buildFolder"):
            buildFolder = arg
        elif opt in ("-i", "--installFolder"):
            installFolder = arg
        elif opt in ("-g", "--generator"):
            generator = arg
        elif opt in ("-x", "--conanBuildFlag"):
            conanBuildFlag = arg.lower()
        elif opt in ("-e", "--buildExamples"):
            buildExamples = "True"

    errprint ("build.py parameters:")       
    errprint (" compiler: " + compiler)
    errprint (" buildtype: " + buildtype)
    errprint (" buildFolder: " + buildFolder)
    errprint (" installFolder: " + installFolder)
    errprint (" generator: " + generator)
    errprint (" conanBuildFlag: " + conanBuildFlag)
    if buildExamples=="False":
        errprint (" skip examples")
    else :
        errprint (" build examples")

    if sys.platform.lower() == "linux":
        if compiler.lower =="msvc":
            errprint("Compiler {} not supported on {} platform. Exit", compiler.upper(), sys.platform)
            sys.exit()
    elif (sys.platform.lower().startswith("win")):
        errprint ("Using {} compiler on {} host", compiler, sys.platform)
    else:
        errprint("Platform {} not supported. Exit", sys.platform)
        sys.exit()

#Flags for conan and CMake
    buildFolderWithType = "{}/{}".format(buildFolder, buildtype)

    conanFlags={  
        # "conanScriptsLoc" : "{}/conan".format(buildFolderWithType),
        # CLI parameter -x --conanBuildFlag <missing, buildAll, thisOnly>
        "missing": "--build=missing",
        "buildall" : "--build",
        "thisonly": "",
    }

    if not conanBuildFlag in conanFlags:
        errprint("usupported option for parameter -x --conanBuildFlag: {}".format(conanBuildFlag))
        __errprintUseMsg__()
        sys.exit()


# compose build commands
    #conan scripts generation, or disabling conan
    cmdConanInstall="conan install . {} -pr=./conan/profiles/{}{} -o buildExamples={} -c tools.cmake.cmaketoolchain.presets:max_schema_version=3"\
            .format(conanFlags[conanBuildFlag], compiler.capitalize(), buildtype.capitalize(), buildExamples)
    
    ## CMake configure
    cmdCmakeConfig="cmake -B {} -G {} --preset={}"\
        .format(buildFolderWithType, generator, buildtype.lower())

    ##CMake build
    cmdCmakeBuild="cmake --build {} --config {} --parallel".format(buildFolderWithType, buildtype)

    ## CMake install
    cmdCmakeInstall="cmake --install {} --prefix {}".format(buildFolderWithType, installFolder)

    ##Conan create: create app4mc.sim conan package in local cache (for now)
    # conanCreate="conan create . {} -pr=./conan/profiles/{}{}"\
    # .format(conanFlags[conanBuildFlag], compiler.capitalize(), buildtype.capitalize())
    conanCreate="conan create . {} -pr=./conan/profiles/{}{}"\
        .format(conanFlags[conanBuildFlag], compiler.capitalize(), buildtype.capitalize())

# dump build command to files (sh for linux, bat for windows)
    shellCmds = \
"""# 2 options to use CMake flow with conan (use on of the following commands)
# createConanPackage: create app4mc.sim conan package in local cache
# editable mode: use conan install and cmake aliases (or buildLocal)
alias conanInstall=\'{}\'
alias cmakeConf=\'{}\'
alias cmakeBuild=\'{}\'
alias cmakeInstall=\'{}\'
alias buildLocal=\'conanInstall && cmakeConf && cmakeBuild && cmakeInstall \'
alias createConanPackage=\'{}\'
""".format(cmdConanInstall, cmdCmakeConfig, cmdCmakeBuild, cmdCmakeInstall, conanCreate)

    try:
        f = open('build.sh', 'w')
        f.write("#!/bin/bash\n")
        f.write(shellCmds)
        f.close()
        print(shellCmds)
    except:
        errprint("build.py: failed to write bash file")

    # We need to restrict the version of CMakeUserPreset.json,
    # using tools.cmake.cmaketoolchain.presets:max_schema_version=3
    # as the default version 4, which is not available in CMake prior v3.23. 
    batchCmds = """\
        set conanInstall=\'{} -c tools.cmake.cmaketoolchain.presets:max_schema_version=3\'
        set cmakeConf=\'{}\'
        set cmakeBuild=\'{}\'
        set cmakeInstall=\'{}\'
        set buildLocal=\'conanInstall && cmakeConf && cmakeBuild && cmakeInstall \'
        set createConanPackage=\'{}\'
        """.format(\
                cmdConanInstall.replace('/', '\\'), \
                cmdCmakeConfig.replace('/', '\\'), \
                cmdCmakeBuild.replace('/', '\\'), \
                cmdCmakeInstall.replace('/', '\\'), \
                conanCreate.replace('/', '\\'))
    try:
        f = open('build.bat', 'w')
        f.write(batchCmds.replace("#", "REM"))
        f.close()
        print(batchCmds)
    except:
        errprint("build.bat: failed to write batch file")




if __name__ == "__main__":
   main(sys.argv[1:])
