# Why synchronize versions?
APP4MC is released regularly as a complete IDE (as a zip-file) or as an update site, that can be installed within the matching Eclipse IDE. Simulation and transformation are supposed to be available for each of those releases, hence their sources in Git repos shall be linked to the release version of APP4MC.

# Proposition: Use Git tags
When a set of sources for transformation and simulation is available for a specific version of APP4MC, these should be tagged **APP4MC_v[version number]**.

If bugfixes are required the tag comprises of the initial tag with a suffix **_bugfix#[ID]**. This ID should be traceable to some sort of bug report, ideally in issue tracking associated with the repository.

## Example version tree

```mermaid
graph BT;
F-->G;
D-->F;
direction LR
E-.tag.-T2[\"APP4MC_v2.0.0_bugfix#123"\];
D-->E;
D-.tag.-T1[\"APP4MC_v2.0.0"\]
C-->D;
B-->D;
C-.tag.-T0[\"APP4MC_v1.2.0"\];
A-->C;
A-->B;
```