/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */
#pragma once

#include "Types.h"
#include <cstdint>
#include <string>
#include "TimeUtils.h"
#include <random>


struct app4mcsim_seed {
  static constexpr int integerCeilDiv(int nom, int den) {
	return (nom + den - 1) / den;
  }
  
  uint64_t operator()();

 private:
  static uint64_t& getSeed();

 public:
  static void setSeed(uint64_t seed);
  static std::string getSeedString();
};



template<typename T>
class IValueDeviation{
    public:
    virtual T getSample()=0;
    virtual ~IValueDeviation(){};

};

using ITimeDeviation = IValueDeviation<Time>;
using IDiscreteValueDeviation = IValueDeviation<ELong>;
using IContinuousValueDeviation = IValueDeviation<double>;

template<typename T>
class ValueInterval{
    T lowerBound;
    T upperBound;
};

template<typename T>
class IValueDeviationConstant : public IValueDeviation<T>{
   public:
    const T value;
    IValueDeviationConstant(T _value): value(_value) {};
    virtual T getSample() override {
        return value;
    };
 
};

using TimeConstant = IValueDeviationConstant<Time>;
using DiscreteValueConstant = IValueDeviationConstant<ELong>;
using ContinuousValueConstant = IValueDeviationConstant<double>;

 template <typename T> 
class ValueGaussDistribution : public IValueDeviation<T> {
  std::normal_distribution<double> dist;
  T lowerBound, upperBound;

//mean and sd are of type Time for Time distributions but double for the others, so we deduce the type here for the proper constructor
  typedef std::conditional_t<std::is_same<T,Time>::value, 
                          Time, 
                          double> meanSdType;

static constexpr T minV(){
  if constexpr (std::is_same<T, Time>::value)
    return Time::min();
  else
    return std::numeric_limits<T>::min();
}

static constexpr T maxV(){
  if constexpr (std::is_same<T, Time>::value)
    return Time::max();
  else
    return std::numeric_limits<T>::max();
}
public:
  ValueGaussDistribution(meanSdType mean, meanSdType sd,T lowerBound = minV(),T upperBound = maxV());
  virtual T getSample() override;
  virtual ~ValueGaussDistribution(){};
};

using TimeGaussDistribution = ValueGaussDistribution<Time>;
using DiscreteValueGaussDistribution = ValueGaussDistribution<ELong>;
using ContinuousValueGaussDistribution = ValueGaussDistribution<double>;

//constexpr bool const_isnan(double x) { return (x != x); }

template <typename T>
class ValueWeibullDistribution : public IValueDeviation<T> {
  public:
  struct weibullParam {
    double shape = 1;
    double scale = 1;
    double requestedPRemain = 0;
    double achievedPRemain = 0;
  };
  private:
  T lowerBound;
  T upperBound;
  std::weibull_distribution<double> dist;

  static constexpr double paramZeroFunction(double shape, double upperBound,
                                            double p_upperBound, double mean);

public:
  ValueWeibullDistribution(double shape, double scale, T lowerBound,
                           T upperBound);
  ValueWeibullDistribution(weibullParam param, T lowerBound, T upperBound);

  static constexpr weibullParam _findParameter(
    double mean,double pRemainPromille,double lowerBound, double upperBound);

  typedef std::conditional_t<std::is_same<T,Time>::value, Time, double> meanType;

  static constexpr weibullParam findParameter(
    meanType mean, double pRemainPromille,T lowerBound, T upperBound) ;
  
  virtual T getSample() override;
};

using TimeWeibullEstimatorsDistribution = ValueWeibullDistribution<Time>;
using DiscreteValueWeibullEstimatorsDistribution = ValueWeibullDistribution<ELong>;
using ContinuousValueWeibullEstimatorsDistribution = ValueWeibullDistribution<double>;

template <typename T>
class ValueStatistics : public ValueGaussDistribution<T>{

  typedef std::conditional_t<std::is_same<T,Time>::value, 
                          Time, 
                          double> meanType;
public:
  ValueStatistics(meanType mean, T lowerBound, T upperBound): ValueGaussDistribution<T>(mean,mean/10,lowerBound,upperBound){};

  virtual ~ValueStatistics(){};
};

using TimeStatistics = ValueStatistics<Time>;
using DiscreteValueStatistics = ValueStatistics<ELong>;
using ContinuousValueStatistics = ValueStatistics<double>;

template <typename T>
class ValueUniformDistribution : public IValueDeviation<T> {
  //since there are different distribution types for integer and double values, we deduce the type here
  typedef std::conditional_t<std::is_same<T,ELong>::value || std::is_same<T,Time>::value, 
                          std::uniform_int_distribution<ELong>, 
                          std::uniform_real_distribution<double> > distType;
  typedef std::conditional_t<std::is_same<T,ELong>::value || std::is_same<T,Time>::value, 
                          ELong, double> paramType;
  distType dist;
  T lowerBound;
  T upperBound;
public:
  ValueUniformDistribution(T lowerBound, T upperBound);
  virtual T getSample() override;
  virtual ~ValueUniformDistribution(){};
};


using TimeUniformDistribution = ValueUniformDistribution<Time>;
using DiscreteValueUniformDistribution = ValueUniformDistribution<ELong>;
using ContinuousValueUniformDistribution = ValueUniformDistribution<double>;

 template <typename T>
class ValueBetaDistribution : public IValueDeviation<T> {
 
 //since the standard do not include a beta distribution, we simulate by 2 independent gamma distributions
 // see: https://en.wikipedia.org/wiki/Beta_distribution#Derived_from_other_distributions
  std::gamma_distribution<double> dist1;
  std::gamma_distribution<double> dist2;
  
  T lowerBound;
  T upperBound;
  double scaling;
public:
  ValueBetaDistribution(double alpha, double beta, T lowerBound, T upperBound);
  virtual T getSample() override;
  virtual ~ValueBetaDistribution(){};
};

using TimeBetaDistribution = ValueBetaDistribution<Time>;
using DiscreteValueBetaDistribution = ValueBetaDistribution<ELong>;
using ContinuousValueBetaDistribution = ValueBetaDistribution<double>;

enum class BoundariesSamplingType{
  AverageCase,
  BestCase,
  CornerCase,
  Uniform,
  WorstCase
};

template <typename T>
class ValueBoundaries : public IValueDeviation<T> {
 
 //since the standard do not include a beta distribution, we simulate by 2 independent gamma distributions
 // see: https://en.wikipedia.org/wiki/Beta_distribution#Derived_from_other_distributions
  std::gamma_distribution<double> dist1;
  std::gamma_distribution<double> dist2;
  
  T lowerBound;
  T upperBound;
  double scaling;
  static constexpr double alphaFromType(BoundariesSamplingType type);
  static constexpr double betaFromType(BoundariesSamplingType type);
public:
  ValueBoundaries(BoundariesSamplingType type, T lowerBound, T upperBound);
  virtual T getSample() override;
  virtual ~ValueBoundaries(){};
};

using TimeBoundaries = ValueBoundaries<Time>;
using DiscreteValueBoundaries = ValueBoundaries<ELong>;
using ContinuousValueBoundaries = ValueBoundaries<double>;



extern template class ValueGaussDistribution<ELong>;
extern template class ValueGaussDistribution<double>;
extern template class ValueGaussDistribution<Time>;

extern template class ValueWeibullDistribution<ELong>;
extern template class ValueWeibullDistribution<double>;
extern template class ValueWeibullDistribution<Time>;

extern template class ValueUniformDistribution<ELong>;
extern template class ValueUniformDistribution<double>;
extern template class ValueUniformDistribution<Time>;

extern template class ValueBetaDistribution<ELong>;
extern template class ValueBetaDistribution<double>;
extern template class ValueBetaDistribution<Time>;

extern template class ValueBoundaries<ELong>;
extern template class ValueBoundaries<double>;
extern template class ValueBoundaries<Time>;


#ifdef ADDED_NOMINMAX
  #undef NOMINMAX
#endif
