/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include <unordered_map>
#include <memory>
#include <utility>
#include <optional>
#include "Memory.h"


class MemoryImpl;
class AbstractMemoryElement;


class MappingModel {
private:
	static std::unordered_map<std::shared_ptr<AbstractMemoryElement>, std::pair<std::shared_ptr<MemoryImpl>,uint64_t>>& memoryMappings();	
	
public:
	static std::shared_ptr<MemoryImpl>	getMemoryMapping  (const std::shared_ptr<AbstractMemoryElement>&_element);

	static void addMemoryMapping(
		std::shared_ptr<AbstractMemoryElement> _element,
		std::shared_ptr<MemoryImpl> _memory) ;


	//overload if argument is a wrapper around wrapped memory (= WrappedType<MemoryImpl>)
	inline static void addMemoryMapping(
		std::shared_ptr<AbstractMemoryElement> _element,
		std::shared_ptr<Memory> _memory)
	{	
		addMemoryMapping(_element, _memory->internalInst());
	}


	static uint64_t getMemoryAddress (const std::shared_ptr<AbstractMemoryElement> &_element);
	static void addMemoryAddress(
		std::shared_ptr<AbstractMemoryElement> _element,
		uint64_t _address);
	static std::optional<std::pair<std::shared_ptr<MemoryImpl>,uint64_t>> getCompleteMapping(const std::shared_ptr<AbstractMemoryElement> &_element);
	static std::string toString();
} ;