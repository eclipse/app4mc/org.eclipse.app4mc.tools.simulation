/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */
#include <cstdint>
#pragma once

namespace sc_core{
	class sc_time;
}

enum class DataRateUnit : uint64_t{
	bitPerSecond = 1,
	kbitPerSecond = 1000 * bitPerSecond,
	MbitPerSecond = 1000 * kbitPerSecond,
	GbitPerSecond = 1000 * MbitPerSecond,
	TbitPerSecond = 1000 * GbitPerSecond,
	KibitPerSecond = 1024,
	MibitPerSecond = 1024 * KibitPerSecond,
	GibitPerSecond = 1024 * MibitPerSecond,
	TibitPerSecond = 1024 * GibitPerSecond
};

struct DataRate {
	uint64_t value = 0;
	DataRateUnit unit = DataRateUnit::bitPerSecond;
	
	[[nodiscard]] sc_core::sc_time getTransmissionDuration(uint64_t size_bytes) const;
};

