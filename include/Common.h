/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once


#include <algorithm>
#include <vector>

template<typename T>
inline bool vectorContains(const std::vector<T> vec, const T &elem){
	auto result = std::find(vec.cbegin(),vec.cend(),elem);
	return result != vec.end();
}

template<typename T>
inline void vectorRemove(std::vector<T> &vec, const T &elem){
    vec.erase(std::remove(vec.begin(), vec.end(), elem), vec.end());
}
