/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 *           Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include <functional>
#include <memory>
#include <type_traits>
#include <utility>
#include <vector>
#include "ModeLiteral.h"
#include "RelationalOperator.h"

class Condition;


class ConditionContainer{
private: 
	std::vector<std::shared_ptr<Condition>> entries;
 
protected:
	std::vector<std::shared_ptr<Condition>>& getConditionEntries()  {return entries;}

public:	
	template<typename ConditionType, typename... Args, typename std::enable_if < std::is_base_of<Condition, ConditionType>::value, bool>::type = true>
	void addCondition(Args&&... args){
		entries.push_back(std::make_shared<ConditionType> (std::forward<Args>(args)...));
	}
};
