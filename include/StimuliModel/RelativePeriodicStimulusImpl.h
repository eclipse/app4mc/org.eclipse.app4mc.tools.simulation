/**
 ********************************************************************************
 * Copyright (c) 2021 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */
#pragma once
#include "Deviation.h"
#include "StimulusImpl.h"
#include "EasyloggingMacros.h"
#include "TimeUtilsSystemC.h"
#include "polymorphic_value.h"


class RelativePeriodicStimulusImpl : public StimulusImpl{
private:

    Time offset;
	isocpp_p0201::polymorphic_value<ITimeDeviation> nextOccurrence;
	sc_core::sc_process_handle handle;

	void stimulate(){
        sc_core::wait(convertTime(offset));
		while (true) {
			VLOG(2) << "RelativePeriodicStimulus " << getDescription();
			StimulusImpl::stimulate();
			auto nextOcurrence = std::max<Time>(nextOccurrence->getSample(),Time(0));
			sc_core::wait(convertTime(nextOcurrence));
		}
	}

public:
	template<typename T>
	explicit RelativePeriodicStimulusImpl(const std::string &name, const T &nextOccurrence, 
		const Time &offset = Time(0)) : 
		StimulusImpl(
			name, 
			[&](){
				RelativePeriodicStimulusImpl::stimulate();
			}
		),
		offset(offset),
		nextOccurrence(isocpp_p0201::make_polymorphic_value<ITimeDeviation,T>(nextOccurrence))
	{}
	
	std::string getDescription() const override{
		//MAYBE: get more information out of deviations
		std::stringstream tmp;
		tmp << "RelativePeriodicStimulus @" << this;
		return tmp.str();
	}
	uint64_t instanceCount() const override {
		return instanceCounter;
	}
};