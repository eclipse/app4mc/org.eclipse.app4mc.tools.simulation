/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include <map>
#include <memory>
#include "TimeUtils.h"
#include "WrappedType.h"
#include "Stimulus.h"

class SingleStimulusImpl;
class Task;
class StimulusImpl;

class SingleStimulus: 
	public WrappedType<SingleStimulusImpl>,
	public Stimulus,
	public std::enable_shared_from_this<SingleStimulus>
{
	std::shared_ptr<Stimulus> getSharedPtrThis() override;// { return shared_from_this(); }
	std::shared_ptr<StimulusImpl> getWrappedInst() override;// { return internalInst(); }
public:
	SingleStimulus(const std::string &name, const Time &occurrence);
	explicit SingleStimulus(const std::shared_ptr<SingleStimulusImpl>& _inst);
	HasCondition& getConditionObj() override;
};
