/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH and others
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */
#pragma once
#include "Event.h"
#include "StimulusImpl.h"
#include "EventManager.h"
#include "EasyloggingMacros.h"

class EventStimulusImpl : public StimulusImpl{
private:
	std::shared_ptr<Event> event;
	uint64_t instanceCounter = 0;

protected:
	void stimulate()  {
		while (true) {
			sc_core::wait(*event->scEvent);
			//add a delta cycle here to ensure events scheduled at this time pass the wait statement above
			//otherwise notifications issued from external sources with time 0 s could swallow these events
			//this might lead to unpredictable behavior
			sc_core::wait(sc_core::SC_ZERO_TIME);
			VLOG(1)<<event->getName()<<" @ "<<sc_core::sc_time_stamp().to_string();
			StimulusImpl::stimulate();
		}
	}

public:
	EventStimulusImpl(const std::string &_name, std::shared_ptr<Event> _event) : 
		StimulusImpl(
			_name, 
			[&](){
				EventStimulusImpl::stimulate();
			}
		),
		event(std::move(_event))
	{}


	uint64_t instanceCount() const override {
		return instanceCounter;
	}

	const std::shared_ptr<Event>& getEvent(){
		return event;
	}
};
