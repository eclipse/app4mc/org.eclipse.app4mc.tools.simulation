/**
 ********************************************************************************
 * Copyright (c) 2021 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */
#pragma once
#include "StimulusImpl.h"
#include "EasyloggingMacros.h"
#include "TimeUtilsSystemC.h"

class FixedPeriodicStimulusImpl : public StimulusImpl{
private:

	Time recurrence;
	Time offset;
	sc_core::sc_process_handle handle;

	void stimulate(){
		sc_core::wait(convertTime(offset));
		while (true) {
			VLOG(2) << "FixedPeriodicStimulus " << getDescription();
			StimulusImpl::stimulate();
			sc_core::wait(convertTime(recurrence));
		}
	}
public:

	FixedPeriodicStimulusImpl(const std::string &name, const Time &recurrence, const Time &offset = Time(0)) : 
		StimulusImpl(
			name, 
			[&](){
				FixedPeriodicStimulusImpl::stimulate();
			}
		),
		recurrence(recurrence),
		offset(offset)
	{}


	std::string getDescription() const override{
		std::stringstream tmp;
		tmp << "FixedPeriodicStimulus @" << this << ", recurrence = " << recurrence.count() << "ps";
		return tmp.str();
	}
	uint64_t instanceCount() const override {
		return instanceCounter;
	}
};
