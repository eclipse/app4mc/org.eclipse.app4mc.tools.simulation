/**
 ********************************************************************************
 * Copyright (c) 2021 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 * - Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */
#pragma once
#include "StimulusImpl.h"

class InterProcessStimulusImpl  : public StimulusImpl{
    
    uint64_t counterInit = 1;
    uint64_t counterState = 1;
    sc_core::sc_event stimulusEvent;
	sc_core::sc_process_handle handle;

public:
    explicit InterProcessStimulusImpl(const std::string &name) : 
    StimulusImpl(
			name, 
			[&](){
				InterProcessStimulusImpl::stimulate();
			}
		)
    {}

    void setCounter(uint64_t counter);
    sc_core::sc_event &getStimulusEvent();
    std::string getDescription() const override;
	void stimulate();
};