/**
 ********************************************************************************
 * Copyright (c) 2021 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 * - Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */
#pragma once
#include "StimulusImpl.h"
#include "TimeUtils.h"

class SingleStimulusImpl : public StimulusImpl{
private:
	Time occurrence;
	void stimulate();
	sc_core::sc_process_handle handle;
public:

	SingleStimulusImpl(const std::string &name, const Time &occurrence) : 
		StimulusImpl(
			name, 
			[&](){
				SingleStimulusImpl::stimulate();
			}
		) , 
		occurrence(occurrence)
	{}


	std::string getDescription() const override;
	uint64_t instanceCount()  const override;
	
};
