/**
 ********************************************************************************
 * Copyright (c) 2021 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */
#pragma once
#include "Deviation.h"
#include "StimulusImpl.h"
#include "EasyloggingMacros.h"
#include "TimeUtilsSystemC.h"
#include "polymorphic_value.h"


class PeriodicStimulusImpl : public StimulusImpl{
private:

	Time recurrence;
    Time offset;
	isocpp_p0201::polymorphic_value<ITimeDeviation> jitter;
	Time minDistance;
	sc_core::sc_process_handle handle;

	void stimulate() {
		auto firstJitter = std::max<Time>(jitter->getSample(),minDistance);
        sc_core::wait(convertTime(offset+firstJitter));
		while (true) {
			VLOG(2) << "PeriodicStimulus " << getDescription();
			StimulusImpl::stimulate();
			auto nextRecurrence = std::max<Time>(recurrence+jitter->getSample(),minDistance);
			sc_core::wait(convertTime(nextRecurrence));
		}
	}
public:
	template<typename T>     
	explicit PeriodicStimulusImpl(const std::string &name, const Time &recurrence,
	 	const Time &offset = Time(0), const T &jitter = TimeConstant(Time(0)),
		const Time &minDistance = Time(0)):
		StimulusImpl(
			name, 
			[&](){
				PeriodicStimulusImpl::stimulate();
			}
		),
		recurrence(recurrence),
		offset(offset), 
		jitter(isocpp_p0201::make_polymorphic_value<ITimeDeviation,T>(jitter)),
		minDistance(minDistance)
	{}

	std::string getDescription() const override{
		std::stringstream tmp;
		tmp << "PeriodicStimulus @" << this << ", recurrence = " << recurrence.count() << "ps";
		return tmp.str();
	}
	uint64_t instanceCount() const override {
		return instanceCounter;
	}
};
