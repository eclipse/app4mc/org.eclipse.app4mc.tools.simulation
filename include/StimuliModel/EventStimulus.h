/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include <map>
#include <memory>
#include "TimeUtils.h"
#include "WrappedType.h"
#include "Stimulus.h"

class Event;
class EventStimulusImpl;
class Task;
class StimulusImpl;

class EventStimulus: 
	public WrappedType<EventStimulusImpl>, 
	public Stimulus, 
	public std::enable_shared_from_this<EventStimulus>
{
	std::shared_ptr<Stimulus> getSharedPtrThis() override;// { return shared_from_this(); }
	std::shared_ptr<StimulusImpl> getWrappedInst() override;// { return internalInst(); }
public:
	EventStimulus(const std::string &_name, std::shared_ptr<Event> _event);
	explicit EventStimulus(const std::shared_ptr<EventStimulusImpl>& _inst);
	HasCondition& getConditionObj() override;
	const std::shared_ptr<Event>& getEvent();

};
