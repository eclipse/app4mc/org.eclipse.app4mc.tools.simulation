/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include <map>
#include <memory>
#include "Deviation.h"
#include "TimeUtils.h"
#include "WrappedType.h"
#include "Stimulus.h"

class PeriodicStimulusImpl;
class Task;
class StimulusImpl;


class PeriodicStimulus: 
	public WrappedType<PeriodicStimulusImpl>, 
	public Stimulus,
	public std::enable_shared_from_this<PeriodicStimulus>
{
	std::shared_ptr<Stimulus> getSharedPtrThis() override;// { return shared_from_this(); }
	std::shared_ptr<StimulusImpl> getWrappedInst() override;// { return internalInst(); }
public:
	template<class T>  
	PeriodicStimulus(const std::string &name,
					 const Time &recurrence, 
					 const Time &offset = Time(0),
					 const T &jitter = TimeConstant(Time(0)),
					 const Time &minDistance = Time(0));
	explicit PeriodicStimulus(const std::shared_ptr<PeriodicStimulusImpl>& _inst);
	HasCondition& getConditionObj() override;
};

extern template PeriodicStimulus::PeriodicStimulus(const std::string&, const Time&, const Time&, const TimeConstant&, const Time&);
extern template PeriodicStimulus::PeriodicStimulus(const std::string&, const Time&, const Time&, const TimeGaussDistribution&, const Time&);
extern template PeriodicStimulus::PeriodicStimulus(const std::string&, const Time&, const Time&, const TimeWeibullEstimatorsDistribution&, const Time&);
extern template PeriodicStimulus::PeriodicStimulus(const std::string&, const Time&, const Time&, const TimeUniformDistribution&, const Time&);
extern template PeriodicStimulus::PeriodicStimulus(const std::string&, const Time&, const Time&, const TimeBetaDistribution&, const Time&);
extern template PeriodicStimulus::PeriodicStimulus(const std::string&, const Time&, const Time&, const TimeBoundaries&, const Time&);


