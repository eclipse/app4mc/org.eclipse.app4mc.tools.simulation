/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include <map>
#include <memory>
#include "Deviation.h"
#include "TimeUtils.h"
#include "WrappedType.h"
#include "Stimulus.h"

class RelativePeriodicStimulusImpl;
class Task;
class StimulusImpl;


class RelativePeriodicStimulus: 
	public WrappedType<RelativePeriodicStimulusImpl>,
	public Stimulus,
	public std::enable_shared_from_this<RelativePeriodicStimulus>
{
	std::shared_ptr<Stimulus> getSharedPtrThis() override;// { return shared_from_this(); }
	std::shared_ptr<StimulusImpl> getWrappedInst() override;// { return internalInst(); }
public:
	template<class T>  
	RelativePeriodicStimulus(const std::string &name,
					 const T &nextOccurrence, 
					 const Time &offset = Time(0));
	explicit RelativePeriodicStimulus(const std::shared_ptr<RelativePeriodicStimulusImpl>& _inst);
	HasCondition& getConditionObj() override;
};

extern template RelativePeriodicStimulus::RelativePeriodicStimulus(const std::string&, const TimeConstant&, const Time&);
extern template RelativePeriodicStimulus::RelativePeriodicStimulus(const std::string&, const TimeGaussDistribution&, const Time&);
extern template RelativePeriodicStimulus::RelativePeriodicStimulus(const std::string&, const TimeWeibullEstimatorsDistribution&, const Time&);
extern template RelativePeriodicStimulus::RelativePeriodicStimulus(const std::string&, const TimeUniformDistribution&, const Time&);
extern template RelativePeriodicStimulus::RelativePeriodicStimulus(const std::string&, const TimeBetaDistribution&, const Time&);
extern template RelativePeriodicStimulus::RelativePeriodicStimulus(const std::string&, const TimeBoundaries&, const Time&);


