/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#pragma once

#include <memory>
#include "HasCondition.h"

class StimulusImpl;
class Task;

class Stimulus{
protected:
	virtual HasCondition& getConditionObj()=0;
	virtual std::shared_ptr<Stimulus> getSharedPtrThis() =0;
	virtual std::shared_ptr<StimulusImpl> getWrappedInst() =0;

public:		
	void addTask(const std::shared_ptr<Task>&);
	
	template<typename ConditionType, typename... Args, typename std::enable_if < std::is_base_of<Condition, ConditionType>::value, bool>::type = true>
	void addCondition(Args&&... args){
		getConditionObj().addCondition<ConditionType>(std::forward<Args>(args)...);
	}

	template<typename... Args>
	void addValueCondition(Args&&... args){
		addCondition<ModeValueCondition>(std::forward<Args> (args)...);
	}
	template<typename... Args>
	void addChannelFillCondition(Args&&... args){
		addCondition<ChannelFillCondition>(std::forward<Args> (args)...);
	}
	template<typename... Args>
	void addLabelCondition(Args&&... args){
		addCondition<ModeLabelCondition>(std::forward<Args> (args)...);
	}

};
