/**
 ********************************************************************************
 * Copyright (c) 2021 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 * - Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */
#pragma once
#include <memory>
#include "HasName.h"
#include "HasDescription.h"
#include "HasCondition.h"
#include "memory"
#include "Task.h"
#include "StimulusRegistry.h"
#include <sysc/kernel/sc_status.h>
#include <sysc/kernel/sc_dynamic_processes.h>
#include <sysc/kernel/sc_object.h>


class StimulusImpl : 
	public HasName, 
	public HasDescription, 
	public HasCondition, 
	public std::enable_shared_from_this<StimulusImpl>,
	public sc_core::sc_object
{
private:
	sc_core::sc_process_handle handle;

public:
	void addTask(const std::shared_ptr<Task>& task)  {
		tasks.push_back(task);
		//task->addStimulus(shared_from_this());
	}

	explicit StimulusImpl(const std::string &name) : HasName(name), sc_core::sc_object(name.c_str()) {}
	
	explicit StimulusImpl(const std::string &name, const std::function<void()>& stimFn) : HasName(name), sc_core::sc_object(name.c_str()) {
		if (sc_core::sc_is_running()){ //== (sc_get_status() & (SC_RUNNING | SC_PAUSED))
			//if simulation is running
			//spawn immediately,do not spawn before start of simulation.
			handle = sc_core::sc_spawn(stimFn);
		}else {
			//register the function to spawn the thread with in the registry
			//if will only spawn the thread when simulation is started.
			//Should the stimulus be deconstructed (e.g. due to loss of containing context),
			//the Simulus will deregister from in its destructor
			StimulusRegistry::Inst().add(
				name, 
				[=](){
					// sc_core::sc_spawn(sc_bind(stimFn, this));
					sc_core::sc_spawn(stimFn);
				}
			);
		}
	}

	~StimulusImpl(){
		StimulusRegistry::Inst().erase(getName());
	}
	
	[[nodiscard]] virtual uint64_t instanceCount() const {return instanceCounter;};

	[[nodiscard]] StimulusImpl traceCopy() const {
		return {getName(),instanceCount()};
	};

	[[nodiscard]] std::string getDescription() const override {
		return "StimulusImpl";
	}

protected:
	void stimulate();
	uint64_t instanceCounter = 0;
	std::vector<std::shared_ptr<Task>> tasks;
	StimulusImpl(const std::string &name, uint64_t instanceCount): HasName(name), instanceCounter(instanceCount){};

};
