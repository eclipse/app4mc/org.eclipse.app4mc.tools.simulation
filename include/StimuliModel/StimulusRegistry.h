/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include <sysc/kernel/sc_module.h>
#include <functional>
#include <map>
#include <memory>
#include <set>


#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>

class StimulusRegistry{
private:
	std::map<std::string, std::function<void()>> callbacks;

	StimulusRegistry() = default;

	static StimulusRegistry& internalInst() {
  		static StimulusRegistry instance = StimulusRegistry();
		return instance;
	}
	
public:
 	static StimulusRegistry &Inst(){
		return StimulusRegistry::internalInst();
	}

	void add(const std::string& name, std::function<void()> f){
		internalInst().callbacks.emplace(name, f);
	}

	void erase(const std::string& name){
		auto it = internalInst().callbacks.find(name);
		if (it != internalInst().callbacks.end()){
			internalInst().callbacks.erase(it);
		}
	}

	void stimulate(){
		for (const auto& [n, f] :callbacks){
			f();
		}
		callbacks.clear();
	}

	void clear(){
		callbacks.clear();
	}
};

#ifdef ADDED_NOMINMAX
#undef NOMINMAX
#endif
