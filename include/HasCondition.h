/**
 ********************************************************************************
 * Copyright (c) 2021 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 * - Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */
#pragma once
#include <optional>
#include <memory>
#include "Condition.h"
#include "ConditionDisjunction.h"
#include "ModeLabelCondition.h"
#include "ModeValueCondition.h"
#include "ChannelFillCondition.h"

class ConditionConjunction;

class HasCondition {
	
private:
	std::optional<ConditionDisjunction> condition;
	
public:

	template<typename ConditionType, typename... Args, typename std::enable_if < std::is_base_of<Condition, ConditionType>::value, bool>::type = true>
	void addCondition(Args&&... args){
		if (!condition)
			condition.emplace();
		condition->addCondition<ConditionType>(std::forward<Args>(args)...);
	}

	template<typename... Args>
	void addValueCondition(Args&&... args){
		addCondition<ModeValueCondition>(std::forward<Args> (args)...);
	}
	template<typename... Args>
	void addChannelFillCondition(Args&&... args){
		addCondition<ChannelFillCondition>(std::forward<Args> (args)...);
	}
	template<typename... Args>
	void addLabelCondition(Args&&... args){
		addCondition<ModeLabelCondition>(std::forward<Args> (args)...);
	}

	//evaluation
	bool checkCondition();

	static std::string conditionNotMetMsg(const std::string& name, const std::string& containerName);

};

