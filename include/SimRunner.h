/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */
#pragma once


#include <sys/types.h>
#include <functional>
#include <memory>
#include <stdexcept>
#include <type_traits>
#include "Deviation.h"
#include "EasyloggingConfig.h"
#include "HwStructure.h"
#include "TimeUtils.h"
#include "WrappedType.h"
#include "iostream"

using timestamp_ns = std::chrono::nanoseconds;

class SimRunner{
private:
	sc_core::sc_trace_file* setScTraceFile(const std::string& path = "./trace");
	std::unique_ptr<HwStructure> root;
	std::string rootName;
	uint64_t resolutioninNs;
	sc_core::sc_trace_file* tf = nullptr;
	bool setupComplete = false;
	//tracer setup
	std::vector<std::string> tracerNames;
	std::string tracerPath;
	bool tracerTimePrefix;
	void instantiateTracers();

	//called in simulate()
	virtual void setup()=0;

protected:
	// static bool& requiresReset(){
	// 	static bool flag = false;
	// 	return flag;
	// }

	std::unique_ptr<HwStructure>& modelRoot();
	Time getCurrentSimulationTime();

public:
	explicit SimRunner(uint64_t randomSeed,  std::string hwRootName = "simulation_hw_root", uint64_t resolution = 1 /*ns*/);
	explicit SimRunner(std::string hwRootName = "simulation_hw_root", uint64_t resolution = 1 /*ns*/) ;
	virtual ~SimRunner();

	//simulation steps
	Time simulate(Time timeInMs);
	static void pause();
	void finish();

	const std::vector<std::string>& getTracerNames();

	void enableTracers(
		const std::vector<std::string>& _tracerNames,  
		const std::string& _tracerPath = "./trace",  
		bool _tracerTimePrefix = true
	);

	static const std::string& version();

	//logger & tracing
	template<class T, decltype(std::declval<T>().isWrappedType()) isWrapped = true>
	inline void scTrace(
		const std::shared_ptr<T>& v,
		const std::string & SigName) 
	{
		//if output directory has not been set,
		//vcd trace file will be generated in default location
		sc_trace(SimRunner::setScTraceFile(), v, SigName);
	}
};