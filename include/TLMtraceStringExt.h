/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include <memory>
#include <tlm>
#include "ProcessingUnitImpl.h"

namespace sc_core{
	class sc_trace_file;
}

struct TLMtraceStringExt : tlm::tlm_extension<TLMtraceStringExt> {

	uint32_t traceStringID;
	std::shared_ptr<ProcessingUnitImpl> source;

	[[nodiscard]] tlm_extension_base* clone() const override {
		TLMtraceStringExt *ext = new TLMtraceStringExt;
		ext->traceStringID = this->traceStringID;
		ext->source = this->source;
		return ext;
	}
	
	void copy_from(tlm_extension_base const& ext) override{
		traceStringID = static_cast<TLMtraceStringExt const &>(ext).traceStringID;
		source = static_cast<TLMtraceStringExt const &>(ext).source;
	}
};

