/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Behnaz Pourmohseni <behnaz.pourmohseni@de.bosch.com>
 ********************************************************************************
 */
#pragma once
#include <memory>
#include <string>
#include <iostream>
#include "EventManager.h"
#include <map>

class Event;
class PubSubEventSubscriber;

class PubSubEventManager : public EventManager {
private:
    
    std::map<std::shared_ptr<Event>, std::shared_ptr<PubSubEventSubscriber>> eventToSubscriberMap;

	explicit PubSubEventManager(std::string name="PubSubEventManager") : EventManager(std::move(name)) {
		static int cnt =0;
		cnt++;
		std::cout<<"PubSubEventManager instance count = "<<cnt<<std::endl;
	};

	inline void assertCondition(const bool& condition, const std::string& message=std::string()) const {
		if(!condition){
			throw std::runtime_error(message.c_str());
		}
  	}

public:

	virtual ~PubSubEventManager()=default;

	void addEvent(const std::shared_ptr<Event>& event, const std::shared_ptr<PubSubEventSubscriber>& subscriber);
	
	void removeEvent(const std::shared_ptr<Event>& event, const std::shared_ptr<PubSubEventSubscriber>& subscriber);

	void removeSubscriber(const std::shared_ptr<PubSubEventSubscriber>& subscriber);
	
	void notify(const std::shared_ptr<Event>& event) override;

	friend class EventManager;
};
