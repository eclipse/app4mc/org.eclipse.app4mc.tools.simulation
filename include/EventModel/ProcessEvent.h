/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Behnaz Pourmohseni <behnaz.pourmohseni@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include "Event.h"

enum class ProcessEventType {ACTIVATE=0, START=1, PREEMPT=2, RESUME=3, TERMINATE=4};

class ProcessEvent : public Event {

private:
	ProcessEventType type;

public:
	ProcessEvent(const std::string& _name, ProcessEventType _type):
		Event(_name), type(_type) {}

	ProcessEventType getType() {
		return type;
	}
};
