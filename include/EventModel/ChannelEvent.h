/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include "Event.h"

enum class ChannelEventType {ACCESS_EVENT = 0, READ_EVENT = 1, WRITE_EVENT = 2};
enum class ChannelEventStateType {START=0, DONE=1};

class ChannelEvent : public Event {

private:
	ChannelEventType type;
	ChannelEventStateType state;

public:
	explicit ChannelEvent(const std::string& _name, 
		ChannelEventType _type=ChannelEventType::ACCESS_EVENT, 
		ChannelEventStateType _state=ChannelEventStateType::START):
		Event(_name), type(_type), state(_state)
	{}

	ChannelEventType getType() {
		return type;
	}

	ChannelEventStateType getState() {
		return state;
	}
};
