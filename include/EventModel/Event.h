/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 *   Author: Behnaz Pourmohseni <behnaz.pourmohseni@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include <memory>
#include "HasName.h"
#include "TimeUtils.h"

class EventStimulusImpl;
namespace sc_core{
	class sc_event;
}

class Event : public HasName, public std::enable_shared_from_this<Event> {
private:
	std::shared_ptr<sc_core::sc_event> scEvent;
public:
	explicit Event(const std::string& _name);

	virtual void notify();

	virtual void notify(const Time& time);

	virtual void cancel();

	void wait();
	
	friend class EventStimulusImpl;
};
