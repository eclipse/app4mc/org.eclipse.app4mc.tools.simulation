/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Behnaz Pourmohseni <behnaz.pourmohseni@de.bosch.com>
 ********************************************************************************
 */
#pragma once
#include <memory>
#include "PubSubEventManager.h"

class Event;

class PubSubEventSubscriber : public std::enable_shared_from_this<PubSubEventSubscriber> {
protected:
	~PubSubEventSubscriber()=default;
public:

	virtual void update(const std::shared_ptr<Event>& event) = 0;

	void addPubSubEvent(const std::shared_ptr<Event>& event);

	void removePubSubEvent(const std::shared_ptr<Event>& event);
	
	void removeSubscriber();
};
