/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Behnaz Pourmohseni <behnaz.pourmohseni@de.bosch.com>
 ********************************************************************************
 */
#pragma once
#include "HasName.h"
#include <iostream>
#include <vector>
#include <memory>

class Event;

class EventManager : public HasName {

private:
 
	/**
	 * provides a static vector of {{EventManager}}s. 
	 * Because of the singleton factory pattern, the set contains at most one instance of each concrete {{EventManager}}
	 */
	static std::vector<std::shared_ptr<EventManager>>& eventManagers(){
		static std::vector<std::shared_ptr<EventManager>> eventManagers{};
		return eventManagers;
	}
	
	static void notifyManagers(const std::shared_ptr<Event>& event) {
		for(auto& manager: eventManagers()){
			manager->notify(event);
		}
	}

protected:

	explicit EventManager(const std::string& name): HasName(name) {}

	/**
	 * defines the behavior of an {{EventManager}} in reaction to an event trigger
	 */
	virtual void notify(const std::shared_ptr<Event>& event) = 0;

	~EventManager()=default;
	
public:


	/**
	 * Static singleton factory method for derived classes
	 */
	template<typename T, typename... Args, typename std::enable_if < std::is_base_of<EventManager, T>::value, bool>::type = true>
	static std::shared_ptr<T> Inst(Args &&...args) {
		// using shared_ptr<T> instead of T&: 
		// in case any other static objects hold a ref to instance, using shared_ptr ensures the instance 
		// stays alive so long as any other object holds a ref to it.
		//static auto instance = std::shared_ptr<T>(new T(std::forward<Args>(args)...));
		static bool added = false;
		static auto instance = std::shared_ptr<T>(new T(std::forward<Args>(args)...));
		if (!added){
		 	eventManagers().push_back(instance);
			std::cout<<"Registered event managers: "<<std::endl;
			for (const auto& em : eventManagers()){
				std::cout<<"  - "<<em->getName()<<std::endl;
			}
			added=true;
		}
		return instance;	
	}

	friend class Event;
};
