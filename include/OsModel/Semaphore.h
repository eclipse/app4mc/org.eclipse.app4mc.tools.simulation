/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include "WrappedType.h"
#include "Types.h"

enum class SemaphoreType { CountingSemaphore, Resource, Spinlock, _undefined_ };

class SemaphoreImpl;

class Semaphore : public WrappedType<SemaphoreImpl> {

protected:
  explicit Semaphore(const std::string& name, EInt maxValue = 1, EInt initialValue = 1, SemaphoreType type = SemaphoreType::_undefined_);
  explicit Semaphore(const std::shared_ptr<SemaphoreImpl>& _inst);
public:
  template<typename... Args>
  static std::shared_ptr<Semaphore> createSemaphore(Args &&...args){
	   return std::make_shared<Semaphore>(Semaphore(std::forward<Args>(args)...));
  }

};
