/**
 ********************************************************************************
 * Copyright (c) 2021 University of Rostock and others
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */

#pragma once


#include <memory>
#include <stdexcept>
#include <string>
#include <utility>
#include "HasName.h"
#include "Common.h"
#include "sysc/kernel/sc_event.h"
#include "sysc/kernel/sc_object.h"
#include "sysc/kernel/sc_time.h"
#include "Semaphore.h"

class SemaphoreAccess;
class ProcessingUnitImpl;

class SemaphoreImpl : HasName {
  SemaphoreType type;
  int64_t maxValue;
  int64_t initialValue;
  int64_t currentState;
  std::unique_ptr<sc_core::sc_event> wakeup;

 public:
  explicit SemaphoreImpl(const std::string& name, EInt maxValue = 1, EInt initialValue = 1, SemaphoreType type = SemaphoreType::_undefined_) :
      HasName(name), type(type), maxValue(maxValue), initialValue(initialValue), currentState(initialValue) 
  {
    if (type == SemaphoreType::Spinlock && (maxValue != 1 || initialValue != 1)) {
      throw std::runtime_error("Semaphoretype Spinlock needs to have maxValue = 1(has " + std::to_string(maxValue) + ") and initialValue = 1 (has " + std::to_string(initialValue) + ")");
    }
    wakeup = std::make_unique<sc_core::sc_event>();
  }


  friend SemaphoreAccess;
  friend ProcessingUnitImpl;

 private:
  void release();

  /**
   * @brief
   *
   * @param interruptEvent
   * @return true if semaphore could be requested
   * @return false if got interrupted by interruptEvent and request was not possible
   */
  bool activeRequest(const sc_core::sc_event &interruptEvent);
};
