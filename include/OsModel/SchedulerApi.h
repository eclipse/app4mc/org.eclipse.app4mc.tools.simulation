/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include "ProcessingUnit.h"
#include "TaskAllocation.h"
#include "TimeUtils.h"
#include <memory>
#include <map>
#include <type_traits>
#include "SchedulingParameter.h"

class Schedulable;
class Task;
class Scheduler;
class TaskAllocation;
class ProcessingUnit;

class SchedulerApi {
private:
	[[nodiscard]] virtual std::shared_ptr<Scheduler> getImplementationInstance() const = 0;
public:

	/*************************************************************
	 Scheduler properties
	*************************************************************/
	void setExecutionCore(std::shared_ptr<ProcessingUnit> pu);
	void addResponsibleCore(std::shared_ptr<ProcessingUnit> pu);
	void addTaskMapping(const std::shared_ptr<Task>& task);
	void addTaskMapping(const std::shared_ptr<SchedulerApi>& task);

	/*************************************************************
	Schedulable properties
	*************************************************************/
	void setTaskAllocation(const TaskAllocation& newAllocation);
	[[nodiscard]] const TaskAllocation& getTaskAllocation() const;

	/*************************************************************
	Scheduling parameter properties
	*************************************************************/
	void setAlgorithmSchedulingParameter(const SchedulingParameter& parameters);
	void addAlgorithmSchedulingParameter(const SchedulingParameter& parameters);
	void setAlgorithmSchedulingParameter(const std::string& name, const ParameterValue& value);
	template<ParameterType type, bool isManyType>
	std::variant_alternative_t<getTypeIndex(type, isManyType), ParameterValue> getAlgorithmParameter(const std::string& name);

};


//Scheduling parameter properties specialiazation
//non many types
extern template std::variant_alternative_t<getTypeIndex(ParameterType::Integer, false), ParameterValue> SchedulerApi::getAlgorithmParameter<ParameterType::Integer, false>(const std::string& name);
extern template std::variant_alternative_t<getTypeIndex(ParameterType::Unsigned, false), ParameterValue> SchedulerApi::getAlgorithmParameter<ParameterType::Unsigned, false>(const std::string& name);
extern template std::variant_alternative_t<getTypeIndex(ParameterType::Float, false), ParameterValue> SchedulerApi::getAlgorithmParameter<ParameterType::Float, false>(const std::string& name);
extern template std::variant_alternative_t<getTypeIndex(ParameterType::Bool, false), ParameterValue> SchedulerApi::getAlgorithmParameter<ParameterType::Bool, false>(const std::string& name);
extern template std::variant_alternative_t<getTypeIndex(ParameterType::String, false), ParameterValue> SchedulerApi::getAlgorithmParameter<ParameterType::String, false>(const std::string& name);
extern template std::variant_alternative_t<getTypeIndex(ParameterType::Time, false), ParameterValue> SchedulerApi::getAlgorithmParameter<ParameterType::Time, false>(const std::string& name);
//many types
extern template std::variant_alternative_t<getTypeIndex(ParameterType::Integer, true), ParameterValue> SchedulerApi::getAlgorithmParameter<ParameterType::Integer, true>(const std::string& name);
extern template std::variant_alternative_t<getTypeIndex(ParameterType::Unsigned, true), ParameterValue> SchedulerApi::getAlgorithmParameter<ParameterType::Unsigned, true>(const std::string& name);
extern template std::variant_alternative_t<getTypeIndex(ParameterType::Float, true), ParameterValue> SchedulerApi::getAlgorithmParameter<ParameterType::Float, true>(const std::string& name);
extern template std::variant_alternative_t<getTypeIndex(ParameterType::Bool, true), ParameterValue> SchedulerApi::getAlgorithmParameter<ParameterType::Bool, true>(const std::string& name);
extern template std::variant_alternative_t<getTypeIndex(ParameterType::String, true), ParameterValue> SchedulerApi::getAlgorithmParameter<ParameterType::String, true>(const std::string& name);
extern template std::variant_alternative_t<getTypeIndex(ParameterType::Time, true), ParameterValue> SchedulerApi::getAlgorithmParameter<ParameterType::Time, true>(const std::string& name);

