/**
 ********************************************************************************
 * Copyright (c) 2022 University of Rostock and others
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */
#pragma once
#include "TimeUtils.h"
#include <cstddef>
#include <initializer_list>
#include <stdexcept>
#include <utility>
#include <variant>
#include <string>
#include <type_traits>
#include <vector>
#include <map>
#include "Types.h"


enum class ParameterType {
    Integer   = 0,
    Unsigned  = 1,
    Float     = 2,
    Bool      = 3,
    String    = 4,
    Time      = 5,
    ManyTypes = 6,
};

using ParameterValue = std::variant<
    int64_t, 
    uint64_t, 
    double, 
    bool, 
    std::string, 
    Time, 
    std::vector<int64_t>, 
    std::vector<uint64_t>, 
    std::vector<double>, 
    std::vector<bool>, 
    std::vector<std::string>, 
    std::vector<Time>>;

constexpr std::size_t getTypeIndex(ParameterType type, bool many) {
    if (many) {
        return static_cast<std::size_t>(type) + static_cast<std::size_t>(ParameterType::ManyTypes);
    }
    return static_cast<std::size_t>(type);
}

constexpr ParameterType getParameterType(std::size_t index) {
    return static_cast<ParameterType>(index % static_cast<std::size_t>(ParameterType::ManyTypes));
}

constexpr bool getIsManyType(std::size_t index) {
    return index >= static_cast<std::size_t>(ParameterType::ManyTypes);
}

template<typename T>
struct isVector : std::false_type {};

template<typename T>
struct isVector<std::vector<T>> : std::true_type {};


/**
 * The getIndexOfType function contains intentionally type depended if constexpr statements, which contain return statements.
 * Therefore for many types the late conditions are never reached. MSVC is therefore complaining about dead code (4702),
 * which we suppress for this function.
 *
 * A possible workaround would be to use else cases and carefully crafted return value assignment, but caused by the recursive
 * nature of this function (needed, because there is currently no constexpr for loop), this would make the function less readable.
 */
#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable: 4702)
#endif
template<class T, std::size_t index = std::variant_size_v<ParameterValue> - 1>
constexpr std::size_t getIndexOfType() {
    /**
     * we need to check bool,integers,floats separately, since there are conversion functions e.g. int to bool, which do not the right thing
     *
     */
    if constexpr (std::is_integral_v<T>) {
        if constexpr (std::is_same_v<T, bool>) {
            return getTypeIndex(ParameterType::Bool, false);
        }
        if constexpr (std::is_floating_point_v<T>) {
            return getTypeIndex(ParameterType::Float, false);
        }
        return getTypeIndex(ParameterType::Integer, false);
    }

    /**
     * We try every type of the variant, whether we convert value into it
     * Since we cannot do a constexpr for loop, we use recursive template instantiation with all
     * indices of the variant
     * this function should in nearly all cases be executed at compile-time
     */
    if constexpr (std::is_convertible_v<T, std::variant_alternative_t<index, ParameterValue>>) {
        return index;
    }
    // recursion to next type index in variant
    if constexpr (index > 1) {
        return getIndexOfType<T, index - 1>();
    }
    // try to deduce, whether the value_type of an vector is a type of the variant
    if constexpr (isVector<T>::value) {
        return getIndexOfType<std::vector<std::variant_alternative_t<getIndexOfType<typename T::value_type>(), ParameterValue>>>();
    }
    // T cannot be stored in variant
    return std::variant_npos;
}

#ifdef _MSC_VER
#pragma warning(pop)
#endif
template<class T>
constexpr std::size_t getIndexOfType([[maybe_unused]] const T& value) {
    return getIndexOfType<T>();
}

template<class T>
using convertedVectorType = std::vector<std::variant_alternative_t<getIndexOfType<T>(), ParameterValue>>;

template<class T>
using isInVariant = std::enable_if_t<getIndexOfType<T>() != std::variant_npos, bool>;

template<class T>
using variantType = std::variant_alternative_t<getIndexOfType<T>(), ParameterValue>;

class Parameter {
    ParameterValue value;

public:
    Parameter() = default;

    Parameter(const ParameterValue& initValue) {
        value = initValue;
    }

    /**
     * @brief Construct a new Parameter out of an convertable type
     *
     * @tparam T
     * @param initValue
     */
    template<class T, isInVariant<T> = true>
    Parameter(const T& initValue): value(std::in_place_index<getIndexOfType<T>()>, (initValue)) 
    {
    }

    /**
     * @brief Construct a new Parameter out of initializer list
     *
     * Since automatic Template argument deduction does not work for std::initializer_list, we need this separate constructor
     * @tparam T
     * @param list
     */
    template<class T, isInVariant<T> = true>
    Parameter(std::initializer_list<T> list):
        // convert T to an actual type of the variant (e.g. int to ELong, or float to double) and create a vector of this type with initializer list
        value(convertedVectorType<T>(list.begin(), list.end())) {
    }

    template<ParameterType type, bool many>
    [[nodiscard]] std::variant_alternative_t<getTypeIndex(type, many), ParameterValue> get() const {
        if (getTypeIndex(type, many) == value.index()) {
            return std::get<getTypeIndex(type, many)>(value);
        }
        throw std::invalid_argument("Requested Type is not in Parameter");
    }

    [[nodiscard]] ParameterValue get() const {
        return value;
    }

    template<class T, isInVariant<T> = true>
    void set(const T& newValue) {
        value = variantType<T>(newValue);
    }

    template<class T, isInVariant<T> = true>
    void add(const T& newValue) {
        if constexpr (getIsManyType(getIndexOfType<T>())) {
            if (value.index() != getIndexOfType<T>()) {
                throw std::invalid_argument("try to add value array of different type");
            }
            auto& ref = std::get<getIndexOfType<T>()>(value);
            ref.insert(ref.end(), newValue.begin(), newValue.end());
        }
        else {
            if (!getIsManyType(value.index())) {
                throw std::invalid_argument("try to add value to scalar type, use set");
            }
            // convert T to an actual type of variant and compare to current type
            if (getIndexOfType<convertedVectorType<T>>() != value.index()) {
                throw std::invalid_argument("tried to add different type to array type, use set to change type");
            }
            std::get<getIndexOfType<convertedVectorType<T>>()>(value).emplace_back(newValue);
        }
    }
};

using SchedulingParameter = std::map<std::string, Parameter>;
