/**
 ********************************************************************************
 * Copyright (c) 2021 University of Rostock and others
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */

#pragma once
#include <optional>
#include <string>
#include "SchedulingParameter.h"

class TaskAllocation {
    SchedulingParameter schedulingParameter;
public:
    TaskAllocation() = default;

    TaskAllocation(const SchedulingParameter &parameters) {
        setSchedulingParameters(parameters);
    }

    void setSchedulingParameters(const SchedulingParameter &parameters) {
        schedulingParameter = parameters;
    }

    template<ParameterType type, bool isManyType>
    std::variant_alternative_t<getTypeIndex(type, isManyType), ParameterValue> getSchedulingParameter(const std::string &name) const {
        return schedulingParameter.at(name).get<type, isManyType>();
    }

    template<class T>
    void setSchedulingParameter(const std::string &name, const T &value) {
        schedulingParameter[name].set(value);
    }

    void setSchedulingParameter(const std::string &name, const ParameterValue &value) {
        schedulingParameter[name] = Parameter({value});
    }

    void setSchedulingParameter(const std::string &name, const ParameterValue &&value) {
        schedulingParameter[name] = Parameter({value});
    }

    
    void setSchedulingParameter(const SchedulingParameter& _schedulingParameter) {
        schedulingParameter = _schedulingParameter;
    }

    [[nodiscard]] bool isParameterSet(const std::string &name) const {
        return schedulingParameter.count(name) == 1;
    }
};