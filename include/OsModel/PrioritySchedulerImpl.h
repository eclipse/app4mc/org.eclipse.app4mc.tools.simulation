/**
 ********************************************************************************
 * Copyright (c) 2022 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 * - Robert Bosch GmbH 
 ********************************************************************************
 */

#pragma once
#include <memory>
#include "Scheduler.h" // provides the declaration of the Scheduler class

class Schedulable;
class ProcessingUnitImpl;
class StimulusImpl;

class PrioritySchedulerImpl: public Scheduler {

protected:    

    std::vector<std::shared_ptr<Schedulable>> readyQueue;
    std::map<std::shared_ptr<ProcessingUnitImpl>,std::shared_ptr<Schedulable>> schedule;

    virtual void enqueueTask(const std::shared_ptr<Schedulable> &task);
    virtual void dequeueTask(const std::shared_ptr<Schedulable> &task);
    virtual void updateSchedule();

    static bool comparePrio(const std::shared_ptr<Schedulable> &a, const std::shared_ptr<Schedulable> &b);

    void printScheduleLog();
    void printReadyQueueLog() const;
    void printStateUpdateLog(const std::shared_ptr<Schedulable> &task, const std::string &_state, const std::shared_ptr<ProcessingUnitImpl> &pu=NULL) const;
    virtual void printLog(const std::string& msg) const;

public: 

    PrioritySchedulerImpl(std::string name): Scheduler(std::move(name)) { }

    void addTaskMapping(std::shared_ptr<Scheduler> scheduler) override;
    void addTaskMapping(std::shared_ptr<Task> task) override;
    void addResponsibleCore(std::shared_ptr<ProcessingUnitImpl> pu) override;
    std::shared_ptr<Schedulable> chooseTask(const std::shared_ptr<ProcessingUnitImpl> &pu) override;
    
    void taskActivate(const std::shared_ptr<Schedulable> &task, const StimulusImpl &stimulus) override;
    void taskStart(const std::shared_ptr<Schedulable> &task, const std::shared_ptr<ProcessingUnitImpl> &pu) override;
    void taskPreemt(const std::shared_ptr<Schedulable> &task, const std::shared_ptr<ProcessingUnitImpl> &pu) override;
    void taskResume(const std::shared_ptr<Schedulable> &task,  const std::shared_ptr<ProcessingUnitImpl> &pu) override;
    void taskTerminate(const std::shared_ptr<Schedulable> &task) override;
};