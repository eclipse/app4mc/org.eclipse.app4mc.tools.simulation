/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include "WrappedType.h"
#include "SchedulingParameter.h"
#include "SchedulerApi.h"

class RandomSchedulerImpl;

class RandomScheduler: public WrappedType<RandomSchedulerImpl>, public SchedulerApi{
private:
	[[nodiscard]] std::shared_ptr<Scheduler> getImplementationInstance() const override;
public:
	explicit RandomScheduler(std::string name);
	explicit RandomScheduler(const std::shared_ptr<RandomSchedulerImpl>& _inst);
};
