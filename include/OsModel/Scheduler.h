/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include <optional>
#include <memory>
#include <algorithm>
#include <random>
#include <set>
#include <map>
#include "Schedulable.h"
#include "EasyloggingMacros.h"

class ProcessingUnitImpl;
class Task;
class StimulusImpl;
class Runnable;

class Scheduler : public std::enable_shared_from_this<Scheduler>, public Schedulable {

protected:
    std::set<std::shared_ptr<Schedulable>> mappedTasks;
    std::shared_ptr<ProcessingUnitImpl> executingCore;
    std::vector<std::shared_ptr<ProcessingUnitImpl>> coresResponsible;
    std::shared_ptr<Scheduler> parentScheduler;
    
    /**
     *  Sets for every state of a Task *
     * 	See task model definition 
     *		https://www.eclipse.org/app4mc/help/latest/index.html#section3.7 
     */

    std::set<std::shared_ptr<Schedulable>> activeTasks;
    std::set<std::shared_ptr<Schedulable>> readyTasks;
    std::map<std::shared_ptr<ProcessingUnitImpl>,std::shared_ptr<Schedulable>> runningTasks;
    //TODO add Parking, Polling and Waiting, when semantics are available

    /*since any function call is always done by one thread we can put here an hint, nonetheless if it would be wrong, it even is no problem*/
    std::map<std::shared_ptr<ProcessingUnitImpl>,std::shared_ptr<Schedulable>>::iterator mapHint;

    /**
     * @brief remove running task from runningTasks map
     * 
     * @param task 
     * @return std::optional<const std::shared_ptr<ProcessingUnit>> the pu the Task ran at termination time
     */
    std::optional<const std::shared_ptr<ProcessingUnitImpl>> removeTaskFromRunningTasks(const std::shared_ptr<Schedulable> &task);

     void executingCoreIsSet();

    SchedulingParameter algorithmSchedulingParameter;

public:
    Scheduler(const std::string &name);

    //Schedulable Interface
    ExecutionItem getNextExecItem(const std::shared_ptr<ProcessingUnitImpl> &pu) override;
    
    void setAlgorithmSchedulingParameter(const SchedulingParameter &parameters) {
      algorithmSchedulingParameter = parameters;
    }

    void addAlgorithmSchedulingParameter(const SchedulingParameter& _schedulingParameter) {
        for (auto _pair : _schedulingParameter){
            const auto [it, success] = algorithmSchedulingParameter.emplace(_pair);
            if (!success){
                VLOG(0)<<"Failed to set scheduling parameter "<<_pair.first<< ". Maybe already set?";
            }
        }
    }

    void setAlgorithmSchedulingParameter(const std::string &name, const ParameterValue &value) {
        algorithmSchedulingParameter[name] = Parameter({value});
    }

    void setAlgorithmSchedulingParameter(const std::string &name, const ParameterValue &&value) {
        algorithmSchedulingParameter[name] = Parameter({value});
    }


    template<ParameterType type, bool isManyType>
    std::variant_alternative_t<getTypeIndex(type, isManyType), ParameterValue> getAlgorithmParameter(const std::string &name) {
      return algorithmSchedulingParameter[name].get<type, isManyType>();
    }

    virtual std::shared_ptr<Schedulable> chooseTask(const std::shared_ptr<ProcessingUnitImpl> &requestingPu) = 0;	

    virtual void addTaskMapping(std::shared_ptr<Task> task);
    virtual void addTaskMapping(std::shared_ptr<Scheduler> task);
    virtual void wakeupPUs();
    virtual void interruptPUs();
    virtual void addResponsibleCore(std::shared_ptr<ProcessingUnitImpl> pu);
    virtual void setExecutionCore(std::shared_ptr<ProcessingUnitImpl> pu);
    virtual void runnableStarted(const std::shared_ptr<Task> &task, const Runnable &runnable);
    virtual void runnableCompleted(const std::shared_ptr<Task> &task, const Runnable &runnable);
    virtual void taskTerminate(const std::shared_ptr<Schedulable> &task); 
    virtual void taskStart(const std::shared_ptr<Schedulable> &task, const std::shared_ptr<ProcessingUnitImpl> &pu);
    virtual void taskResume(const std::shared_ptr<Schedulable> &task,  const std::shared_ptr<ProcessingUnitImpl> &pu);
    virtual void taskPreemt(const std::shared_ptr<Schedulable> &task,  const std::shared_ptr<ProcessingUnitImpl> &pu);
    virtual void taskActivate(const std::shared_ptr<Schedulable> &task, const StimulusImpl &stimulus);
    void storeInterruptedExecutionItem(ExecutionItem execItem);
};



