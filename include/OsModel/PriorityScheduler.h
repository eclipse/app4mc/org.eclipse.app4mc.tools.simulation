/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include "WrappedType.h"
#include "SchedulingParameter.h"
#include "SchedulerApi.h"

class PrioritySchedulerImpl;

class PriorityScheduler: public WrappedType<PrioritySchedulerImpl>, public SchedulerApi{
private:
	[[nodiscard]] std::shared_ptr<Scheduler> getImplementationInstance() const override;
public:
	explicit PriorityScheduler(std::string name);
	explicit PriorityScheduler(const std::shared_ptr<PrioritySchedulerImpl>& _inst);
};