/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Behnaz Pourmohseni <behnaz.pourmohseni@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include <systemc>
#include <memory>
#include <algorithm>
#include "PriorityRoundRobinSchedulerImpl.h"

class Schedulable;
class ProcessingUnitImpl;
class Event;

class PosixRealtimeSchedulerImpl: public PriorityRoundRobinSchedulerImpl {

public: 

    explicit PosixRealtimeSchedulerImpl(std::string name): PriorityRoundRobinSchedulerImpl(std::move(name)) { }

    std::shared_ptr<Schedulable> chooseTask(const std::shared_ptr<ProcessingUnitImpl> &pu) override;

    void taskStart(const std::shared_ptr<Schedulable> &task, const std::shared_ptr<ProcessingUnitImpl> &pu) override;
    void taskPreemt(const std::shared_ptr<Schedulable> &task, const std::shared_ptr<ProcessingUnitImpl> &pu) override;
    void taskResume(const std::shared_ptr<Schedulable> &task,  const std::shared_ptr<ProcessingUnitImpl> &pu) override;
    void taskTerminate(const std::shared_ptr<Schedulable> &task) override;
    
    enum class PosixSchedPolicy {SCHED_POLICY_FIFO, SCHED_POLICY_RR};
    std::string getSchedulingPolicyName(const PosixSchedPolicy &policy) const;
    PosixSchedPolicy getSchedulingPolicy(const std::string &policyName) const;

protected:

    PosixSchedPolicy getSchedulingPolicy(const std::shared_ptr<Schedulable> &task) const;
    typedef std::map<std::shared_ptr<Schedulable>,PosixSchedPolicy> schedulingPolicy;
};