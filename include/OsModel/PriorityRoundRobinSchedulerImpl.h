/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Behnaz Pourmohseni <behnaz.pourmohseni@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include <systemc>
#include <memory>
#include <algorithm>
#include "PrioritySchedulerImpl.h"

class Schedulable;
class ProcessingUnitImpl;

class PriorityRoundRobinSchedulerImpl: public PrioritySchedulerImpl {

protected:    
    
    std::map<std::shared_ptr<Schedulable>,sc_core::sc_time> lastPuAssignTime; // start, resume, or continue to execute
    std::map<std::shared_ptr<Schedulable>,sc_core::sc_time> usedSlicePortion;

    // timer events and timer threads
    std::map<std::shared_ptr<ProcessingUnitImpl>,std::shared_ptr<sc_core::sc_event>> timerTimeoutEvents;
    std::map<std::shared_ptr<ProcessingUnitImpl>,bool> timerExpired;
    std::map<std::shared_ptr<ProcessingUnitImpl>,std::shared_ptr<sc_core::sc_spawn_options>> timerOptions;

    void PuTimerTimeoutService(const std::shared_ptr<ProcessingUnitImpl> &pu);
    void cancelTimer(const std::shared_ptr<ProcessingUnitImpl> &pu);
    void setTimer(const std::shared_ptr<ProcessingUnitImpl> &pu, const sc_core::sc_time time);
    
    void updateSchedule() override;
    
    sc_core::sc_time getTimeSliceLength();

public: 

    PriorityRoundRobinSchedulerImpl(std::string name): PrioritySchedulerImpl(std::move(name)) { }

    std::shared_ptr<Schedulable> chooseTask(const std::shared_ptr<ProcessingUnitImpl> &pu) override;
    void addResponsibleCore(std::shared_ptr<ProcessingUnitImpl> pu) override;

    void taskStart(const std::shared_ptr<Schedulable> &task, const std::shared_ptr<ProcessingUnitImpl> &pu) override;
    void taskPreemt(const std::shared_ptr<Schedulable> &task, const std::shared_ptr<ProcessingUnitImpl> &pu) override;
    void taskResume(const std::shared_ptr<Schedulable> &task,  const std::shared_ptr<ProcessingUnitImpl> &pu) override;
    void taskTerminate(const std::shared_ptr<Schedulable> &task) override;
};