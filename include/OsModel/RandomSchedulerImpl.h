/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 * - University of Rostock - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */
#pragma once
#include <memory>
#include "Scheduler.h"

class RandomSchedulerImpl : public Scheduler{
public:
	std::shared_ptr<Schedulable> chooseTask(const std::shared_ptr<ProcessingUnitImpl> &requestingPu) override;
	RandomSchedulerImpl(std::string name) : Scheduler(std::move(name)){};
};