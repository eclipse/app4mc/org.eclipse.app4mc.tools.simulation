/**
 ********************************************************************************
 * Copyright (c) 2023 Rober Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Sebastian Reiser<sebastian.reiser@de.bosch.com> - initial contribution
 ********************************************************************************
 */

#pragma once
#include <string>
#include <cstdint>
#include "TimeUtils.h"
#include "SchedulerApi.h"


struct SchedulingParameterHelper{

    //policy
    inline static const std::string& PolicyKey(){static std::string s = "schedulingPolicy"; return s;};
    inline static const std::string& PolicySchedPolicyFifo(){static std::string s = "SCHED_POLICY_FIFO"; return s;};
    inline static const std::string& PolicySchedPolicyRoundRobin(){static std::string s = "SCHED_POLICY_RR"; return s;};

    //round robin parameters
    inline static const std::string& RoundRobinTimesliceLengthKey(){static std::string s = "timeSliceLength"; return s;};
    inline static const Time& RoundRobinTimesliceLengthDefault(){static Time s = 1_ms; return s;};

};