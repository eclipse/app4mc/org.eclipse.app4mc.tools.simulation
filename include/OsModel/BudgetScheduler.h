/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include "SchedulerApi.h"
#include "WrappedType.h"
#include <map>

class BudgetSchedulerImpl;

class BudgetScheduler: public WrappedType<BudgetSchedulerImpl>, public SchedulerApi{
private:
	[[nodiscard]] std::shared_ptr<Scheduler> getImplementationInstance() const override;
public:
	BudgetScheduler(std::string name,const std::map<std::shared_ptr<Schedulable>,double>& budget, const Time& updateCycle);
	explicit BudgetScheduler(const std::shared_ptr<BudgetSchedulerImpl>& _inst);
};
