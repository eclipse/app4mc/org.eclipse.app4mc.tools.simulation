/**
 ********************************************************************************
 * Copyright (c) 2021 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */

#pragma once
#include "HasName.h"
#include <memory>
#include "TaskAllocation.h"

enum class SchedulableState {
    suspended,
    ready,
    running,
    preempted
};

class ProcessingUnitImpl;
class ExecutionItem;
class StimulusImpl;

class Schedulable: public HasName {
private:
    TaskAllocation allocation;
    SchedulableState state = SchedulableState::suspended;
protected:
    SchedulableState getState() {
      return state;
    };
    
public:
    explicit Schedulable(const std::string& name) : HasName(name){};
    virtual ExecutionItem getNextExecItem(const std::shared_ptr<ProcessingUnitImpl> &pu) = 0;
    void setTaskAllocation(const TaskAllocation &newAllocation){
        allocation = newAllocation;
    }
    
    TaskAllocation& getTaskAllocation() {return allocation;}
    
    //state changes
    virtual void activate([[maybe_unused]]const StimulusImpl &stimulus) {
        if (state==SchedulableState::suspended){
            state = SchedulableState::ready;
        }
        else {
            throw std::runtime_error("Invalid process state change. A change of state to \"ready\" is only possible from \"suspended\".");
        }
    }

    virtual void start([[maybe_unused]]const std::shared_ptr<ProcessingUnitImpl> &pu){
        if (state==SchedulableState::ready){
            state = SchedulableState::running;
        }
        else {
            throw std::runtime_error("Invalid process state change. A change of state to \"running\" via edge start is only possible from \"ready\".");
        }
    }

    virtual void preempt([[maybe_unused]]const std::shared_ptr<ProcessingUnitImpl> &pu){
        if (state==SchedulableState::running){
            state = SchedulableState::preempted;
        }
        else {
            throw std::runtime_error("Invalid process state change. A change of state to \"preempted\" is only possible from \"running\".");
        }
    }

    virtual void resume([[maybe_unused]]const std::shared_ptr<ProcessingUnitImpl> &pu){
        if (state==SchedulableState::preempted){
            state = SchedulableState::running;
        }
        else {
            throw std::runtime_error("Invalid process state change. A change of state to \"running\"  via edge resume is only possible from \"preempted\".");
        }
    }

    virtual void terminate([[maybe_unused]]const std::shared_ptr<ProcessingUnitImpl> &pu){
        if (state==SchedulableState::running){
            state = SchedulableState::suspended;
        }
        else {
           throw std::runtime_error("Invalid process state change. A change of state to \"suspended\" is only possible from \"running\".");
        }
    }
};