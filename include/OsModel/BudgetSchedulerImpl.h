/**
 ********************************************************************************
 * Copyright (c) 2020 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */

#pragma once
#include <sysc/kernel/sc_spawn_options.h>
#include <systemc>
#include <memory>
#include <algorithm>
#include "Scheduler.h"

/** 
 * This Scheduler works with a updateCycle, in which every Task receives a runtime budget of budget[task] * updateCycle * #cores time.
 * A timer set to the runtime budget of the Task with the smallest budget, will recalculate the actual chosen tasks for calculation.
 * 
 * The Set of running tasks are the tasks with the biggest remaining budget.  
 *  
 * If a task overuses its budget, because of a memory transaction, which did not finish before the budget, it will calculate an 
 * overbudget for the next updateCycles.
 * 
 * In the current version, it is not totally fair or prioritize tasks with high budgets. This need FIXME!
 * 
 * It is more a basis for advanced budget based scheduler, but maybe there are also bugs in it, which causes the faulty behavior.
 * 
 * This Scheduler has currently no counterpart in AMALTHEA.
 */
class BudgetSchedulerImpl: public Scheduler{
private: 

    std::map<std::shared_ptr<Schedulable>,double> budgetPercent;
    std::map<std::shared_ptr<Schedulable>,sc_core::sc_time> budget;
    std::map<std::shared_ptr<Schedulable>,sc_core::sc_time> overBudget;
    std::map<std::shared_ptr<Schedulable>,sc_core::sc_time> lastStart;
    std::set<std::shared_ptr<Schedulable>> nextSchedulerDecision;
    

    sc_core::sc_time budgetUpdateCycle;
    sc_core::sc_event timerResetEvent;
    sc_core::sc_time currentTimeout;
    sc_core::sc_spawn_options timerOptions;
    sc_core::sc_spawn_options updateOptions;
public:
    //TODO use SchedulingParameters to express budget, this is old broken code (not working with real AMALTHEA models anyways)
    /*the parameter budget is a easy example, which is the percentage of budget per updateCycle*/
    BudgetSchedulerImpl(std::string name,const std::map<std::shared_ptr<Schedulable>,double> &budget, const sc_core::sc_time &updateCycle);
    void updateBudget();
    void timerFunction();
    std::shared_ptr<Schedulable> chooseTask(const std::shared_ptr<ProcessingUnitImpl> &requestingPu) override;
    void taskTerminate(const std::shared_ptr<Schedulable> &task) override;
	virtual void taskStart(const std::shared_ptr<Schedulable> &task, const std::shared_ptr<ProcessingUnitImpl> &pu) override;
	virtual void taskResume(const std::shared_ptr<Schedulable> &task,  const std::shared_ptr<ProcessingUnitImpl> &pu) override;
	virtual void taskActivate(const std::shared_ptr<Schedulable> &task, const StimulusImpl &stimulus) override;
};