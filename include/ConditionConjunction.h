/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 *           Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include <stdexcept>
#include <vector>
#include <random>
#include <memory>
#include "Condition.h"
#include "ConditionContainer.h"

class ConditionDisjunction;

class ConditionConjunction : public Condition, public ConditionContainer{
public:

	bool checkCond() {
		const auto& e = getConditionEntries();
		if (e.empty()){
			//std::all_of returns true upon empty container, throw runtime_error as the behavior for this case is not properly defined
			throw std::runtime_error("ConditionConjunction::checkCond: conjunction does not contain any conditions");
		}
		return std::all_of(e.begin(), e.end(), [](const std::shared_ptr<Condition>& entry){
			return entry->checkCond();
		});
	}
	friend ConditionDisjunction;
};
