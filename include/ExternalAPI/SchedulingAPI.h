/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Sebastian Reiser <sebastian.reiser@de.bosch.com> - initial contribution
 ********************************************************************************
 */


#pragma once
#include <stdexcept>
#include <string.h>

namespace externalAPI{
    struct JSON_TAGS{
    //SCHEDULER API
        //common
        inline static const char* TimeBase = "timeBase";
        inline static const char* Name = "name";
        //initial 
        inline static const char* SchedulerName = "name";
        inline static const char* TasksArray = "tasks";
        inline static const char* TaskName = "name";
        inline static const char* TaskPriority = "priority";
        inline static const char* TaskActivationLimit = "activationLimit";
        inline static const char* ResponsibleCores = "cores";
        inline static const char* CoreName = "name";
        //general
        inline static const char* Timestamp = "timestamp";
        //task state event
        inline static const char* TaskEvent = "taskEvent";
        inline static const char* activate = "activate";
        inline static const char* start = "start";
        inline static const char* preempt = "preempt";
        inline static const char* resume = "resume";
        inline static const char* terminate = "terminate";
        inline static const char* chooseTask = "chooseTask";
        //pu-task mappings
        inline static const char* ProcessingUnit = "PU";
        inline static const char* Task = "task";
    };

    enum class SchedulableState {
        notActivated,
        activated,
        ready,
        running,
        terminated
    };      

}