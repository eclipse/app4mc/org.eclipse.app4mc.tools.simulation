/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Sebastian Reiser <sebastian.reiser@de.bosch.com> - initial contribution
 ********************************************************************************
 */

#pragma once
#include <stdexcept>
#include <string.h>

namespace eventAPI{
    struct JSON_TAGS{
        inline static const char* TIMEBASE_VALUE = "timeBaseValue";
        inline static const char* TIMEBASE_UNIT = "timeBaseUnit";
        inline static const char* EVENTS = "events";
        inline static const char* NAME = "name";    
        inline static const char* TIME_VALUE = "currentTimeValue";
        inline static const char* TIME_UNIT = "currentTimeUnit";
    };
}