/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include <chrono>
#include <cmath>
#include <cstring>
#include <memory>
#include <stdexcept>


using Time = typename std::chrono::duration<int64_t, std::nano>;

template<typename Ratio>
using TimeParameter = typename std::chrono::duration<int64_t, Ratio>;

template<class ratio>
constexpr Time convertDoubleTime(long double val) {
  if (!std::isfinite(val)) {
	throw std::invalid_argument("time value needs to be a finite value");
  }
  return std::chrono::duration_cast<Time>(std::chrono::duration<long double, ratio>(val));
}

constexpr Time operator"" _h(long double val) {
  return convertDoubleTime<std::ratio<3600, 1>>(val);
}

constexpr Time operator"" _min(long double val) {
  return convertDoubleTime<std::ratio<60, 1>>(val);
}

constexpr Time operator"" _s(long double val) {
  return convertDoubleTime<std::ratio<1, 1>>(val);
}

constexpr Time operator"" _ms(long double val) {
  return convertDoubleTime<std::milli>(val);
}

constexpr Time operator"" _us(long double val) {
  return convertDoubleTime<std::micro>(val);
}

constexpr Time operator"" _ns(long double val) {
  return convertDoubleTime<std::nano>(val);
}

// constexpr Time operator"" _ps(long double val) {
//   return convertDoubleTime<std::pico>(val);
// }

constexpr Time operator"" _h(unsigned long long int val) {
  return TimeParameter<std::ratio<3600, 1>>(val);
}

constexpr Time operator"" _min(unsigned long long int val) {
  return TimeParameter<std::ratio<60, 1>>(val);
}

constexpr Time operator"" _s(unsigned long long int val) {
  return TimeParameter<std::ratio<1, 1>>(val);
}

constexpr Time operator"" _ms(unsigned long long int val) {
  return TimeParameter<std::milli>(val);
}

constexpr Time operator"" _us(unsigned long long int val) {
  return TimeParameter<std::micro>(val);
}

constexpr Time operator"" _ns(unsigned long long int val) {
  return TimeParameter<std::nano>(val);
}

// constexpr Time operator"" _ps(unsigned long long int val) {
//   return TimeParameter<std::pico>(val);
// }

//this template is used to reduce bloat in deviations for type dependend specializations
template<class ReturnT,class InputT>
ReturnT constexpr valueOf(InputT in){
	if constexpr (std::is_same<InputT,Time>::value)
		return static_cast<ReturnT>(in.count());
	else
		return static_cast<ReturnT>(in);
}

inline std::shared_ptr<struct tm> gmtime_safe(const time_t *timer) {
  auto returnTm = std::make_shared<struct tm>();
#ifdef _MSC_VER
  auto rv = gmtime_s(returnTm.get(), timer);
  if (rv != 0) {
	// we do not use oldschool strerror handling here, because this is another deprecated function
	throw std::runtime_error(std::string("gmtime failed with error number:") + std::to_string(rv));
  }
  return returnTm;
// gcc/clang/ ...
#else
  auto rv = gmtime_r(timer, returnTm.get());
  if (rv == nullptr) {
	throw std::runtime_error(std::string("gmtime failed with error:") + std::strerror(errno));
  }
  return returnTm;
#endif
}

inline std::shared_ptr<struct tm> localtime_safe(const time_t *timer) {
  auto returnTm = std::make_shared<struct tm>();
#ifdef _MSC_VER
  auto rv = localtime_s(returnTm.get(), timer);
  if (rv != 0) {
	// we do not use oldschool strerror handling here, because this is another deprecated function
	throw std::runtime_error(std::string("localtime failed with error number:") + std::to_string(rv));
  }
  return returnTm;
// gcc/clang/ ...
#else
  auto rv = localtime_r(timer, returnTm.get());
  if (rv == nullptr) {
	throw std::runtime_error(std::string("localtime failed with error:") + std::strerror(errno));
  }
  return returnTm;
#endif
}

// 2^53 is the maximum integer which can exactly be represented in a double
double constexpr maxDoubleInteger = 9007199254740992.0;

template<class T>
bool constexpr checkDoubleCast(T val){
	if constexpr (std::is_same<T, Time>::value)
		return (std::abs(static_cast<double>(val.count())) > maxDoubleInteger);
	else{
		return (std::abs(static_cast<double>(val)) > maxDoubleInteger);
	}
}

