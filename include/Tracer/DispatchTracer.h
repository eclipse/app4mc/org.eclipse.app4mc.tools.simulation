/**
 ********************************************************************************
 * Copyright (c) 2020 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */

#pragma once

#include "TracerApi.h"
#include <vector>
#include <functional>


class DispatchTracer : public TracerApi {
  std::vector<std::shared_ptr<TracerApi>> allTracers;

  template<typename Function, typename... Args>
  void dispatch(Function &&function, Args &&...args) {
    for (const auto &tracer : allTracers) {
      // create object (this) ptr from all tracer, and call the dispatched function on all tracers with the same
      // arguments
      function(&(*tracer), std::forward<Args>(args)...);
    }
  }

  void writeCommentSymbol() override{
    dispatch(std::mem_fn(&TracerApi::writeCommentSymbol));
  }

 public:
  void addTracer(const std::shared_ptr<TracerApi> &tracer) {
    allTracers.emplace_back(tracer);
  }

  void removeAll() {
    allTracers.clear();
  }

  bool isEmpty() {
    return allTracers.empty();
  }

  void comment(const std::string &msg) override {
  	dispatch(std::mem_fn(&TracerApi::comment), msg);
  }

  template<typename Function, typename... Args>
  void commentedApiFunction(Function &&function, Args &&...args) {
    dispatch(std::mem_fn(&TracerApi::commentedApiFunction<Function, Args...>), std::mem_fn(function), std::forward<Args>(args)...);
  }

  void puBusy(const ProcessingUnitImpl &pu, const ExecutionItem &executionItem) override {
	dispatch(std::mem_fn(&TracerApi::puBusy), pu, executionItem);
  };
  void puIdle(const ProcessingUnitImpl &pu, const ExecutionItem &executionItem) override {
	dispatch(std::mem_fn(&TracerApi::puIdle), pu, executionItem);
  };

  void processActivate(const Task &task, const Scheduler &scheduler, const StimulusImpl &stimulus) override {
	dispatch(std::mem_fn(&TracerApi::processActivate), task, scheduler, stimulus);
  };
  void processStart(const Task &task, const Scheduler &scheduler, const ProcessingUnitImpl &pu) override {
	dispatch(std::mem_fn(&TracerApi::processStart), task, scheduler, pu);
  };
  void processResume(const Task &task, const Scheduler &scheduler, const ProcessingUnitImpl &pu) override {
	dispatch(std::mem_fn(&TracerApi::processResume), task, scheduler, pu);
  };
  void processPreemt(const Task &task, const Scheduler &scheduler, const ProcessingUnitImpl &preemtPu) override {
	dispatch(std::mem_fn(&TracerApi::processPreemt), task, scheduler, preemtPu);
  };
  void processTerminate(const Task &task, const Scheduler &scheduler, const ProcessingUnitImpl &pu) override {
	dispatch(std::mem_fn(&TracerApi::processTerminate), task, scheduler, pu);
  };
  void taskMTALimitExceeded(const Task &task, const Scheduler &scheduler, const StimulusImpl &stimulus) override {
	dispatch(std::mem_fn(&TracerApi::taskMTALimitExceeded), task, scheduler, stimulus);
  };
  void runnableStart(const Runnable &runnable, const Task &task, const Scheduler &scheduler) override {
	dispatch(std::mem_fn(&TracerApi::runnableStart), runnable, task, scheduler);
  };
  void runnableSuspend(const Runnable &runnable, const Task &task, const Scheduler &scheduler) override {
	dispatch(std::mem_fn(&TracerApi::runnableSuspend), runnable, task, scheduler);
  };
  void runnableResume(const Runnable &runnable, const Task &task, const Scheduler &scheduler) override {
	dispatch(std::mem_fn(&TracerApi::runnableResume), runnable, task, scheduler);
  };
  void runnableTerminate(const Runnable &runnable, const Task &task, const Scheduler &scheduler) override {
	dispatch(std::mem_fn(&TracerApi::runnableTerminate), runnable, task, scheduler);
  };

  void stimulus(const StimulusImpl &stimulus) override {
	dispatch(std::mem_fn(&TracerApi::stimulus), stimulus);
  };
  void activityGraphItemStart(const ContainsActivityGraphItems &executionContext,
                              const ActivityGraphItem &item) override {
	dispatch(std::mem_fn(&TracerApi::activityGraphItemStart), executionContext, item);
  };
  void activityGraphItemFinish(const ContainsActivityGraphItems &executionContext,
                               const ActivityGraphItem &item) override {
	dispatch(std::mem_fn(&TracerApi::activityGraphItemFinish), executionContext, item);
  };
  void activityGraphItemBranch(const ContainsActivityGraphItems &executionContext,
                               const ActivityGraphItem &item) override {
	dispatch(std::mem_fn(&TracerApi::activityGraphItemBranch), executionContext, item);
  };
  void activityGraphItemBranchFinish(const ContainsActivityGraphItems &executionContext,
                                     const ActivityGraphItem &item) override {
	dispatch(std::mem_fn(&TracerApi::activityGraphItemBranchFinish), executionContext, item);
  };

  void signalRead(const Task &task, const AbstractMemoryElement &label) override {
	dispatch(std::mem_fn(&TracerApi::signalRead), task, label);
  };
  void signalWrite(const Task &task, const AbstractMemoryElement &label) override {
	dispatch(std::mem_fn(&TracerApi::signalWrite), task, label);
  };
};