/**
 ********************************************************************************
 * Copyright (c) 2020 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */
#pragma once
#include <memory>
#include <string_view>
#include "TracerApi.h"
#include "TracerInfo.h"
#include "Task.h"
#include "ContainsActivityGraphItems.h"
#include "Scheduler.h"
#include "ProcessingUnitImpl.h"
#include "StimulusImpl.h"

enum class BtfVersion {
  v2_1_5,
  v2_2_0
};

class BtfTracer : public TracerApi {
    inline const static TracerInfo<BtfTracer> info{"BtfTracer","basic BTF trace (default)"};
protected:
    std::fstream btfFile;
    const BtfVersion version;

    void writeEventLine(sc_core::sc_time ts,std::string_view source, int64_t sourceInstance, std::string_view targetType, std::string_view target, int64_t targetInstance, std::string_view eventName,std::string_view note){
        btfFile << ts.value() << ",";
        btfFile << source << "," << sourceInstance << "," <<  targetType << "," << target << "," << targetInstance << "," << eventName << "," << note << "\n"; 
    }


public:
    constexpr static std::string_view tracerName(){return "BtfTracer";};
    BtfTracer(const std::string &traceFile = "trace.btf", BtfVersion version = BtfVersion::v2_2_0, const std::string &description = "");
    
    void writeCommentSymbol() override{
        btfFile<<"#";
    }
    
    void comment(const std::string& msg) override {
        writeCommentSymbol();
        btfFile << sc_core::sc_time_stamp() << "," << msg << std::endl;
    }

    template<typename Function, typename... Args>
    void commentedApiFunction(Function &&function, Args &&...args)  {
         btfFile << "#";
         function(this, std::forward<Args>(args)...);
    }

    void processActivate(const Task &task, const Scheduler &/* scheduler */,const StimulusImpl &stimulus) override {
        writeEventLine(sc_core::sc_time_stamp(),stimulus.getName(),stimulus.instanceCount(),"T",task.getName(),task.instanceCount(),"activate","");
    };
    void taskMTALimitExceeded(const Task &task, const Scheduler &/* scheduler */,const StimulusImpl &stimulus) override {
        writeEventLine(sc_core::sc_time_stamp(),stimulus.getName(),stimulus.instanceCount(),"T",task.getName(),task.instanceCount(),"mtalimitexceeded","");
    };
    void processStart(const Task &task, const Scheduler &/* scheduler */,const ProcessingUnitImpl &pu) override {
       writeEventLine(sc_core::sc_time_stamp(),pu.getName(),0,"T",task.getName(),task.instanceCount(),"start","");
    };
    void processResume(const Task &task, const Scheduler &/* scheduler */,const ProcessingUnitImpl &pu) override {
        writeEventLine(sc_core::sc_time_stamp(),pu.getName(),0,"T",task.getName(),task.instanceCount(),"resume","");
    };
    void processPreemt(const Task &task, const Scheduler &/* scheduler */,const ProcessingUnitImpl &preemtPu) override {
        writeEventLine(sc_core::sc_time_stamp(),preemtPu.getName(),0,"T",task.getName(),task.instanceCount(),"preempt","");
    };
    void processTerminate(const Task &task, const Scheduler &/* scheduler */,const ProcessingUnitImpl &pu) override {
        writeEventLine(sc_core::sc_time_stamp(),pu.getName(),0,"T",task.getName(),task.instanceCount(),"terminate","");
    };
    void puBusy(const ProcessingUnitImpl &/* pu */,  const ExecutionItem &/* executionItem */) override {};
    void puIdle(const ProcessingUnitImpl &/* pu */,  const ExecutionItem &/* executionItem */) override {};
    void stimulus(const StimulusImpl &stimulus) override {
        //TODO differentiate InterTaskStimulus
        if (version == BtfVersion::v2_1_5) {
            writeEventLine(sc_core::sc_time_stamp(),"SIM",-1,"STI",stimulus.getName(),stimulus.instanceCount(),"trigger","");
        } else if (version == BtfVersion::v2_2_0) {
            writeEventLine(sc_core::sc_time_stamp(),stimulus.getName(),stimulus.instanceCount(),"STI",stimulus.getName(),stimulus.instanceCount(),"trigger","");
        }
    }

    void runnableStart(const Runnable &runnable, const Task &task, const Scheduler &/* scheduler */) override{
        writeEventLine(sc_core::sc_time_stamp(),task.getName(),task.getInstanceCount(),"R",runnable.getName(),runnable.getInstanceCount(),"start","");
    };
    void runnableSuspend(const Runnable &/* runnable */, const Task &/* task */, const Scheduler &/* scheduler */) override{};
    void runnableResume(const Runnable &/* runnable */, const Task &/* task */, const Scheduler &/* scheduler */) override{};
    void runnableTerminate(const Runnable &runnable, const Task &task, const Scheduler &/* scheduler */) override{
        writeEventLine(sc_core::sc_time_stamp(),task.getName(),task.getInstanceCount(),"R",runnable.getName(),runnable.getInstanceCount(),"terminate","");
    };
    void activityGraphItemStart(const ContainsActivityGraphItems &/*executionContext*/, const ActivityGraphItem &/*item*/) override{};
    void activityGraphItemFinish(const ContainsActivityGraphItems &/*executionContext*/, const ActivityGraphItem &/*item*/) override{};
    void activityGraphItemBranch(const ContainsActivityGraphItems &/*executionContext*/, const ActivityGraphItem &/*item*/) override{};
    void activityGraphItemBranchFinish(const ContainsActivityGraphItems &/*executionContext*/, const ActivityGraphItem &/*item*/) override{}
    
    void signalRead(const Task &/*task*/, const AbstractMemoryElement &/*label*/) override{};
    void signalWrite(const Task &/*task*/, const AbstractMemoryElement &/*label*/) override{};   
};

class BtfTracerAgi : public BtfTracer {
    inline const static TracerInfo<BtfTracerAgi> info{"BtfTracerAgi","extended BTF trace with activity graph item logging (AGI tags)"};
protected:

public:
    constexpr static std::string_view tracerName(){return "BtfTracerAgi";};

    BtfTracerAgi(const std::string &traceFile = "traceAGI.btf", BtfVersion version = BtfVersion::v2_2_0, const std::string &description = "") : BtfTracer(traceFile, version, description){};
    
    void activityGraphItemStart(const ContainsActivityGraphItems &executionContext, const ActivityGraphItem &item) override {
         writeEventLine(sc_core::sc_time_stamp(),executionContext.getName(),executionContext.getInstanceCount(),"AGI",item.getType(),-1,"start",item.getTraceString());
    };
    void activityGraphItemFinish(const ContainsActivityGraphItems &executionContext, const ActivityGraphItem &item) override{
        writeEventLine(sc_core::sc_time_stamp(),executionContext.getName(),executionContext.getInstanceCount(),"AGI",item.getType(),-1,"finish",item.getTraceString());
    };
    void activityGraphItemBranch(const ContainsActivityGraphItems &executionContext, const ActivityGraphItem &item) override{
        writeEventLine(sc_core::sc_time_stamp(),executionContext.getName(),executionContext.getInstanceCount(),"AGI",item.getType(),-1,"branch",item.getTraceString());
    };
    void activityGraphItemBranchFinish(const ContainsActivityGraphItems &executionContext, const ActivityGraphItem &item) override{
        writeEventLine(sc_core::sc_time_stamp(),executionContext.getName(),executionContext.getInstanceCount(),"AGI",item.getType(),-1,"branch join",item.getTraceString());
    };


};



class BtfTracerAgiBranch : public BtfTracerAgi {
    inline const static TracerInfo<BtfTracerAgiBranch> info{"BtfTracerAgiBranch","same as \"BtfTracerAgi\" with additional logging of branches and conditions"};
public: 
    constexpr static std::string_view tracerName(){return "BtfTracerAgiBranch";};
    BtfTracerAgiBranch(const std::string &traceFile = "traceAGIhierarchy.btf", BtfVersion version = BtfVersion::v2_2_0, const std::string &description = "") : BtfTracerAgi(traceFile, version, description){};
    
    void activityGraphItemStart(const ContainsActivityGraphItems &executionContext, const ActivityGraphItem &item) override {
         writeEventLine(sc_core::sc_time_stamp(),executionContext.getName(),executionContext.getInstanceCount(),"AGI",item.getType(),-1,"start",item.getTraceString());
    };
    void activityGraphItemFinish(const ContainsActivityGraphItems &executionContext, const ActivityGraphItem &item) override{
        writeEventLine(sc_core::sc_time_stamp(),executionContext.getName(),executionContext.getInstanceCount(),"AGI",item.getType(),-1,"finish",item.getTraceString());
    };
    void activityGraphItemBranch(const ContainsActivityGraphItems &executionContext, const ActivityGraphItem &item) override{
        writeEventLine(sc_core::sc_time_stamp(),executionContext.getName(),executionContext.getInstanceCount(),"AGI",item.getType(),-1,"branch",item.getTraceString());
    };
    void activityGraphItemBranchFinish(const ContainsActivityGraphItems &executionContext, const ActivityGraphItem &item) override{
        writeEventLine(sc_core::sc_time_stamp(),executionContext.getName(),executionContext.getInstanceCount(),"AGI",item.getType(),-1,"branch join",item.getTraceString());
    };
};

class BtfTracerSignal : public BtfTracer {
    inline const static TracerInfo<BtfTracerSignal> info{"BtfTracerSignal","same as \"BtfTracer\" with additional logging of signals"};
public:
    constexpr static std::string_view tracerName(){return "BtfTracerSignal";};
    BtfTracerSignal(const std::string &traceFile = "traceSignal.btf", BtfVersion version = BtfVersion::v2_2_0, const std::string &description = "") : BtfTracer(traceFile, version, description){};
    
    void signalRead(const Task &task, const AbstractMemoryElement &label) override {
        writeEventLine(sc_core::sc_time_stamp(),task.getName(),task.instanceCount(),"SIG",label.getName(),0,"read","");
    };
    void signalWrite(const Task &task, const AbstractMemoryElement &label) override {
         writeEventLine(sc_core::sc_time_stamp(),task.getName(),task.instanceCount(),"SIG",label.getName(),0,"write","");
    };
};