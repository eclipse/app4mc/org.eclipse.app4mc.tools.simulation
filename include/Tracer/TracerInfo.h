/**
 ********************************************************************************
 * Copyright (c) 2020 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */

#pragma once
#include "TracerApi.h"
#include "TraceManager.h"

template<class T>
class TracerInfo {
 public:
  std::string_view tracerName() {
	return tracerNameString;
  };
  std::string_view cliHelpText() {
	return cliHelpTextString;
  };

 private:
  uint32_t ID;
  std::string tracerNameString;
  std::string cliHelpTextString;

 public:
  uint32_t getID() {
	return ID;
  };
   TracerInfo(std::string tracerName, std::string cliHelpText) : tracerNameString(std::move(tracerName)), cliHelpTextString(std::move(cliHelpText)){
    ID = TraceManager::registerTracer(tracerNameString,cliHelpTextString, [](){return std::static_pointer_cast<TracerApi>(std::make_shared<T>());},typeid(T));
  };
};
