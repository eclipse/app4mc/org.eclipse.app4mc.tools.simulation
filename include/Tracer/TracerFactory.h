/**
 ********************************************************************************
 * Copyright (c) 2020 University of Rostock and others
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Sebastian Reiser <sebastian.reiser@de.bosch.com> - initial contribution
 ********************************************************************************
 */

#pragma once
#include <memory>
#include <vector>
#include <filesystem>
#include <string_view>

enum TracerSelect { BTF = 1, BTF_AGI = 2, BTF_AGI_BRANCH = 3, BTF_SIGNAL = 4 };

class TracerApi;

class TracerFactory {
public:
 	// this method is only transitional to keep older transformation versions running, the new API is always with TraceManager
  	[[deprecated]] static std::shared_ptr<TracerApi> createTracer(
		const int select, 
		const std::string& _path = "./",
		[[maybe_unused]] const std::string& _filename = "trace.btf");
  	static const std::vector<std::string>& getTracerNames();

  	static std::string_view getTracerName(int _id);
};
