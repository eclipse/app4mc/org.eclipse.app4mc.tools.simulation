/**
 ********************************************************************************
 * Copyright (c) 2020 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */

#pragma once
#include "TracerApi.h"
#include "DispatchTracer.h"
#include <filesystem>

template<class T> class TracerInfo ;

class TraceManager {
  struct TracerEntry {
	std::string name;
	std::string cliHelpText;
	uint32_t ID;
	std::function<std::shared_ptr<TracerApi>()> creator;
    const std::type_info &type;
  };

  static const std::shared_ptr<DispatchTracer> &internalInst();
  static std::vector<TracerEntry> &registry() {
    static std::vector<TracerEntry> registry;
    return registry;
  };

  static uint32_t registerTracer(std::string tracerName, std::string cliHelpText, std::function<std::shared_ptr<TracerApi>()> creator, const std::type_info &type);
  static std::filesystem::path& pathRef();

  static bool &useTimePrefix();

 public:
  static TracerApi &Inst();
  static void addTracer(const std::shared_ptr<TracerApi> &newTracer);
  static void removeAll();
  static void createTracerFromList(const std::vector<std::string> &list);
  static std::string getCliHelpText();
  static void setTraceDirectory(const std::string &path);
  static std::filesystem::path createTraceFilePath(const std::string &baseName);
  static void enableTimePrefix();

  template<class T>
  friend class TracerInfo;
};

