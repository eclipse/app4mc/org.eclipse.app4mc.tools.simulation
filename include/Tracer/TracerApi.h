/**
 ********************************************************************************
 * Copyright (c) 2020 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */

#pragma once
#include <sys/types.h>
#include <cstdint>
#include <functional>
#include <memory>
#include <string>
#include <fstream>
#include <string_view>
#include <type_traits>
#include <typeinfo>
#include <utility>


class ProcessingUnitImpl;
class Runnable;
class Task;
class ContainsActivityGraphItems;
class AbstractMemoryElement;
class StimulusImpl;
class ExecutionItem;
class Scheduler;
class ActivityGraphItem;

class TracerApi{
protected:
public:
    virtual ~TracerApi() = default;

    virtual void writeCommentSymbol() = 0;
    virtual void comment(const std::string& msg) = 0;
    
    template<typename Function, typename... Args>
    void commentedApiFunction(Function &&function, Args &&...args){
         writeCommentSymbol();
         function(this, std::forward<Args>(args)...);
         
    }
    
    virtual void puBusy(const ProcessingUnitImpl &pu,  const ExecutionItem &executionItem) = 0;
    virtual void puIdle(const ProcessingUnitImpl &pu,  const ExecutionItem &executionItem) = 0;

    virtual void processActivate(const Task &task, const Scheduler &scheduler, const StimulusImpl &stimulus) = 0;
    virtual void processStart(const Task &task, const Scheduler &scheduler,const ProcessingUnitImpl &pu) = 0;
    virtual void processResume(const Task &task, const Scheduler &scheduler,const ProcessingUnitImpl &pu) = 0;
    virtual void processPreemt(const Task &task, const Scheduler &scheduler,const ProcessingUnitImpl &preemtPu) = 0;
    virtual void processTerminate(const Task &task, const Scheduler &scheduler,const ProcessingUnitImpl &pu) = 0;
    virtual void taskMTALimitExceeded(const Task &task, const Scheduler &scheduler, const StimulusImpl &stimulus) = 0;
    virtual void runnableStart(const Runnable &runnable, const Task &task, const Scheduler &scheduler) = 0;
    virtual void runnableSuspend(const Runnable &runnable, const Task &task, const Scheduler &scheduler) = 0;
    virtual void runnableResume(const Runnable &runnable, const Task &task, const Scheduler &scheduler) = 0;
    virtual void runnableTerminate(const Runnable &runnable, const Task &task, const Scheduler &scheduler) = 0;
   
    virtual void stimulus(const StimulusImpl &stimulus) =0;
    virtual void activityGraphItemStart(const ContainsActivityGraphItems &executionContext, const ActivityGraphItem &item) = 0;
    virtual void activityGraphItemFinish(const ContainsActivityGraphItems &executionContext, const ActivityGraphItem &item) = 0;
    virtual void activityGraphItemBranch(const ContainsActivityGraphItems &executionContext, const ActivityGraphItem &item) = 0;
    virtual void activityGraphItemBranchFinish(const ContainsActivityGraphItems &executionContext, const ActivityGraphItem &item) = 0;
    
    virtual void signalRead(const Task &task, const AbstractMemoryElement &label) = 0;
    virtual void signalWrite(const Task &task, const AbstractMemoryElement &label) = 0;
    
};


