/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include <sysc/kernel/sc_time.h>
#include <chrono>
#include <ratio>
#include <stdexcept>
#include "TimeUtils.h"


/**
 * Get the precision of SystemC simulation (which is only known at runtime, but cannot change after init)
 */
std::chrono::duration<int64_t,std::nano> getTimeresolutionAsDuration();

template<class period>
sc_core::sc_time convertTime(std::chrono::duration<int64_t,period> time){
	if (time.count() != 0 && time/getTimeresolutionAsDuration() == 0){
			throw std::runtime_error("Supplied Time is not convertible due to SystemC coarser time resolution" );
	}
	return sc_core::sc_time::from_value(time/getTimeresolutionAsDuration());
}

