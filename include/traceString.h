/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include <mutex>
#include <map>
#include <memory>
#include <functional>
#include "Types.h"

namespace sc_core{
	class sc_trace_file;
}

class traceString {
	// standard technique to avoid static initilization problems, all static "variables" are transformed to functions
	static std::mutex &fileMut();
	static std::ofstream &translationFile();
	static uint32_t &maxID();
	static std::map<std::string, uint32_t, std::less<>> &IDMap();
	uint32_t stringID = 0;
public:
	// optimize the lookup of the idle string id
	// it's saved in a static variable after created once in whole runtime with a lambda expression
	static traceID getIdleStringID();
    explicit traceString(const std::function<std::string()> &stringFunc) ;
    explicit traceString(const std::function<std::string_view()> &stringFunc);
    explicit traceString(std::string_view string); 
	static const std::string& outputPath(const std::string& path = ".");
	void set(std::string_view string);
	uint32_t getID() const;
	void setID(uint32_t newID);

	static void systemc_trace(sc_core::sc_trace_file *tf,const uint32_t& v, const std::string &name);
	inline friend void sc_trace(sc_core::sc_trace_file *tf,const traceString *v, const std::string &name) {
		systemc_trace(tf, v->stringID,name);
	}
	inline friend void sc_trace(sc_core::sc_trace_file *tf, const traceString &v, const std::string &name) {
		systemc_trace(tf, v.stringID, name);
	}
};

