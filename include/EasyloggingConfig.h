/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */
#pragma once

#include <memory>
class EasyloggingConfig{
public:
	static void configure(const std::string fileName="app4mcsim.log", unsigned short verbose=0, bool toStdOut=true, bool toFile=true);
	static void enable(bool enable = true);
	static void disable();	
	
	//to allow logging from SimRunner children
	static void msg(const std::string&, int verbose =0);
};



