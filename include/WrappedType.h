/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include <memory>

namespace sc_core{
	class sc_trace_file;
} // namespace sc_core

template<
	typename InternalType
>
class WrappedType{ 
private:
	const std::shared_ptr<InternalType> inst;

	static void sc_trace_internal(sc_core::sc_trace_file *tf, const std::shared_ptr<WrappedType<InternalType>>& v,
		const std::string & SigName);

protected:

public:
	template<typename ...Args>
	explicit WrappedType(Args&& ...args):		
		inst(std::make_shared<InternalType>(std::forward<Args>(args)...)){}

	explicit WrappedType(const std::shared_ptr<InternalType>& _internalInst):		
		inst(_internalInst){}
	
	const std::shared_ptr<InternalType>& internalInst() const {
		return inst;
	}

	inline friend void sc_trace(sc_core::sc_trace_file *tf, const std::shared_ptr<WrappedType<InternalType>>& v,
		const std::string & SigName) 
	{
		sc_trace_internal(tf, v, SigName);
	}

 	static constexpr bool isWrappedType(){return true;}
};
