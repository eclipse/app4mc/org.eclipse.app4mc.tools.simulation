/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include "MemoryAccessType.h"
#include "WrappedType.h"
#include "Deviation.h"
#include "DataRate.h"

class ConnectionHandlerImpl;
class Connection;
class Scheduler;
class HwStructureImpl;

class ConnectionHandler: public WrappedType<ConnectionHandlerImpl>{
	friend HwStructureImpl;
private: 
	explicit ConnectionHandler(const std::string& _name, bool _use_global_queue = false);
	explicit ConnectionHandler(const std::shared_ptr<ConnectionHandlerImpl>& _inst);
public:


	template<class T> void setReadLatency( const T &lat);
	template<class T> void setWriteLatency( const T &lat);
	void setDataRate(const DataRate &rate);
	
	//HW module basics
	void setClockPeriod(const Time& _clock_period);

	//(named) ports & connections
	void addInitPort(const std::string &name);
	void addTargetPort(const std::string &name);
	void bindConnection(const std::shared_ptr<Connection> &connection, const std::string &portName);


};

extern template void ConnectionHandler::setReadLatency(const DiscreteValueConstant& lat);
extern template void ConnectionHandler::setReadLatency(const DiscreteValueGaussDistribution& lat);
extern template void ConnectionHandler::setReadLatency(const DiscreteValueWeibullEstimatorsDistribution& lat);
extern template void ConnectionHandler::setReadLatency(const DiscreteValueUniformDistribution& lat);
extern template void ConnectionHandler::setReadLatency(const DiscreteValueBetaDistribution& lat);
extern template void ConnectionHandler::setReadLatency(const DiscreteValueBoundaries& lat);

extern template void ConnectionHandler::setWriteLatency(const DiscreteValueConstant& lat);
extern template void ConnectionHandler::setWriteLatency(const DiscreteValueGaussDistribution& lat);
extern template void ConnectionHandler::setWriteLatency(const DiscreteValueWeibullEstimatorsDistribution& lat);
extern template void ConnectionHandler::setWriteLatency(const DiscreteValueUniformDistribution& lat);
extern template void ConnectionHandler::setWriteLatency(const DiscreteValueBetaDistribution& lat);
extern template void ConnectionHandler::setWriteLatency(const DiscreteValueBoundaries& lat);