
/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include <systemc>
#include <tlm>
#include <tlm_utils/simple_initiator_socket.h>
#include <tlm_utils/simple_target_socket.h>
#include <functional>
#include <memory>
#include <optional>
#include "Common.h"
#include "Deviation.h"
#include "ProcessingUnitDefinition.h"
#include "ChannelEvent.h"
#include "ExecutionItem.h"
#include "Scheduler.h"
#include <cstdint>
#include <queue>

#include "HwAccessElement.h"
#include "HwAccessPathTLMext.h"

struct HwAccessPathTLMext : tlm::tlm_extension<HwAccessPathTLMext> {

	std::shared_ptr<HwAccessElement> elem;

	virtual tlm_extension_base* clone() const {
		HwAccessPathTLMext *ext = new  HwAccessPathTLMext;
		ext->elem = this->elem;
		return ext;

	}

	virtual void copy_from(tlm_extension_base const& ext)
	{
		elem = static_cast<HwAccessPathTLMext const &>(ext).elem;
	}

};
