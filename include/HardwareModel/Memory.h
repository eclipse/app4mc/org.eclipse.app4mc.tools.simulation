/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include "WrappedType.h"
#include "Deviation.h"
#include "TimeUtils.h"

class Connection;
class MemoryImpl;
class HwStructureImpl;
struct DataRate;

class Memory: public WrappedType<MemoryImpl>{	
	friend HwStructureImpl;
private: 
	explicit Memory(const std::string& _name, uint32_t size);
	explicit Memory(const std::shared_ptr<MemoryImpl>& _inst);
public:

	//memory element details
	template<class T>void setAccessLatency( const T &lat);
	//specializations
	void setDataRate(const DataRate &rate);

	//HW module basics
	void setClockPeriod(const Time&  _clock_period);

	//(named) ports & connections
	void addInitPort(const std::string &name);
	void addTargetPort(const std::string &name);
	void bindConnection(const std::shared_ptr<Connection> &connection, const std::string &portName);
};

extern template void Memory::setAccessLatency(const DiscreteValueConstant& lat);
extern template void Memory::setAccessLatency(const DiscreteValueGaussDistribution& lat);
extern template void Memory::setAccessLatency(const DiscreteValueWeibullEstimatorsDistribution& lat);
extern template void Memory::setAccessLatency(const DiscreteValueUniformDistribution& lat);
extern template void Memory::setAccessLatency(const DiscreteValueBetaDistribution& lat);
extern template void Memory::setAccessLatency(const DiscreteValueBoundaries& lat);