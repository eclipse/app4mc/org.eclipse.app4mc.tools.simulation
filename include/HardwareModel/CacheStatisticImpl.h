/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include "HwModule.h"
#include "DataRate.h"


class CacheStatisticImpl : public HwModule<CacheStatisticImpl> {
	double hitRate;
	std::unique_ptr<IValueDeviation<ELong>> accessLatency;
	DataRate dataRate = {0 , DataRateUnit::bitPerSecond};
	size_t lineSize = 64;
	size_t offsetBits = (size_t)(ceil(log2(lineSize)));
	size_t size = (size_t)pow(2,14);
	tlm::tlm_generic_payload missTransaction;

	void sanityChecks() override;
	void registerCallbacks() override;

public:
	template<class T>
	void setAccessLatency( const T &lat) {
		accessLatency = std::make_unique<T>(lat);
	}
	
	CacheStatisticImpl(const std::string&, double _hitRate);

	void setDataRate(const DataRate &rate);

	/* rounds up to next power of 2*/
	void setLineSize(size_t _lineSize);

	/* round up to next multiple of lineSize which is power of 2*/
	void setSize(size_t _size);

	void b_transport(tlm::tlm_generic_payload& trans, sc_core::sc_time& delay);
};
