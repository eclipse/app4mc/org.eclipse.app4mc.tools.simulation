/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include <string>
#include <vector>
#include "HasName.h"

class HwFeature{
public:
	HwFeature(std::string _name, double _value) : name(_name), value(_value) {};
	std::string name;
	double value;
};

/**
 * Struct to refer to a Class of ProcessingUnit with the same characteristics
 * this is needed as key e.g. CallGraphItem TicksEntry, to specialize execution time
 * for ProcessingUnits
 */
class ProcessingUnitDefinition : public HasName{
	
	std::vector<HwFeature> features;
public:
	ProcessingUnitDefinition(const std::string &_name) : HasName(_name){};
	void addFeature(const std::string &_name, double value){
		features.emplace_back(HwFeature{_name,value});
	}

	void addFeature(const HwFeature &_hwFeature) {
		features.emplace_back(_hwFeature);
	}

};