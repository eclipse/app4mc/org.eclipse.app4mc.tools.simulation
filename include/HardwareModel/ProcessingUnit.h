/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include <memory>
#include "ProcessingUnitDefinition.h"
#include "WrappedType.h"
#include "Connection.h"
#include "Deviation.h"

class ProcessingUnitImpl;
class ProcessingUnitDefinition;
struct HwAccessElement;
class Scheduler;
class HwStructureImpl;

class ProcessingUnit: public WrappedType<ProcessingUnitImpl>{
		friend HwStructureImpl;
private: 
	ProcessingUnit(const std::string &name, const std::shared_ptr<ProcessingUnitDefinition> &definition);
	explicit ProcessingUnit(const std::shared_ptr<ProcessingUnitImpl>& _inst);
public:
	void setScheduler(std::shared_ptr<Scheduler> sched);

	void addHWAccessElement(HwAccessElement& elem);
	
	[[nodiscard]] const std::shared_ptr<ProcessingUnitDefinition>& getProcessingUnitDefinition() const;

	//HW module basics
	void setClockPeriod(const Time& _clock_period);

	//(named) ports & connections
	void addInitPort(const std::string &name);
	void addTargetPort(const std::string &name);
	void bindConnection(const std::shared_ptr<Connection> &connection, const std::string &portName);

};