/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include <cstddef>
#include <stdexcept>
#include <systemc>
#include <tlm>
#include <tlm_utils/simple_initiator_socket.h>
#include <tlm_utils/simple_target_socket.h>
#include <functional>
#include <memory>
#include <optional>
#include <cstdint>
#include <queue>
#include <type_traits>
#include <utility>
#include "Common.h"
#include "HwModuleBase.h"
#include "WrappedType.h"
#include "hasNamedPorts.h"
#include "Deviation.h"
#include "ChannelEvent.h"
#include "ExecutionItem.h"
#include "Scheduler.h"
#include "HwModuleBase.h"
#include "ProcessingUnitDefinition.h"
#include "ProcessingUnitImpl.h"
#include "ConnectionImpl.h"
#include "ConnectionHandlerImpl.h"
#include "CacheStatisticImpl.h"
#include "MemoryImpl.h"


class HwStructureImpl : public hasNamedPorts<HwStructureImpl,tlm::tlm_initiator_socket<>,tlm::tlm_target_socket<>>, public HasName {
	
	std::vector<std::shared_ptr<HwStructureImpl>> subStructures;
	std::vector<std::shared_ptr<HwModuleBase>> modules;
	std::vector<std::shared_ptr<ConnectionImpl>> connections;

	std::vector < std::pair < std::shared_ptr<ConnectionImpl>&, std::string>> delegatedPortBindingsDeferred;
	std::unordered_map<std::string, std::pair<std::shared_ptr<ConnectionImpl>, std::shared_ptr<ConnectionImpl>>> portBindingsDeferred;

	void registerCallbacks() override{
		//nothing to do here, as HwStrucuture ports are only a transaction proxies
	}

		/**
		generally connections in the deferred bindings (std::pair) are not expected to be ordered,
		beyond the time they have been added. However it is important to establish an order to
		correctly connect the proxy ports that only route transactions through hierarchy boundaries.
		This function establishes an order for target and initiator ports that resembles the direction
		of data flowing. Data is fed into a structure through a proxy target port and coming out of a structure
		through a proxy initiator port
	**/
	void sortConnectionsAsDataFlows(){
		for (const auto&[portName, initiatorPort] : initPortMap) {
			/*	after sorting the init/target port map with its connection pair resembles this

					|''HwStructureImpl''''|
					|    [x--con1---o] -> o -> [x--con1---o]	
					|_____________________|
					-> = binding
					x=target port
					o=init port
					con1 = pair.first
					con2 = pair.second
			*/

			auto [con1, con2] = portBindingsDeferred[portName];
			if (con1->getHierarchicalParent() == this &&!(con2->getHierarchicalParent() == this)){
				continue; //nothing to do
			} else if (con2->getHierarchicalParent() == this && !(con1->getHierarchicalParent() == this)){
				portBindingsDeferred[portName] = std::make_pair(con2, con1);
			} else {
				throw std::runtime_error("Initiator proxy port " + portName + "is connected to connections " + \
					con1->getName() +", " + con2->getName() + \
					". One connection must be defined within hw structure" + getName() + \
					" to establish a connection via hierarchal boundaries");
			}
		}

		for (const auto&[portName, tartgetPort] : targetPortMap) {
			auto [con1, con2] = portBindingsDeferred[portName];
			if (con1->getHierarchicalParent() == this &&!(con2->getHierarchicalParent() == this)){
				/*	after sorting the target port map with its connection pair resembles this

									|''HwStructureImpl''''''''|
				   [x--con1---o] ->	x -> [x--con2---o ]   |
									|_____________________|
					-> = binding
					x=target port
					o=init port
					con1 = pair.first
					con2 = pair.second
				*/
			
				portBindingsDeferred[portName] = std::make_pair(con2, con1);
			} else if (con2->getHierarchicalParent() == this && !(con1->getHierarchicalParent() == this)){
				continue; //order ok, nothing to do
			} else {
				throw std::runtime_error("Target proxy port " + portName + "is connected to connections " + \
					con1->getName() +", " + con2->getName() + \
					". One connection must be defined within hw structure" + getName() + \
					" to establish a connection via hierarchal boundaries");
			}
		}
	}

public: 
	virtual	~HwStructureImpl() = default;

	std::shared_ptr<HwStructureImpl> createSubStructure(const std::string& name){
		sc_core::sc_get_curr_simcontext()->hierarchy_push(this);
		auto ptr = std::make_shared<HwStructureImpl>(name);
		subStructures.emplace_back(ptr);
		this->add_child_object(&(*ptr));
		sc_core::sc_get_curr_simcontext()->hierarchy_pop();
		return ptr;
	}


	std::shared_ptr<ConnectionImpl> createConnection(const std::string& name) {
		/* hierarchy_push is needed for correct naming */
		sc_core::sc_get_curr_simcontext()->hierarchy_push(this);
		auto ptr = std::make_shared<ConnectionImpl>(name);
		connections.emplace_back(ptr);
		this->add_child_object(&(*ptr));
		sc_core::sc_get_curr_simcontext()->hierarchy_pop();
		return ptr;
	}
	
	//T is derived from template class WrappedType
	template<
		class T, 
		class... con_args, 
		decltype(std::declval<T>().isWrappedType()) isWrapped = true
	>
	std::shared_ptr<T> createModule(con_args&&... _Args) 
	{			
		sc_core::sc_get_curr_simcontext()->hierarchy_push(this);
		//auto ptr = std::make_shared<T>(std::forward<con_args>(_Args)...);
		auto ptr = [&](){
			//hw modules' ctor's are private --> cannot constructed using make_shared
			auto* rawPtr = new T(std::forward<con_args>(_Args)...);
			return std::shared_ptr<T>(rawPtr);
		}();
		
		modules.emplace_back(ptr->internalInst());
		this->add_child_object(&(*(ptr->internalInst())));
		sc_core::sc_get_curr_simcontext()->hierarchy_pop();
		return ptr;
	}
	
	explicit HwStructureImpl(const std::string& name) : hasNamedPorts(name), HasName(name) {};

	void bindPorts() override{
		sortConnectionsAsDataFlows();
		//bind  outgoing connections to inititor port
		for (const auto&[portName, initiatorPort] : initPortMap) {
			const auto& [internalConnection, externalConnection] = portBindingsDeferred[portName];
			outgoingConnectionToPort[externalConnection] = initiatorPort;
			/*Note:
			for inbound connections via proxy the
			internal (proxy=initiator->initiator)
			connection must be bound prior
			to the external connection 
			*/
			//bind (hierarchical) init port to init port (proxy)
			internalConnection->getInitiatorPort().bind(*initiatorPort);
			//bind external connection (initiator->target)
			initiatorPort->bind(externalConnection->getTargetPort());
		}
		//bind  outgoing connections to inititor port
		for (const auto&[portName, targetPort] : targetPortMap) {
			const auto& [externalConnection, internalConnection] = portBindingsDeferred[portName];
			incomingPortToConnection[targetPort] = externalConnection;
			/*Note:
			for outbound connections via proxy the
			internal (proxy=target->target)
			connection must be bound prior
			to the external connection 
			*/
			//bind (hierarchical) target port to target port (proxy)
			targetPort->bind(internalConnection->getTargetPort());
			//bind external connection (initiator->target)
			externalConnection->getInitiatorPort().bind(*targetPort);
			//internalConnection->getTargetPort().bind(*targetPort);
		}
    }

	/*
		bind Connection needs an override, as a port on a HW Structure port is a proxy port, which is bound to two connections
	*/
	void bindConnection(const std::shared_ptr<ConnectionImpl> &connection, const std::string &portName) {
		if (portBindingsDeferred.count(portName)==0){
			portBindingsDeferred.emplace(portName, std::make_pair(connection, nullptr));
			return;
		} else if (portBindingsDeferred[portName].second == nullptr){
				portBindingsDeferred[portName].second = connection;
				return;
		}
		throw std::runtime_error("Port " + portName + "already bound to connections " + \
		portBindingsDeferred[portName].first->getName() + " and " + portBindingsDeferred[portName].second->getName() \
			+ ". Cannot re-bind to connection " + connection->getName());
	}

	virtual void sanityChecks() override {
		for (const auto &[portName, _] : initPortMap) {
			if (portBindingsDeferred.count(portName) == 0) {
				throw std::runtime_error("Hierarchical initiator port " + portName + " of " + name() + " not bound");
			} else {
				const auto& [con1, con2] = portBindingsDeferred[portName];
				if (!con1 || !con2){
					std::string con1Name, con2Name = "UNDEFINED";
					if (con1){
						con1Name = con1->getName();
					}
					if (con2){
						con2Name = con2->getName();
					}
					throw std::runtime_error("Hierarchical initiator port " + portName + " of " + name() + " not fully bound:" +\
					" connection 1 = " + con1Name + " connection 2 = " + con2Name);
				}
			}
		}
		for (const auto &[portName, _] : targetPortMap) {
			if (portBindingsDeferred.count(portName) == 0) {
				throw std::runtime_error("Hierarchical target port " + portName + " of " + name() + " not bound");
			} else {
				const auto& [con1, con2] = portBindingsDeferred[portName];
				if (!con1 || !con2){
					std::string con1Name, con2Name = "UNDEFINED";
					if (con1){
						con1Name = con1->getName();
					}
					if (con2){
						con2Name = con2->getName();
					}
					throw std::runtime_error("Hierarchical target port " + portName + " of " + name() + " not fully bound:" +\
					" connection 1 = " + con1Name + " connection 2 = " + con2Name);
				}
			}
		}
		/*check for bad bindings for debugging purposes, too few bound ports are already checked by the
		exceptions above */
		if (targetPortMap.size() + initPortMap.size() < portBindingsDeferred.size()) {
			for (const auto &[portName, _ /*connectionPair*/] : portBindingsDeferred) {
				if (!portExists(portName)) {
					throw std::runtime_error(std::string(name()) + "has no port " + portName + ". Ensure port has been added to hw module." );
				}
			}
		}
	}
};