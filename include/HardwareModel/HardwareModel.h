/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include "WrappedType.h"
#include "CacheStatistic.h"
#include "ConnectionHandler.h"
#include "Connection.h"
#include "Memory.h"
#include "HwAccessElement.h"
#include "HwStructure.h"
#include "ProcessingUnit.h"
#include "ProcessingUnitDefinition.h"
