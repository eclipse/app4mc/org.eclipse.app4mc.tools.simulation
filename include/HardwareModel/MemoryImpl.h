
/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include "HwModule.h"
#include "polymorphic_value.h"
#include "DataRate.h"

class MemoryImpl : public HwModule<MemoryImpl>{
public:

	void registerCallbacks() override{
		for (const auto & [tp, _] : incomingPortToConnection){
			tp->register_b_transport(this, &MemoryImpl::b_transport);
		}
	}
	
	uint32_t sizeBytes;
	isocpp_p0201::polymorphic_value<IValueDeviation<ELong>> accessLatency;
	DataRate dataRate = {0 , DataRateUnit::bitPerSecond};

	MemoryImpl(const std::string& name, uint32_t size) : 
		HwModule(name), 
		sizeBytes(size) 
	{	}
	
	template<class T>
	void setAccessLatency( const T &lat) {
		accessLatency = isocpp_p0201::make_polymorphic_value<IValueDeviation<ELong>,T>( lat);
	}

	void setDataRate(const DataRate &rate){
		dataRate = rate;
	}

private:
	void b_transport(tlm::tlm_generic_payload& trans, sc_core::sc_time& delay);
};

