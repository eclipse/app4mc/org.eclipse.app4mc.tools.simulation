
/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include "HwModuleBase.h"
#include "hasNamedPorts.h"

template<
	typename HwType, 
	typename initPortsType   = tlm_utils::simple_initiator_socket_optional<HwType>,
	typename targetPortsType = tlm_utils::simple_target_socket_optional<HwType>
>
class HwModule : public hasNamedPorts<HwType, initPortsType, targetPortsType > , public HwModuleBase {
public:
	explicit HwModule(const std::string& name) : hasNamedPorts<HwType, initPortsType, targetPortsType>(name), HwModuleBase(std::string(name)) {};
};
