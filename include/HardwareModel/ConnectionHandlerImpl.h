/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include <tlm_utils/simple_initiator_socket.h>
#include <tlm_utils/simple_target_socket.h>
#include "HwModule.h"
#include "polymorphic_value.h"
#include <queue>
#include "DataRate.h"

class ConnectionHandlerImpl : 
	public HwModule<
		ConnectionHandlerImpl, 
		tlm_utils::simple_initiator_socket_tagged_optional<ConnectionHandlerImpl>,
		tlm_utils::simple_target_socket_tagged_optional<ConnectionHandlerImpl>>
{
	using ConnectionHandlerInitPort = tlm_utils::simple_initiator_socket_tagged_optional<ConnectionHandlerImpl>;
	using ConnectionHandlerTargetPort = tlm_utils::simple_target_socket_tagged_optional<ConnectionHandlerImpl>;
private:
	std::vector<std::unique_ptr<sc_core::sc_event>> wakeups;
	std::map<int, std::shared_ptr<ConnectionHandlerTargetPort>> targetPortIds;
	std::map<std::shared_ptr<ConnectionHandlerInitPort>, std::queue<int, std::deque<int>>> init_fifos;
	std::queue<int, std::deque<int>> global_fifo;
	DataRate dataRate = {0 , DataRateUnit::bitPerSecond};
	const bool use_global_queue;
public:
	isocpp_p0201::polymorphic_value<IValueDeviation<ELong>> readLatency;
	isocpp_p0201::polymorphic_value<IValueDeviation<ELong>> writeLatency;

	explicit ConnectionHandlerImpl(
		const std::string& _name, 
		bool _use_global_queue = false);

	template<class T>
	void setReadLatency( const T &lat) {
		readLatency = isocpp_p0201::make_polymorphic_value<IValueDeviation<ELong>,T>( lat);
	}
		
	template<class T>
	void setWriteLatency( const T &lat) {
		writeLatency = isocpp_p0201::make_polymorphic_value<IValueDeviation<ELong>,T>( lat);
	}

	void setDataRate(const DataRate &rate);

	void registerCallbacks() override;

	void b_transport(int id, tlm::tlm_generic_payload& trans, sc_core::sc_time& delay);

};