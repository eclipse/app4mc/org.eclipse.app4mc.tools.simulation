/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include <tlm_utils/simple_initiator_socket.h>
#include <tlm_utils/simple_target_socket.h>
#include <cstddef>
#include <functional>
#include <memory>
#include <stdexcept>
#include <type_traits>
#include <set>
#include "TraceMemTransactionOccupation.h"
#include "ConnectionImpl.h"
#include "Callbacks.h"

template<
	class HwType, 
	typename initPortsType = tlm_utils::simple_initiator_socket_optional<HwType>, 
	typename targetPortsType = tlm_utils::simple_target_socket_optional<HwType>>
class hasNamedPorts : public sc_core::sc_module, public TraceMemTransactionOccupation {
private:
		
	std::unordered_map<std::string, std::shared_ptr<ConnectionImpl>> portBindingsDeferred;
protected:
	std::map<std::string, std::shared_ptr<initPortsType>> initPortMap;
	std::map<std::string, std::shared_ptr<targetPortsType>> targetPortMap;

	//derived lookup		
	std::unordered_map<std::shared_ptr<targetPortsType>, std::shared_ptr<ConnectionImpl>> incomingPortToConnection;
	std::unordered_map<std::shared_ptr<ConnectionImpl>,std::shared_ptr<initPortsType>> outgoingConnectionToPort;
	
	const std::string& getPortName(const std::shared_ptr<initPortsType>& port){
		for (const auto& [_name, _port]: initPortMap){
			if (_port == port){
				return _name;
			}
		}
	}

	const std::string& getPortName(const std::shared_ptr<targetPortsType>& port){
		for (const auto& [_name, _port]: targetPortMap){
			if (_port == port){
				return _name;
			}
		}
	}

	bool portExists(const std::string &name){
		return ((initPortMap.count(name) + targetPortMap.count(name)) != 0);
	}

	virtual void sanityChecks() {
		for (const auto &[portName, _] : initPortMap) {
			if (portBindingsDeferred.count(portName) == 0) {
				throw std::runtime_error("Initiator port " + portName + " of " + name() + " not bound");
			}
		}
		for (const auto &[portName, _] : targetPortMap) {
			if (portBindingsDeferred.count(portName) == 0) {
				throw std::runtime_error("Target port " + portName + " of " + name() + " not bound");
			}
		}
		/*check for bad bindings for debugging purposes, too few bound ports are already checked by the
		exceptions above */
		if (targetPortMap.size() + initPortMap.size() < portBindingsDeferred.size()) {
			for (const auto &[portName, connection] : portBindingsDeferred) {
				if (!portExists(portName)) {
					throw std::runtime_error(std::string(name()) + "has no port " + portName + ". Ensure port has been added to hw module." );
				}
			}
		}
	}

    virtual void bindPorts() {
		//bind  outgoing connections to inititor port
		for (const auto&[portName, initiatorPort] : initPortMap) {
			const auto& connection = portBindingsDeferred[portName];
			outgoingConnectionToPort[connection] = initiatorPort;
			connection->getTargetPort().bind(*initiatorPort);
		}
		//bind target ports to incoming connections
		for (const auto &[portName, targetPort] : targetPortMap) {
			const auto& connection = portBindingsDeferred[portName];
			incomingPortToConnection[targetPort] = connection;
			targetPort->bind(connection->getInitiatorPort());
		}
    }

	virtual void registerCallbacks()=0;

	std::function<void()> f_before_end_of_elaboration = NULL;
	std::function<void()> f_end_of_elaboration = NULL;
	std::function<void()> f_start_of_simulation = NULL;
	std::function<void()> f_end_of_simulation =  NULL;


public:

	explicit hasNamedPorts(const std::string& name) : sc_module(sc_core::sc_module_name(name.c_str())) {
	};

	void before_end_of_elaboration() override {
		sanityChecks();
		createPorts();
		bindPorts();
		createTracings();
		registerCallbacks();
		if (f_before_end_of_elaboration != NULL){
			f_before_end_of_elaboration();
		}
	}

	void addInitPort(const std::string &name) {
		initPortMap[name] = nullptr;
	}

	void addTargetPort(const std::string &name) {
		targetPortMap[name] = nullptr;
	}

	void createPorts(){
		sc_core::sc_get_curr_simcontext()->hierarchy_push(this);
		for (const auto [name, _] :  initPortMap){
			initPortMap[name] = std::make_shared<initPortsType>(name.c_str());
			addTracingPort(name);
		}
		for (const auto [name, _] :  targetPortMap){
			targetPortMap[name] = std::make_shared<targetPortsType>(name.c_str());
			addTracingPort(name);
		}
		sc_core::sc_get_curr_simcontext()->hierarchy_pop();
	}

	void bindConnection(const std::shared_ptr<ConnectionImpl> &connection, const std::string &portName) {
		if (portBindingsDeferred.count(portName)==0){
			portBindingsDeferred.emplace(portName, connection);
			return;
		}
		throw std::runtime_error("Port " + portName + "already bound to connection " + portBindingsDeferred[portName]->getName() \
			+ ". Cannot re-bind to connection " + connection->getName());
	}


	void setSimCallback(const sim_callback_type& type, const std::function<void()>&& f){
		switch (type){
			case sim_callback_type::before_end_of_elaboration: 
				f_before_end_of_elaboration = f;
				break;
			case sim_callback_type::end_of_elaboration: 
				f_end_of_elaboration = f;
				break;
			case sim_callback_type::start_of_simulation: 
				f_start_of_simulation = f;
				break;
			case sim_callback_type::end_of_simulation: 
				f_end_of_simulation = f;
				break;
		}
	}
	
	void end_of_elaboration()  override {
		if (f_end_of_elaboration != NULL){
			f_end_of_elaboration();
		}
	}

	void start_of_simulation()  override {
	if (f_start_of_simulation != NULL){
			f_start_of_simulation();
		}
	}
	
	void end_of_simulation()  override {
		if (f_end_of_simulation != NULL){
			f_end_of_simulation();	
		}
	}

};

