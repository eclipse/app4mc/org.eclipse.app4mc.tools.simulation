/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */
#pragma once

#include <memory>
#include <stdexcept>
#include <functional>
#include "WrappedType.h"
#include "Callbacks.h"

class HwStructureImpl;
class Connection;
class ProcessingUnit;
class ProcessingUnitDefinition;
class Memory;
class ConnectionHandler;
class CacheStatistic;
class SimRunner;

class HwStructure : public WrappedType<HwStructureImpl>
{
	friend SimRunner; //ensure any hw model element (structure, memory, processing unit ...) is constructed sub-element to model root
private:
	//hide constructors, but allow access via SimRunner class (--> friend)
	explicit HwStructure(const std::string& name);
	explicit HwStructure(const std::shared_ptr<HwStructureImpl>& inst);
public:
	~HwStructure()=default;
//contained elements
	std::shared_ptr<HwStructure> createSubStructure(const std::string& name);
	std::shared_ptr<Connection> createConnection(const std::string& name);	
	std::shared_ptr<ProcessingUnit> createProcessingUnit(const std::string &name, const std::shared_ptr<ProcessingUnitDefinition> &definition);
	std::shared_ptr<Memory> createMemory(const std::string& name, uint32_t size);
	std::shared_ptr<ConnectionHandler> createConnectionHandler(const std::string& name, bool use_global_queue = false);
	std::shared_ptr<CacheStatistic> createCacheStatistic(const std::string&, double hitRate);
//ports and connections
	void addInitPort(const std::string &name);
	void addTargetPort(const std::string &name);
	void bindConnection(const std::shared_ptr<Connection> &connection, const std::string &portName);
//simulation callbacks
	void setSimCallback(const sim_callback_type& type, const std::function<void()>&& f);
};




