/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include "WrappedType.h"
#include "Deviation.h"

class CacheStatisticImpl;
class Connection;
class HwStructureImpl;
struct DataRate;
// namespace sc_core{
// 	class sc_time;
// }

class CacheStatistic: public WrappedType<CacheStatisticImpl>{
	friend HwStructureImpl;
private:
	CacheStatistic(const std::string&, double _hitRate);
	explicit CacheStatistic(const std::shared_ptr<CacheStatisticImpl>& _inst);
public:

	template<class T>
	void setAccessLatency( const T &lat);

	void setDataRate(const DataRate &rate);

	/* rounds up to next power of 2*/
	void setLineSize(size_t _lineSize);

	/* round up to next multiple of lineSize which is power of 2*/
	void setSize(size_t _size);
	
	void setClockPeriod(const Time& _clock_period);

	//Hw module functions
	void addInitPort(const std::string &name);
	void addTargetPort(const std::string &name);
	void bindConnection(const std::shared_ptr<Connection> &connection, const std::string &portName);
};

extern template void CacheStatistic::setAccessLatency(const DiscreteValueConstant& lat);
extern template void CacheStatistic::setAccessLatency(const DiscreteValueGaussDistribution& lat);
extern template void CacheStatistic::setAccessLatency(const DiscreteValueWeibullEstimatorsDistribution& lat);
extern template void CacheStatistic::setAccessLatency(const DiscreteValueUniformDistribution& lat);
extern template void CacheStatistic::setAccessLatency(const DiscreteValueBetaDistribution& lat);
extern template void CacheStatistic::setAccessLatency(const DiscreteValueBoundaries& lat);


