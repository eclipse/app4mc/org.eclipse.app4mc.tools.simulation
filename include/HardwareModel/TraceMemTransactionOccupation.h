/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include <functional>
#include <memory>
#include <optional>
#include "traceString.h"

namespace sc_core{
	class st_trace_file;
}

namespace tlm{
	class tlm_generic_payload;
}

/* this class is used by HwModules and HwConnection to Trace the current Transaction executed on their Ports
   deriving classes need to call createTracings in their end_of_elaboration callback, and call updateTraceString in their b_transport function*/
class TraceMemTransactionOccupation {

	//mapping between portNames to traceStrings
	std::map<std::string, traceString> memTransactionOccStrings;
	//saved sc_trace calls on this structure
	std::vector<std::pair<sc_core::sc_trace_file *, const std::string>> tracings;
	bool isTraced = false;
	
public:
	void updateTraceString(const std::string &PortName, std::optional<std::reference_wrapper<tlm::tlm_generic_payload>> payloadHandle = std::nullopt);
	virtual void addTracingPort(std::string portName);
	/* simple callback for inheriting classes*/
	virtual void createTracings();

	static void systemc_trace(sc_core::sc_trace_file *tf, TraceMemTransactionOccupation *v, const std::string & SigName);
	inline friend void sc_trace(sc_core::sc_trace_file *tf, TraceMemTransactionOccupation *v,
		const std::string & sigName) {
		systemc_trace(tf, v, sigName);
	};

};