/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include <memory>
#include <sysc/kernel/sc_module.h>
#include <sysc/kernel/sc_object.h>
#include <tlm_utils/simple_initiator_socket.h>
#include <tlm_utils/simple_target_socket.h>
#include "ConnectionImpl.h"
#include "DataRate.h"
#include "Deviation.h"
#include "HasName.h"
#include "HwModuleBase.h"
#include "TraceMemTransactionOccupation.h"

class ConnectionImpl : 
	public HasName,
	public sc_core::sc_module, 
	public TraceMemTransactionOccupation
{
	using ConnectionInitiatorSocket=tlm_utils::simple_initiator_socket<ConnectionImpl>;
	using ConnectionTargetSocket=tlm_utils::simple_target_socket<ConnectionImpl>;

	std::unique_ptr<IValueDeviation<ELong>> readLatency;
	std::unique_ptr<IValueDeviation<ELong>> writeLatency;
	std::shared_ptr<HwModuleBase> freqDomainPtr = nullptr;
	DataRate dataRate = {0 , DataRateUnit::bitPerSecond};
	std::unique_ptr<ConnectionTargetSocket> in;
	std::unique_ptr<ConnectionInitiatorSocket> out;
public:

	explicit ConnectionImpl(const std::string& _name);
	
	template<class T>
	void setReadLatency( const T &lat) {
		readLatency = std::make_unique<T>( lat);
	}
		
	template<class T>
	void setWriteLatency( const T &lat) {
		writeLatency = std::make_unique<T>( lat);
	}

	void setDataRate(const DataRate &rate);

	void setFreqDomain(std::shared_ptr<HwModuleBase> hwModule);

	void b_transport(tlm::tlm_generic_payload& trans, sc_core::sc_time& delay);

	void end_of_elaboration() override;

	ConnectionInitiatorSocket& getInitiatorPort();
	
	ConnectionTargetSocket& getTargetPort();

	sc_core::sc_object* getHierarchicalParent();

};
