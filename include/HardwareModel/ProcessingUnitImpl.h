/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include <systemc>
#include <tlm>
#include <tlm_utils/simple_initiator_socket.h>
#include <tlm_utils/simple_target_socket.h>
#include <functional>
#include <memory>
#include <optional>
#include <queue>
#include <cstdint>
#include <utility>

#include "HwModuleBase.h"
#include "hasNamedPorts.h"
#include "traceString.h"
#include "Deviation.h"
#include "ProcessingUnitDefinition.h"
#include "ChannelEvent.h"
#include "ExecutionItem.h"
#include "Scheduler.h"
#include "HwModule.h"
#include "HwAccessElement.h"
#include "ConnectionImpl.h"

class ProcessingUnit;

class ProcessingUnitImpl : 
	public HwModule<
		ProcessingUnitImpl, 
		tlm_utils::simple_initiator_socket_optional<ProcessingUnitImpl>,
		tlm_utils::simple_target_socket_optional<ProcessingUnitImpl>> , 
	public std::enable_shared_from_this<ProcessingUnitImpl> 
{
	using ProcessingUnitInitPortType = tlm_utils::simple_initiator_socket_optional<ProcessingUnitImpl>;
	using ProcessingUnitTargetPortType= tlm_utils::simple_target_socket_optional<ProcessingUnitImpl>;

	std::shared_ptr<Scheduler> scheduler;
	
	/*this map is used to quickly resolve destination to own init port*/
	std::map<std::shared_ptr<HwModuleBase> , std::shared_ptr<HwAccessElement>> hwAccessElems;

	traceString currentRunnableTrace;
	traceString currentTaskTrace;
	traceString currentActivityGraphItemType;
	std::shared_ptr<ProcessingUnitDefinition> definition;

	sc_core::sc_event interruptEv;
	sc_core::sc_event wakeUpEv;

	void registerCallbacks() override {};
	
public:
	virtual ~ProcessingUnitImpl() = default;
	
	void interrupt(){
		interruptEv.notify(sc_core::SC_ZERO_TIME);
	}

	void interrupt(const sc_core::sc_time& t){
		interruptEv.notify(t);
	}

	void wakeUp(){
		wakeUpEv.notify(sc_core::SC_ZERO_TIME);
	}

	void startOfRunnableCallback(const Runnable &runnable);

	void endOfRunnableCallback(const Runnable &/*runnable*/);

	void setScheduler(std::shared_ptr<Scheduler> sched) {
		this->scheduler = std::move(sched);
	};

	const std::shared_ptr<ProcessingUnitDefinition>& getProcessingUnitDefinition() const {
		return definition;
	}

	void addHWAccessElement(HwAccessElement& elem) {
		elem.setSource(shared_from_this());
		hwAccessElems[elem.getDest()] = std::make_shared<HwAccessElement>(elem);
	}

	SC_HAS_PROCESS(ProcessingUnitImpl);
	ProcessingUnitImpl(const std::string &name, const std::shared_ptr<ProcessingUnitDefinition>& definition);

	void executionLoop();

	void notifyEvents(ExecutionItem &execItem, ChannelEventStateType state);


	static void systemc_trace(sc_core::sc_trace_file *tf, ProcessingUnitImpl *v, const std::string & SigName);

	inline friend void sc_trace(sc_core::sc_trace_file *tf, ProcessingUnitImpl *v,
		const std::string & SigName) {
		systemc_trace(tf, v, SigName);
	}

	friend class ProcessingUnit;
};
