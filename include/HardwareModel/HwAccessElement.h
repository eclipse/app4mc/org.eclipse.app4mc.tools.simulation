/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include "Deviation.h"
#include "DataRate.h"
#include "Memory.h"
#include <memory>
#include <stdexcept>
#include <vector>
#include "Memory.h"

//forward declare wrapped classes
class Connection;
class ConnectionImpl;
class ProcessingUnit;
class ProcessingUnitImpl;
class HwModuleBase;
class MemoryImpl;


struct HwAccessElement {
private:
	std::shared_ptr<HwModuleBase> dest;
	std::shared_ptr<ProcessingUnitImpl> source;
	std::vector<std::shared_ptr<ConnectionImpl>> accessPath;
	void setDest(const std::shared_ptr<HwModuleBase>& _dest);
	void setSource(const std::shared_ptr<ProcessingUnitImpl>& _source);
	void setAccessPath(const std::vector<std::shared_ptr<ConnectionImpl>>& _accessPath);
public:
	
	std::shared_ptr<IDiscreteValueDeviation> readLatency;
	std::shared_ptr<IDiscreteValueDeviation> writeLatency;
	DataRate dataRate;

	template<class T>
	void setReadLatency( const T &lat) {
		readLatency = std::make_shared<T>( lat);
	}
		
	template<class T>
	void setWriteLatency( const T &lat) {
		writeLatency = std::make_shared<T>( lat);
	}

	void setDataRate(const DataRate &rate);

	std::shared_ptr<ConnectionImpl> getAccessPathSuccessor(const std::shared_ptr<ConnectionImpl>& ref);

	void setDest(const std::shared_ptr<Memory>& _dest);

	void setAccessPath(const std::vector<std::shared_ptr<Connection>>& _accessPath);

	const std::vector<std::shared_ptr<ConnectionImpl>>& getAccessPath();

	std::string printAccessPath();

	[[nodiscard]] const std::shared_ptr<HwModuleBase>& getDest() const; 
	[[nodiscard]] const std::shared_ptr<ProcessingUnitImpl>& getSource() const;

	friend ProcessingUnitImpl;
	friend ProcessingUnit;
	friend ConnectionImpl;
	friend MemoryImpl;
};
