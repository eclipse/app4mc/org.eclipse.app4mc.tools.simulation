/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */
#pragma once
#include "ActivityGraphItem.h"
#include <memory>
#include "Types.h"
#include "traceString.h"
#include "MemoryAccessType.h"

class Channel;
class traceString;


class ChannelAccess : public ActivityGraphItem {
protected:
	traceString debugString;
	std::shared_ptr<Channel> channel;
	const uint64_t labelAccessStatistic;
	const MemoryAccessType command;

public:
	virtual std::string createDebugString(MemoryAccessType _command) = 0;
	ChannelAccess(std::shared_ptr<Channel> _queue, uint64_t _labelAccessStatistic,  MemoryAccessType _command);
	ChannelAccess(const ChannelAccess&) = default;
	[[nodiscard]] traceID getTraceName() const override;
	ExecutionItem getNextExecItem(ExecStack &stack, const ProcessingUnitDefinition &/*puDef*/) override;

};