/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include <memory>
#include "Condition.h"
#include "RelationalOperator.h"

class Channel;


class ChannelFillCondition : public Condition {
	std::shared_ptr<Channel> channel;
	uint64_t fillLevel;
	const RelationalOperator op;
public:

	ChannelFillCondition(std::shared_ptr<Channel> _channel, const uint64_t _fillLevel, const RelationalOperator& _op=RelationalOperator::EQUAL) : 
		channel(_channel), fillLevel(_fillLevel), op(_op) {}
	
	bool checkCond() override;
};





