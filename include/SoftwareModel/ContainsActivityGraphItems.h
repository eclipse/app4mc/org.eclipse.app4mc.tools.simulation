/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */
#pragma once
#include <memory>
#include <vector>
#include <utility>
#include <sstream>
#include "HasName.h"
#include "traceString.h"
#include "ActivityGraphItem.h"

/* this interface is used for List of ActivityGraphItems which are Copied/Moved in Value Semantics*/
class ContainsActivityGraphItems : public HasName {
protected:
	std::vector<std::shared_ptr<ActivityGraphItem>> activityGraphItems;
	
public:
	traceString nameTrace;
	virtual ~ContainsActivityGraphItems() = default;

	//forward parameter pack to default constructor, to construct shared ptr for vector
	template <class T, typename... Args>
	void addActivityGraphItem(Args&&... args) {
		activityGraphItems.emplace_back(std::make_shared<T>(std::forward<Args>(args)...));
	}

	//one parameter supplied that is of base ActivityGraph Iten
	//use copy constructor to create new shared pointer
	template <class T, typename std::enable_if < std::is_base_of<ActivityGraphItem, T>::value, bool>::type = true>
	void addActivityGraphItem(const T& item) {
		activityGraphItems.emplace_back(std::make_shared<T>(item));
	}

	[[nodiscard]] virtual int64_t getInstanceCount() const =0;

	static std::string getEmptyActivityGraphMsg(const std::string& name, const std::string& contextName){
		std::ostringstream buf;
        buf<<"ActivityGraph of "<< name << " triggered within " << contextName << "is empty. Skipping!";
		return buf.str();
	}

	void clearActivityGraph(){
		activityGraphItems.clear();
	}

protected: 
	explicit ContainsActivityGraphItems(const std::string& name) :  HasName(name), nameTrace(name){};
};