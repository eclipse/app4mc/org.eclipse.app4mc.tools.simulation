/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once

#include <memory>
#include <type_traits>
#include "Deviation.h"
#include "ProcessingUnitDefinition.h"
#include "traceString.h"
#include "ActivityGraphItem.h"
#include "ExecStack.h"
#include "ExecutionItem.h"

class Ticks : public ActivityGraphItem{    
    std::shared_ptr<IValueDeviation<ELong>> defaultTicks;
    std::map<std::string,std::shared_ptr<IValueDeviation<ELong>>> extendedTicks;
	traceString debugString;
    std::string description;
    ELong lastTicks = 0;
public:
    
    template<typename T>
    Ticks(const T &deviation) : Ticks(){
        setDefaultTicks(deviation);
    }

    template<typename T, typename std::enable_if<std::is_base_of<IValueDeviation<ELong>, T>::value, bool>::type=true >
    void addExtendedTicks(const ProcessingUnitDefinition &puDef, const T &deviation){
        extendedTicks.insert_or_assign(puDef.getName(),std::make_shared<T>(deviation));
    }

    template<typename T, typename std::enable_if<std::is_base_of<IValueDeviation<ELong>, T>::value, bool>::type=true >
    void setDefaultTicks(const T &deviation){
        //defaultTicks = isocpp_p0201::make_polymorphic_value<IValueDeviation<ELong>,T>(deviation);
        defaultTicks = std::make_shared<T>(deviation);
    }

	Ticks();
	ExecutionItem getNextExecItem(ExecStack &stack, const ProcessingUnitDefinition &puDef) override;
	traceID getTraceName() const override;
	std::string getDescription() const override;
    std::string_view getType() const override;
    std::string_view getTraceString() const override;
};