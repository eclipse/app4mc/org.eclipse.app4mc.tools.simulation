/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once

#include <memory>
#include <functional>
#include <optional>
#include "Types.h"


class Runnable;
class ContainsActivityGraphItems;
class Task;
class ActivityGraphItem;
class ExecStack;
class Scheduler;
class SemaphoreImpl;
class ChannelEvent;
enum class WaitingBehaviour;
namespace tlm{
	class tlm_generic_payload;
}
namespace sc_core{
	class sc_event_or_list;
	class sc_event_and_list;
}

class ExecutionItem {
public:
  	ExecutionItem(const ExecStack &stack, const ActivityGraphItem &item);
	cycle_t executingCycles = 0;
	std::shared_ptr<tlm::tlm_generic_payload> memoryTransaction;
	uint64_t memoryTransactionStatistic = 0;
	const ContainsActivityGraphItems &getExecutionContext(){return currentExecutionContext;};
	const Task &getTask(){return currentTask;};
	const ActivityGraphItem &getActivityGraphItem(){return currentActivityGraphItem;};
	bool taskCompleted = false;
	std::vector<std::shared_ptr<ChannelEvent>> events;
	std::shared_ptr<SemaphoreImpl> requestedSemaphore;
	std::optional<WaitingBehaviour> waiting;
	std::function<void()> finishedItemCallback;
    bool isEmptyExecItem(){return emptyExecItem;};
    static const ExecutionItem& getEmptyExecutionItem();
	bool activityGraphItemWasInterrupted = false;

	std::shared_ptr<sc_core::sc_event_or_list> orWaitList;
	std::shared_ptr<sc_core::sc_event_and_list> andWaitList;

private:



    bool emptyExecItem = true;
    std::reference_wrapper<const ContainsActivityGraphItems> currentExecutionContext;
	std::reference_wrapper<Task> currentTask;
	std::reference_wrapper<const ActivityGraphItem> currentActivityGraphItem;

    friend class ActivityGraphItem;
    friend class Task;
    friend class RunnableCall;
	friend class ExecStack;
	friend class Scheduler;
	friend class Runnable;
};

