/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 *           Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include <memory>
#include "Condition.h"
#include "RelationalOperator.h"
#include "ModeLabel.h"

class ModeLabelCondition : public Condition {
	std::shared_ptr<ModeLabel> stateProvider1;
	std::shared_ptr<ModeLabel> stateProvider2;
	const RelationalOperator op;
public:
	ModeLabelCondition(const std::shared_ptr<ModeLabel>& _stateProvider1, const std::shared_ptr<ModeLabel>& _stateProvider2, const RelationalOperator& _op = RelationalOperator::EQUAL) 
		: stateProvider1(_stateProvider1), stateProvider2(_stateProvider2), op(_op) {}
	
	bool checkCond() override;
};
