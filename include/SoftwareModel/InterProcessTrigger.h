/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#pragma once

#include "traceString.h"
#include "ActivityGraphItem.h"
#include "ExecutionItem.h"


/** For incremental split up of headers, we need to include the header here 
 * TODO: make a cleaner separation between interface and implementation for whole header
*/
#include "InterProcessStimulus.h"

class InterProcessTrigger : public ActivityGraphItem{
    std::shared_ptr<InterProcessStimulus> stimulus;
    traceString debugString;
public:
    InterProcessTrigger(const std::shared_ptr<InterProcessStimulus> stimulus); 
    std::string getDescription() const override;
	std::string_view getType() const override;
    traceID getTraceName() const override;
	ExecutionItem getNextExecItem(ExecStack &stack, const ProcessingUnitDefinition &/*puDef*/) override;
	std::string_view getTraceString() const override;
};



