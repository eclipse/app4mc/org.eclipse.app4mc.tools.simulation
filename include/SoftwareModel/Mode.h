/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */


#pragma once
#include "HasName.h"
#include <sstream>
#include <string>
#include <memory>
#include <map>

class Mode :  public  HasName{
protected: 
	explicit Mode(const std::string &_name) : HasName{_name}{} 
public:
	virtual std::string str(const int& _val);

	static std::shared_ptr<Mode> Inst() {
		static auto defaultNumericMode = std::shared_ptr<Mode>(new Mode("NumericMode"));
		return defaultNumericMode;
	};

	static std::shared_ptr<Mode> Inst(const std::string &name) {
		return std::shared_ptr<Mode>(new Mode(name));
	};

	virtual ~Mode()=default;
};

using NumericMode = Mode;


class ModeLiteral;
class EnumMode : public Mode, public std::enable_shared_from_this <EnumMode>{	
	std::map<const int, std::shared_ptr<ModeLiteral>> literalsViaID;	
	
	explicit EnumMode(const std::string &_name) : Mode{_name}{}
	
	//reduce visibility, as enum modes should supply a name
	static std::shared_ptr<Mode> Inst() {
		return nullptr;
	};
public:
	//this factory pattern ensures, that only Modes with shared pointers are created, and all created Literalrefs are always valid
	static std::shared_ptr<EnumMode> Inst(const std::string &name) {
		return std::shared_ptr<EnumMode>(new EnumMode(name));
	};
	
	int addLiteral(const std::string&& literal);
 	
	std::string str(const int& _val) override;

	~EnumMode() override{
		literalsViaID.clear();
	}
};

