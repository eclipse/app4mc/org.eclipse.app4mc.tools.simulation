/**
 ********************************************************************************
 * Copyright (c) 2012 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 ********************************************************************************
 */


#pragma once

enum class ModeAccessType{
	SET,
	READ,
	INCREMENT,
	DECREMENT
};


