/**
 ********************************************************************************
 * Copyright (c) 2021 University of Rostock and others
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */

#pragma once
#include "ContainsActivityGraphItems.h"
#include "HasCondition.h"
#include <string>
#include <string_view>
#include <cstdint>
#include <functional>

class WhileLoop : public ActivityGraphItem, public HasCondition, public ContainsActivityGraphItems {
  mutable std::string debugString;
  std::uint64_t currentIteration = 0;
  std::function<void()> callback;
 public:
  WhileLoop();
  traceID getTraceName() const override;
  ExecutionItem getNextExecItem(ExecStack &execStack, const ProcessingUnitDefinition &puDef) override;
  std::string getDescription() const override;
  std::string_view getType() const override;
  std::string_view getTraceString() const override;
  int64_t getInstanceCount() const override;
};
