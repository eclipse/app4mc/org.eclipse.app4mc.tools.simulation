/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include "ChannelAccess.h"

class ChannelRead : public ChannelAccess {
	mutable std::string traceString;
public:
	std::string createDebugString(MemoryAccessType _command) override;
	explicit ChannelRead(std::shared_ptr<Channel> _queue, uint64_t _labelAccessStatistic = 1);
	ChannelRead(const ChannelRead &other) = default;
	void notifyEvents();
	ExecutionItem getNextExecItem(ExecStack &stack, const ProcessingUnitDefinition &puDef) override;
	std::string getDescription() const override;
	std::string_view getType() const override;
	std::string_view getTraceString()const override;

};
