/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include <string>
#include <cstdint>
#include <vector>
#include <memory>
#include "HasName.h"

class MemoryImpl;
class ChannelEvent;
enum class ChannelEventType;
enum class ChannelEventStateType;

class AbstractMemoryElement : public HasName
{
protected:
	
	size_t sizeBytes = 0;
	std::vector<std::shared_ptr<ChannelEvent>> events;

	explicit AbstractMemoryElement(const std::string &_name) : HasName(_name) {};
	AbstractMemoryElement(const std::string &_name, size_t  _size): HasName(_name), sizeBytes(_size){};

public:
	[[nodiscard]] size_t getSizeBytes() const { return sizeBytes; };
	[[nodiscard]] std::vector<std::shared_ptr<ChannelEvent>> getEvents() const { return events; };

	std::shared_ptr<ChannelEvent> addEvent(const std::string& _name, ChannelEventType _type, ChannelEventStateType _state);
};
