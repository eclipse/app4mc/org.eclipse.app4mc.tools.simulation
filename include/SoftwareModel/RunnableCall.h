/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once

#include "ActivityGraphItem.h"
#include "traceString.h"
#include "Runnable.h"


class RunnableCall : public ActivityGraphItem {
	std::string description;
	traceString nameTrace;

	std::shared_ptr<Runnable> runnable;

	static void systemc_trace(sc_core::sc_trace_file *tf, const RunnableCall *v, const std::string & signame);

public:
	RunnableCall(std::shared_ptr<Runnable> runnable) : description("RunnableCall: " + runnable->getName()), nameTrace(description), runnable(runnable) {};
	ExecutionItem getNextExecItem(ExecStack &stack, const ProcessingUnitDefinition &puDef) override;

	inline friend void sc_trace(sc_core::sc_trace_file *tf, const RunnableCall *v,
		const std::string & signame) {
		systemc_trace(tf, v, signame);
	}

	traceID getTraceName() const override {
		return nameTrace.getID();
	}

	std::string getDescription() const override {
		return description;
	}

	std::string_view getType() const override {
		return "RunnableCall";
	}

	std::string_view getTraceString() const override {
		return description;
	}
};
