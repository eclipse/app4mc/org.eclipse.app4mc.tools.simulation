/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */


#pragma once
#include <memory>
#include <sstream>
#include <string_view>
#include "Mode.h"
#include "ModeLiteral.h"
#include "ActivityGraphItem.h"
#include "ModeAccessType.h"
#include "ModeLabel.h"
#include "traceString.h"


class ModeLabelAccess : public ActivityGraphItem, std::enable_shared_from_this<ModeLabelAccess> {
	const std::shared_ptr<ModeLabel> label;
	const int value = 0;
	const ModeAccessType access; 
	const traceString debugString;
	mutable std::string currentStateString;
	std::string description;
public:
	ModeLabelAccess(std::shared_ptr<ModeLabel> _label, int _value, ModeAccessType _access = ModeAccessType::SET);
	explicit ModeLabelAccess(std::shared_ptr<ModeLabel>_label, const ModeAccessType _access = ModeAccessType::READ);		
	std::string createDebugString(ModeAccessType);
	ExecutionItem getNextExecItem(ExecStack &stack, const ProcessingUnitDefinition &/*puDef*/) override;
	traceID getTraceName() const override;
	std::string getDescription() const override;
	std::string_view getType() const override;
	std::string_view getTraceString() const override;
};
