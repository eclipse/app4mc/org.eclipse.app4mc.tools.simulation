/**
 ********************************************************************************
 * Copyright (c) 2020 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */

#pragma once
#include <memory>
#include <vector>
#include "ActivityGraphItem.h"
#include "OsEvent.h"
#include "Types.h"
#include "traceString.h"

class Task;
class OsEvent;

class SetEvent : public ActivityGraphItem{
    std::vector<std::shared_ptr<OsEvent>> eventMask;
    std::string traceStr;
    traceString trace;
    std::shared_ptr<Task> task;
    cycle_t overhead = 0;
    std::string generateTraceString();
public:
    explicit SetEvent(const std::shared_ptr<Task> &task);
    void addOsEventToMask(const std::shared_ptr<OsEvent> &osEvent);
    void setTask(const std::shared_ptr<Task> &_task);
    [[nodiscard]] std::string getDescription() const override;
    [[nodiscard]] traceID getTraceName()  const override;
    [[nodiscard]] std::string_view getType() const override;
    [[nodiscard]] std::string_view getTraceString() const override;
    /*TODO implement OSapiOverhead mechanism*/ void setOverhead(cycle_t cycles);
    ExecutionItem getNextExecItem(ExecStack &stack, const ProcessingUnitDefinition &/*puDef*/) override;
};
