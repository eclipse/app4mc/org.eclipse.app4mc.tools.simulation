/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */


#pragma once
#include "HasDescription.h"
#include "Types.h"

class ExecStack;
class ExecutionItem;
class Mode; 
class ProcessingUnitDefinition;


class ActivityGraphItem : public HasDescription {
public:
	virtual ~ActivityGraphItem();
	[[nodiscard]] virtual traceID getTraceName() const = 0;
	[[nodiscard]] virtual std::string_view getTraceString() const = 0;
	/* return the type of the Item for tracing (should be a static string)*/
	[[nodiscard]] virtual std::string_view getType() const = 0;
	/* getNextItem return the next Exec Item and set the stack to the currentActivityGraphItem if necessary*/
	virtual ExecutionItem getNextExecItem(ExecStack &stack, const ProcessingUnitDefinition &puDef) = 0;
};
