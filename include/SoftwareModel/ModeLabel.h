/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */


#pragma once
#include "AbstractMemoryElement.h"
#include "Mode.h"
#include "ModeAccessType.h"
#include <string>

class ModeLabelAccess;

class ModeLabel : public AbstractMemoryElement {
	const std::shared_ptr<Mode> mode;
	int state;
	ModeLabel(const std::string &_name, std::shared_ptr<Mode> _mode, const int& _state , size_t _sizeBytes);
public:
	static std::shared_ptr<ModeLabel> Inst(const std::string &_name, std::shared_ptr<Mode> _mode, const int& _state, size_t _sizeBytes=0);
	static std::shared_ptr<ModeLabel> Inst(const std::string &_name, const int& _state, size_t _sizeBytes=0);
	const int& getState();
	void setState(const int& _state, const ModeAccessType _access);
	[[nodiscard]] std::string str() const;
	friend ModeLabelAccess;
};


