/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */
#pragma once
#include <vector>
#include "ContainsActivityGraphItems.h"
#include "HasCondition.h"



class Runnable : public ContainsActivityGraphItems, public HasCondition
{
	int64_t instanceCounter = -1;
public:
	int64_t getInstanceCount() const override {
		return instanceCounter;
	};
	void incrementInstanceCount(){
		++instanceCounter;
	};	
	Runnable(std::string name) : ContainsActivityGraphItems(std::move(name)) {};
	friend class RunnableCall;
};
