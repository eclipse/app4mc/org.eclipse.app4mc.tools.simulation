/**
 ********************************************************************************
 * Copyright (c) 2020 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */

#pragma once
#include <memory>
#include <vector>
#include <string>
#include <string_view>
#include "Types.h"
#include "ActivityGraphItem.h"
#include "traceString.h"

class ProcessingUnitDefinition;
class ExecutionItem;
class ExecStack;
class OsEvent; 

enum class WaitEventType {AND, OR};
enum class WaitingBehaviour {active, passive};

class WaitEvent : public ActivityGraphItem{
    std::vector<std::shared_ptr<OsEvent>> eventMask;
    WaitEventType type;
    WaitingBehaviour waiting;
    std::string traceStr;
    traceString trace;
    cycle_t overhead = 0;
    std::string generateTraceString();
public:
    explicit WaitEvent(WaitEventType type = WaitEventType::OR, WaitingBehaviour waiting = WaitingBehaviour::active); 
    void addOsEventToMask(const std::shared_ptr<OsEvent> &osEvent);
    [[nodiscard]] traceID getTraceName()  const override;
    [[nodiscard]] std::string getDescription() const override;
    [[nodiscard]] std::string_view getType() const override;
    [[nodiscard]] std::string_view getTraceString() const override;
    /*TODO implement proper OSapiOverhead mechanism*/    
    void setOverhead(cycle_t cycles);
	/* getNextItem return the next Exec Item and set the stack to the currentActivityGraphItem if necessary*/
	ExecutionItem getNextExecItem(ExecStack &stack, const ProcessingUnitDefinition &puDef) override;
};

