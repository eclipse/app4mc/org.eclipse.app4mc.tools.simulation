/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include <tlm>
#include "AbstractMemoryElement.h"

struct labelTLMext : tlm::tlm_extension<labelTLMext> {

	std::shared_ptr<AbstractMemoryElement> label;
	std::function< void() > writeCallback; 

	virtual tlm_extension_base* clone() const {
		labelTLMext *ext = new labelTLMext;
		ext->label = this->label;
		ext->writeCallback = this->writeCallback;
		return ext;

	}

	virtual void copy_from(tlm_extension_base const& ext)
	{
		label = static_cast<labelTLMext const &>(ext).label;
	}

};




