/**
 ********************************************************************************
 * Copyright (c) 2020 University of Rostock and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */

#pragma once
#include <vector>
#include <string_view>
#include <memory>
#include "Types.h"
#include "ActivityGraphItem.h"
#include "traceString.h"

class ExecutionItem;
class ExecStack;
class ProcessingUnitDefinition;
class OsEvent;

class ClearEvent : public ActivityGraphItem{
    std::vector<std::shared_ptr<OsEvent>> eventMask;
    std::string traceStr;
    traceString trace;
    cycle_t overhead = 0;
    std::string generateTraceString();
public:
    ClearEvent();
    void addOsEventToMask(const std::shared_ptr<OsEvent> &osEvent);
    [[nodiscard]] std::string getDescription() const override;
    [[nodiscard]] traceID getTraceName()  const override;
    [[nodiscard]] std::string_view getType() const override;
    [[nodiscard]] std::string_view getTraceString() const override;
    void setOverhead(cycle_t cycles);/*TODO implement proper OSapiOverhead mechanism*/
    ExecutionItem getNextExecItem(ExecStack &stack, const ProcessingUnitDefinition &/*puDef*/) override;
};