/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include <memory>
#include "ContainsActivityGraphItems.h"
#include "Runnable.h"
#include "ExecStack.h"
#include "Schedulable.h"
#include "ProcessEvent.h"
#include "traceString.h"
#include "Stimulus.h"

class OsEvent;
class StimulusImpl;
class Scheduler;
namespace sc_core{
    class sc_time;
    class sc_event;
}

struct TaskOsEventEntry{
    std::shared_ptr<sc_core::sc_event> event;
    bool isSet;
};

class Task : public std::enable_shared_from_this<Task>, public ContainsActivityGraphItems, public Schedulable{
private:
    traceString nameTracer;
    std::shared_ptr<Scheduler> scheduler;
    std::vector<std::shared_ptr<Stimulus>> stimuli;
    ExecStack execStack;
    traceID currentRunnable;
    uint32_t activationLimit = 1;
    // uint32_t activations = 0;
    int64_t instanceCounter = -1;
    std::vector<std::tuple<sc_core::sc_time, StimulusImpl>> queuedStimuli;
    //this maps an OsEvent to a Task-specific sc_event, and a bool which describes whether it is set
    std::map<std::shared_ptr<OsEvent>,TaskOsEventEntry> osEvents;
    std::optional<ExecutionItem> preemptedExecutionItem;
    std::vector<std::shared_ptr<ProcessEvent>> events;
     //hidden constructor, use createTask to get shared ptr
    Task(std::string name, uint32_t _activationLimit);
    static void systemc_trace(sc_core::sc_trace_file *tf, const Task *v,const std::string & SigName);
//Stimulus
    void addStimulus( std::shared_ptr<Stimulus> stimulusApi);
    void stimulateTask(const StimulusImpl& stimulus);
//state changes (accessible via base class Schedulable)
    void activate(const StimulusImpl &stimulus) override;
    void start(const std::shared_ptr<ProcessingUnitImpl> &pu) override;
    void preempt(const std::shared_ptr<ProcessingUnitImpl> &pu) override;
    void resume(const std::shared_ptr<ProcessingUnitImpl> &pu) override;
    void terminate(const std::shared_ptr<ProcessingUnitImpl> &pu) override;
public:
    static std::shared_ptr<Task> createTask(const std::string &_name, uint32_t _activationLimit=1);
    //bool isTask() override {return true;};
//runnable callback
    void runnableStartedCallback(Runnable &runnable);
    void runnableCompletedCallback(const Runnable &runnable);
    ExecutionItem getNextExecItem(const std::shared_ptr<ProcessingUnitImpl> &pu) override;
//event assignments
    void addOsEvent(const std::shared_ptr<OsEvent> &osEvent);
    void setOsEvent(const std::shared_ptr<OsEvent> &osEvent);
    void clearOsEvent(const std::shared_ptr<OsEvent> &osEvent);
    bool eventUsedInTask(const std::shared_ptr<OsEvent> &event);
    bool eventIsSet(const std::shared_ptr<OsEvent> &event);
    std::shared_ptr<sc_core::sc_event> & getScEvent(const std::shared_ptr<OsEvent> &event);
//Scheduler Interface
    std::vector<std::shared_ptr<ProcessEvent>> getEvents() const;
    std::shared_ptr<ProcessEvent> addEvent(const std::string& _name, ProcessEventType _type);
    void storePreemptedExecutionItem(ExecutionItem execItem);

    inline friend void sc_trace(sc_core::sc_trace_file *tf, const Task *v,
        const std::string & SigName) {
        systemc_trace(tf, v, SigName);
    }

    // this non-virtual getName() impl avoids ambiguous function name (getName from ContainsActivityGraphItem and Schedulable)
    // or otherwise unnecessary virtual inheritance or polymorphism, for the cost of storing the same name-string twice
    const std::string &getName() const {
      return Schedulable::getName();
    };

    int64_t getInstanceCount() const override {
        return instanceCounter;
    };

    traceID getTraceID() const {
        return nameTracer.getID();
    }

    void setScheduler(std::shared_ptr<Scheduler> sched) {
        scheduler = sched;
    }

    uint32_t getActivationLimit() const;
    void setActivationLimit(uint32_t newActivationLimit);
    uint32_t getActivations() const;

    uint64_t instanceCount() const{
        return instanceCounter;
    }

    //Stimulus
    const std::vector<std::shared_ptr<Stimulus>>& getStimuli(){
        return stimuli;
    }

    friend class ExecutionItem; //to access execStack
    friend class Stimulus;
    friend class StimulusImpl;
};

