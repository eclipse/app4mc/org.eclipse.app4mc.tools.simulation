/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */


#pragma once

#include <random>
#include "ContainsActivityGraphItems.h"
class ProbabilitySwitch;

class ProbabilitySwitchEntry : public ContainsActivityGraphItems{
	
public:
	double probWeight;
	explicit ProbabilitySwitchEntry(double _probWeight) : ContainsActivityGraphItems("Probability" + std::to_string(_probWeight)),  probWeight(_probWeight) {};
	int64_t getInstanceCount() const override {
		return -1;
	};
	friend ProbabilitySwitch;
};


class ProbabilitySwitch : public ActivityGraphItem{
	//save pair of name and list of ActivityGraphItem for each entry
	std::vector<ProbabilitySwitchEntry> entries;
	std::discrete_distribution<> dist;
	int lastIndexTaken = -1;
	mutable std::string debugString;
public:
	traceID getTraceName() const override;
	void calculateDist();
	void addEntry(const ProbabilitySwitchEntry &entry);
	/* getNextItem return the next Exec Item and set the stack to the following execItem*/
    ExecutionItem getNextExecItem(ExecStack &execStack, const ProcessingUnitDefinition &puDef) override;
  	std::string getDescription() const override;
	std::string_view getType() const override;	
    std::string_view getTraceString() const override;	
};
