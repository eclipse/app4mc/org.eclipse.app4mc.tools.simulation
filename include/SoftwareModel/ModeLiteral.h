
/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include <cstddef>
#include <memory>
#include <stdexcept>
#include <utility>
#include "Mode.h"


class ModeLiteral {
	const std::string literalString;
	const EnumMode& parent;
	const int literalId;
	static int id(){
		//generate global id
		static int cnt =0;
		return ++cnt;
	}
public:
	ModeLiteral(const std::string& _literalString, const EnumMode& _parent) : 
		literalString(_literalString),  parent(_parent), literalId(id()) {};
	friend EnumMode; //allow usage of private constructor
	[[nodiscard]] std::string str() const {
		return std::string(parent.getName()) + "::" + literalString;
	};
	[[nodiscard]] int getLiteralId() const{return literalId;};
};
