/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include <tlm_core/tlm_2/tlm_generic_payload/tlm_gp.h>
#include <stdexcept>
#include <tlm>
#include "ActivityGraphItem.h"
#include "MemoryAccessType.h"
#include "ModeAccessType.h"
#include <memory>
#include <map>
#include <functional>

constexpr tlm::tlm_command getTlmCommand(const MemoryAccessType _type){
	switch (_type){
		case MemoryAccessType::READ: return tlm::tlm_command::TLM_READ_COMMAND;
		case MemoryAccessType::WRITE: return tlm::tlm_command::TLM_WRITE_COMMAND;
		default: throw std::runtime_error("Memory access type not specified");
	}
}


constexpr tlm::tlm_command getTlmCommand(ModeAccessType type)
{
	switch (type){
		case ModeAccessType::READ:
			return tlm::tlm_command::TLM_READ_COMMAND;
		case ModeAccessType::SET:
			return tlm::tlm_command::TLM_WRITE_COMMAND;
		case ModeAccessType::INCREMENT:
			return tlm::tlm_command::TLM_WRITE_COMMAND;
		case ModeAccessType::DECREMENT:
			return tlm::tlm_command::TLM_WRITE_COMMAND;
		default:
			return tlm::tlm_command::TLM_IGNORE_COMMAND;
	}
}


class PayloadCache {
	std::map<ActivityGraphItem*, std::shared_ptr<tlm::tlm_generic_payload>> cache;
	PayloadCache() = default;
public:
	static PayloadCache& Inst(){
		static auto instance = PayloadCache();
		return instance;
	}

	std::tuple<std::shared_ptr<tlm::tlm_generic_payload>, bool> getOrInit(
		ActivityGraphItem* src) 
	{
		if (cache.count(src)>0){
			return {cache[src], true};
		} else {
			//create payload and add to cache
			auto tmp= std::make_shared<tlm::tlm_generic_payload>();
			cache[src] = tmp;
			return {tmp, false};
		}
	}

	void add(
		ActivityGraphItem* src, 
		std::shared_ptr<tlm::tlm_generic_payload> f) 
	{
		cache[src] = f; 
	}

	void remove(
		ActivityGraphItem* src) 
	{
		cache.erase(src); 
	}
};