/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include <vector>
#include <functional>
#include <optional>
#include <memory>
#include "ExecutionItem.h"
#include "ActivityGraphItem.h"
#include "ContainsActivityGraphItems.h"
#include "ProcessingUnitDefinition.h"

class Task;

class ExecStack {
  
  struct StackEntryType{
	std::vector<std::shared_ptr<ActivityGraphItem>>::iterator currentExecutionPointer;
	std::reference_wrapper<std::vector<std::shared_ptr<ActivityGraphItem>>> currentExecutionContainer;
	// callback for reaching the end of an ActivityGraphItemContainer (e.g. to realize an end of Runnable signalling)
	std::function<void()> endOfExecutionContainerCallback;
	bool repeat = false;
  };
  using stackType = std::vector<StackEntryType>;

  stackType stack;


	std::optional<std::reference_wrapper<const ContainsActivityGraphItems>> currentContainer; //container may be more fine granular than runnable, e.g. SwitchEntry or group
	std::optional<std::reference_wrapper<const ContainsActivityGraphItems>> currentContext;
	std::shared_ptr<Task> parentTask;
	
	ExecStack() = default;

public:
	ExecStack(std::shared_ptr<Task> parentTask);

  void pushExecutionLevel(std::vector<std::shared_ptr<ActivityGraphItem>> &newLevel, std::function<void()> completionCallback);

  void addLoopIteration(std::vector<std::shared_ptr<ActivityGraphItem>> &loopContainer);

	void setExecutionContext(const ContainsActivityGraphItems &context){
		currentContext = std::cref(context);
	};

	void setActivityItemContainer(const ContainsActivityGraphItems &container){
		currentContainer = std::cref(container);
	};
	
	std::shared_ptr<Task> getParentTask() const {return parentTask;}

	const ContainsActivityGraphItems& getExecutionContext() const;
	const ContainsActivityGraphItems& getActivityItemContainer () const;
	ExecutionItem getNextExecutionItem(const ProcessingUnitDefinition &puDef, bool firstAccess);
	
	bool isEmpty() const {
		return stack.empty();
	}

	friend class Task;

};