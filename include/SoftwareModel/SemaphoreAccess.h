/**
 ********************************************************************************
 * Copyright (c) 2021 University of Rostock and others
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de> - initial contribution
 ********************************************************************************
 */

#pragma once

#include <memory>
#include <stdexcept>
#include <string>
#include <string_view>
#include "Semaphore.h"
#include "ActivityGraphItem.h"
#include "ExecutionItem.h"
#include "WaitEvent.h"
#include "traceString.h"

enum class SemaphoreAccessType { release, request };

class SemaphoreAccess : public ActivityGraphItem {
  SemaphoreAccessType accessType;
  std::shared_ptr<SemaphoreImpl> semaphore;
  /*[[maybe_unused]]*/WaitingBehaviour waitingBehavior;
  traceString debugString;
  std::string description;
  void generateTraceString();
 public:
  SemaphoreAccess(const std::shared_ptr<Semaphore>& semaphore, SemaphoreAccessType accessType, WaitingBehaviour waitingBehavior);
  [[nodiscard]] traceID getTraceName() const override;
  [[nodiscard]] std::string_view getTraceString() const override;
  [[nodiscard]] std::string_view getType() const override;
  [[nodiscard]] std::string getDescription() const override;
  /* getNextItem return the next Exec Item and set the stack to the currentActivityGraphItem if necessary*/
  ExecutionItem getNextExecItem(ExecStack &stack, [[maybe_unused]] const ProcessingUnitDefinition &puDef) override;
};