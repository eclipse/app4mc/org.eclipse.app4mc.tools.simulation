/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include "DataLabel.h"
#include "ExecutionItem.h"
#include "ActivityGraphItem.h"
#include "MemoryAccessType.h"
#include "traceString.h"


class LabelAccess : public ActivityGraphItem {
	const MemoryAccessType command;
	const std::shared_ptr<DataLabel> label;
	const uint32_t labelAccessStatistic;
	std::string description;
	traceString debugString;
public:
	LabelAccess( std::shared_ptr<DataLabel> _label, uint32_t _AccessLabelStatistic, MemoryAccessType _command);

	std::string createDebugString(MemoryAccessType _command);

	LabelAccess(std::shared_ptr<DataLabel> _label, MemoryAccessType _command);

	ExecutionItem getNextExecItem(ExecStack &stack, const ProcessingUnitDefinition &/*puDef*/) override;

	virtual traceID getTraceName() const override;
	std::string getDescription() const override;
	std::string_view getType() const override;
	std::string_view getTraceString() const override;
};
