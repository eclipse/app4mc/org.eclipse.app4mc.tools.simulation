/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */

#pragma once
#include "Condition.h"
#include <memory>
#include "ModeLiteral.h"
#include "RelationalOperator.h"
#include "ModeLabel.h"


class ModeValueCondition : public Condition {
	std::shared_ptr<ModeLabel> stateProvider;
	const int value;
	const RelationalOperator op;
public:
	ModeValueCondition(const std::shared_ptr<ModeLabel>& /*_stateProvider*/, const int& /*_value*/, const RelationalOperator& _op=RelationalOperator::EQUAL);
	ModeValueCondition(const std::shared_ptr<ModeLabel>& /*_stateProvider*/, const std::shared_ptr<ModeLiteral>& /*_stateProvider*/, const RelationalOperator& _op=RelationalOperator::EQUAL);
	bool checkCond() override;
};
