/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Sebastian Reiser <sebastian.reiser@de.bosch.com>
 ********************************************************************************
 */

#pragma once
#include "AbstractMemoryElement.h"
#include "HasDescription.h"
#include "traceString.h"

class Channel : public AbstractMemoryElement, public HasDescription {
protected:
	uint64_t maxElements = 0;
	uint64_t fillLevel = 0;
public:
	Channel(const std::string &_name, size_t _sizeBytes,uint64_t _defaultElements, uint64_t _maxElements);
	uint64_t getFillLevel() const;
	uint64_t incrementFillLevel(uint64_t _incr);
	uint64_t decrementFillLevel(uint64_t _decr);
	uint64_t incrementFillLevel();
	uint64_t decrementFillLevel();
	std::string getDescription() const override;
	virtual traceString getTraceString() const;
	virtual ~Channel(){};
};



