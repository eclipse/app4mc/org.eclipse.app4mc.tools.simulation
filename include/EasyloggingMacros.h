/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * - Robert Bosch GmbH - initial contribution
 *   Author: Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
 ********************************************************************************
 */


//freaky quirk, because easylogging includes windows.h which defines min() and max() which broke std-cpp library
#ifndef NOMINMAX
#define NOMINMAX
#define ADDED_NOMINMAX
#endif
#include "easylogging++.h"


#define LOG_SCMODULE(lev) LOG((lev)) << "time: " << sc_core::sc_time_stamp() << " | deltacycle: "<< sc_core::sc_delta_count()  << " | " << sc_module::name() << std::string(" | ")
#define VLOG_SCMODULE(lev) VLOG((lev)) << "time: " << sc_core::sc_time_stamp() << " | deltacycle: "<< sc_core::sc_delta_count()  << " | " << sc_module::name() << std::string(" | ")
#define VLOG_SCOBJECT(lev) VLOG((lev)) << "time: " << sc_core::sc_time_stamp() << " | deltacycle: "<< sc_core::sc_delta_count()  << " | " << sc_object::name() << std::string(" | ")

#define VLOG_TIME(lev) VLOG((lev)) << "time: " << sc_core::sc_time_stamp() << " | "
