#/**
# ********************************************************************************
# * Copyright (c) 2020 University of Rostock
# *
# * This program and the accompanying materials are made
# * available under the terms of the Eclipse Public License 2.0
# * which is available at https://www.eclipse.org/legal/epl-2.0/
# *
# * SPDX-License-Identifier: EPL-2.0
# *
# * Contributors:
# * - University of Rostock - Benjamin Beichler <Benjamin.Beichler@uni-rostock.de>
# ********************************************************************************
# */

#initialize submodules if not already done (only at configuration time, not at build-time)
find_package(Git QUIET)
if(GIT_FOUND AND EXISTS "${PROJECT_SOURCE_DIR}/.git")
# Update submodules as needed
    option(GIT_SUBMODULE "Check submodules during build" ON)
    if(GIT_SUBMODULE)
        message(STATUS "Submodule update/init")
        execute_process(COMMAND ${GIT_EXECUTABLE} submodule update --init --recursive
                        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                        RESULT_VARIABLE GIT_SUBMOD_RESULT)
        if(NOT GIT_SUBMOD_RESULT EQUAL "0")
            message(FATAL_ERROR "git submodule update --init failed with ${GIT_SUBMOD_RESULT}, please checkout submodules")
        endif()
    endif()
endif()

#set CCACHE base dir, since otherwise the cached results are only used, if the source is in the exact same path
find_program(CCACHE_PROGRAM ccache)
if(CCACHE_PROGRAM)
    set(CMAKE_CXX_COMPILER_LAUNCHER "CCACHE_BASEDIR=${PROJECT_SOURCE_DIR}" "${CCACHE_PROGRAM}")
else()
	#try sccache
	find_program(SCCACHE_PROGRAM sccache)
	if(SCCACHE_PROGRAM)
		set(CMAKE_CXX_COMPILER_LAUNCHER "${SCCACHE_PROGRAM}")
        #sccache do not work with /Zi on cl.exe, therefore here the workaround to use /Z7
        if (MSVC)
            if(CMAKE_BUILD_TYPE STREQUAL "Debug")
                string(REPLACE "/Zi" "/Z7" CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG}")
                string(REPLACE "/Zi" "/Z7" CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG}")
            elseif(CMAKE_BUILD_TYPE STREQUAL "Release")
                string(REPLACE "/Zi" "/Z7" CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE}")
                string(REPLACE "/Zi" "/Z7" CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE}")
            elseif(CMAKE_BUILD_TYPE STREQUAL "RelWithDebInfo")
                string(REPLACE "/Zi" "/Z7" CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO}")
            string(REPLACE "/Zi" "/Z7" CMAKE_C_FLAGS_RELWITHDEBINFO "${CMAKE_C_FLAGS_RELWITHDEBINFO}")
            endif()
        endif()
	endif()
endif()

